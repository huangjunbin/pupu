package com.financialnet.net.http;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.financialnet.app.AppContext;
import com.financialnet.app.ui.LoginActivity;
import com.financialnet.model.RequestModel;
import com.financialnet.model.ResponeModel;
import com.financialnet.service.MemberService;
import com.financialnet.util.JsonUtil;
import com.financialnet.util.UIHelper;
import com.financialnet.widget.MyProgressDialog;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import cn.jpush.android.api.JPushInterface;

public class CustomAsyncHttpClient {
    String TAG = "CustomAsyncHttpClient";

    private AsyncHttpClient asyncHttpClient;
    private MyProgressDialog dialog;
    private Context mContext;
    private ResponeModel baseModel;

    public CustomAsyncHttpClient(Context context) {
        asyncHttpClient = new AsyncHttpClient();
        mContext = context;
        if (mContext != null) {
            dialog = new MyProgressDialog(mContext, "", true);
            dialog.tv_value.setVisibility(View.GONE);
        }
        baseModel = new ResponeModel();
    }

    public void post(final RequestModel requestModel,
                     final CustomAsyncResponehandler responseHandler) {
        RequestParams newParams = new RequestParams();
        com.alibaba.fastjson.JSONObject jsonObject = new com.alibaba.fastjson.JSONObject();
        for (BasicNameValuePair param : requestModel.getParams()
                .getParamsList()) {
            jsonObject.put(param.getName(), param.getValue());
        }

        newParams.fileParams = requestModel.getParams().fileParams;

        newParams.put("p", jsonObject.toString());
        Log.d(TAG, requestModel.getUrl() + "?" + newParams.toString());
        asyncHttpClient.addHeader("App-Authorization", AppContext.getCurrentMember() == null ? "" : AppContext.getCurrentMember().getAuthorization());
        asyncHttpClient.post(requestModel.getUrl(), newParams,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        Log.d(TAG, "onStart________________________");
                        if (requestModel.isShowDialog()) {// 显示网络对话框
                            if (mContext != null) {
                                dialog.show();
                            }
                        }
                        responseHandler.onStart();
                    }

                    @Override
                    public void onFinish() {
                        Log.d(TAG, "onFinish________________________");
                        if (requestModel.isShowDialog()) {// 隐藏网络对话框
                            if (mContext != null) {
                                dialog.dismiss();
                            }
                        }
                        responseHandler.onFinish();
                    }

                    @Override
                    public void onSuccess(String content) {
                        Log.d(TAG, "onSuccess________________________"
                                + content);

                        // TODO:解密返回的参数
                        baseModel = JsonUtil.convertJsonToObject(content,
                                ResponeModel.class);

                        if (baseModel != null) {
                            if ("200".equals(baseModel.getCode())) {
                                baseModel.setStatus(true);
                            }
                            if ("-1".equals(baseModel.getCode())) {
                                if (mContext != null) {
                                    MemberService.getInstance(
                                            mContext).loginOut();
                                    AppContext.getApplication()
                                            .logout();
                                    JPushInterface
                                            .stopPush(mContext);
                                    JPushInterface
                                            .clearAllNotifications(mContext);
                                    mContext.startActivity(new Intent(mContext, LoginActivity.class));
                                    Toast.makeText(mContext, "请重新登陆", Toast.LENGTH_SHORT).show();

                                }
                                return;
                            }
                            if (baseModel.isStatus()) {
                                baseModel.setJson(content);
                                if (baseModel.getData() != null
                                        && baseModel.getData().length() > 0) {
                                    String Head = baseModel.getData()
                                            .substring(0, 1);
                                    // Data是对象
                                    if ("{".equals(Head)) {
                                        JSONObject object;
                                        try {
                                            object = new JSONObject(baseModel
                                                    .getData());

                                            baseModel.setDataResult(object);

                                            if (!object.isNull("totalCount")) {// list
                                                // 带总条数
                                                baseModel.setTotalCount(object
                                                        .getString("totalCount"));
                                                // 设置其他分页信息
                                            }

                                            if (!object.isNull("result")) {// list
                                                // 带分页
                                                baseModel.setResult(JsonUtil
                                                        .convertJsonToList(
                                                                object.get(
                                                                        "result")
                                                                        .toString(),
                                                                requestModel
                                                                        .getCls()));
                                            } else {
                                                baseModel.setResult(JsonUtil.convertJsonToObject(
                                                        baseModel.getData(),
                                                        requestModel.getCls()));
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    } else if ("[".equals(Head)) {// data直接是数组
                                        baseModel.setResult(JsonUtil
                                                .convertJsonToList(
                                                        baseModel.getData(),
                                                        requestModel.getCls()));
                                    }
                                }
                            } else {
                                if (requestModel.isShowErrorMessage()
                                        && !requestModel.isLetsChildShow()) {
                                    if (mContext != null) {
                                        UIHelper.ShowMessage(mContext,
                                                baseModel.getMsg());
                                    }
                                }
                            }
                            responseHandler.onSuccess(baseModel);
                        } else {
                            if (requestModel.isShowErrorMessage()) {
                                if (mContext != null) {
                                    UIHelper.ShowMessage(mContext, "网络异常");
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable error, String content) {
                        responseHandler.onFailure(error, content);
                        Log.d(TAG, "onFailure________________________"
                                + content);
                        if (content == null || content.length() < 1) {
                            if (mContext != null) {
                                UIHelper.ShowMessage(mContext, "服务器异常");
                            }
                            return;
                        }
                        JSONObject jsonobj;
                        try {
                            jsonobj = new JSONObject(content);

                            baseModel = new ResponeModel();
                            baseModel.setMsg(jsonobj.getString("msg"));
                            baseModel.setCode(Integer.parseInt(jsonobj
                                    .getString("code")));
                            baseModel.setData(jsonobj.getString("data"));
                            baseModel.setStatus(Boolean.parseBoolean(jsonobj
                                    .getString("status")));
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                        System.out.println(baseModel);
                        if (mContext != null) {
                            if (baseModel.getCode() == -1) {
                                if (mContext != null) {
                                    MemberService.getInstance(
                                            mContext).loginOut();
                                    AppContext.getApplication()
                                            .logout();
                                    JPushInterface
                                            .stopPush(mContext);
                                    JPushInterface
                                            .clearAllNotifications(mContext);
                                    mContext.startActivity(new Intent(mContext, LoginActivity.class));
                                    Toast.makeText(mContext, "账号异地登陆，请重新登陆", Toast.LENGTH_SHORT).show();

                                }
                            }
                            if (requestModel.isShowErrorMessage()) {
                                if (mContext != null && baseModel.getCode() != -1) {

                                    UIHelper.ShowMessage(mContext, "网络异常");
                                }
                            }
                        }
                    }
                });
    }

    public void postOrign(final RequestModel requestModel,
                          final CustomAsyncResponehandler responseHandler) {
        RequestParams newParams = new RequestParams();
        com.alibaba.fastjson.JSONObject jsonObject = new com.alibaba.fastjson.JSONObject();
        for (BasicNameValuePair param : requestModel.getParams()
                .getParamsList()) {
            jsonObject.put(param.getName(), param.getValue());
        }

        newParams.fileParams = requestModel.getParams().fileParams;

        newParams.put("p", jsonObject.toString());
        Log.d(TAG, requestModel.getUrl() + "?" + newParams.toString());

        asyncHttpClient.post(requestModel.getUrl(), newParams,
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        Log.d(TAG, "onStart________________________");
                        if (requestModel.isShowDialog()) {// 显示网络对话框
                            if (mContext != null) {
                                dialog.show();
                            }
                        }
                        responseHandler.onStart();
                    }

                    @Override
                    public void onFinish() {
                        Log.d(TAG, "onFinish________________________");
                        if (requestModel.isShowDialog()) {// 隐藏网络对话框
                            if (mContext != null) {
                                dialog.dismiss();
                            }
                        }
                        responseHandler.onFinish();
                    }

                    @Override
                    public void onSuccess(String content) {
                        Log.d(TAG, "onSuccess________________________"
                                + content);
                        JSONObject jsonobj;
                        try {
                            jsonobj = new JSONObject(content);

                            baseModel = new ResponeModel();
                            baseModel.setMsg(jsonobj.getString("msg"));
                            baseModel.setCode(Integer.parseInt(jsonobj
                                    .getString("code")));
                            baseModel.setData(jsonobj.getString("data"));
                            baseModel.setStatus(Boolean.parseBoolean(jsonobj
                                    .getString("status")));
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                        // TODO:解密返回的参数
                        if (baseModel != null) {

                            if (baseModel.isStatus()) {
                                baseModel.setJson(content);
                                if (baseModel.getData() != null
                                        && baseModel.getData().length() > 0) {
                                    String Head = baseModel.getData()
                                            .substring(0, 1);
                                    // Data是对象
                                    if ("{".equals(Head)) {
                                        JSONObject object;
                                        try {
                                            object = new JSONObject(baseModel
                                                    .getData());

                                            if (!object.isNull("totalCount")) {// list
                                                // 带总条数
                                                baseModel.setTotalCount(object
                                                        .getString("totalCount"));
                                                // 设置其他分页信息
                                            }

                                            if (!object.isNull("result")) {// list
                                                // 带分页
                                                baseModel.setResult(JsonUtil
                                                        .convertJsonToList(
                                                                object.get(
                                                                        "result")
                                                                        .toString(),
                                                                requestModel
                                                                        .getCls()));
                                            } else {
                                                baseModel.setResult(JsonUtil.convertJsonToObject(
                                                        baseModel.getData(),
                                                        requestModel.getCls()));
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    } else if ("[".equals(Head)) {// data直接是数组
                                        baseModel.setResult(JsonUtil
                                                .convertJsonToList(
                                                        baseModel.getData(),
                                                        requestModel.getCls()));
                                    }
                                }
                            } else {
                                if (requestModel.isShowErrorMessage()) {
                                    if (mContext != null) {
                                        UIHelper.ShowMessage(mContext,
                                                baseModel.getMsg());
                                    }
                                }
                            }
                            responseHandler.onSuccess(baseModel);
                        }
                    }

                    @Override
                    public void onFailure(Throwable error, String content) {
                        responseHandler.onFailure(error, content);
                        Log.d(TAG, "onFailure________________________"
                                + content);
                        if (content == null || content.length() < 1) {
                            UIHelper.ShowMessage(mContext, "服务器异常");
                            return;
                        }
                        JSONObject jsonobj;
                        try {
                            jsonobj = new JSONObject(content);

                            baseModel = new ResponeModel();
                            baseModel.setMsg(jsonobj.getString("msg"));
                            baseModel.setCode(Integer.parseInt(jsonobj
                                    .getString("code")));
                            baseModel.setData(jsonobj.getString("data"));
                            baseModel.setStatus(Boolean.parseBoolean(jsonobj
                                    .getString("status")));
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                        System.out.println(baseModel);
                        if (baseModel.getCode() == -1) {

                            if (mContext != null) {
                                MemberService.getInstance(
                                        mContext).loginOut();
                                AppContext.getApplication()
                                        .logout();
                                JPushInterface
                                        .stopPush(mContext);
                                JPushInterface
                                        .clearAllNotifications(mContext);
                                mContext.startActivity(new Intent(mContext, LoginActivity.class));
                                Toast.makeText(mContext, "账号异地登陆，请重新登陆", Toast.LENGTH_SHORT).show();

                            }
                        }
                        if (requestModel.isShowErrorMessage()) {

                            if (requestModel.isShowErrorMessage()) {
                                if (mContext != null && baseModel.getCode() != -1) {

                                    UIHelper.ShowMessage(mContext, "网络异常");
                                }
                            }

                        }
                    }
                });
    }
}
