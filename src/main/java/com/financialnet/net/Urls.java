package com.financialnet.net;

/**
 * 业务数据URL地址（注意标注地址注释）
 *
 * @author allen
 * @version 1.0.0
 * @created 2014-7-10
 */
public class Urls {
    private static int DEVELOP_MPDE = 1;// 开发
    private static int TEST_MPDE = 2;// 测试
    private static int ONLINE_MODE = 3;// 上线

    public static final int MODE = ONLINE_MODE;// 当前连接服务器模式，测试模式还是产线模式

    /**
     * 默认的API头地址
     */
    public static final String DEVELOP__HEAD_URL = "http://121.40.155.118:8080/pupu/";
    public static final String TEST_HEAD_URL = "http://121.40.155.118:8181/pupu/";
    public static final String ONLINE_HEAD_URL = "http://42.120.60.105/pupu/";
    public static final String HEAD_URL = (MODE == ONLINE_MODE) ? ONLINE_HEAD_URL
            : (MODE == DEVELOP_MPDE) ? DEVELOP__HEAD_URL : TEST_HEAD_URL;

    public static final String TEST_IMAGE_URL = "http://42.120.60.105";
    public static final String ONLINE_IMAGE_URL = "http://pupuoss.oss-cn-hangzhou.aliyuncs.com/";
    public static final String IMAGE_HEAD_URL = ONLINE_IMAGE_URL;

    // 商户服务器下单地址，此地址为商户平台测试环境下单地址，商户接入需改为商户自己的服务器下单地址
    public final static String URL_PAY_ORDER = "http://115.29.224.52:8080/pupu-pay/";
    // 模拟通知商户地址，建议在接收到支付成功结果时，通知商户服务器
    public final static String URL_PAY_NOTIFY = "http://10.123.54.49:8080/Notify.do";
    // 广播地址，用于接收易联支付插件支付完成之后回调客户端
    public final static String BROADCAST_PAY_END = "com.merchant.demo.broadcast";

    // 上传图片
    public static final String UpLoadImg_action = HEAD_URL
            + "toUploadFile/uploadFile.action";

    // 登出
    public static final String login_out_action = HEAD_URL
            + "toPhoneUser/appUserLogout.action";
    // 登陆
    public static final String login_action = HEAD_URL
            + "toPhoneUser/appUserLogin.action";
    // 获取验证码
    public static final String GetAutoCode_action = HEAD_URL
            + "toPhoneUser/produceUserValiCode.action";
    // 校验验证码
    public static final String CheckoutAutoCode_action = HEAD_URL
            + "toPhoneUser/validateCode.action";

    // 校验手机号码
    public static final String ConfirmMobileNumber_action = HEAD_URL
            + "toPhoneUser/forgetPassword.action";

    // 修改密码
    public static final String updatePassword_action = HEAD_URL
            + "toPhoneUser/updateUserByPassword.action";

    // 忘记密码
    public static final String setNewPassword_action = HEAD_URL
            + "toPhoneUser/phoneUserUpdate.action";

    // 注册
    public static final String Register_action = HEAD_URL
            + "toPhoneUser/phoneRegistUser.action";

    // 获取投资类型
    public static final String GetInvestType_action = HEAD_URL
            + "toMemberInfoType/queryGeneralInvestType.action";

    // 获取投资时间类型
    public static final String GetInvestTimeType_action = HEAD_URL
            + "toMemberInfoType/queryGeneralInvestTime.action";

    // 获取投资专项类型
    public static final String GetInvestSpecialTypr_action = HEAD_URL
            + "toMemberInfoType/queryGeneralInvestSpeciality.action";

    // 更新个人信息
    public static final String updateMemberInfo_action = HEAD_URL
            + "toPhoneUser/updateMemberInfo.action";

    // 获取数据字典
    public static final String GetDataBook_action = HEAD_URL
            + "toMemberInfoType/queryMemberInfo.action";

    // 普普操盘手
    public static final String GetPupuTrader = HEAD_URL
            + "myTrader/traderList.action";

    // 我的普普操盘手
    public static final String GetMyPupuTrader = HEAD_URL
            + "myTrader/traderList4App.action";

    // 操盘手排行榜
    public static final String GetPupuTraderRank = HEAD_URL
            + "traderStatistic/statisticRank.action";

    // 获取操盘手信息
    public static final String GetTraderInfo = HEAD_URL
            + "myTrader/traderInfo.action";

    // 获取操盘手评论
    public static final String GetTraderComment = HEAD_URL
            + "myTrader/traderComment.action";

    // 发布操盘手评论
    public static final String PublistComment = HEAD_URL
            + "myTrader/traderCommentInsert.action";

    // 操盘手过往交易
    public static final String GetTraderPassTrade = HEAD_URL
            + "statisticsStock/stockTrader.action";

    // 操盘手过往交易详情
    public static final String GetTradePassTradeDetail = HEAD_URL
            + "statisticsStock/stockTraderDetail.action";

    // 操盘手今日关注 sysToday
    public static final String GetStockAttentionToday = HEAD_URL
            + "stockInterest/interestToday.action";

    // 操盘手今日关注详情
    public static final String GetStockAttentionTodayDetail = HEAD_URL
            + "stockInterest/interestToday.action";

    // 现时持仓
    public static final String GetTraderNowHold = HEAD_URL
            + "nowTrader/nowTraderStock.action";

    // 现时持仓详情
    public static final String GetTraderNowHoldDetail = HEAD_URL
            + "nowTrader/stockTraderDetail.action";

    // 最新过往交易与30天前过往交易
    public static final String GetPassTradeStockList = HEAD_URL
            + "queryTrader/traderStockDetail";

    // 持仓操盘手
    public static final String GetTraderHaveStock_action = HEAD_URL
            + "nowTrader/traderHaveStock";

    // 获取热门资讯列表
    public static final String GetHotInfoList = HEAD_URL
            + "toConsult/phoneQueryHotNews.action";

    // 我的股票
    public static final String GetMyStock = HEAD_URL
            + "myStocks/myStockList.action";

    // 股票搜索
    public static final String SearchStock_action = HEAD_URL
            + "myStocks/queryStockLike.action";

    // 添加股票
    public static final String AddStock_action = HEAD_URL
            + "myStocks/insertMyStock.action";

    // 删除股票
    public static final String deleteStock_action = HEAD_URL
            + "myStocks/deteleMyStockList";

    // 我的股票行情
    public static final String GetMyStockDetail = HEAD_URL
            + "myStocks/myStockDetail.action";

    // 我的股票新闻
    public static final String GetMyStockNews = HEAD_URL
            + "stockNew/consultNew";

    // 我的股票新闻新闻详情
    public static final String GetMyStockNewsDetail = HEAD_URL
            + "stockNew/consultNew";

    // 付费首页
    public static final String GetPayHomePageData = HEAD_URL
            + "toIndex/payIndex.action";

    // 未付费首页
    public static final String GetNoPayHomePageData = HEAD_URL
            + "toIndex/index.action";

    // 操盘手最新动向
    public static final String GetTraderNewTrend_action = HEAD_URL
            + "newStock/stockTrader.action";

    // 获取好友列表
    public static final String GetFriendList = HEAD_URL + "toRest/toGetFriend";

    // 搜索好友
    public static final String SearchFriends = HEAD_URL
            + "userInfo/queryFriends";

    // pay
    public static final String PayOrder = URL_PAY_ORDER + "toOrder/toPay";

    // 检查版本更新
    public static final String version_action = HEAD_URL
            + "appVersion/updateVersion";
    // 根据组ID获取组成员
    public static final String getGroupMember = HEAD_URL
            + "toRest/getGroupFriendByEaseMob";

    // 根据操盘手ID获取其所在的组
    public static final String getGroupByTradeId = HEAD_URL + "toRest/getGroup";

    // 统计
    public static final String traderFirstPageInsert = HEAD_URL
            + "myTrader/traderFirstPageInsert";

    // 获取博客列表
    public static final String traderBlog = HEAD_URL
            + "getTraderBolg/traderIdBolg";

    // 获取博客详情
    public static final String traderBlogDetail = HEAD_URL
            + "getTraderBolg/traderBolgDeatil";

    // 获取投资教室列表
    public static final String traderInvest = HEAD_URL
            + "traderTutorial/getTraderTutorial";

    // 获取投资教室详情
    public static final String traderInvestDetail = HEAD_URL
            + "traderTutorial/getTraderTutorialDetail";

    // 获取过往关注
    public static final String attentionPass = HEAD_URL
            + "stockInterest/traderWatchListHistory";

    // 表现统计
    public static final String tradecount = HEAD_URL + "chart/profitchart.html";

    //获取系统通知
    public static final String sysNotification = HEAD_URL + "myTrader/myTraderNotified";

    //系统消息过往交易
    public static final String sysPassTrade = HEAD_URL + "newStock/stockTraderById";

    //系统消息今日关注
    public static final String sysToday = HEAD_URL + "stockInterest/interestTodayByMemberId";

    //首页更多博客专栏
    public static final String getTraderBolg = HEAD_URL + "getTraderBolg/memberIdBlog";

    public static final String redSwanRequestContractPDF = HEAD_URL + "toPhoneUser/redSwanRequestContractPDF";

    public static final String redSwanCapitalWithdrawHistory = HEAD_URL + "toPhoneUser/redSwanCapitalWithdrawHistory";

    public static final String redSwanBonusHistory = HEAD_URL + "toPhoneUser/redSwanBonusHistory";

    public static final String redSwanCashWithdrawHistory = HEAD_URL + "toPhoneUser/redSwanCashWithdrawHistory";

    public static final String redSwanCashWithdrawal = HEAD_URL + "toPhoneUser/redSwanCashWithdrawal";

    public static final String redSwanCapitalWithdrawal = HEAD_URL + "toPhoneUser/redSwanCapitalWithdrawal";

}
