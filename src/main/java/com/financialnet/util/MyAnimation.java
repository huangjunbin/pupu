package com.financialnet.util;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.RotateAnimation;

public class MyAnimation {
	// 入动画
	public static void startAnimationIN(final View viewGroup, int duration) {

		Animation animation;
		/**
		 * 旋转动画 RotateAnimation(fromDegrees, toDegrees, pivotXType, pivotXValue,
		 * pivotYType, pivotYValue) fromDegrees 开始旋转角度 toDegrees 旋转到的角度
		 * pivotXType X轴 参照物 pivotXValue x轴 旋转的参考点 pivotYType Y轴 参照物 pivotYValue
		 * Y轴 旋转的参考点
		 */
		animation = new RotateAnimation(0, 90, Animation.RELATIVE_TO_SELF,
				0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		animation.setFillAfter(true);// 停留在动画结束位置
		animation.setDuration(duration);
		animation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {

			}
		});

		viewGroup.startAnimation(animation);

	}

	// 出动画
	public static void startAnimationOUT(final View viewGroup, int duration,
			int startOffSet) {

		Animation animation;
		animation = new RotateAnimation(90, 180, Animation.RELATIVE_TO_SELF,
				0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		animation.setFillAfter(true);// 停留在动画结束位置
		animation.setDuration(duration);
		animation.setStartOffset(startOffSet);
		animation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				viewGroup.clearAnimation();

			}
		});

		viewGroup.startAnimation(animation);
	}

	// 旋转动画
	public static void roateAnimation(final View viewGroup, int duration,
			int start, int stop) {
		Animation animation;
		animation = new RotateAnimation(start, stop,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		animation.setFillAfter(true);// 停留在动画结束位置
		animation.setDuration(duration);
		animation.setStartOffset(0);
		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {

			}
		});

		viewGroup.startAnimation(animation);
	}
}