package com.financialnet.util;

import android.annotation.SuppressLint;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

//时间格式师列：	yyyy-MM-dd HH:mm:ss
@SuppressLint("SimpleDateFormat")
public class DateUtil {
	public static Date date = null;

	public static DateFormat dateFormat = null;

	public static Calendar calendar = null;

	public static String[] weekDays = { "周日", "周一", "周二", "周三", "周四", "周五",
			"周六" };

	public static Date getDate(String dateTime, String formet) {
		SimpleDateFormat format = new SimpleDateFormat(formet);
		Date date = null;
		try {
			date = format.parse(dateTime);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Date parseDate(String dateStr, String format) {
		try {
			dateFormat = new SimpleDateFormat(format);
			String dt = dateStr.replaceAll("-", "/");
			if ((!dt.equals("")) && (dt.length() < format.length())) {
				dt += format.substring(dt.length()).replaceAll("[YyMmDdHhSs]",
						"0");
			}
			date = (Date) dateFormat.parse(dt);
		} catch (Exception e) {
		}
		return date;
	}

	public static Date parseDate(String dateStr) {
		return parseDate(dateStr, "yyyy-MM-dd");
	}

	public static String format(Date date, String format) {
		String result = "";
		try {
			if (date != null) {
				dateFormat = new SimpleDateFormat(format);
				result = dateFormat.format(date);
			}
		} catch (Exception e) {
		}
		return result;
	}

	public static String format(Date date) {
		return format(date, "yyyy-MM-dd");
	}

	public static int getYear(Date date) {
		calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}

	public static int getMonth(Date date) {
		calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MONTH) + 1;
	}

	public static int getDay(Date date) {
		calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	public static int getHour(Date date) {
		calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	public static int getMinute(Date date) {
		calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MINUTE);
	}

	public static int getSecond(Date date) {
		calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.SECOND);
	}

	public static long getMillis(Date date) {
		calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.getTimeInMillis();
	}

	public static String getDate(Date date) {
		return format(date, "yyyy-MM-dd");
	}

	public static String getTime(Date date) {
		return format(date, "HH:mm:ss");
	}

	public static String getDateTime(Date date) {
		return format(date, "yyyy-MM-dd HH:mm:ss");
	}

	public static String getDateTime(Date date, String formet) {
		return format(date, formet);
	}

	public static String getCurrentTime() {
		return getDateTime(new Date(System.currentTimeMillis()));
	}

	public static String getCurrentTime(String formet) {
		return getDateTime(new Date(System.currentTimeMillis()), formet);
	}

	public static String getDateMonth(Date date, String formet) {
		return format(date, formet);
	}

	/**
	 * 判断开始时间和结束时间哪个大
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isFirstLagre(String startTime, String endTime,
			String format) {

		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date d = null;
		Date d2 = null;
		try {
			d = sdf.parse(startTime);
			d2 = sdf.parse(endTime);
			long startLong = d.getTime();
			long endLong = d2.getTime();
			return startLong > endLong ? true : false;
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 判断当前时间是否比当前时间大
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isCurrentLagre(String anyTime, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date d = null;
		try {
			d = sdf.parse(anyTime);
			long currentLong = System.currentTimeMillis();
			long endLong = d.getTime();
			return currentLong > endLong ? true : false;
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static Date addDate(Date date, int day) {
		calendar = Calendar.getInstance();
		long millis = getMillis(date) + ((long) day) * 24 * 3600 * 1000;
		calendar.setTimeInMillis(millis);
		return calendar.getTime();
	}

	public static Date addDate_m(Date date, int minute) {
		calendar = Calendar.getInstance();
		long millis = getMillis(date) + ((long) minute) * 60 * 1000;
		calendar.setTimeInMillis(millis);
		return calendar.getTime();
	}

	public static int diffDate(Date date, Date date1) {
		return (int) ((getMillis(date) - getMillis(date1)) / (24 * 3600 * 1000));
	}

	public static String getMonthBegin(String strdate) {
		date = parseDate(strdate);
		return format(date, "yyyy-MM") + "-01";
	}

	public static String getMonthEnd(String strdate) {
		date = parseDate(getMonthBegin(strdate));
		calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, 1);
		calendar.add(Calendar.DAY_OF_YEAR, -1);
		return formatDate(calendar.getTime());
	}

	public static String formatDate(Date date) {
		return formatDateByFormat(date, "yyyy-MM-dd");
	}

	public static String formatDateByFormat(Date date, String format) {
		String result = "";
		if (date != null) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(format);
				result = sdf.format(date);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	// 5:00 AM 2014年5月17日
	public static String getFormatDate(String dateTime, String formet) {
		Date date = getDate(dateTime, formet);
		if (date == null) {
			return null;
		}
		String flag = "AM";
		String ymd = format(date, "yyyy年M月d");
		int minute = getMinute(date);
		int hour = getHour(date);
		if (hour > 12) {
			hour = hour - 12;
			flag = "PM";
		}

		return hour + ":" + minute + flag + "	" + ymd;
	}

	public static String getWeek(String pTime) {

		String Week = "";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(format.parse(pTime));

		} catch (ParseException e) {

			e.printStackTrace();
		}

		Week = weekDays[c.get(Calendar.DAY_OF_WEEK) - 1];
		return Week;
	}

	public static String getWeek(Calendar c) {
		String Week = "";
		Week = weekDays[c.get(Calendar.DAY_OF_WEEK) - 1];
		return Week;
	}
}
