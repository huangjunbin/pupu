package com.financialnet.model;

import java.io.Serializable;

/**
 * @className：HotInfo.java
 * @author: li mingtao
 * @Function: 热门资讯列表项
 * @createDate: 2014-8-19上午10:49:51
 * @update:
 */
public class HotInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1323841156109896797L;
	private String content;// 内容（Html）
	private String id;// id
	private String createTime;// 时间
	private String title;// 标题
	private String bigImageFile;// 大图
	private String smallImageFile;// 小图
	private String flagPublish;// 标记（不用）
	private String operator;// 类别（不用）

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBigImageFile() {
		return bigImageFile;
	}

	public void setBigImageFile(String bigImageFile) {
		this.bigImageFile = bigImageFile;
	}

	public String getSmallImageFile() {
		return smallImageFile;
	}

	public void setSmallImageFile(String smallImageFile) {
		this.smallImageFile = smallImageFile;
	}

	public String getFlagPublish() {
		return flagPublish;
	}

	public void setFlagPublish(String flagPublish) {
		this.flagPublish = flagPublish;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "HotInfo [content=" + content + ", id=" + id + ", createTime="
				+ createTime + ", title=" + title + ", bigImageFile="
				+ bigImageFile + ", smallImageFile=" + smallImageFile
				+ ", flagPublish=" + flagPublish + ", operator=" + operator
				+ "]";
	}
}
