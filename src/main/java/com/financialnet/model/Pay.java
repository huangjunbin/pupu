package com.financialnet.model;

public class Pay {
	private String RetCode;// 状态（0000表成功）
	private String RetMsg;// 下单是否成功说明
	private String Version;// 通讯协议版本号(固定2.0.0)
	private String MerchantId;// 商户代码
	private String Amount;// 付款金额
	private String TradeTime;// 交易时间
	private String OrderId;// 插件平台返回的订单系统订单号
	private String Sign;// 签名
	private String MerchOrderId;// 商户订单号

	public String getRetCode() {
		return RetCode;
	}

	public void setRetCode(String retCode) {
		RetCode = retCode;
	}

	public String getRetMsg() {
		return RetMsg;
	}

	public void setRetMsg(String retMsg) {
		RetMsg = retMsg;
	}

	public String getVersion() {
		return Version;
	}

	public void setVersion(String version) {
		Version = version;
	}

	public String getMerchantId() {
		return MerchantId;
	}

	public void setMerchantId(String merchantId) {
		MerchantId = merchantId;
	}

	public String getAmount() {
		return Amount;
	}

	public void setAmount(String amount) {
		Amount = amount;
	}

	public String getTradeTime() {
		return TradeTime;
	}

	public void setTradeTime(String tradeTime) {
		TradeTime = tradeTime;
	}

	public String getOrderId() {
		return OrderId;
	}

	public void setOrderId(String orderId) {
		OrderId = orderId;
	}

	public String getSign() {
		return Sign;
	}

	public void setSign(String sign) {
		Sign = sign;
	}

	public String getMerchOrderId() {
		return MerchOrderId;
	}

	public void setMerchOrderId(String merchOrderId) {
		MerchOrderId = merchOrderId;
	}

	@Override
	public String toString() {
		return "Pay [RetCode=" + RetCode + ", RetMsg=" + RetMsg + ", Version="
				+ Version + ", MerchantId=" + MerchantId + ", Amount=" + Amount
				+ ", TradeTime=" + TradeTime + ", OrderId=" + OrderId
				+ ", Sign=" + Sign + ", MerchOrderId=" + MerchOrderId + "]";
	}

}
