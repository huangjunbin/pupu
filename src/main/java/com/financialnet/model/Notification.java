package com.financialnet.model;

import com.simple.util.db.annotation.SimpleColumn;
import com.simple.util.db.annotation.SimpleId;
import com.simple.util.db.annotation.SimpleTable;

@SimpleTable(name = "t_notification")
public class Notification {
	@SimpleId
	@SimpleColumn(name = "id")
	private String id;
	@SimpleColumn(name = "type")
	private String type;// 0过往交易 1今日关注 2投资教师 3博客专栏
	@SimpleColumn(name = "createdTime")
	private String createdTime;
	@SimpleColumn(name = "alert")
	private String alert;
	@SimpleColumn(name = "state")
	private int state = 0;// 0未读 1已读


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getAlert() {
		return alert;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}


	@Override
	public String toString() {
		return "Notification{" +

				", id='" + id + '\'' +
				", type='" + type + '\'' +
				", createdTime='" + createdTime + '\'' +
				", alert='" + alert + '\'' +
				", state=" + state +

				'}';
	}
}
