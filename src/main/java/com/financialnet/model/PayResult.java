package com.financialnet.model;

import android.annotation.SuppressLint;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @className：@PayResult.java
 * @author: li mingtao
 * @Function:付费返回结果模型
 * @createDate: @d2014-10-13@下午3:24:48
 * @update:
 */
public class PayResult {
	private List<Trade> TradeNewTrendList;
	private List<Trade> todayAttentionList;
	private List<Stock> myStockList;
	private List<Trade> tradeBlog;
	private List<Tutorial> tutorialList;
	private List<Notification> notified;
	private List<Company> companyNote;
	@SuppressLint("UseSparseArrays")
	private Map<Integer, MainTradeProfit> tradeRankListMap = new HashMap<Integer, MainTradeProfit>();

	public List<Trade> getTradeNewTrendList() {
		return TradeNewTrendList;
	}

	public void setTradeNewTrendList(List<Trade> tradeNewTrendList) {
		TradeNewTrendList = tradeNewTrendList;
	}

	public List<Trade> getTodayAttentionList() {
		return todayAttentionList;
	}

	public void setTodayAttentionList(List<Trade> todayAttentionList) {
		this.todayAttentionList = todayAttentionList;
	}

	public List<Stock> getMyStockList() {
		return myStockList;
	}

	public void setMyStockList(List<Stock> myStockList) {
		this.myStockList = myStockList;
	}

	public Map<Integer, MainTradeProfit> getTradeRankListMap() {
		return tradeRankListMap;
	}

	public void setTradeRankListMap(
			Map<Integer, MainTradeProfit> tradeRankListMap) {
		this.tradeRankListMap = tradeRankListMap;
	}

	public List<Trade> getTradeBlog() {
		return tradeBlog;
	}

	public void setTradeBlog(List<Trade> tradeBlog) {
		this.tradeBlog = tradeBlog;
	}

	public List<Tutorial> getTutorialList() {
		return tutorialList;
	}

	public void setTutorialList(List<Tutorial> tutorialList) {
		this.tutorialList = tutorialList;
	}

	public List<Notification> getNotified() {
		return notified;
	}

	public void setNotified(List<Notification> notified) {
		this.notified = notified;
	}

	public List<Company> getCompanyNote() {
		return companyNote;
	}

	public void setCompanyNote(List<Company> companyNote) {
		this.companyNote = companyNote;
	}

	@Override
	public String toString() {
		return "PayResult{" +
				"TradeNewTrendList=" + TradeNewTrendList +
				", todayAttentionList=" + todayAttentionList +
				", myStockList=" + myStockList +
				", tradeBlog=" + tradeBlog +
				", tutorialList=" + tutorialList +
				", notified=" + notified +
				", companyNote=" + companyNote +
				", tradeRankListMap=" + tradeRankListMap +
				'}';
	}
}
