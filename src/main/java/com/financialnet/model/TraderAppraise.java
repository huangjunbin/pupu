package com.financialnet.model;

public class TraderAppraise {
	private String memberName;// 会员名称
	private String nickName;// 评论这昵称
	private String comment;// 评论内容
	private String score;// 评分
	private String createDate;// 日期

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	@Override
	public String toString() {
		return "TraderAppraise [memberName=" + memberName + ", nickName="
				+ nickName + ", comment=" + comment + ", score=" + score
				+ ", createDate=" + createDate + "]";
	}
}
