package com.financialnet.model;

public class TradeBlog {

	private String id;// ID
	private String title;// 博客标题
	private String avatarsAddress;// 图片地址
	private String createTime;// 日期
	private String traderName;
	private String traderId;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAvatarsAddress() {
		return avatarsAddress;
	}

	public void setAvatarsAddress(String avatarsAddress) {
		this.avatarsAddress = avatarsAddress;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getTraderName() {
		return traderName;
	}

	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}

	public String getTraderId() {
		return traderId;
	}

	public void setTraderId(String traderId) {
		this.traderId = traderId;
	}

	@Override
	public String toString() {
		return "TradeBlog{" +
				"id='" + id + '\'' +
				", title='" + title + '\'' +
				", avatarsAddress='" + avatarsAddress + '\'' +
				", createTime='" + createTime + '\'' +
				", traderName='" + traderName + '\'' +
				", traderId='" + traderId + '\'' +
				'}';
	}
}
