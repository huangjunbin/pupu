package com.financialnet.model;

/**
 * @className：SearchStock.java
 * @author: li mingtao
 * @Function:搜索的结果模型
 * @createDate: 2014-8-19下午11:33:46
 * @update:
 */
public class SearchStock {
	private String searchOlder;// 股票代号
	private String searchName;// 股票名称

	public String getSearchOlder() {
		return searchOlder;
	}

	public void setSearchOlder(String searchOlder) {
		this.searchOlder = searchOlder;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

}
