package com.financialnet.model;

/**
 * @className：AboutNew.java
 * @author: li mingtao
 * @Function: 相关新闻中的列表项
 * @createDate: 2014-8-19下午3:41:49
 * @update:
 */
public class AboutNew {
	private String newsIntrduce;// 新闻简介
	private String newsFrom;// 新闻出处
	private String newsTime;// 新闻发布时间

	public String getNewsIntrduce() {
		return newsIntrduce;
	}

	public void setNewsIntrduce(String newsIntrduce) {
		this.newsIntrduce = newsIntrduce;
	}

	public String getNewsFrom() {
		return newsFrom;
	}

	public void setNewsFrom(String newsFrom) {
		this.newsFrom = newsFrom;
	}

	public String getNewsTime() {
		return newsTime;
	}

	public void setNewsTime(String newsTime) {
		this.newsTime = newsTime;
	}
}
