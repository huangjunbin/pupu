package com.financialnet.model;

/**
 * @className：AddStockRequest.java
 * @author: lmt
 * @Function: 添加股票请求模型
 * @createDate: 2014-10-17 下午5:44:29
 * @update:
 */
public class AddStockRequest {
	private String marketID;// 市场Id
	private String stockCode;// 股票编号
	private String memberId;// 用户Id
	private String stockName;// 股票名称

	public String getMarketID() {
		return marketID;
	}

	public void setMarketID(String marketID) {
		this.marketID = marketID;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	@Override
	public String toString() {
		return "AddStock [marketID=" + marketID + ", stockCode=" + stockCode
				+ ", memberId=" + memberId + ", stockName=" + stockName + "]";
	}
}
