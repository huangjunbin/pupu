package com.financialnet.model;

import java.io.Serializable;

/**
 * 操盘手
 * 
 * @author allen
 * @time 2014-8-18
 */
public class Trade implements Serializable {
	private static final long serialVersionUID = 1L;
	private String memberType = "";// 操盘手类型 0：我最爱的操盘手 1：我已订阅的操盘手
	private String traderId = "";// 操盘手Id
	private String traderName = "";// 操盘手名称
	private String nickName = "";// 昵称
	private String score = "";// 评分
	private String earningsMoney = "";// 收益
	private String earningPercentage = "";// 收益百分比
	private String logoUrl = "";// 操盘手地址
	private String bigImage = "";// 大图
	private String avatarsAddress = "";// 操盘手图像地址
	private String selfIntroduction = "";// 自我介绍
	private String comment = "";// 评论
	private String title = "";// 博客标题

	private String typeDBCode = "";// 投资模式
	private String speDBCode = "";// 投资时间类型
	private String gitDBcode = "";// 专攻投资项
	private String investType = "";//
	private String investTime = "";//
	private String investSpeciality = "";//

	private String day = "";// 7天收益
	private String dayPercentage = "";// 7天收益百分比
	private String month = "";// 30天收益
	private String monthPercentage = "";// 30天收益百分比
	private String odds = "";// 胜率
	private String stalker = "";// 跟踪者
	private String rank = "";// 排行
	private String selft = "";// 收益
	private String selftInfo = "";// 收益百分比
	private String speCode = "";// 交易观点
	private String trader = "";// 交易观点

	private String stockPrice = "";// 最新动向
	private String stockCode = "";// 股票编号
	private String marketCode = "";// 市场编号
	private String stockName = "";// 股票名称
	private String flagTrade = "";// 买入、卖出类型 0买入，1卖出

	private String stopPrice = "";// 卖出价
	private String tradeTime = "";// 时间
	private String comments = "";// 评论
	private String contents = "";// 评论
	private String createTime = "";// 时间
	private String createDate = "";// 时间
	private String quantity = "";// 数量
	private String costPrice = "";// 成本价

	private String price = "";// 买入、卖出价
	private String flagAuthentication = "";// 是否认证：0否，1是

	private String marketID = "";// 市场id

	private String profit = "";// 盈利
	private String loss = "";// 亏损次数
	private String profitPercentage = "";// 盈利百分比
	private String lossPercentage = "";// 亏损百分比
	private String fail = "";
	private String profitEarningsMoney = "";
	private String failEarningsMoney = "";
	private String homePage;

	public String getTraderId() {
		return traderId;
	}

	public void setTraderId(String traderId) {
		this.traderId = traderId;
	}

	public String getTraderName() {
		return traderName;
	}

	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}



	public String getEarningsMoney() {
		return earningsMoney;
	}

	public void setEarningsMoney(String earningsMoney) {
		this.earningsMoney = earningsMoney;
	}

	public String getEarningPercentage() {
		return earningPercentage;
	}

	public void setEarningPercentage(String earningPercentage) {
		this.earningPercentage = earningPercentage;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getAvatarsAddress() {
		return avatarsAddress;
	}

	public void setAvatarsAddress(String avatarsAddress) {
		this.avatarsAddress = avatarsAddress;
	}

	public String getSelfIntroduction() {
		return selfIntroduction;
	}

	public void setSelfIntroduction(String selfIntroduction) {
		this.selfIntroduction = selfIntroduction;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTypeDBCode() {
		return typeDBCode;
	}

	public void setTypeDBCode(String typeDBCode) {
		this.typeDBCode = typeDBCode;
	}

	public String getSpeDBCode() {
		return speDBCode;
	}

	public void setSpeDBCode(String speDBCode) {
		this.speDBCode = speDBCode;
	}

	public String getGitDBcode() {
		return gitDBcode;
	}

	public void setGitDBcode(String gitDBcode) {
		this.gitDBcode = gitDBcode;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getDayPercentage() {
		return dayPercentage;
	}

	public void setDayPercentage(String dayPercentage) {
		this.dayPercentage = dayPercentage;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getMonthPercentage() {
		return monthPercentage;
	}

	public void setMonthPercentage(String monthPercentage) {
		this.monthPercentage = monthPercentage;
	}

	public String getOdds() {
		return odds;
	}

	public void setOdds(String odds) {
		this.odds = odds;
	}

	public String getStalker() {
		return stalker;
	}

	public void setStalker(String stalker) {
		this.stalker = stalker;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getSpeCode() {
		return speCode;
	}

	public void setSpeCode(String speCode) {
		this.speCode = speCode;
	}

	public String getTrader() {
		return trader;
	}

	public void setTrader(String trader) {
		this.trader = trader;
	}

	public String getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(String stockPrice) {
		this.stockPrice = stockPrice;
	}

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getMarketCode() {
		return marketCode;
	}

	public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getFlagTrade() {
		return flagTrade;
	}

	public void setFlagTrade(String flagTrade) {
		this.flagTrade = flagTrade;
	}

	public String getStopPrice() {
		return stopPrice;
	}

	public void setStopPrice(String stopPrice) {
		this.stopPrice = stopPrice;
	}

	public String getTradeTime() {
		return tradeTime;
	}

	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getFlagAuthentication() {
		return flagAuthentication;
	}

	public void setFlagAuthentication(String flagAuthentication) {
		this.flagAuthentication = flagAuthentication;
	}

	public String getProfit() {
		return profit;
	}

	public void setProfit(String profit) {
		this.profit = profit;
	}

	public String getLoss() {
		return loss;
	}

	public void setLoss(String loss) {
		this.loss = loss;
	}

	public String getProfitPercentage() {
		return profitPercentage;
	}

	public void setProfitPercentage(String profitPercentage) {
		this.profitPercentage = profitPercentage;
	}

	public String getLossPercentage() {
		return lossPercentage;
	}

	public void setLossPercentage(String lossPercentage) {
		this.lossPercentage = lossPercentage;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getMarketID() {
		return marketID;
	}

	public void setMarketID(String marketID) {
		this.marketID = marketID;
	}

	public String getInvestType() {
		return investType;
	}

	public void setInvestType(String investType) {
		this.investType = investType;
	}

	public String getInvestTime() {
		return investTime;
	}

	public void setInvestTime(String investTime) {
		this.investTime = investTime;
	}

	public String getInvestSpeciality() {
		return investSpeciality;
	}

	public void setInvestSpeciality(String investSpeciality) {
		this.investSpeciality = investSpeciality;
	}

	public String getSelft() {
		return selft;
	}

	public void setSelft(String selft) {
		this.selft = selft;
	}

	public String getSelftInfo() {
		return selftInfo;
	}

	public void setSelftInfo(String selftInfo) {
		this.selftInfo = selftInfo;
	}

	/**
	 * @return the memberType
	 */
	public String getMemberType() {
		return memberType;
	}

	/**
	 * @param memberType
	 *            the memberType to set
	 */
	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	/**
	 * @return the bigImage
	 */
	public String getBigImage() {
		return bigImage;
	}

	/**
	 * @param bigImage
	 *            the bigImage to set
	 */
	public void setBigImage(String bigImage) {
		this.bigImage = bigImage;
	}

	public String getFail() {
		return fail;
	}

	public void setFail(String fail) {
		this.fail = fail;
	}

	public String getProfitEarningsMoney() {
		return profitEarningsMoney;
	}

	public void setProfitEarningsMoney(String profitEarningsMoney) {
		this.profitEarningsMoney = profitEarningsMoney;
	}

	public String getFailEarningsMoney() {
		return failEarningsMoney;
	}

	public void setFailEarningsMoney(String failEarningsMoney) {
		this.failEarningsMoney = failEarningsMoney;
	}

	public String getHomePage() {
		return homePage;
	}

	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}



    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Trade{" +
                "memberType='" + memberType + '\'' +
                ", traderId='" + traderId + '\'' +
                ", traderName='" + traderName + '\'' +
                ", nickName='" + nickName + '\'' +
                ", score='" + score + '\'' +
                ", earningsMoney='" + earningsMoney + '\'' +
                ", earningPercentage='" + earningPercentage + '\'' +
                ", logoUrl='" + logoUrl + '\'' +
                ", bigImage='" + bigImage + '\'' +
                ", avatarsAddress='" + avatarsAddress + '\'' +
                ", selfIntroduction='" + selfIntroduction + '\'' +
                ", comment='" + comment + '\'' +
                ", title='" + title + '\'' +
                ", typeDBCode='" + typeDBCode + '\'' +
                ", speDBCode='" + speDBCode + '\'' +
                ", gitDBcode='" + gitDBcode + '\'' +
                ", investType='" + investType + '\'' +
                ", investTime='" + investTime + '\'' +
                ", investSpeciality='" + investSpeciality + '\'' +
                ", day='" + day + '\'' +
                ", dayPercentage='" + dayPercentage + '\'' +
                ", month='" + month + '\'' +
                ", monthPercentage='" + monthPercentage + '\'' +
                ", odds='" + odds + '\'' +
                ", stalker='" + stalker + '\'' +
                ", rank='" + rank + '\'' +
                ", selft='" + selft + '\'' +
                ", selftInfo='" + selftInfo + '\'' +
                ", speCode='" + speCode + '\'' +
                ", trader='" + trader + '\'' +
                ", stockPrice='" + stockPrice + '\'' +
                ", stockCode='" + stockCode + '\'' +
                ", marketCode='" + marketCode + '\'' +
                ", stockName='" + stockName + '\'' +
                ", flagTrade='" + flagTrade + '\'' +
                ", stopPrice='" + stopPrice + '\'' +
                ", tradeTime='" + tradeTime + '\'' +
                ", comments='" + comments + '\'' +
                ", contents='" + contents + '\'' +
                ", createTime='" + createTime + '\'' +
                ", createDate='" + createDate + '\'' +
                ", quantity='" + quantity + '\'' +
                ", costPrice='" + costPrice + '\'' +
                ", price='" + price + '\'' +
                ", flagAuthentication='" + flagAuthentication + '\'' +
                ", marketID='" + marketID + '\'' +
                ", profit='" + profit + '\'' +
                ", loss='" + loss + '\'' +
                ", profitPercentage='" + profitPercentage + '\'' +
                ", lossPercentage='" + lossPercentage + '\'' +
                ", fail='" + fail + '\'' +
                ", profitEarningsMoney='" + profitEarningsMoney + '\'' +
                ", failEarningsMoney='" + failEarningsMoney + '\'' +
                ", homePage='" + homePage + '\'' +
                '}';
    }
}
