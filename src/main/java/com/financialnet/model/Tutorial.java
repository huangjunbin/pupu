package com.financialnet.model;

public class Tutorial {
	private String id;
	private String  createTime;
	private String  title;
	private String  traderId;
	private String  traderName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTraderId() {
		return traderId;
	}
	public void setTraderId(String traderId) {
		this.traderId = traderId;
	}
	public String getTraderName() {
		return traderName;
	}
	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}
	@Override
	public String toString() {
		return "Tutorial [id=" + id + ", createTime=" + createTime + ", title="
				+ title + ", traderId=" + traderId + ", traderName="
				+ traderName + "]";
	}
	
}
