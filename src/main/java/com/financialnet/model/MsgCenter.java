package com.financialnet.model;

/**
 * @className：MsgCenter.java
 * @author: li mingtao
 * @Function: 个人中心
 * @createDate: 2014-8-18下午2:04:55
 * @update:
 */
public class MsgCenter {
	public static final int HAVE_RIGHT_TEXT_ITEM = 1;// 右边是文字的item
	public static final int NO_RIGHT_TEXT_ITEM = 2;// 右边没有文字的item
	public static final int RIGHT_BUTTON_ITEM = 3;// 右边有按钮的item

	// --
	public static final String RECIVE_MY_STOCK_NOTIFY = "recive_mystock_notify";// 接受我的股票通知
	public static final String RECIVE_MY_TRADER_NOTIFY = "recive_mytrader_notify";// 接受我的操盘手通知
	public static final String RECIVE_HOTNEWS_NOTIFY = "recive_hotnews_notify";// 接受热门资讯通知

	// --
	int type;
	String item_Text;
	// for HAVE_RIGHT_TEXT_ITEM
	String choose_Text;
	// for RIGHT_BUTTON_ITEM
	boolean button_Choosed;
	String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getItem_Text() {
		return item_Text;
	}

	public void setItem_Text(String item_Text) {
		this.item_Text = item_Text;
	}

	public String getChoose_Text() {
		return choose_Text;
	}

	public void setChoose_Text(String choose_Text) {
		this.choose_Text = choose_Text;
	}

	public boolean isButton_Choosed() {
		return button_Choosed;
	}

	public void setButton_Choosed(boolean button_Choosed) {
		this.button_Choosed = button_Choosed;
	}
}
