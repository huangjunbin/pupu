package com.financialnet.model;

/**
 * @className：HoldDoyen.java
 * @author: li mingtao
 * @Function: 持仓达人数据模型
 * @createDate: 2014-8-19下午4:49:34
 * @update:
 */
public class HoldDoyen {
	private int doyenImg;// 达人图像
	private String doyenNickName;// 达人称号
	private String doyenProfit;// 达人收益
	private String doyenPrice;// 成本价啊

	public int getDoyenImg() {
		return doyenImg;
	}

	public void setDoyenImg(int doyenImg) {
		this.doyenImg = doyenImg;
	}

	public String getDoyenNickName() {
		return doyenNickName;
	}

	public void setDoyenNickName(String doyenNickName) {
		this.doyenNickName = doyenNickName;
	}

	public String getDoyenProfit() {
		return doyenProfit;
	}

	public void setDoyenProfit(String doyenProfit) {
		this.doyenProfit = doyenProfit;
	}

	public String getDoyenPrice() {
		return doyenPrice;
	}

	public void setDoyenPrice(String doyenPrice) {
		this.doyenPrice = doyenPrice;
	}
}
