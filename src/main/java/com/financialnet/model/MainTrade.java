package com.financialnet.model;

import java.io.Serializable;

/**
 * @className：MainTrade.java
 * @author: allen
 * @Function: 主页操盘手
 * @createDate: 2014-9-1
 * @update:
 */
public class MainTrade implements Serializable {

	private static final long serialVersionUID = -1323841156109896797L;
	private String name;
	private String stockName;
	private String stockTime;
	private String stockValue;
	private String time;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getStockTime() {
		return stockTime;
	}

	public void setStockTime(String stockTime) {
		this.stockTime = stockTime;
	}

	public String getStockValue() {
		return stockValue;
	}

	public void setStockValue(String stockValue) {
		this.stockValue = stockValue;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "MainTrade [name=" + name + ", stockName=" + stockName
				+ ", stockTime=" + stockTime + ", stockValue=" + stockValue
				+ ", time=" + time + "]";
	}

}