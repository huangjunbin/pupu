package com.financialnet.model;

import android.annotation.SuppressLint;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NoPayResult {
	private Trade radomTrader;
	private List<Trade> tradeBlog;
	private List<Trade> TradeComments;
	private List<Trade> TradeMounthPreTrend;
	private List<Company> companyNote;
	@SuppressLint("UseSparseArrays")
	private Map<Integer, MainTradeProfit> tradeRankListMap = new HashMap<Integer, MainTradeProfit>();

	public Trade getRadomTrader() {
		return radomTrader;
	}

	public void setRadomTrader(Trade radomTrader) {
		this.radomTrader = radomTrader;
	}

	 
	public List<Trade> getTradeBlog() {
		return tradeBlog;
	}

	public void setTradeBlog(List<Trade> tradeBlog) {
		this.tradeBlog = tradeBlog;
	}

	public List<Trade> getTradeComments() {
		return TradeComments;
	}

	public void setTradeComments(List<Trade> tradeComments) {
		TradeComments = tradeComments;
	}

	public List<Trade> getTradeMounthPreTrend() {
		return TradeMounthPreTrend;
	}

	public void setTradeMounthPreTrend(List<Trade> tradeMounthPreTrend) {
		TradeMounthPreTrend = tradeMounthPreTrend;
	}

	public Map<Integer, MainTradeProfit> getTradeRankListMap() {
		return tradeRankListMap;
	}

	public void setTradeRankListMap(
			Map<Integer, MainTradeProfit> tradeRankListMap) {
		this.tradeRankListMap = tradeRankListMap;
	}

	public List<Company> getCompanyNote() {
		return companyNote;
	}

	public void setCompanyNote(List<Company> companyNote) {
		this.companyNote = companyNote;
	}

	@Override
	public String toString() {
		return "NoPayResult{" +
				"radomTrader=" + radomTrader +
				", tradeBlog=" + tradeBlog +
				", TradeComments=" + TradeComments +
				", TradeMounthPreTrend=" + TradeMounthPreTrend +
				", companyNote=" + companyNote +
				", tradeRankListMap=" + tradeRankListMap +
				'}';
	}
}
