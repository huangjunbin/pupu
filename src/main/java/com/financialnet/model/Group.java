package com.financialnet.model;

public class Group {
	private int belongroup;
	private String groupDescription;
	private String id;
	private String flagIsActivate;
	private String groupName;
	private String groupID;
	private String status;
	private String joinTime;
	private String groupSummary;
	private String creatorID;
	private String memberID;
	private String flagPublic;

	public Group(int mBelongroup, String mGroupDescription) {
		setBelongroup(mBelongroup);
		setGroupDescription(mGroupDescription);
	}

	public Group() {

	}

	public int getBelongroup() {
		return belongroup;
	}

	public void setBelongroup(int belongroup) {
		this.belongroup = belongroup;
	}

	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFlagIsActivate() {
		return flagIsActivate;
	}

	public void setFlagIsActivate(String flagIsActivate) {
		this.flagIsActivate = flagIsActivate;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupID() {
		return groupID;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getJoinTime() {
		return joinTime;
	}

	public void setJoinTime(String joinTime) {
		this.joinTime = joinTime;
	}

	public String getGroupSummary() {
		return groupSummary;
	}

	public void setGroupSummary(String groupSummary) {
		this.groupSummary = groupSummary;
	}

	public String getCreatorID() {
		return creatorID;
	}

	public void setCreatorID(String creatorID) {
		this.creatorID = creatorID;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getFlagPublic() {
		return flagPublic;
	}

	public void setFlagPublic(String flagPublic) {
		this.flagPublic = flagPublic;
	}

	@Override
	public String toString() {
		return "Group [belongroup=" + belongroup + ", groupDescription="
				+ groupDescription + ", id=" + id + ", flagIsActivate="
				+ flagIsActivate + ", groupName=" + groupName + ", groupID="
				+ groupID + ", status=" + status + ", joinTime=" + joinTime
				+ ", groupSummary=" + groupSummary + ", creatorID=" + creatorID
				+ ", memberID=" + memberID + ", flagPublic=" + flagPublic + "]";
	}

}
