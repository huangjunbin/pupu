package com.financialnet.model;

public class Version {
	private String id;// ID
	private String versionNo;// 版本号
	private String appName;// 版本名称
	private String content;// 内容
	private String versionCode;// 版本code
	private String type;// 类型：0 ios , 1 :android
	private String url;// 下载地址
	private String createDate;// 时间
	private int flagUpdate;//1 强制 0否

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the versionNo
	 */
	public String getVersionNo() {
		return versionNo;
	}

	/**
	 * @param versionNo
	 *            the versionNo to set
	 */
	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

	/**
	 * @return the appName
	 */
	public String getAppName() {
		return appName;
	}

	/**
	 * @param appName
	 *            the appName to set
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the versionCode
	 */
	public String getVersionCode() {
		return versionCode;
	}

	/**
	 * @param versionCode
	 *            the versionCode to set
	 */
	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the createDate
	 */
	public String getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate
	 *            the createDate to set
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public int getFlagUpdate() {
		return flagUpdate;
	}

	public void setFlagUpdate(int flagUpdate) {
		this.flagUpdate = flagUpdate;
	}

	@Override
	public String toString() {
		return "Version{" +
				"id='" + id + '\'' +
				", versionNo='" + versionNo + '\'' +
				", appName='" + appName + '\'' +
				", content='" + content + '\'' +
				", versionCode='" + versionCode + '\'' +
				", type='" + type + '\'' +
				", url='" + url + '\'' +
				", createDate='" + createDate + '\'' +
				", flagUpdate=" + flagUpdate +
				'}';
	}
}
