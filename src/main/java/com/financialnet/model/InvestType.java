package com.financialnet.model;

/**
 * @className: InvestType.java
 * @author: limingtao
 * @function: 投资类型模型
 * @date: 2013年9月18日下午1:36:03
 * @update:
 */
public class InvestType {
	private String description;// 投资描述
	private int dbCode;// 投资描述编码

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDbCode() {
		return dbCode;
	}

	public void setDbCode(int dbCode) {
		this.dbCode = dbCode;
	}

	@Override
	public String toString() {
		return "InvestType [description=" + description + ", dbCode=" + dbCode
				+ "]";
	}
}
