package com.financialnet.model;


public class FundDetail {

    private String tradeTime;
    private String amount;
    private String status;
    private String bonusTime;
    private String bonusType;

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public String getBonusTime() {
        return bonusTime;
    }

    public void setBonusTime(String bonusTime) {
        this.bonusTime = bonusTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBonusType() {
        return bonusType;
    }

    public void setBonusType(String bonusType) {
        this.bonusType = bonusType;
    }

    @Override
    public String toString() {
        return "FundDetail{" +
                "tradeTime='" + tradeTime + '\'' +
                ", amount='" + amount + '\'' +
                ", status='" + status + '\'' +
                ", bonusTime='" + bonusTime + '\'' +
                ", bonusType='" + bonusType + '\'' +
                '}';
    }
}
