package com.financialnet.model;

public class Investment {
	private String id;// 视频ID
	private String traderID;// 操盘手ID
	private String content;// 内容
	private String vedioLink;// 视频地址
	private String createTime;// 时间
	private String flagPublish;// 否发布0否，1是
	private String title;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTraderID() {
		return traderID;
	}

	public void setTraderID(String traderID) {
		this.traderID = traderID;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getVedioLink() {
		return vedioLink;
	}

	public void setVedioLink(String vedioLink) {
		this.vedioLink = vedioLink;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getFlagPublish() {
		return flagPublish;
	}

	public void setFlagPublish(String flagPublish) {
		this.flagPublish = flagPublish;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "Investment [id=" + id + ", traderID=" + traderID + ", content="
				+ content + ", vedioLink=" + vedioLink + ", createTime="
				+ createTime + ", flagPublish=" + flagPublish + ", title="
				+ title + "]";
	}

}
