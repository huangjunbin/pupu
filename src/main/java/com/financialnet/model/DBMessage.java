package com.financialnet.model;

import java.io.Serializable;

import com.simple.util.db.annotation.SimpleColumn;
import com.simple.util.db.annotation.SimpleId;
import com.simple.util.db.annotation.SimpleTable;

/**
 * IM消息记录
 * 
 * @author bin
 * @version 1.0.0
 * @created 2014-3-26
 */
@SimpleTable(name = "t_message")
public class DBMessage implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 消息类型，用户自定义消息类别
	 */
	@SimpleId
	@SimpleColumn(name = "mid")
	private String mid;

	/**
	 * 消息类型，用户自定义消息类别
	 */
	@SimpleColumn(name = "type")
	private String type;
	/**
	 * 消息标题
	 */
	@SimpleColumn(name = "title")
	private String title;
	/**
	 * 消息类容，于type 组合为任何类型消息，content 根据 format 可表示为 text,json ,xml数据格式
	 */
	@SimpleColumn(name = "content")
	private String content;

	/**
	 * 消息发送者账号
	 */
	@SimpleColumn(name = "sender")
	private String sender;
	/**
	 * 消息发送者接收者
	 */
	@SimpleColumn(name = "receiver")
	private String receiver;

	/**
	 * 文件 url
	 */
	@SimpleColumn(name = "file")
	private String file;
	/**
	 * 文件类型
	 */
	@SimpleColumn(name = "fileType")
	private String fileType;

	/**
	 * content 内容格式
	 */
	@SimpleColumn(name = "format")
	private String format = "txt";

	@SimpleColumn(name = "timestamp")
	private long timestamp;

	/**
	 * 标注消息是否已读 0表示未读 1表示已读
	 */
	@SimpleColumn(name = "state")
	private int state;

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	@Override
	public String toString() {
		return "DBMessage [mid=" + mid + ", type=" + type + ", title=" + title
				+ ", content=" + content + ", sender=" + sender + ", receiver="
				+ receiver + ", file=" + file + ", fileType=" + fileType
				+ ", format=" + format + ", timestamp=" + timestamp
				+ ", state=" + state + "]";
	}
}
