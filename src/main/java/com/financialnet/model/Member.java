package com.financialnet.model;

import com.simple.util.db.annotation.SimpleColumn;
import com.simple.util.db.annotation.SimpleId;
import com.simple.util.db.annotation.SimpleTable;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * User用户表
 *
 * @author allen
 */
@SimpleTable(name = "t_member")
public class Member implements Serializable {
    private static final long serialVersionUID = 1L;
    @SimpleId
    @SimpleColumn(name = "memberID")
    private Integer memberID;// 会员ID

    @SimpleColumn(name = "memberName")
    private String memberName;// 会员昵称

    @SimpleColumn(name = "selfIntroduction")
    private String selfIntroduction;// 金句

    @SimpleColumn(name = "gitDBcode")
    private String gitDBcode;// 投资时间类型

    @SimpleColumn(name = "typeDBCode")
    private String typeDBCode;// 投资类型

    @SimpleColumn(name = "speDBCode")
    private String speDBCode;// 投资专项类型

    @SimpleColumn(name = "flagStockPush")
    private Integer flagStockPush;// 股票推送 0:no;1:yes

    @SimpleColumn(name = "flagTraderPush")
    private Integer flagTraderPush;// 热门咨询推送 0:no;1:yes

    @SimpleColumn(name = "flagConsultPush")
    private Integer flagConsultPush;// 操盘手推送 0:no;1:yes

    @SimpleColumn(name = "flagIsConfirmed")
    private Integer flagIsConfirmed;// 会员级别（0:未付费会员 1::已付费会员）

    @SimpleColumn(name = "mobileNumber")
    private String mobileNumber;// 手机号码

    @SimpleColumn(name = "isRemember")
    private Integer isRemember;// 是否记住密码 0不记住 1记住

    @SimpleColumn(name = "avatarsAddress")
    private String avatarsAddress;// 图像地址

    @SimpleColumn(name = "password")
    private String password;// 图像地址

    @SimpleColumn(name = "typeDescription")
    private String typeDescription;// 投资类型str

    @SimpleColumn(name = "speDescription")
    private String speDescription;// 投资项目类型str

    @SimpleColumn(name = "timeDescription")
    private String timeDescription;// 投资时间类型str

    @SimpleColumn(name = "memberClass")
    private Integer memberClass;// 会员级别 1黄金 2白金 3钻石

    @SimpleColumn(name = "authorization")
    private String authorization;//鉴权字段

    @SimpleColumn(name = "kmemberKey")
    private Integer kmemberKey; // 计划会员ID

    @SimpleColumn(name = "kMemberPlan")
    private Integer kMemberPlan;// 计划ID

    @SimpleColumn(name = "kMemberPlanName")
    private String kMemberPlanName;// // 计划名称

    @SimpleColumn(name = "kFlagActivate")
    private Integer kFlagActivate; // 计划激活，0 没激活，1 已激活

    @SimpleColumn(name = "kFlagLock")
    private Integer kFlagLock; // 计划锁定，0 没锁定，1 已锁定

    @SimpleColumn(name = "kFlagMature")
    private Integer kFlagMature; //计划完结，0 没完结，1 已完结

    @SimpleColumn(name = "kFlagDeleted")
    private Integer kFlagDeleted; // 计划删除，0 没删除，1 已删除

    @SimpleColumn(name = "kLastWDCashStatus")
    private Integer kLastWDCashStatus; // 提取现⾦状况，-1 没记录，0 在审核，1 已批准

    @SimpleColumn(name = "kLastWDCapiatlStatus")
    private Integer kLastWDCapiatlStatus;// 提取本⾦状况，-1 没记录，0 在审核，1 已批准

    @SimpleColumn(name = "kMemberPlanActivateTime")
    private String kMemberPlanActivateTime; // 计划激活⽇期

    @SimpleColumn(name = "kMemberPlanEndTime")
    private String kMemberPlanEndTime; // 计划到期⽇期

    @SimpleColumn(name = "kCapital")
    private Float kCapital; // 计划本⾦

    @SimpleColumn(name = "kCapitalWord")
    private String kCapitalWord; // 计划本⾦⽂字

    @SimpleColumn(name = "kBonusLeft")
    private Float kBonusLeft; // 收益⾦额

    @SimpleColumn(name = "kBonusLeftWord")
    private String kBonusLeftWord; // 收益⾦额⽂字

    @SimpleColumn(name = "kBonusSum")
    private Float kBonusSum; // 收益合共⾦额

    @SimpleColumn(name = "kBonusSumWord")
    private String kBonusSumWord; //收益合共⾦额⽂字

    @SimpleColumn(name = "kCashOnHand")
    private Float kCashOnHand;// 计划现⾦

    @SimpleColumn(name = "kCashOnHandWord")
    private String kCashOnHandWord; // 计划现⾦⽂字

    @SimpleColumn(name = "kRedSwanTotal")
    private Float kRedSwanTotal; // 计划合共⾦额

    @SimpleColumn(name = "kRedSwanTotalWord")
    private String kRedSwanTotalWord; // 计划合共⾦额⽂字

    @SimpleColumn(name = "kIsAfterPenaltyTime")
    private Integer kIsAfterPenaltyTime; // 提取本⾦处罚，0 需要处罚，1 不需要处罚

    @SimpleColumn(name = "kMemberPlanContractPDF")
    private String kMemberPlanContractPDF; //电⼦合约地址


    public Integer getMemberID() {
        return memberID;
    }

    public void setMemberID(Integer memberID) {
        this.memberID = memberID;
    }

    public String getMemberName() {
        if (memberName != null) {
            return memberName;
        } else {
            return "";
        }
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getSelfIntroduction() {
        return selfIntroduction;
    }

    public void setSelfIntroduction(String selfIntroduction) {
        this.selfIntroduction = selfIntroduction;
    }

    public String getGitDBcode() {
        return gitDBcode;
    }

    public void setGitDBcode(String gitDBcode) {
        this.gitDBcode = gitDBcode;
    }

    public String getTypeDBCode() {
        return typeDBCode;
    }

    public void setTypeDBCode(String typeDBCode) {
        this.typeDBCode = typeDBCode;
    }

    public String getSpeDBCode() {
        return speDBCode;
    }

    public void setSpeDBCode(String speDBCode) {
        this.speDBCode = speDBCode;
    }

    public Integer getFlagStockPush() {
        return flagStockPush;
    }

    public void setFlagStockPush(Integer flagStockPush) {
        this.flagStockPush = flagStockPush;
    }

    public Integer getFlagTraderPush() {
        return flagTraderPush;
    }

    public void setFlagTraderPush(Integer flagTraderPush) {
        this.flagTraderPush = flagTraderPush;
    }

    public Integer getFlagConsultPush() {
        return flagConsultPush;
    }

    public void setFlagConsultPush(Integer flagConsultPush) {
        this.flagConsultPush = flagConsultPush;
    }

    public Integer getFlagIsConfirmed() {
        return flagIsConfirmed;
    }

    public void setFlagIsConfirmed(Integer flagIsConfirmed) {
        this.flagIsConfirmed = flagIsConfirmed;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Integer getIsRemember() {
        return isRemember;
    }

    public void setIsRemember(Integer isRemember) {
        this.isRemember = isRemember;
    }

    public String getAvatarsAddress() {
        if (avatarsAddress != null) {
            return avatarsAddress;
        } else {
            return "";
        }
    }

    public void setAvatarsAddress(String avatarsAddress) {
        this.avatarsAddress = avatarsAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTypeDescription() {
        return typeDescription;
    }

    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }

    public String getSpeDescription() {
        return speDescription;
    }

    public void setSpeDescription(String speDescription) {
        this.speDescription = speDescription;
    }

    public String getTimeDescription() {
        return timeDescription;
    }

    public void setTimeDescription(String timeDescription) {
        this.timeDescription = timeDescription;
    }

    public Integer getMemberClass() {
        return memberClass;
    }

    public void setMemberClass(Integer memberClass) {
        this.memberClass = memberClass;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public int getKmemberKey() {
        return kmemberKey;
    }

    public void setKmemberKey(int kmemberKey) {
        this.kmemberKey = kmemberKey;
    }

    public int getKMemberPlan() {
        return kMemberPlan;
    }

    public void setKMemberPlan(int kMemberPlan) {
        this.kMemberPlan = kMemberPlan;
    }

    public String getKMemberPlanName() {
        return kMemberPlanName;
    }

    public void setKMemberPlanName(String kMemberPlanName) {
        this.kMemberPlanName = kMemberPlanName;
    }

    public int getKFlagActivate() {
        return kFlagActivate;
    }

    public void setKFlagActivate(int kFlagActivate) {
        this.kFlagActivate = kFlagActivate;
    }

    public int getKFlagLock() {
        return kFlagLock;
    }

    public void setKFlagLock(int kFlagLock) {
        this.kFlagLock = kFlagLock;
    }

    public int getKFlagMature() {
        return kFlagMature;
    }

    public void setKFlagMature(int kFlagMature) {
        this.kFlagMature = kFlagMature;
    }

    public int getKFlagDeleted() {
        return kFlagDeleted;
    }

    public void setKFlagDeleted(int kFlagDeleted) {
        this.kFlagDeleted = kFlagDeleted;
    }

    public int getKLastWDCashStatus() {
        return kLastWDCashStatus;
    }

    public void setKLastWDCashStatus(int kLastWDCashStatus) {
        this.kLastWDCashStatus = kLastWDCashStatus;
    }

    public int getKLastWDCapiatlStatus() {
        return kLastWDCapiatlStatus;
    }

    public void setKLastWDCapiatlStatus(int kLastWDCapiatlStatus) {
        this.kLastWDCapiatlStatus = kLastWDCapiatlStatus;
    }

    public String getKMemberPlanActivateTime() {
        return kMemberPlanActivateTime;
    }

    public void setKMemberPlanActivateTime(String kMemberPlanActivateTime) {
        this.kMemberPlanActivateTime = kMemberPlanActivateTime;
    }

    public String getKMemberPlanEndTime() {
        return kMemberPlanEndTime;
    }

    public void setKMemberPlanEndTime(String kMemberPlanEndTime) {
        this.kMemberPlanEndTime = kMemberPlanEndTime;
    }

    public float getKCapital() {
        return kCapital;
    }

    public void setKCapital(float kCapital) {
        this.kCapital = kCapital;
    }

    public String getKCapitalWord() {
        return kCapitalWord;
    }

    public void setKCapitalWord(String kCapitalWord) {
        this.kCapitalWord = kCapitalWord;
    }

    public float getKBonusLeft() {
        return kBonusLeft;
    }

    public void setKBonusLeft(float kBonusLeft) {
        this.kBonusLeft = kBonusLeft;
    }

    public String getKBonusLeftWord() {
        return kBonusLeftWord;
    }

    public void setKBonusLeftWord(String kBonusLeftWord) {
        this.kBonusLeftWord = kBonusLeftWord;
    }

    public float getKBonusSum() {
        return kBonusSum;
    }

    public void setKBonusSum(float kBonusSum) {
        this.kBonusSum = kBonusSum;
    }

    public String getKBonusSumWord() {
        return kBonusSumWord;
    }

    public void setKBonusSumWord(String kBonusSumWord) {
        this.kBonusSumWord = kBonusSumWord;
    }

    public float getKCashOnHand() {
        return kCashOnHand;
    }

    public void setKCashOnHand(float kCashOnHand) {
        this.kCashOnHand = kCashOnHand;
    }

    public String getKCashOnHandWord() {
        return kCashOnHandWord;
    }

    public void setKCashOnHandWord(String kCashOnHandWord) {
        this.kCashOnHandWord = kCashOnHandWord;
    }

    public float getKRedSwanTotal() {
        return kRedSwanTotal;
    }

    public void setKRedSwanTotal(float kRedSwanTotal) {
        this.kRedSwanTotal = kRedSwanTotal;
    }

    public String getKRedSwanTotalWord() {
        return kRedSwanTotalWord;
    }

    public void setKRedSwanTotalWord(String kRedSwanTotalWord) {
        this.kRedSwanTotalWord = kRedSwanTotalWord;
    }

    public int getKIsAfterPenaltyTime() {
        return kIsAfterPenaltyTime;
    }

    public void setKIsAfterPenaltyTime(int kIsAfterPenaltyTime) {
        this.kIsAfterPenaltyTime = kIsAfterPenaltyTime;
    }

    public String getKMemberPlanContractPDF() {
        return kMemberPlanContractPDF;
    }

    public void setKMemberPlanContractPDF(String kMemberPlanContractPDF) {
        this.kMemberPlanContractPDF = kMemberPlanContractPDF;
    }


    public Object deepClone() {
        // 将对象写到流里
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        try {
            ObjectOutputStream oo = new ObjectOutputStream(bo);
            oo.writeObject(this);
            // 从流里读出来
            ByteArrayInputStream bi = new ByteArrayInputStream(bo.toByteArray());
            ObjectInputStream oi = new ObjectInputStream(bi);
            return (oi.readObject());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public String toString() {
        return "Member{" +
                "memberID=" + memberID +
                ", memberName='" + memberName + '\'' +
                ", selfIntroduction='" + selfIntroduction + '\'' +
                ", gitDBcode='" + gitDBcode + '\'' +
                ", typeDBCode='" + typeDBCode + '\'' +
                ", speDBCode='" + speDBCode + '\'' +
                ", flagStockPush=" + flagStockPush +
                ", flagTraderPush=" + flagTraderPush +
                ", flagConsultPush=" + flagConsultPush +
                ", flagIsConfirmed=" + flagIsConfirmed +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", isRemember=" + isRemember +
                ", avatarsAddress='" + avatarsAddress + '\'' +
                ", password='" + password + '\'' +
                ", typeDescription='" + typeDescription + '\'' +
                ", speDescription='" + speDescription + '\'' +
                ", timeDescription='" + timeDescription + '\'' +
                ", memberClass=" + memberClass +
                ", authorization='" + authorization + '\'' +
                ", kmemberKey=" + kmemberKey +
                ", kMemberPlan=" + kMemberPlan +
                ", kMemberPlanName='" + kMemberPlanName + '\'' +
                ", kFlagActivate=" + kFlagActivate +
                ", kFlagLock=" + kFlagLock +
                ", kFlagMature=" + kFlagMature +
                ", kFlagDeleted=" + kFlagDeleted +
                ", kLastWDCashStatus=" + kLastWDCashStatus +
                ", kLastWDCapiatlStatus=" + kLastWDCapiatlStatus +
                ", kMemberPlanActivateTime='" + kMemberPlanActivateTime + '\'' +
                ", kMemberPlanEndTime='" + kMemberPlanEndTime + '\'' +
                ", kCapital=" + kCapital +
                ", kCapitalWord='" + kCapitalWord + '\'' +
                ", kBonusLeft=" + kBonusLeft +
                ", kBonusLeftWord='" + kBonusLeftWord + '\'' +
                ", kBonusSum=" + kBonusSum +
                ", kBonusSumWord='" + kBonusSumWord + '\'' +
                ", kCashOnHand=" + kCashOnHand +
                ", kCashOnHandWord='" + kCashOnHandWord + '\'' +
                ", kRedSwanTotal=" + kRedSwanTotal +
                ", kRedSwanTotalWord='" + kRedSwanTotalWord + '\'' +
                ", kIsAfterPenaltyTime=" + kIsAfterPenaltyTime +
                ", kMemberPlanContractPDF='" + kMemberPlanContractPDF + '\'' +
                '}';
    }
}
