package com.financialnet.model;

import java.io.Serializable;

import com.simple.util.db.annotation.SimpleColumn;
import com.simple.util.db.annotation.SimpleTable;

@SimpleTable(name = "t_databook")
public class DataBook implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SimpleColumn(name = "id")
	private String id;
	@SimpleColumn(name = "type")
	private String type;
	@SimpleColumn(name = "dbCode")
	private String dbCode;
	@SimpleColumn(name = "value")
	private String value;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDbCode() {
		return dbCode;
	}

	public void setDbCode(String dbCode) {
		this.dbCode = dbCode;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "DataBook [id=" + id + ", type=" + type + ", dbCode=" + dbCode
				+ ", value=" + value + "]";
	}
}
