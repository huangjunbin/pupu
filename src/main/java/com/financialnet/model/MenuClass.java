/**   
 * @Title: FirstType.java 
 * @Package com.imoda.shedian.model 
 * @Description: TODO
 * @author BaoHang baohang2011@gmail.com   
 * @date 2014年6月2日 下午1:05:09 
 * @version V1.0   
 */
package com.financialnet.model;

import java.io.Serializable;

/**
 * @ClassName: FirstType
 * @Description: TODO
 * @author BaoHang baohang2011@gmail.com
 * @date 2014年6月2日 下午1:05:09
 */
public class MenuClass implements Serializable {

	private static final long serialVersionUID = 1L;
	private String acskey;// Id
	private String cat_name;// 分类名称
	private String parent_id;// 上级id
	private int num = 0;// 图标图片路径

	public String getAcskey() {
		return acskey;
	}

	public void setAcskey(String acskey) {
		this.acskey = acskey;
	}

	public String getCat_name() {
		return cat_name;
	}

	public void setCat_name(String cat_name) {
		this.cat_name = cat_name;
	}

	public String getParent_id() {
		return parent_id;
	}

	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	@Override
	public String toString() {
		return "MenuClass [acskey=" + acskey + ", cat_name=" + cat_name
				+ ", parent_id=" + parent_id + ", num=" + num + "]";
	}

}
