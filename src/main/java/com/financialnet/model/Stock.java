package com.financialnet.model;

import java.io.Serializable;

/**
 * 股票
 * 
 * @author allen
 * @time 2014-8-18
 */
public class Stock implements Serializable {
	private static final long serialVersionUID = -2410617453519563883L;
	private String stockCode;// 股票编号
	private String stockName;// 股票名称
	private String stopPrice;// 成交价/昨收
	private String quantity;// 数量
	private String earningsMoney;// 收益
	private String earningPercentage;// 收益百分比
	private String tradeTime;// 交易时间
	private String certificateAddr;// 股票图片地址
	private String comments;// 评论
	private String targetPrice;// 股票买入价
	private String tagetPrice;
	private String marketCode;// 市场编号
	private String price;// 买入、卖出价
	private int flagTrade;// 0买入，1卖出
	private String flagAuthentication;// 是否认证：0否，1是
	private String marketID;// 市场Id
	private String stockPrice;// 股票现价
	private String lowPrice;// 最低价
	private String upPrice;// 最高阶
	private String percentage;// 升跌百分比
	private String nowPrice;// 现价
	private String startPrice;// 今开
	private String upOrDown;// 升跌
	private String volume;// 成交量
	private String totalTurnover;// 成交额
	private String imageUrl;// K线图
	private String createDate;// 成交时间
	private String traderName;// 操盘手名称
	private String traderId;// 操盘手id
	private String costPrice;// 成本价
	private int belongroup;// 所属分组
	private String createTime;
	private String tagetSale;
	
	private String stockPhotoAddr;
	private String serialNumber;
    private String tradeTimeSample;
    private String chartResultUrl;
	private String avatarsAddress;
	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getStopPrice() {
		return stopPrice;
	}

	public void setStopPrice(String stopPrice) {
		this.stopPrice = stopPrice;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getEarningsMoney() {
		return earningsMoney;
	}

	public void setEarningsMoney(String earningsMoney) {
		this.earningsMoney = earningsMoney;
	}

	public String getEarningPercentage() {
		return earningPercentage;
	}

	public void setEarningPercentage(String earningPercentage) {
		this.earningPercentage = earningPercentage;
	}

	public String getTradeTime() {
		return tradeTime;
	}

	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}

	public String getCertificateAddr() {
		return certificateAddr;
	}

	public void setCertificateAddr(String certificateAddr) {
		this.certificateAddr = certificateAddr;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getTargetPrice() {
		return targetPrice;
	}

	public void setTargetPrice(String targetPrice) {
		this.targetPrice = targetPrice;
	}

	public String getMarketCode() {
		return marketCode;
	}

	public void setMarketCode(String marketCode) {
		this.marketCode = marketCode;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public int getFlagTrade() {
		return flagTrade;
	}

	public void setFlagTrade(int flagTrade) {
		this.flagTrade = flagTrade;
	}

	public String getFlagAuthentication() {
		return flagAuthentication;
	}

	public void setFlagAuthentication(String flagAuthentication) {
		this.flagAuthentication = flagAuthentication;
	}

	public String getMarketID() {
		return marketID;
	}

	public void setMarketID(String marketID) {
		this.marketID = marketID;
	}

	public String getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(String stockPrice) {
		this.stockPrice = stockPrice;
	}

	public String getLowPrice() {
		return lowPrice;
	}

	public void setLowPrice(String lowPrice) {
		this.lowPrice = lowPrice;
	}

	public String getUpPrice() {
		return upPrice;
	}

	public void setUpPrice(String upPrice) {
		this.upPrice = upPrice;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public String getNowPrice() {
		return nowPrice;
	}

	public void setNowPrice(String nowPrice) {
		this.nowPrice = nowPrice;
	}

	public String getStartPrice() {
		return startPrice;
	}

	public void setStartPrice(String startPrice) {
		this.startPrice = startPrice;
	}

	public String getUpOrDown() {
		return upOrDown;
	}

	public void setUpOrDown(String upOrDown) {
		this.upOrDown = upOrDown;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getTotalTurnover() {
		return totalTurnover;
	}

	public void setTotalTurnover(String totalTurnover) {
		this.totalTurnover = totalTurnover;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getTraderName() {
		return traderName;
	}

	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}

	public String getTraderId() {
		return traderId;
	}

	public void setTraderId(String traderId) {
		this.traderId = traderId;
	}

	public String getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}

	public int getBelongroup() {
		return belongroup;
	}

	public void setBelongroup(int belongroup) {
		this.belongroup = belongroup;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getIconFlag() {
		return iconFlag;
	}

	public void setIconFlag(int iconFlag) {
		this.iconFlag = iconFlag;
	}

	public String getStockCase() {
		return StockCase;
	}

	public void setStockCase(String stockCase) {
		StockCase = stockCase;
	}

	public String getProfit() {
		return profit;
	}

	public void setProfit(String profit) {
		this.profit = profit;
	}

	public String getStockDes() {
		return stockDes;
	}

	public void setStockDes(String stockDes) {
		this.stockDes = stockDes;
	}

	public String getBuyPricae() {
		return buyPricae;
	}

	public void setBuyPricae(String buyPricae) {
		this.buyPricae = buyPricae;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getNewPrice() {
		return newPrice;
	}

	public void setNewPrice(String newPrice) {
		this.newPrice = newPrice;
	}

	public String getLowerEst() {
		return lowerEst;
	}

	public void setLowerEst(String lowerEst) {
		this.lowerEst = lowerEst;
	}

	public String getHigher() {
		return higher;
	}

	public void setHigher(String higher) {
		this.higher = higher;
	}

	public String getRiseFall() {
		return riseFall;
	}

	public void setRiseFall(String riseFall) {
		this.riseFall = riseFall;
	}

	public String getStockHoldeName() {
		return stockHoldeName;
	}

	public void setStockHoldeName(String stockHoldeName) {
		this.stockHoldeName = stockHoldeName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTagetPrice() {
		return tagetPrice;
	}

	public void setTagetPrice(String tagetPrice) {
		this.tagetPrice = tagetPrice;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	// --------------------------------------
	private String id;// 股票id
	private String name;// 股票名称
	private String date;// 时间

	// 过往交易；现时持仓
	private int iconFlag;// 1:黄色；其它灰色
	private String StockCase;// 股票的出入情况
	private String profit;// 收益

	// 今日关注
	private String stockDes;// 股票描述
	private String buyPricae;// 股票买入价

	private String index;

	//
	private String content;

	// 我的股票
	private String newPrice;// 现价
	private String lowerEst;// 最低
	private String higher;// 最高
	private String riseFall;// 升跌

	// 股票最新动态
	private String stockHoldeName;

	public String getTagetSale() {
		return tagetSale;
	}

	public void setTagetSale(String tagetSale) {
		this.tagetSale = tagetSale;
	}

	public String getStockPhotoAddr() {
		return stockPhotoAddr;
	}

	public void setStockPhotoAddr(String stockPhotoAddr) {
		this.stockPhotoAddr = stockPhotoAddr;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

    public String getTradeTimeSample() {
        return tradeTimeSample;
    }

    public void setTradeTimeSample(String tradeTimeSample) {
        this.tradeTimeSample = tradeTimeSample;
    }

    public String getChartResultUrl() {
        return chartResultUrl;
    }

    public void setChartResultUrl(String chartResultUrl) {
        this.chartResultUrl = chartResultUrl;
    }

	public String getAvatarsAddress() {
		return avatarsAddress;
	}

	public void setAvatarsAddress(String avatarsAddress) {
		this.avatarsAddress = avatarsAddress;
	}

	@Override
	public String toString() {
		return "Stock{" +
				"stockCode='" + stockCode + '\'' +
				", stockName='" + stockName + '\'' +
				", stopPrice='" + stopPrice + '\'' +
				", quantity='" + quantity + '\'' +
				", earningsMoney='" + earningsMoney + '\'' +
				", earningPercentage='" + earningPercentage + '\'' +
				", tradeTime='" + tradeTime + '\'' +
				", certificateAddr='" + certificateAddr + '\'' +
				", comments='" + comments + '\'' +
				", targetPrice='" + targetPrice + '\'' +
				", tagetPrice='" + tagetPrice + '\'' +
				", marketCode='" + marketCode + '\'' +
				", price='" + price + '\'' +
				", flagTrade=" + flagTrade +
				", flagAuthentication='" + flagAuthentication + '\'' +
				", marketID='" + marketID + '\'' +
				", stockPrice='" + stockPrice + '\'' +
				", lowPrice='" + lowPrice + '\'' +
				", upPrice='" + upPrice + '\'' +
				", percentage='" + percentage + '\'' +
				", nowPrice='" + nowPrice + '\'' +
				", startPrice='" + startPrice + '\'' +
				", upOrDown='" + upOrDown + '\'' +
				", volume='" + volume + '\'' +
				", totalTurnover='" + totalTurnover + '\'' +
				", imageUrl='" + imageUrl + '\'' +
				", createDate='" + createDate + '\'' +
				", traderName='" + traderName + '\'' +
				", traderId='" + traderId + '\'' +
				", costPrice='" + costPrice + '\'' +
				", belongroup=" + belongroup +
				", createTime='" + createTime + '\'' +
				", tagetSale='" + tagetSale + '\'' +
				", stockPhotoAddr='" + stockPhotoAddr + '\'' +
				", serialNumber='" + serialNumber + '\'' +
				", tradeTimeSample='" + tradeTimeSample + '\'' +
				", chartResultUrl='" + chartResultUrl + '\'' +
				", avatarsAddress='" + avatarsAddress + '\'' +
				", id='" + id + '\'' +
				", name='" + name + '\'' +
				", date='" + date + '\'' +
				", iconFlag=" + iconFlag +
				", StockCase='" + StockCase + '\'' +
				", profit='" + profit + '\'' +
				", stockDes='" + stockDes + '\'' +
				", buyPricae='" + buyPricae + '\'' +
				", index='" + index + '\'' +
				", content='" + content + '\'' +
				", newPrice='" + newPrice + '\'' +
				", lowerEst='" + lowerEst + '\'' +
				", higher='" + higher + '\'' +
				", riseFall='" + riseFall + '\'' +
				", stockHoldeName='" + stockHoldeName + '\'' +
				'}';
	}
}
