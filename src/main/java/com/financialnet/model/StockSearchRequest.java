package com.financialnet.model;

/**
 * @className：StockSearchRequest.java
 * @author: lmt
 * @Function:
 * @createDate: 2014-10-17 下午3:08:44
 * @update:
 */
public class StockSearchRequest {
	private String stockCode;
	private int pageNumber;
	private int pageSize;

	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	@Override
	public String toString() {
		return "StockSearchRequest [stockCode=" + stockCode + ", pageNumber="
				+ pageNumber + ", pageSize=" + pageSize + "]";
	}
}
