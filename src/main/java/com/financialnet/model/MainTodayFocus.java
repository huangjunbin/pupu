package com.financialnet.model;

import java.io.Serializable;

/**
 * @className：MainTodayFocus.java
 * @author: allen
 * @Function: 主页今日关注
 * @createDate: 2014-9-1
 * @update:
 */
public class MainTodayFocus implements Serializable {

	private static final long serialVersionUID = -1323841156109896797L;
	private String name;
	private String content;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "MainTodayFocus [name=" + name + ", content=" + content + "]";
	}

}