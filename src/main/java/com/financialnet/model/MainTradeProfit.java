package com.financialnet.model;

import java.util.List;

/**
 * @className：MainTradeProfit.java
 * @author: allen
 * @Function: 主页操盘手收益列表
 * @createDate: 2014-9-1
 * @update:
 */
public class MainTradeProfit {
	private String profit;
	private List<Trade> tradeList;

	public String getProfit() {
		return profit;
	}

	public void setProfit(String profit) {
		this.profit = profit;
	}

	public List<Trade> getTradeList() {
		return tradeList;
	}

	public void setTradeList(List<Trade> tradeList) {
		this.tradeList = tradeList;
	}

	@Override
	public String toString() {
		return "MainTradeProfit [profit=" + profit + ", tradeList=" + tradeList
				+ "]";
	}

}
