package com.financialnet.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.JsResult;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.financialnet.app.R;

public class ProgressWebView extends WebView {

	private ProgressBar progressbar;
	private Context context;

	@SuppressWarnings("deprecation")
	public ProgressWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		progressbar = new ProgressBar(context, null,
				android.R.attr.progressBarStyleHorizontal);
		progressbar.setIndeterminate(false);
		progressbar.setProgressDrawable(getResources().getDrawable(
				R.drawable.progressbar_mini));
		progressbar.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
				4, 0, 0));
		addView(progressbar);
		setWebChromeClient(new WebChromeClient());
	}

	public class WebChromeClient extends android.webkit.WebChromeClient {
		@Override
		public void onProgressChanged(WebView view, int newProgress) {
			if (newProgress == 100) {
				progressbar.setVisibility(GONE);
			} else {
				if (progressbar.getVisibility() == GONE)
					progressbar.setVisibility(VISIBLE);
				progressbar.setProgress(newProgress);
			}
			super.onProgressChanged(view, newProgress);
		}

		@Override
		public boolean onJsAlert(WebView view, String url, String message,
				JsResult result) {
			Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
			result.cancel();
			return true;
		}

	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		LayoutParams lp = (LayoutParams) progressbar.getLayoutParams();
		lp.x = l;
		lp.y = t;
		progressbar.setLayoutParams(lp);
		super.onScrollChanged(l, t, oldl, oldt);
	}

}
