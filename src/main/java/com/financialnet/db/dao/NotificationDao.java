package com.financialnet.db.dao;

import android.content.Context;

import com.financialnet.db.DBHelper;
import com.financialnet.model.Notification;
import com.simple.util.db.operation.TemplateDAO;

import java.util.ArrayList;
import java.util.List;

public class NotificationDao extends TemplateDAO<Notification> {

    public NotificationDao(Context context) {
        super(new DBHelper(context));
    }

    public void setNotification(List<Notification> notificationList) {

        List<Notification> lists = new ArrayList<Notification>();
        if (notificationList != null) {
            for (Notification notification : notificationList) {
                Notification item = get(notification.getId());
                if (item == null  ) {

                    lists.add(notification);
                }
            }
        }

        for (Notification notification : lists) {
            insert(notification);
        }
    }

    public boolean hasNotRead() {
        List<Notification> lists = find(
                null,
                "state= ?",
                new String[]{"0"},
                null, null, null, null);
        if (lists != null && lists.size() > 0) {
            return true;
        }
        {
            return false;
        }
    }

    public   List<Notification> findAll() {
        List<Notification> lists = find(
                null,
                null,
                null,
                null, null, "createdTime desc", null);
        if (lists != null && lists.size() > 0) {
            return lists;
        }
       else  {
            return new ArrayList<>();
        }
    }
}
