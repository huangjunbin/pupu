package com.financialnet.db.dao;

import java.util.List;

import android.content.Context;

import com.financialnet.db.DBHelper;
import com.financialnet.model.Member;
import com.simple.util.db.operation.TemplateDAO;

/**
 * MemberDao.java
 * 
 * @author: allen
 * @Function: 会员DAo操作记录
 * @createDate: 2014-8-29
 * @update:
 */
public class MemberDao extends TemplateDAO<Member> {

	public MemberDao(Context context) {
		super(new DBHelper(context));
	}

	private List<Member> memberList;

	public Member getMemberByMoblie(String mobileNumber) {
		memberList = find(null, "mobileNumber= ?",
				new String[] { mobileNumber }, null, null, null, null);

		if (memberList != null && memberList.size() > 0) {
			return memberList.get(0);
		} else {
			return null;
		}
	}

	public Member getMemberByDefault() {
		memberList = find();
		if (memberList != null && memberList.size() > 0) {
			return memberList.get(0);
		} else {
			return null;
		}
	}
}
