package com.financialnet.db.dao;

import java.util.List;

import android.content.Context;
import android.util.Log;

import com.financialnet.db.DBHelper;
import com.financialnet.model.DataBook;
import com.simple.util.db.operation.TemplateDAO;

public class DataBookDao extends TemplateDAO<DataBook> {
	private String TAG = "DataBookDao";
	public static final int INVESTTYPE_TAG = 2;// 投资类型
	public static final int INVESTTIME_TAG = 1;// 投资时间类型
	public static final int INVESTSPE_TAG = 0;// 投资专项类型(投资项目)

	private List<DataBook> dataBookList;

	// private List<DataBook> investTypeList;
	// private List<DataBook> investTimeList;
	// private List<DataBook> investSpeList;

	public DataBookDao(Context context) {
		super(new DBHelper(context));
	}

	/**
	 * 获取数据
	 * 
	 * @return
	 */
	public List<DataBook> getDataBook() {
		dataBookList = find();
		if (dataBookList != null && dataBookList.size() > 0) {
			return dataBookList;
		} else {
			return null;
		}
	}

	/**
	 * 获取某一类型的数据字典
	 * 
	 * @param type
	 *            类型
	 * @return 某一类型的数据字典
	 */
	public List<DataBook> getDataBookByType(int type) {
		dataBookList = find(null, "type= ?", new String[] { type + "" }, null,
				null, null, null);
		if (dataBookList != null && dataBookList.size() > 0) {
			return dataBookList;
		} else {
			return null;
		}
	}

	/**
	 * 更具DbCode查找指定数据字典中的值
	 * 
	 * @param list
	 *            指定数据字典
	 * @param dbCode
	 *            标示
	 * @return
	 */
	public String getValueByDbCode(int type, String dbCode) {
		List<DataBook> temp = getDataBookByType(type);
		if (temp != null && temp.size() > 0) {
			for (DataBook db : temp) {
				if (dbCode.equals(db.getDbCode())) {
					return db.getValue();
				}
			}
		}
		return null;
	}

	/**
	 * 插入数据字典
	 * 
	 * @param entitys
	 */
	public void insert(List<DataBook> entitys) {
		Log.d(TAG, entitys.size() + "________");
		// TODO Auto-generated method stub
		if (entitys != null && entitys.size() > 0) {
			for (DataBook db : entitys) {
				super.insert(db);
				Log.d(TAG, "___________");
			}
		}
	}
}
