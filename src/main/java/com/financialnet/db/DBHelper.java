package com.financialnet.db;

import android.content.Context;

import com.financialnet.model.DataBook;
import com.financialnet.model.Member;
import com.financialnet.model.Notification;
import com.simple.util.db.operation.SimpleDbHelper;

public class DBHelper extends SimpleDbHelper {

	private static final String DBNAME = "FinancialNet.db";
	private static final int DBVERSION = 17;
	private static final Class<?>[] clazz = { Member.class, DataBook.class,
			Notification.class };

	public DBHelper(Context context) {
		super(context, DBNAME, null, DBVERSION, clazz);
	}
}