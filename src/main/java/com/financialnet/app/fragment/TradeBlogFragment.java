package com.financialnet.app.fragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONException;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.TradeBlogAdapter;
import com.financialnet.app.ui.TradeDetailActivity;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.TradeBlog;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.TraderService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.JsonUtil;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;

/**
 * @className: TradePassTransactionFragment.java
 * @author: limingtao
 * @function: 过往交易，今日关注，现时持仓
 * @date: 2013年9月24日下午4:11:44
 * @update:
 */
@SuppressLint("InflateParams")
public class TradeBlogFragment extends BaseFragment implements
		IXListViewListener {

	public static TradeBlogFragment newInstance(int index) {
		TradeBlogFragment frag = new TradeBlogFragment();
		return frag;
	}

	private TextView tvNoData;
	private XListView xListView;
	private TradeBlogAdapter adapter;
	private List<TradeBlog> blogList;
	private int pageNumber = 0;
	private int pageCurrentSize = 10;
	private TradeDetailActivity detailActivity;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater
				.inflate(R.layout.fragment_trade_blog, container, false);
		detailActivity = (TradeDetailActivity) getActivity();
		return mView;
	}

	@Override
	protected void initData() {
		blogList = new ArrayList<TradeBlog>();
		adapter = new TradeBlogAdapter(detailActivity, blogList,
				R.drawable.news_ph_tn_default);
		xListView.setAdapter(adapter);
	}

	@Override
	protected void initView() {
		xListView = findViewWithId(R.id.trade_blog_list);
		tvNoData = findViewWithId(R.id.trade_empty_view);
	}

	@Override
	protected void bindView() {
		xListView.setXListViewListener(this);
		onRefresh();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onRefresh() {
		xListView.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		pageCurrentSize =10;
		getBlogList(AppContext.REFRESH);

	}

	@Override
	public void onLoadMore() {

		if (blogList.size() >= pageCurrentSize) {
			pageCurrentSize += 10;
		}
		getBlogList(AppContext.LOADMORE);

	}

	/**
	 * 获取股票列表
	 */
	public void getBlogList(final int state) {
		TraderService.getInstance(context).getTradeBlog(
				detailActivity.tempTrade.getTraderId(), pageNumber + "",
				pageCurrentSize + "", new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							try {
								String result = baseModel.getDataResult()
										.getString("result");
								int totalCount = baseModel.getDataResult()
										.getInt("totalCount");
								if (totalCount == 0) {
									tvNoData.setVisibility(View.VISIBLE);
									xListView.setVisibility(View.GONE);
									return;
								}
								if (result != null) {
									List<TradeBlog> list = JsonUtil
											.convertJsonToList(result,
													TradeBlog.class);
									if (list != null && list.size() > 0) {
										blogList.clear();
										blogList.addAll(list);
										adapter.notifyDataSetChanged();

										if (blogList.size() >= totalCount) {
											xListView.setPullLoadEnable(false);
										} else {
											xListView.setPullLoadEnable(true);
										}

									}
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}

						}
					}

					@Override
					public void onFinish() {
						super.onFinish();
						xListView.stopRefresh();
						xListView.stopLoadMore();
					}
				});
	}

}