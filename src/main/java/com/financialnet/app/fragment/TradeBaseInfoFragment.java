package com.financialnet.app.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.TradeDetailActivity;
import com.financialnet.app.ui.TraderDiscussActivity;
import com.financialnet.app.ui.WebViewActivity;
import com.financialnet.widget.CircleImageView;

/**
 * @TradeBaseInfoFragment.java
 * @author li mingtao
 * @function :操盘手的基本资料页面
 * @2013-9-3@下午4:28:04
 * @update:
 */
@SuppressLint("HandlerLeak")
public class TradeBaseInfoFragment extends BaseFragment {
	public static final int UPDATE_VIEW = 1;// 更新页面
	// -------------
	private TextView textName, txtTradeMotto;
	private RatingBar rtTraderStart;
	private View viewAppraiseArea, viewWeb;
	private TextView txtTraderAppraise, txtTraderInvestType,
			txtTraderInvestTimeType, txtTraderInvestMajorType,
			txtTraderInvestMent, txtTraderSevenDays, txtTraderMounths,
			txtTraderWinRate, txtTraderStalker;
	private TradeDetailActivity detailActivity;
	private CircleImageView imgTraderPhoto;

	public static TradeBaseInfoFragment newInstance(int index) {
		TradeBaseInfoFragment frag = new TradeBaseInfoFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("index", index);
		frag.setArguments(bundle);
		return frag;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		detailActivity = (TradeDetailActivity) getActivity();
		mIndex = getArguments() != null ? getArguments().getInt("index") : 1;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.fragment_trade_baseinfo, container,
				false);
		return mView;
	}

	@Override
	protected void initData() {

	}

	@Override
	protected void initView() {
		textName = (TextView) mView.findViewById(R.id.trade_name_);
		mView.findViewById(R.id.trader_motto_).setVisibility(View.GONE);
		txtTradeMotto = (TextView) mView.findViewById(R.id.trader_motto_2);
		rtTraderStart = (RatingBar) mView.findViewById(R.id.trader_ratebar);
		rtTraderStart.setMax(10);
		viewAppraiseArea = findViewWithId(R.id.appraise_area);
		viewWeb = findViewWithId(R.id.appraise_web);
		imgTraderPhoto = (CircleImageView) mView
				.findViewById(R.id.mine_memberheader);
		imgTraderPhoto.setBackgroundResource(R.drawable.pro_ph_mask_default);
		txtTraderAppraise = (TextView) mView.findViewById(R.id.trader_appraise);
		txtTraderInvestType = (TextView) mView
				.findViewById(R.id.trader_investType);
		txtTraderInvestTimeType = (TextView) mView
				.findViewById(R.id.trader_investtimetype);
		txtTraderInvestMajorType = (TextView) mView
				.findViewById(R.id.trader_investMajor);
		txtTraderInvestMent = (TextView) mView
				.findViewById(R.id.trader_investMent);
		txtTraderSevenDays = (TextView) mView
				.findViewById(R.id.trader_sevenDays);
		txtTraderMounths = (TextView) mView.findViewById(R.id.trader_mounths);
		txtTraderWinRate = (TextView) mView.findViewById(R.id.trader_winRate);
		txtTraderStalker = (TextView) mView.findViewById(R.id.trader_stalker);
	}

	@Override
	protected void bindView() {

		updateView();

		viewAppraiseArea.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(),
						TraderDiscussActivity.class);
				startActivity(intent);
			}
		});
		viewWeb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), WebViewActivity.class);
				intent.putExtra("title",
						TradeDetailActivity.currentTrader.getTraderName()
								+ "个人网页");
				intent.putExtra("url",
						TradeDetailActivity.currentTrader.getHomePage());
				startActivity(intent);
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		updateView();
	}

	private void updateView() {
		if (detailActivity.tempTrade != null) {
			textName.setText(detailActivity.tempTrade.getTraderName());
		}
		if (TradeDetailActivity.currentTrader != null) {
			txtTraderAppraise.setText(TradeDetailActivity.currentTrader
					.getComment());

			txtTraderInvestType.setText(TradeDetailActivity.currentTrader
					.getInvestType());
			txtTraderInvestTimeType.setText(TradeDetailActivity.currentTrader
					.getInvestTime());
			txtTraderInvestMajorType.setText(TradeDetailActivity.currentTrader
					.getInvestSpeciality());

			StringBuffer intentMent = new StringBuffer();
			intentMent.append(TradeDetailActivity.currentTrader
					.getEarningsMoney());
			intentMent.append("("
					+ TradeDetailActivity.currentTrader.getEarningPercentage()
					+ ")");

			txtTraderInvestMent.setText(intentMent.toString());

			// 周收益
			txtTraderSevenDays.setText(TradeDetailActivity.currentTrader
					.getDay());

			// 月收益
			txtTraderMounths.setText(TradeDetailActivity.currentTrader
					.getMonth());

			// 胜率
			txtTraderWinRate.setText(TradeDetailActivity.currentTrader
					.getOdds());

			// 跟踪者
			txtTraderStalker.setText(TradeDetailActivity.currentTrader
					.getStalker());

			// 操盘手星级
			try {
				float scroe = Float.valueOf(TradeDetailActivity.currentTrader
						.getScore());
				rtTraderStart.setProgress((int) scroe);
			} catch (Exception e) {
				rtTraderStart.setProgress(0);
			}

			// 金句
			txtTradeMotto.setText(TradeDetailActivity.currentTrader
					.getSelfIntroduction());

			AppContext.setImageNo(
					TradeDetailActivity.currentTrader.getLogoUrl(),
					imgTraderPhoto, AppContext.memberPhotoOption);
		}
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case UPDATE_VIEW:
				updateView();
				break;
			}
		}
	};
}