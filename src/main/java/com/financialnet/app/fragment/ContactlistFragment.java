/**
 * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.financialnet.app.fragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.easemob.chat.EMContactManager;
import com.easemob.exceptions.EaseMobException;
import com.financialnet.app.AppConfig;
import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.ContactAdapter;
import com.financialnet.app.ui.AddContactActivity;
import com.financialnet.app.ui.ChatActivity;
import com.financialnet.app.ui.NewFriendsMsgActivity;
import com.financialnet.db.InviteMessgeDao;
import com.financialnet.model.Member;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.User;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.IMService;
import com.financialnet.util.DateUtil;
import com.financialnet.widget.Sidebar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;

/**
 * 联系人列表页
 * 
 */
public class ContactlistFragment extends Fragment {
	private String TAG = "ContactlistFragment";
	private ContactAdapter adapter;
	private List<User> contactList;
	private XListView listView;
	private boolean hidden;
	private Sidebar sidebar;
	private InputMethodManager inputMethodManager;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_contact_list, container,
				false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		inputMethodManager = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN|WindowManager.LayoutParams.SOFT_INPUT_MASK_ADJUST);
		listView = (XListView) getView().findViewById(R.id.list);
		sidebar = (Sidebar) getView().findViewById(R.id.sidebar);
		sidebar.setListView(listView);
		contactList = new ArrayList<User>();
		
		// 设置adapter
		adapter = new ContactAdapter(getActivity(), R.layout.row_contact,
				contactList, sidebar);
		listView.setAdapter(adapter);
		refreshContactList();
		getView().findViewById(R.id.btn_top_back).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						getActivity().finish();
					}
				});
		listView.setPullLoadEnable(false);
		listView.setFooterDividersEnabled(false);
		listView.setXListViewListener(new IXListViewListener() {

			@Override
			public void onRefresh() {
				listView.setRefreshTime(DateUtil.getDateTime(new Date(System
						.currentTimeMillis())));
				refreshContactList();

			}

			@Override
			public void onLoadMore() {

			}
		});
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String username = adapter.getItem(position - 1).getUsername();
				if (AppConfig.NEW_FRIENDS_USERNAME.equals(username)) {
					// TODO进入申请与通知页面
					User user = AppContext.getApplication().getContactList()
							.get(AppConfig.NEW_FRIENDS_USERNAME);
					user.setUnreadMsgCount(0);
					startActivity(new Intent(getActivity(),
							NewFriendsMsgActivity.class));
				} else {

					// demo中直接进入聊天页面，实际一般是进入用户详情页
					startActivity(new Intent(getActivity(), ChatActivity.class)
							.putExtra("userId",
									adapter.getItem(position - 1).getId())
							.putExtra("userName",
									adapter.getItem(position - 1).getUsername()));

				}
			}
		});
		listView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// 隐藏软键盘
				if (getActivity().getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
					if (getActivity().getCurrentFocus() != null)
						inputMethodManager.hideSoftInputFromWindow(
								getActivity().getCurrentFocus()
										.getWindowToken(),
								InputMethodManager.HIDE_NOT_ALWAYS);
				}
				return false;
			}
		});

		ImageView addContactView = (ImageView) getView().findViewById(
				R.id.iv_new_contact);
		// 进入添加好友页
		addContactView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(),
						AddContactActivity.class));
			}
		});
		registerForContextMenu(listView);

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		// 长按前两个不弹menu
		if (((AdapterContextMenuInfo) menuInfo).position > 2) {
			getActivity().getMenuInflater().inflate(
					R.menu.context_contact_list, menu);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.delete_contact) {
			User tobeDeleteUser = adapter
					.getItem(((AdapterContextMenuInfo) item.getMenuInfo()).position - 1);
			// 删除此联系人
			deleteContact(tobeDeleteUser);
			// 删除相关的邀请消息
			InviteMessgeDao dao = new InviteMessgeDao(getActivity());
			dao.deleteMessage(tobeDeleteUser.getUsername());
			return true;
		} /*
		 * else if (item.getItemId() == R.id.add_to_blacklist) { User user =
		 * adapter.getItem(((AdapterContextMenuInfo) item
		 * .getMenuInfo()).position); moveToBlacklist(user.getUsername());
		 * return true; }
		 */
		return super.onContextItemSelected(item);
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		this.hidden = hidden;
		if (!hidden) {
			refresh();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!hidden) {
			refresh();
		}
	}

	/**
	 * 删除联系人
	 * 
	 * @param toDeleteUser
	 */
	public void deleteContact(final User tobeDeleteUser) {
		final ProgressDialog pd = new ProgressDialog(getActivity());
		pd.setMessage("正在删除...");
		pd.setCanceledOnTouchOutside(false);
		pd.show();
		new Thread(new Runnable() {
			public void run() {
				try {
					EMContactManager.getInstance().deleteContact(
							tobeDeleteUser.getId());
					// 删除db和内存中此用户的数据

					AppContext.getApplication().getContactList()
							.remove(tobeDeleteUser.getId());
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							pd.dismiss();
							adapter.remove(tobeDeleteUser);
							adapter.notifyDataSetChanged();

						}
					});
				} catch (final Exception e) {
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							pd.dismiss();
							Toast.makeText(getActivity(),
									"删除失败: " + e.getMessage(), 1).show();
						}
					});

				}

			}
		}).start();

	}

	/**
	 * 把user移入到黑名单
	 */
	private void moveToBlacklist(final String username) {
		final ProgressDialog pd = new ProgressDialog(getActivity());
		pd.setMessage("正在移入黑名单...");
		pd.setCanceledOnTouchOutside(false);
		pd.show();
		new Thread(new Runnable() {
			public void run() {
				try {
					// 加入到黑名单
					EMContactManager.getInstance().addUserToBlackList(username,
							true);
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							pd.dismiss();
							Toast.makeText(getActivity(), "移入黑名单成功", 0).show();
						}
					});
				} catch (EaseMobException e) {
					e.printStackTrace();
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							pd.dismiss();
							Toast.makeText(getActivity(), "移入黑名单失败", 0).show();
						}
					});
				}
			}
		}).start();

	}

	// 刷新ui
	public void refresh() {
		try {
			// 可能会在子线程中调到这方法
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					refreshContactList();
					adapter.notifyDataSetChanged();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getContactList() {
		contactList.clear();
		Map<String, User> users = AppContext.getApplication().getContactList();
		Iterator<Entry<String, User>> iterator = users.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, User> entry = iterator.next();
			if (!entry.getKey().equals(AppConfig.NEW_FRIENDS_USERNAME)
					&& !entry.getKey().equals(AppConfig.GROUP_USERNAME))
				contactList.add(entry.getValue());
		}
		// 排序
		Collections.sort(contactList, new Comparator<User>() {

			@Override
			public int compare(User lhs, User rhs) {
				return lhs.getUsername().compareToIgnoreCase(rhs.getUsername());
			}
		});

		// 加入"申请与通知"和"群聊"
		// contactList.add(0, users.get(AppConfig.GROUP_USERNAME));
		// 把"申请与通知"添加到首位
		contactList.add(0, users.get(AppConfig.NEW_FRIENDS_USERNAME));
		for (User user : contactList) {
			if (user.getHeader() != null && !"".equals(user.getHeader())) {
				user.setHeader(user.getHeader().toUpperCase());
			}
		}

		for (User user : contactList) {
			Log.d(TAG, user.getUsername());
		}
	}

	private void refreshContactList() {
		IMService.getInstance(getActivity()).getFriendsList(
				AppContext.getCurrentMember().getMobileNumber(),
				new CustomAsyncResponehandler() {

					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						listView.stopRefresh();
						List<Member> memberList = (List<Member>) baseModel
								.getResult();
						Map<String, User> userlist = new HashMap<String, User>();
						if (memberList != null) {

							for (Member member : memberList) {
								User user = new User();
								user.setUsername(member.getMemberName());
								IMService.getInstance(getActivity())
										.setUserHearder(member.getMemberName(),
												user);
								user.setId(member.getMobileNumber());
								user.setUrl(member.getAvatarsAddress());
								userlist.put(member.getMobileNumber(), user);
							}

							User newFriends = new User();
							newFriends
									.setUsername(AppConfig.NEW_FRIENDS_USERNAME);
							newFriends.setNick("申请与通知");
							newFriends.setHeader("");
							userlist.put(AppConfig.NEW_FRIENDS_USERNAME,
									newFriends);

							// 存入内存
							AppContext.getApplication()
									.setContactList(userlist);

							getContactList();
							adapter.notifyDataSetChanged();
						}
						super.onSuccess(baseModel);
					}

					@Override
					public void onFinish() {
						super.onFinish();
						listView.stopRefresh();
					}
				});
	}
}
