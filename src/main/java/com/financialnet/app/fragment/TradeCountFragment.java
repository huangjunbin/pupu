package com.financialnet.app.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.financialnet.app.R;

/**
 * @className: TradePassTransactionFragment.java
 * @author: limingtao
 * @function: 过往交易，今日关注，现时持仓
 * @date: 2013年9月24日下午4:11:44
 * @update:
 */
@SuppressLint("InflateParams")
public class TradeCountFragment extends BaseFragment {

	public static TradeCountFragment newInstance(int index) {
		TradeCountFragment frag = new TradeCountFragment();
		return frag;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.fragment_webview, container, false);
		return mView;
	}

	/**
	 * 控件
	 * **/
	public static WebView webview; // 主网页加载控件

	/***
	 * 数据
	 */
	public boolean isRefresh = false; // 判断webview当前是否属于刷新

	private String url = "";

	private String str = "";

	/**
	 * @Name: InitControl
	 * @Description: 初始化控件
	 * @Author: 黄俊彬
	 * @Version: V1.00
	 * @Create 2013-8-7
	 * @Parameters: 无
	 * @Return: 无
	 */
	private void InitControl() {

		webview = (WebView) findViewWithId(R.id.webView);
	}

	/**
	 * @Name: InitData
	 * @Description: 初始化数据
	 * @Author: 黄俊彬
	 * @Version: V1.00
	 * @Create 2013-8-7
	 * @Parameters: 无
	 * @Return: 无
	 */
	private void InitData() {
		initWebView();

	}

	/**
	 * @Name: 綁定控件
	 * @Description: 初始化数据
	 * @Author: 黄俊彬
	 * @Version: V1.00
	 * @Create 2013-8-7
	 * @Parameters: 无
	 * @Return: 无
	 */
	private void BindControl() {

	}

	/**
	 * @Name: initWebView
	 * @Description: 初始化webview控件属性
	 * @Author: 黄俊彬
	 * @Version: V1.00
	 * @Create 2013-8-7
	 * @Parameters: 无
	 * @Return: 无
	 */
	private void initWebView() {
		WebSettings settings = webview.getSettings();
		// 设置WebView属性，能够执行Javascript脚本
		settings.setJavaScriptEnabled(true);

		// 设置WebView不支持缩放 settings.setBuiltInZoomControls(false);
		// 设置WebView允许缓存 settings.setAppCacheEnabled(true);
		String cache_dir = getActivity().getApplicationContext()
				.getDir("cache", Context.MODE_PRIVATE).getPath();
		settings.setAppCachePath(cache_dir);// 设置应用缓存的路径 // 设置缓存的模式
											// 如果内容已经存在cache
											// 则使用cache，即使是过去的历史记录。如果cache中不存在，从网络中获取
		settings.setCacheMode(WebSettings.LOAD_DEFAULT);
		settings.setAppCacheMaxSize(1024 * 1024 * 8);// 设置应用缓存的最大尺寸
		settings.setAllowFileAccess(true);// 可以读取文件缓存(manifest生效)
		settings.setRenderPriority(RenderPriority.HIGH);
		settings.setJavaScriptCanOpenWindowsAutomatically(true);
		settings.setBlockNetworkImage(false);

		webview.requestFocus();
		webview.setScrollBarStyle(0);
		// set.setUserAgentString(SmartConfig.getWebUserAgent(mContext));
		// 可模拟其他浏览器内核登陆
		/**
		 * 使用户跳转到系统网络连接界面
		 * */

		webview.setWebViewClient(new WebViewClientDemo());
		if (url != null && url.length() > 0) {
			webview.loadUrl(url);
		}
		if (str != null && str.length() > 0) {
			try {

				str = str.replaceAll("&amp;", "");
				str = str.replaceAll("quot;", "\"");
				str = str.replaceAll("lt;", "<");
				str = str.replaceAll("gt;", ">");
				webview.loadDataWithBaseURL(null, str, "text/html", "utf-8",
						null);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * WebViewClientDemo类 主要帮助WebView处理各种通知、请求事件
	 * 
	 * @author 黃俊彬
	 * @version 1.0
	 * @created 2013-8-7
	 */
	private class WebViewClientDemo extends WebViewClient {
		@Override
		// 在WebView中而不是默认浏览器中显示页面
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			isRefresh = false;
			view.getSettings().setBlockNetworkImage(false);
		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			view.stopLoading();
			view.clearView();

		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			isRefresh = true;
			view.getSettings().setBlockNetworkImage(true);
		}

	}

	public void initView() {
		InitControl();
	}

	public void initData() {
		url = getArguments().getString("url");
		str = getArguments().getString("str");
		InitData();
	}

	public void bindView() {
		BindControl();
	}

}