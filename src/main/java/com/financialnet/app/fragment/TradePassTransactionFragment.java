package com.financialnet.app.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.financialnet.app.AppConfig;
import com.financialnet.app.AppContext;
import com.financialnet.app.AppStart;
import com.financialnet.app.R;
import com.financialnet.app.adapter.PassTradeAdapter;
import com.financialnet.app.adapter.StockAdapter;
import com.financialnet.app.ui.BaseActivity.OnSurePress;
import com.financialnet.app.ui.MemerPlanActivity;
import com.financialnet.app.ui.PassAttentionActivity;
import com.financialnet.app.ui.TradeActivity;
import com.financialnet.app.ui.TradeDetailActivity;
import com.financialnet.model.Group;
import com.financialnet.model.Member;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.MemberService;
import com.financialnet.service.StockService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.JsonUtil;
import com.financialnet.util.StringUtils;
import com.financialnet.util.UIHelper;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @className: TradePassTransactionFragment.java
 * @author: limingtao
 * @function: 过往交易，今日关注，现时持仓
 * @date: 2013年9月24日下午4:11:44
 * @update:
 */
@SuppressLint("InflateParams")
public class TradePassTransactionFragment extends BaseFragment implements
		IXListViewListener {
	private static final int PASSTRADEMAXESHOW = 3;// 过往交易的最新交易最多显示条数

	public static TradePassTransactionFragment newInstance(int index, int type,
			List<Stock> stockList) {
		TradePassTransactionFragment frag = new TradePassTransactionFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("index", index);
		bundle.putInt("type", type);
		frag.setArguments(bundle);
		return frag;
	}

	private View viewTraderListBar;
	private XListView xListView;
	private StockAdapter stockAdapter;
	private View btnToMember;
	private List<Stock> mstockList;
	private int stockType;// 股票类型
	private int pageNumber = 0;
	private int pageCurrentSize = AppContext.PAGE_SIZE;
	private TradeDetailActivity detailActivity;
	private PassTradeAdapter PassTradeAdp;
	private List<Object> passTradeList;
	private List<Group> groupKey;
	private boolean isPassTrade = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mIndex = getArguments() != null ? getArguments().getInt("index") : 1;
		stockType = getArguments() != null ? getArguments().getInt("type") : 1;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.fragment_trade_passtransaction,
				container, false);
		detailActivity = (TradeDetailActivity) getActivity();
		return mView;
	}

	@Override
	protected void initData() {
		// stockAdapter = new StockAdapter(getActivity(), mstockList,
		// stockType);
	}

	@Override
	protected void initView() {
		if (AppContext.getCurrentMember() == null||AppContext.getCurrentMember().getMemberID()==null) {
			Member member = MemberService.getInstance(getActivity())
					.getCacheMember();

			if (member != null) {
				AppContext.setCurrentMember(member);
			}
		}
		if (AppContext.getCurrentMember() == null||AppContext.getCurrentMember().getMemberID()==null) {
			startActivity(new Intent(getActivity(), AppStart.class));
			getActivity().finish();
			return;
		}
		// 权限管理
		if (AppContext.getCurrentMember().getFlagIsConfirmed() == AppConfig.PAY_NO) {
			if (TradeDetailActivity.tradeType == TradeActivity.MY_TRADER) {// 我的操盘手
				// 该处不会执行：没有付费之我的操盘手不可进入(finish)
			} else if (TradeDetailActivity.tradeType == TradeActivity.PUPU_TRADER) {// 普普操盘手
				if (stockType == StockAdapter.PASS_TRADE) {// 过往交易
					isPassTrade = true;
					initPassListView();
				} else if (stockType == StockAdapter.HOLD_NOW) {// 现时持仓
					initBtnView();
				} else if (stockType == StockAdapter.ATTENTION_TODAY) {// 今日关注
					initBtnView();
				}
			} else if (TradeDetailActivity.tradeType == TradeActivity.SEVEVN_PROFIT_RANK
					|| TradeDetailActivity.tradeType == TradeActivity.THIETY_PROFIT_RANK
					|| TradeDetailActivity.tradeType == TradeActivity.STEADY_PROFIT_RANK) {// 排行榜
				// 该处不会执行：不能查看详情，点击查看详情弹出付费页面(finish) //备注：该描述已过时，此处和普普操盘手一样
				if (stockType == StockAdapter.PASS_TRADE) {// 过往交易
					isPassTrade = true;
					initPassListView();
				} else if (stockType == StockAdapter.HOLD_NOW) {// 现时持仓
					initBtnView();
				} else if (stockType == StockAdapter.ATTENTION_TODAY) {// 今日关注
					initBtnView();
				}
			}
		} else if (AppContext.getCurrentMember().getFlagIsConfirmed() == AppConfig.PAY) {
			System.out.println("2:"+detailActivity.tempTrade.getMemberType());
			if (TradeDetailActivity.tradeType == TradeActivity.MY_TRADER) {// 我的操盘手
				initListView();
			} else if (TradeDetailActivity.tradeType == TradeActivity.PUPU_TRADER) {// 普普操盘手
				if (AppContext.getCurrentMember().getMemberClass() == AppConfig.CLASS_DIAMOND
						|| "1".equals(detailActivity.tempTrade.getMemberType())) {
					initListView();
				} else {
					if (stockType == StockAdapter.PASS_TRADE) {// 过往交易
						isPassTrade = true;
						initPassListView();
					} else if (stockType == StockAdapter.HOLD_NOW) {// 现时持仓
						initBtnView();
					} else if (stockType == StockAdapter.ATTENTION_TODAY) {// 今日关注
						initBtnView();
					}
				}
			} else if (TradeDetailActivity.tradeType == TradeActivity.SEVEVN_PROFIT_RANK
					|| TradeDetailActivity.tradeType == TradeActivity.THIETY_PROFIT_RANK
					|| TradeDetailActivity.tradeType == TradeActivity.STEADY_PROFIT_RANK) {// 排行榜
				if (AppContext.getCurrentMember().getMemberClass() == AppConfig.CLASS_DIAMOND
						|| "1".equals(detailActivity.tempTrade.getMemberType())) {
					initListView();
				} else {
					if (stockType == StockAdapter.PASS_TRADE) {// 过往交易
						isPassTrade = true;
						initPassListView();
					} else if (stockType == StockAdapter.HOLD_NOW) {// 现时持仓
						initBtnView();
					} else if (stockType == StockAdapter.ATTENTION_TODAY) {// 今日关注
						initBtnView();
					}
				}
			}
		}
	}

	/**
	 * 初始化付费视图
	 */
	private void initBtnView() {

		btnToMember = mView.findViewById(R.id.btn_toMember);
		btnToMember.setVisibility(View.VISIBLE);
		btnToMember.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				UIHelper.showSysDialog(getActivity(),
						"付费会员跟随此操盘手，即可任意查询操盘手最新关注的股票及市场动向", new OnSurePress() {
							@Override
							public void onClick(View view) {
								Intent intent = new Intent(getActivity(),
										MemerPlanActivity.class);
								// 设置跳转标志为如此Activity存在则把其从任务堆栈中取出放到最上方
								intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(intent);
								getActivity().overridePendingTransition(
										android.R.anim.fade_in,
										android.R.anim.fade_in);
							}
						}, false);
			}
		});
	}

	/**
	 * 初始化xlistView视图
	 */
	private void initPassListView() {
		viewTraderListBar = mView.findViewById(R.id.trader_list_bar);
		viewTraderListBar.setVisibility(View.VISIBLE);
		xListView = (XListView) mView
				.findViewById(R.id.trade_passtransaction_list);
		xListView.setVisibility(View.VISIBLE);
		groupKey = new ArrayList<Group>();
		Group group1 = new Group(PassTradeAdapter.lastTradeRecorde, "最新交易");
		Group group2 = new Group(PassTradeAdapter.passTradeRecorde,
				"过往交易列表(30天前)");
		groupKey.add(group1);
		groupKey.add(group2);

		passTradeList = new ArrayList<Object>();
		passTradeList.add(group1);
		passTradeList.add(group2);
		PassTradeAdp = new PassTradeAdapter(getActivity(), passTradeList,
				groupKey);
		// View emptyView = getActivity().getLayoutInflater().inflate(
		// R.layout.empty_view,null);
		// ((ViewGroup)xListView.getParent()).addView(emptyView,2);
		xListView.setEmptyView(mView.findViewById(R.id.trade_empty_view));

		xListView.setPullRefreshEnable(true);
		xListView.setPullLoadEnable(false);
		xListView.setFooterDividersEnabled(false);
		xListView.setAdapter(PassTradeAdp);
		xListView.setXListViewListener(this);
		onRefresh();
	}

	/**
	 * 初始化xlistView视图
	 */
	private void initListView() {
		viewTraderListBar = mView.findViewById(R.id.trader_list_bar);
		viewTraderListBar.setVisibility(View.VISIBLE);
		xListView = (XListView) mView
				.findViewById(R.id.trade_passtransaction_list);
		xListView.setVisibility(View.VISIBLE);
		mstockList = new ArrayList<Stock>();
		stockAdapter = new StockAdapter(getActivity(), mstockList, stockType,
				R.drawable.pro_ph_mask_default);
		xListView.setEmptyView(mView.findViewById(R.id.trade_empty_view));

		xListView.setPullRefreshEnable(true);
		xListView.setPullLoadEnable(true);
		xListView.setFooterDividersEnabled(false);
		xListView.setAdapter(stockAdapter);
		xListView.setXListViewListener(this);
		onRefresh();
	}

	@Override
	protected void bindView() {
		if (stockType == StockAdapter.ATTENTION_TODAY) {
			View v = getLayoutInflater(getArguments()).inflate(
					R.layout.item_trade_investment, null);
			TextView tv = (TextView) v.findViewById(R.id.item_invest_content);
			tv.setText("过往关注");
			v.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent();
					i.putExtra("tradeID",
							detailActivity.tempTrade.getTraderId());
					i.setClass(getActivity(), PassAttentionActivity.class);
					startActivity(i);
				}
			});
			if (xListView != null) {
				xListView.addFooterView(v);
			}
		}
	}

	@Override
	public void onResume() {

		super.onResume();
	}

	@Override
	public void onRefresh() {
		xListView.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		pageCurrentSize = AppContext.PAGE_SIZE;
		if (isPassTrade) {
			getPassTradeStockList(AppContext.REFRESH);
		} else {
			getSockList(AppContext.REFRESH);
		}
	}

	@Override
	public void onLoadMore() {
		if (isPassTrade) {
			if (passTradeList.size() >= pageCurrentSize) {
				pageCurrentSize += AppContext.PAGE_STEP_SIZE;
			}

			getPassTradeStockList(AppContext.LOADMORE);
		} else {
			if (mstockList.size() >= pageCurrentSize) {
				pageCurrentSize += AppContext.PAGE_STEP_SIZE;
			}

			getSockList(AppContext.LOADMORE);
		}
	}

	/**
	 * 付费动作
	 */
	class PayAction implements OnClickListener {
		@Override
		public void onClick(View arg0) {

			Intent intent = new Intent(getActivity(), MemerPlanActivity.class);
			// 设置跳转标志为如此Activity存在则把其从任务堆栈中取出放到最上方
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			getActivity().overridePendingTransition(android.R.anim.fade_in,
					android.R.anim.fade_in);
		}
	};

	/**
	 * 获取股票列表
	 */
	public void getSockList(final int state) {
		StockService.getInstance(context).getStockTradeList(stockType,
				detailActivity.tempTrade.getTraderId(), pageNumber + "",
				pageCurrentSize + "", new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							@SuppressWarnings("unchecked")
							List<Stock> list = (List<Stock>) baseModel
									.getResult();
							if (list != null && list.size() > 0) {
								mstockList.clear();
								mstockList.addAll(list);
								stockAdapter.notifyDataSetChanged();
								if (!StringUtils.isStringNone(baseModel
										.getTotalCount())) {
									if (mstockList.size() >= Integer
											.parseInt(baseModel.getTotalCount())) {
										xListView.setPullLoadEnable(false);
									} else {
										xListView.setPullLoadEnable(true);
									}
								}
							}
						}
					}

					@Override
					public void onFinish() {
						super.onFinish();
						xListView.stopRefresh();
						xListView.stopLoadMore();
					}
				});
	}

	private void getPassTradeStockList(final int state) {
		StockService.getInstance(context).getPassTradeStockList(
				detailActivity.tempTrade.getTraderId(), pageNumber + "",
				pageCurrentSize + "", new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							int totalCount = 0;
							int cutCount = 0;
							List<Stock> lastTemp = new ArrayList<Stock>(), passTemp = new ArrayList<Stock>();
							// ----------
							try {
								JSONObject object = baseModel.getDataResult();

								if (!object.isNull("traderStockStatistics")) {// 解析最近过往交易
									JSONArray jr1 = object
											.getJSONArray("traderStockStatistics");
									// JSONObject objectG1 = object
									// .getJSONObject("traderStockStatistics");
									// JSONObject objectG1 = new
									// JSONObject(object.getString("traderStockStatistics"));
									JSONObject objectG1 = (JSONObject) jr1
											.get(0);

									if (!objectG1
											.isNull("traderStockStatisticsList")) {
										lastTemp = JsonUtil.convertJsonToList(
												objectG1.getString("traderStockStatisticsList"),
												Stock.class);
										// 限制显示次数
										if (lastTemp != null
												&& lastTemp.size() > PASSTRADEMAXESHOW) {
											lastTemp = lastTemp.subList(0,
													PASSTRADEMAXESHOW);
											cutCount = lastTemp.size()
													- PASSTRADEMAXESHOW;
										}
									}

									if (!objectG1.isNull("totalCount")) {
										String tempCount1 = objectG1
												.getString("totalCount");
										if (!StringUtils
												.isStringNone(tempCount1)) {
											totalCount += Integer
													.parseInt(tempCount1);
										}
									}
								}

								if (!object.isNull("monthTrader")) {// 解析30天前过往交易
									JSONArray jr2 = object
											.getJSONArray("monthTrader");
									JSONObject objectG2 = (JSONObject) jr2
											.get(0);
									if (!objectG2.isNull("monthTraderList")) {
										passTemp = JsonUtil.convertJsonToList(
												objectG2.getString("monthTraderList"),
												Stock.class);
									}

									if (!objectG2.isNull("totalCount")) {
										String tempCount2 = objectG2
												.getString("totalCount");
										if (!StringUtils
												.isStringNone(tempCount2)) {
											totalCount += Integer
													.parseInt(tempCount2);
										}
									}
								}
								// ------------------------
								int lastSize = 0;
								if (state == AppContext.LOADMORE)
									lastSize = passTradeList.size() + cutCount;
								passTradeList.clear();
								passTradeList.add(groupKey.get(0));
								if (lastTemp != null) {
									for (Stock stock : lastTemp) {
										stock.setBelongroup(PassTradeAdapter.lastTradeRecorde);
									}
									passTradeList.addAll(lastTemp);
								}

								passTradeList.add(groupKey.get(1));
								if (passTemp != null) {
									for (Stock stock : passTemp) {
										stock.setBelongroup(PassTradeAdapter.passTradeRecorde);
									}
									passTradeList.addAll(passTemp);
								}

								PassTradeAdp.notifyDataSetChanged();
								// if (state == AppContext.LOADMORE)
								// xListView.setSelection(lastSize);
								// ----------
								if (passTradeList.size() - 2 >= totalCount) {
									xListView.setPullLoadEnable(false);
								} else {
									xListView.setPullLoadEnable(true);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}

					@Override
					public void onFinish() {
						super.onFinish();
						xListView.stopRefresh();
						xListView.stopLoadMore();
					}
				});
	}
}