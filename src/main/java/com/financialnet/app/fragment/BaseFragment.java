package com.financialnet.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

public abstract class BaseFragment extends Fragment {

	protected Context context;
	protected View mView;
	protected int mIndex;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	protected abstract void initData();

	protected abstract void initView();

	protected abstract void bindView();

	public void lastLoad() {
	};

	@SuppressWarnings("unchecked")
	public <T> T findViewWithId(int resId) {
		return (T) mView.findViewById(resId);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
		initData();
		bindView();
		lastLoad();
	}

}
