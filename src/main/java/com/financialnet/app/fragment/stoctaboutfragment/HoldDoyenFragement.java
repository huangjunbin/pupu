package com.financialnet.app.fragment.stoctaboutfragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.HoldDoyenAdapter;
import com.financialnet.app.fragment.BaseFragment;
import com.financialnet.app.ui.StockAboutActivity;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Trade;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.TraderService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.MyAnimation;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;

/**
 * @className：HoldDoyenFragement.java
 * @author: li mingtao
 * @Function: 持仓达人
 * @createDate: 2014-8-19下午2:54:24
 * @update:
 */
@SuppressLint("InflateParams")
public class HoldDoyenFragement extends BaseFragment implements
		IXListViewListener {
	private XListView xlistHoldDyen;
	private List<Trade> listHoldDoyen;
	private View headView;
	private HoldDoyenAdapter holdDoyenAdp;
	private int pageNumber = 0, pageCurSize = AppContext.PAGE_SIZE;
	private StockAboutActivity stockAboutActivity;
	private int type = 0;// 排序方式：0收益，1成本价

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.holddoyen, null);
		headView = inflater.inflate(R.layout.holddoyen_title, null);
		stockAboutActivity = (StockAboutActivity) getActivity();
		return mView;
	}

	@Override
	protected void initData() {
		listHoldDoyen = new ArrayList<Trade>();
		holdDoyenAdp = new HoldDoyenAdapter(getActivity(), listHoldDoyen,
				R.drawable.default_image);
		xlistHoldDyen.setAdapter(holdDoyenAdp);
	}

	@Override
	protected void initView() {
		stockAboutActivity.headerBar.top_sp_txt.setText(stockAboutActivity
				.getResString(R.string.main_order));
		stockAboutActivity.headerBar.top_sp_img
				.setBackgroundResource(R.drawable.sort_arrow);

		xlistHoldDyen = (XListView) mView.findViewById(R.id.holddoyen_list);
		xlistHoldDyen.addHeaderView(headView);
		xlistHoldDyen.setPullRefreshEnable(true);
		xlistHoldDyen.setPullLoadEnable(false);
		xlistHoldDyen.setFooterDividersEnabled(false);
		xlistHoldDyen.setHeaderDividersEnabled(true);
		xlistHoldDyen.setXListViewListener(this);

	}

	@Override
	protected void bindView() {
		stockAboutActivity.headerBar.topSpBar
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						stockAboutActivity.headerBar.top_sp_img
								.clearAnimation();
						if (0 == type) {
							type = 1;
							MyAnimation.roateAnimation(
									stockAboutActivity.headerBar.top_sp_img,
									300, 0, 180);
						} else {
							type = 0;
							MyAnimation.roateAnimation(
									stockAboutActivity.headerBar.top_sp_img,
									300, 180, 360);
						}
						onRefresh();
					}
				});
	}

	@Override
	public void lastLoad() {
		// TODO Auto-generated method stub
		super.lastLoad();
		onRefresh();
	}

	@Override
	public void onRefresh() {
		xlistHoldDyen.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		pageCurSize = AppContext.PAGE_SIZE;
		getStockHaveTrader();
	}

	@Override
	public void onLoadMore() {
		if (listHoldDoyen != null) {
			if (listHoldDoyen.size() >= pageCurSize)
				pageCurSize += AppContext.PAGE_STEP_SIZE;
		}
		getStockHaveTrader();
	}

	private void getStockHaveTrader() {
		TraderService.getInstance(context).getTradeHaveStock(
				String.valueOf(stockAboutActivity.tempStock.getMarketID()),
				String.valueOf(stockAboutActivity.tempStock.getStockCode()),
				String.valueOf(stockAboutActivity.tempStock.getStockCode()),
				String.valueOf(pageNumber), String.valueOf(pageCurSize),stockAboutActivity.tempStock.getNowPrice(),
				new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							@SuppressWarnings("unchecked")
							List<Trade> temp = (List<Trade>) baseModel
									.getResult();
							if (temp != null && temp.size() > 0) {
								int last = listHoldDoyen.size();
								listHoldDoyen.clear();
								listHoldDoyen.addAll(temp);
								holdDoyenAdp.notifyDataSetChanged();
								xlistHoldDyen.setSelection(last);
								if (!StringUtils.isStringNone(baseModel
										.getTotalCount())) {
									if (listHoldDoyen.size() >= Integer
											.parseInt(baseModel.getTotalCount())) {
										xlistHoldDyen.setPullLoadEnable(false);
									} else {
										xlistHoldDyen.setPullLoadEnable(true);
									}
								}
							} else {
								xlistHoldDyen.setPullLoadEnable(false);
							}
						}
					}

					@Override
					public void onFinish() {
						super.onFinish();
						xlistHoldDyen.stopLoadMore();
						xlistHoldDyen.stopRefresh();
					}
				});
	}
}
