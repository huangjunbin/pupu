package com.financialnet.app.fragment.stoctaboutfragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.HotInfoAdapter;
import com.financialnet.app.fragment.BaseFragment;
import com.financialnet.app.ui.HotInfoDetailActivity;
import com.financialnet.app.ui.StockAboutActivity;
import com.financialnet.model.HotInfo;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.StockService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;

/**
 * @className：AboutNewsFragement.java
 * @author: li mingtao
 * @Function: 相关新闻
 * @createDate: 2014-8-19下午2:54:08
 * @update:
 */
@SuppressLint("InflateParams")
public class AboutNewsFragement extends BaseFragment implements
		IXListViewListener {
	private XListView xlistNews;
	private List<HotInfo> listHotInfoData;
	private HotInfoAdapter hotInfoAdp;
	private StockAboutActivity stockAboutActivity;
	private int curPageNumber = 0, curePageSize = AppContext.PAGE_SIZE;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.about_news, null);
		stockAboutActivity = (StockAboutActivity) getActivity();
		return mView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void lastLoad() {
		super.lastLoad();
		onRefresh();
	}

	@Override
	protected void initData() {
		listHotInfoData = new ArrayList<HotInfo>();
		hotInfoAdp = new HotInfoAdapter(getActivity(), listHotInfoData,
				R.drawable.news_ph_tn_default,
				HotInfoDetailActivity.COMEFROM_STOCKNEW);
		xlistNews.setAdapter(hotInfoAdp);
	}

	@Override
	protected void initView() {
		xlistNews = (XListView) mView.findViewById(R.id.news_list);
		xlistNews.setEmptyView(mView.findViewById(R.id.news_empty_view));
		xlistNews.setPullRefreshEnable(true);
		xlistNews.setPullLoadEnable(false);
		xlistNews.setXListViewListener(this);
		xlistNews.setFooterDividersEnabled(false);
	}

	@Override
	protected void bindView() {
	}

	@Override
	public void onRefresh() {
		xlistNews.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		curePageSize = AppContext.PAGE_SIZE;
		getMyStockNews();
	}

	@Override
	public void onLoadMore() {
		if (listHotInfoData != null) {
			if (listHotInfoData.size() >= curePageSize)
				curePageSize += AppContext.PAGE_STEP_SIZE;
		}
		getMyStockNews();
	}

	private void getMyStockNews() {
		StockService.getInstance(context).getMyStockNews(
				String.valueOf(stockAboutActivity.tempStock.getMarketID()),
				String.valueOf(stockAboutActivity.tempStock.getStockCode()),
				String.valueOf(curPageNumber), String.valueOf(curePageSize),
				new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							@SuppressWarnings("unchecked")
							List<HotInfo> temp = (List<HotInfo>) baseModel
									.getResult();
							if (temp != null && temp.size() > 0) {
								int last = listHotInfoData.size();
								listHotInfoData.clear();
								listHotInfoData.addAll(temp);
								hotInfoAdp.notifyDataSetChanged();
								xlistNews.setSelection(last);
								if (!StringUtils.isStringNone(baseModel
										.getTotalCount())) {
									if (listHotInfoData.size() >= Integer
											.parseInt(baseModel.getTotalCount())) {
										xlistNews.setPullLoadEnable(false);
									} else {
										xlistNews.setPullLoadEnable(true);
									}
								}
							} else {
								xlistNews.setPullLoadEnable(false);
							}
						}
					}

					@Override
					public void onFinish() {
						super.onFinish();
						xlistNews.stopLoadMore();
						xlistNews.stopRefresh();
					}
				});
	}
}
