package com.financialnet.app.fragment.stoctaboutfragment;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.financialnet.app.R;
import com.financialnet.app.fragment.BaseFragment;
import com.financialnet.app.ui.StockAboutActivity;
import com.financialnet.model.LineEntity;
import com.financialnet.model.OHLCEntity;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.StockService;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.chat.MASlipCandleStickChart;

/**
 * @className：StockMarketFragement.java
 * @author: li mingtao
 * @Function: 股票行情
 * @createDate: 2014-8-19下午2:54:43
 * @update:
 */
public class StockMarketFragement extends BaseFragment {
    String TAG = "StockMarketFragement";
    private static final String TIME_LINE_TYPE = "0";// 分时线
    private static final String DAY_LINE_TYPE = "1";// 日线
    private static final String WEEK_LINE_TYPE = "2";// 周线
    private static final String MOUNTH_LINE_TYPE = "3";// 月线
    // ---------------------------
    private StockAboutActivity stockAboutActivity;
    private Stock currentStock;
    private TextView txtStockprice, txtUpOrDown, txtUpOrDownPercentage;
    private TextView txtTodayOpenPrice, txtYestayGetPrice;
    private TextView txtUpPrice, txtLowerPrice;
    private TextView txtVolume, txtTotalTurnover;
    // private DisplayImageOptions stockOption;

    private ImageView imgStockKline;

    private RadioGroup rgKLineChoose;

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.stockmarket, null);
        stockAboutActivity = (StockAboutActivity) getActivity();
        return mView;
    }

    @Override
    protected void initData() {
        // stockOption = new DisplayImageOptions.Builder()
        // .showStubImage(R.drawable.mylogo)
        // .showImageForEmptyUri(R.drawable.mylogo)
        // .showImageOnFail(R.drawable.mylogo).cacheInMemory(true)
        // .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    @Override
    protected void initView() {
        txtStockprice = (TextView) mView.findViewById(R.id.mk_stockprice);
        txtUpOrDown = (TextView) mView.findViewById(R.id.mk_upordown);
        txtUpOrDownPercentage = (TextView) mView
                .findViewById(R.id.mk_upordownpercentage);
        txtTodayOpenPrice = (TextView) mView
                .findViewById(R.id.mk_todayopenprice);
        txtYestayGetPrice = (TextView) mView
                .findViewById(R.id.mk_yestaygetprice);
        txtUpPrice = (TextView) mView.findViewById(R.id.mk_upprice);
        txtLowerPrice = (TextView) mView.findViewById(R.id.mk_lowPrice);
        txtVolume = (TextView) mView.findViewById(R.id.mk_volume);
        txtTotalTurnover = (TextView) mView.findViewById(R.id.mk_totalTurnover);
        imgStockKline = (ImageView) mView.findViewById(R.id.mk_k_line);
        rgKLineChoose = (RadioGroup) mView.findViewById(R.id.mk_line_choose);
        // 外部插件
        // initOHLC();
        // initMASlipCandleStickChart();
    }

    /**
     * 跟新股票行情视图
     */
    private void updateStockMarketView() {
        if (currentStock != null) {
            txtStockprice.setText(currentStock.getNowPrice());// 现价
            if (!StringUtils.isStringNone(currentStock.getUpOrDown())) {// 升跌
                float UpOrDownStr = Float.valueOf(currentStock.getUpOrDown());
                if (UpOrDownStr > 0) {
                    txtUpOrDown.setText("+" + currentStock.getUpOrDown());
                } else {
                    txtUpOrDown.setText(currentStock.getUpOrDown());
                }
            }

            if (!StringUtils.isStringNone(currentStock.getPercentage())) {// 升跌百分比
                try {
                    float percentageStr = Float.valueOf(currentStock
                            .getPercentage());
                    percentageStr = percentageStr * 100;
                    if (percentageStr > 0) {
                        txtUpOrDownPercentage.setText("+" + percentageStr + "%");
                    } else {
                        txtUpOrDownPercentage.setText(percentageStr + "%");
                    }
                } catch (Exception e) {
                    txtUpOrDownPercentage.setText(currentStock.getPercentage());
                }
            }

            txtTodayOpenPrice.setText(currentStock.getStartPrice());// 今开
            txtYestayGetPrice.setText(currentStock.getStopPrice());// 昨收

            txtUpPrice.setText(currentStock.getUpPrice());// 最高
            txtLowerPrice.setText(currentStock.getLowPrice());// 最低

            txtVolume.setText(currentStock.getVolume());// 成交量
            txtTotalTurnover.setText(currentStock.getTotalTurnover());// 成交额

            try {
                Glide.with(StockMarketFragement.this)
                        .load(currentStock.getImageUrl()).asBitmap()
                        .into(imgStockKline);
            } catch (Exception e) {
                Log.d(TAG, e.toString());
            }
            // AppContext.setImage(currentStock.getImageUrl(), imgStockKline,
            // stockOption);// k线图
        }
    }

    @Override
    protected void bindView() {
        rgKLineChoose.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.mk_parttimeline:// 分时线
                        setKline(TIME_LINE_TYPE);
                        break;
                    case R.id.mk_dayline:// 日K
                        setKline(DAY_LINE_TYPE);
                        break;
                    case R.id.mk_weekline:// 周K
                        setKline(WEEK_LINE_TYPE);
                        break;
                    case R.id.mk_mounthline:// 月K
                        setKline(MOUNTH_LINE_TYPE);
                        break;
                }
            }
        });
        // ----------------
        setKline(TIME_LINE_TYPE);// 设置默认线为分时线
    }

    private void setKline(String kType) {
        StockService.getInstance(context).getMineStockDetail(
                stockAboutActivity.tempStock.getMarketCode(),
                stockAboutActivity.tempStock.getStockCode(), kType,
                new CustomAsyncResponehandler() {
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel.isStatus()) {
                            Stock tempStock = (Stock) baseModel.getResult();
                            if (tempStock != null) {
                                currentStock = tempStock;
                                stockAboutActivity.tempStock.setNowPrice(tempStock.getNowPrice());
                                System.out.println(stockAboutActivity.tempStock);
                                updateStockMarketView();
                            }
                        }
                    }
                });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach");
    }

    // 外部插件---------------------------------------------
    private MASlipCandleStickChart maslipcandlestickchart;
    private List<OHLCEntity> ohlc;

    @SuppressWarnings("unused")
    private void initMASlipCandleStickChart() {
        this.maslipcandlestickchart = (MASlipCandleStickChart) mView
                .findViewById(R.id.maslipcandlestickchart);
        List<LineEntity<Float>> lines = new ArrayList<LineEntity<Float>>();

        // 计算5日均线
        LineEntity<Float> MA5 = new LineEntity<Float>();
        MA5.setTitle("MA5");
        MA5.setLineColor(Color.WHITE);
        MA5.setLineData(initMA(5));
        lines.add(MA5);

        // 计算10日均线
        LineEntity<Float> MA10 = new LineEntity<Float>();
        MA10.setTitle("MA10");
        MA10.setLineColor(Color.RED);
        MA10.setLineData(initMA(10));
        lines.add(MA10);

        // 计算25日均线
        LineEntity<Float> MA25 = new LineEntity<Float>();
        MA25.setTitle("MA25");
        MA25.setLineColor(Color.GREEN);
        MA25.setLineData(initMA(25));
        lines.add(MA25);

        maslipcandlestickchart.setAxisXColor(Color.LTGRAY);
        maslipcandlestickchart.setAxisYColor(Color.LTGRAY);
        maslipcandlestickchart.setLatitudeColor(Color.GRAY);
        maslipcandlestickchart.setLongitudeColor(Color.GRAY);
        maslipcandlestickchart.setBorderColor(Color.LTGRAY);
        maslipcandlestickchart.setLongitudeFontColor(Color.WHITE);
        maslipcandlestickchart.setLatitudeFontColor(Color.WHITE);
        maslipcandlestickchart.setAxisMarginRight(1);

        // 最大纬线数
        maslipcandlestickchart.setLatitudeNum(5);
        // 最大经线数
        maslipcandlestickchart.setLongitudeNum(3);
        // 最大价格
        maslipcandlestickchart.setMaxValue(1000);
        // 最小价格
        maslipcandlestickchart.setMinValue(200);

        maslipcandlestickchart.setDisplayFrom(10);

        maslipcandlestickchart.setDisplayNumber(30);

        maslipcandlestickchart.setMinDisplayNumber(5);

        maslipcandlestickchart.setZoomBaseLine(0);

        maslipcandlestickchart.setDisplayAxisXTitle(true);
        maslipcandlestickchart.setDisplayAxisYTitle(true);
        maslipcandlestickchart.setDisplayLatitude(true);
        maslipcandlestickchart.setDisplayLongitude(true);
        maslipcandlestickchart.setBackgroundColor(Color.BLACK);

        // 为chart2增加均线
        maslipcandlestickchart.setLineData(lines);

        // 为chart2增加均线
        maslipcandlestickchart.setOHLCData(ohlc);

    }

    private List<Float> initMA(int days) {

        if (days < 2) {
            return null;
        }

        List<Float> MA5Values = new ArrayList<Float>();

        float sum = 0;
        float avg = 0;
        for (int i = 0; i < this.ohlc.size(); i++) {
            float close = (float) ohlc.get(i).getClose();
            if (i < days) {
                sum = sum + close;
                avg = sum / (i + 1f);
            } else {
                sum = sum + close - (float) ohlc.get(i - days).getClose();
                avg = sum / days;
            }
            MA5Values.add(avg);
        }

        return MA5Values;
    }

    @SuppressWarnings("unused")
    private void initOHLC() {
        List<OHLCEntity> ohlc = new ArrayList<OHLCEntity>();

        this.ohlc = new ArrayList<OHLCEntity>();
        ohlc.add(new OHLCEntity(246, 248, 235, 235, 20110825));
        ohlc.add(new OHLCEntity(240, 242, 236, 242, 20110824));
        ohlc.add(new OHLCEntity(236, 240, 235, 240, 20110823));
        ohlc.add(new OHLCEntity(232, 236, 231, 236, 20110822));
        ohlc.add(new OHLCEntity(240, 240, 235, 235, 20110819));
        ohlc.add(new OHLCEntity(240, 241, 239, 240, 20110818));
        ohlc.add(new OHLCEntity(242, 243, 240, 240, 20110817));
        ohlc.add(new OHLCEntity(239, 242, 238, 242, 20110816));
        ohlc.add(new OHLCEntity(239, 240, 238, 239, 20110815));
        ohlc.add(new OHLCEntity(230, 238, 230, 238, 20110812));
        ohlc.add(new OHLCEntity(236, 237, 234, 234, 20110811));
        ohlc.add(new OHLCEntity(226, 233, 223, 232, 20110810));
        ohlc.add(new OHLCEntity(239, 241, 229, 232, 20110809));
        ohlc.add(new OHLCEntity(242, 244, 240, 242, 20110808));
        ohlc.add(new OHLCEntity(248, 249, 247, 248, 20110805));
        ohlc.add(new OHLCEntity(245, 248, 245, 247, 20110804));
        ohlc.add(new OHLCEntity(249, 249, 245, 247, 20110803));
        ohlc.add(new OHLCEntity(249, 251, 248, 250, 20110802));
        ohlc.add(new OHLCEntity(250, 252, 248, 250, 20110801));
        ohlc.add(new OHLCEntity(250, 251, 248, 250, 20110729));
        ohlc.add(new OHLCEntity(249, 252, 248, 252, 20110728));
        ohlc.add(new OHLCEntity(248, 250, 247, 250, 20110727));
        ohlc.add(new OHLCEntity(256, 256, 248, 248, 20110726));
        ohlc.add(new OHLCEntity(257, 258, 256, 257, 20110725));
        ohlc.add(new OHLCEntity(259, 260, 256, 256, 20110722));
        ohlc.add(new OHLCEntity(261, 261, 257, 259, 20110721));
        ohlc.add(new OHLCEntity(260, 260, 259, 259, 20110720));
        ohlc.add(new OHLCEntity(262, 262, 260, 261, 20110719));
        ohlc.add(new OHLCEntity(260, 262, 259, 262, 20110718));
        ohlc.add(new OHLCEntity(259, 261, 258, 261, 20110715));
        ohlc.add(new OHLCEntity(255, 259, 255, 259, 20110714));
        ohlc.add(new OHLCEntity(258, 258, 255, 255, 20110713));
        ohlc.add(new OHLCEntity(258, 260, 258, 260, 20110712));
        ohlc.add(new OHLCEntity(259, 260, 258, 259, 20110711));
        ohlc.add(new OHLCEntity(261, 262, 259, 259, 20110708));
        ohlc.add(new OHLCEntity(261, 261, 258, 261, 20110707));
        ohlc.add(new OHLCEntity(261, 261, 259, 261, 20110706));
        ohlc.add(new OHLCEntity(257, 261, 257, 261, 20110705));
        ohlc.add(new OHLCEntity(256, 257, 255, 255, 20110704));
        ohlc.add(new OHLCEntity(253, 257, 253, 256, 20110701));
        ohlc.add(new OHLCEntity(255, 255, 252, 252, 20110630));
        ohlc.add(new OHLCEntity(256, 256, 253, 255, 20110629));
        ohlc.add(new OHLCEntity(254, 256, 254, 255, 20110628));
        ohlc.add(new OHLCEntity(247, 256, 247, 254, 20110627));
        ohlc.add(new OHLCEntity(244, 249, 243, 248, 20110624));
        ohlc.add(new OHLCEntity(244, 245, 243, 244, 20110623));
        ohlc.add(new OHLCEntity(242, 244, 241, 244, 20110622));
        ohlc.add(new OHLCEntity(243, 243, 241, 242, 20110621));
        ohlc.add(new OHLCEntity(246, 247, 244, 244, 20110620));
        ohlc.add(new OHLCEntity(248, 249, 246, 246, 20110617));
        ohlc.add(new OHLCEntity(251, 253, 250, 250, 20110616));
        ohlc.add(new OHLCEntity(249, 253, 249, 253, 20110615));
        ohlc.add(new OHLCEntity(248, 250, 246, 250, 20110614));
        ohlc.add(new OHLCEntity(249, 250, 247, 250, 20110613));
        ohlc.add(new OHLCEntity(254, 254, 250, 250, 20110610));
        ohlc.add(new OHLCEntity(254, 255, 251, 255, 20110609));
        ohlc.add(new OHLCEntity(252, 254, 251, 254, 20110608));
        ohlc.add(new OHLCEntity(250, 253, 250, 252, 20110607));
        ohlc.add(new OHLCEntity(251, 252, 247, 250, 20110603));
        ohlc.add(new OHLCEntity(253, 254, 252, 254, 20110602));
        ohlc.add(new OHLCEntity(250, 254, 250, 254, 20110601));
        ohlc.add(new OHLCEntity(250, 252, 248, 250, 20110531));
        ohlc.add(new OHLCEntity(253, 254, 250, 251, 20110530));
        ohlc.add(new OHLCEntity(255, 256, 253, 253, 20110527));
        ohlc.add(new OHLCEntity(256, 257, 253, 254, 20110526));
        ohlc.add(new OHLCEntity(256, 257, 254, 256, 20110525));
        ohlc.add(new OHLCEntity(265, 265, 257, 257, 20110524));
        ohlc.add(new OHLCEntity(265, 266, 265, 265, 20110523));
        ohlc.add(new OHLCEntity(267, 268, 265, 266, 20110520));
        ohlc.add(new OHLCEntity(264, 267, 264, 267, 20110519));
        ohlc.add(new OHLCEntity(264, 266, 262, 265, 20110518));
        ohlc.add(new OHLCEntity(266, 267, 264, 264, 20110517));
        ohlc.add(new OHLCEntity(264, 267, 263, 267, 20110516));
        ohlc.add(new OHLCEntity(266, 267, 264, 264, 20110513));
        ohlc.add(new OHLCEntity(269, 269, 266, 268, 20110512));
        ohlc.add(new OHLCEntity(267, 269, 266, 269, 20110511));
        ohlc.add(new OHLCEntity(266, 268, 266, 267, 20110510));
        ohlc.add(new OHLCEntity(264, 268, 263, 266, 20110509));
        ohlc.add(new OHLCEntity(265, 268, 265, 267, 20110506));
        ohlc.add(new OHLCEntity(271, 271, 266, 266, 20110505));
        ohlc.add(new OHLCEntity(271, 273, 269, 273, 20110504));
        ohlc.add(new OHLCEntity(268, 271, 267, 271, 20110503));

        for (int i = ohlc.size(); i > 0; i--) {
            this.ohlc.add(ohlc.get(i - 1));
        }
    }
}
