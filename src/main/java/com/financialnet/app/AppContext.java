package com.financialnet.app;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ImageView;

import com.easemob.chat.ConnectionListener;
import com.easemob.chat.EMChat;
import com.easemob.chat.EMChatManager;
import com.easemob.chat.EMChatOptions;
import com.easemob.chat.EMMessage;
import com.easemob.chat.EMMessage.ChatType;
import com.easemob.chat.OnNotificationClickListener;
import com.financialnet.app.ui.ChatActivity;
import com.financialnet.app.ui.MainActivity;
import com.financialnet.db.DbOpenHelper;
import com.financialnet.model.Member;
import com.financialnet.model.User;
import com.financialnet.net.Urls;
import com.financialnet.util.PreferenceUtils;
import com.financialnet.util.SPUtil;
import com.financialnet.util.StringUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * 全局应用程序类：用于保存和调用全局应用配置及访问网络数据
 *
 * @author bin
 * @version 1.0.0
 * @created 2014-2-24
 */
public class AppContext extends Application {
    public static final int NETTYPE_WIFI = 0x01;
    public static final int NETTYPE_CMWAP = 0x02;
    public static final int NETTYPE_CMNET = 0x03;
    public static final int PAGE_SIZE = 20;// 默认分页大小
    public static final int PAGE_STEP_SIZE = 10;// 分页增加步长
    public static final int YES = 1;
    public static final int NOT = 2;
    public static final int EFFECT_TIME = 60;// 验证码有效期限
    public static final int RAINNG_STAR = 2;// 星星的最小进度
    public static final int RAINNGTOTAL_STAR = 10;// 星星的最小进度

    public static final String INVEST_TYPE = "investType";// 投资类型
    public static final String INVESTSPECIALITY_TYPE = "investSpeciality";// 投资专项类型
    public static final String INVESTTIME_TYPE = "investTimeID";// 投资时间类型
    public static final int REFRESH = 11;
    public static final int LOADMORE = 12;

    private static Vibrator mVibrato;
    private static AppContext application;
    private static Member currentMember = new Member();
    public static ImageLoader imageLoader;
    public static DisplayImageOptions memberPhotoOption;

    // login user name
    public final String PREF_USERNAME = "username";
    private String userName = null;
    // login password
    private static final String PREF_PWD = "pwd";
    private String password = null;
    /**
     * 当前用户nickname,为了苹果推送不是userid而是昵称
     */
    public static String currentUserNick = "";
    public static Context applicationContext;
    private Map<String, User> contactList = new HashMap<String, User>();
    public static DisplayImageOptions imageOption;

    public static void shake() {
        mVibrato.vibrate(20);
    }

    public static Member getCurrentMember() {
        if (currentMember != null) {
            return currentMember;
        } else {
            currentMember=SPUtil.getObjectFromShare("user");
            return currentMember;
        }
    }

    public static void setCurrentMember(Member currentMember) {
        SPUtil.saveObjectToShare("user",currentMember);
        AppContext.currentMember = currentMember;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        applicationContext = this;
        managerBitmap();
        initBitmap();
        mVibrato = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        // 初始化环信SDK,一定要先调用init()
        EMChat.getInstance().init(applicationContext);
        EMChat.getInstance().setDebugMode(true);
        Log.d("EMChat Demo", "initialize EMChat SDK");
        // debugmode设为true后，就能看到sdk打印的log了

        // 获取到EMChatOptions对象
        EMChatOptions options = EMChatManager.getInstance().getChatOptions();
        // 默认添加好友时，是不需要验证的，改成需要验证
        options.setAcceptInvitationAlways(false);
        // 设置收到消息是否有新消息通知，默认为true
        options.setNotifyBySoundAndVibrate(PreferenceUtils.getInstance(
                applicationContext).getSettingMsgNotification());
        // 设置收到消息是否有声音提示，默认为true
        options.setNoticeBySound(PreferenceUtils
                .getInstance(applicationContext).getSettingMsgSound());
        // 设置收到消息是否震动 默认为true
        options.setNoticedByVibrate(PreferenceUtils.getInstance(
                applicationContext).getSettingMsgVibrate());
        // 设置语音消息播放是否设置为扬声器播放 默认为true
        options.setUseSpeaker(PreferenceUtils.getInstance(applicationContext)
                .getSettingMsgSpeaker());
        // 设置notification消息点击时，跳转的intent为自定义的intent

        options.setOnNotificationClickListener(new OnNotificationClickListener() {

            @Override
            public Intent onNotificationClick(EMMessage message) {
                Intent intent = new Intent(applicationContext,
                        ChatActivity.class);
                ChatType chatType = message.getChatType();
                if (chatType == ChatType.Chat) { // 单聊信息
                    intent.putExtra("userId", message.getFrom());
                    intent.putExtra("chatType", ChatActivity.CHATTYPE_SINGLE);
                } else { // 群聊信息
                    // message.getTo()为群聊id
                    intent.putExtra("groupId", message.getTo());
                    intent.putExtra("chatType", ChatActivity.CHATTYPE_GROUP);
                }
                return intent;
            }
        });
        EMChatManager.getInstance().addConnectionListener(
                new MyConnectionListener());
        // 注册一个语言电话的广播接收者
        /*
         * IntentFilter callFilter = new
		 * IntentFilter(EMChatManager.getInstance()
		 * .getIncomingVoiceCallBroadcastAction()); registerReceiver(new
		 * VoiceCallReceiver(), callFilter);
		 */

        // 添加user"申请与通知"
        User newFriends = new User();
        newFriends.setUsername(AppConfig.NEW_FRIENDS_USERNAME);
        newFriends.setNick("申请与通知");
        newFriends.setHeader("");
        contactList.put(AppConfig.NEW_FRIENDS_USERNAME, newFriends);
        if (!AppConfig.isDevelopMode) {
            // 注册App异常崩溃处
            Thread.setDefaultUncaughtExceptionHandler(AppException
                    .getAppExceptionHandler());
        }
        initVersion();
    }

    private void initVersion() {
        PackageManager packageManager = getPackageManager();
        PackageInfo packInfo;
        try {
            packInfo = packageManager.getPackageInfo(getPackageName(), 0);
            AppConfig.localVersionCode = packInfo.versionCode;
            AppConfig.localVersionName = packInfo.versionName;
        } catch (NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    private void initBitmap() {
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        memberPhotoOption = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.pro_ph_mask_default)
                .showImageForEmptyUri(R.drawable.pro_ph_mask_default)
                .showImageOnFail(R.drawable.pro_ph_mask_default)
                .cacheInMemory(true).cacheOnDisc(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        imageOption = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.default_img)
                .showImageForEmptyUri(R.drawable.default_img)
                .showImageOnFail(R.drawable.default_img).cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    public static void setImage(String url, ImageView imageView,
                                DisplayImageOptions options) {
        imageLoader.displayImage(Urls.IMAGE_HEAD_URL + url, imageView, options);
    }

    public static void setImageNo(String url, ImageView imageView,
                                  DisplayImageOptions options) {
        imageLoader.displayImage(url, imageView, options);
    }

    public static void clearData() {
    }

    /**
     *
     */
    private void managerBitmap() {

    }

    public static AppContext getApplication() {
        return application;
    }

    /**
     * 检测当前系统声音是否为正常模式
     *
     * @return
     */
    public boolean isAudioNormal() {
        AudioManager mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        return mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL;
    }

    /**
     * 应用程序是否发出提示音
     *
     * @return
     */
    public boolean isAppSound() {
        return isAudioNormal();
    }

    /**
     * 检测网络是否可用
     *
     * @return
     */
    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnectedOrConnecting();
    }

    /**
     * 检测网络是否可用
     *
     * @return
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnectedOrConnecting();
    }

    /**
     * 获取当前网络类型
     *
     * @return 0：没有网络 1：WIFI网络 2：WAP网络 3：NET网络
     */
    @SuppressLint("DefaultLocale")
    public int getNetworkType() {
        int netType = 0;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo == null) {
            return netType;
        }
        int nType = networkInfo.getType();
        if (nType == ConnectivityManager.TYPE_MOBILE) {
            String extraInfo = networkInfo.getExtraInfo();
            if (!StringUtils.isEmpty(extraInfo)) {
                if (extraInfo.toLowerCase().equals("cmnet")) {
                    netType = NETTYPE_CMNET;
                } else {
                    netType = NETTYPE_CMWAP;
                }
            }
        } else if (nType == ConnectivityManager.TYPE_WIFI) {
            netType = NETTYPE_WIFI;
        }
        return netType;
    }

    /**
     * 判断当前版本是否兼容目标版本的方法
     *
     * @param VersionCode
     * @return
     */
    public static boolean isMethodsCompat(int VersionCode) {
        int currentVersion = android.os.Build.VERSION.SDK_INT;
        return currentVersion >= VersionCode;
    }

    /**
     * 获取App安装包信息
     *
     * @return
     */
    public PackageInfo getPackageInfo() {
        PackageInfo info = null;
        try {
            info = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (NameNotFoundException e) {
            e.printStackTrace(System.err);
        }
        if (info == null)
            info = new PackageInfo();
        return info;
    }

    /**
     * 保存磁盘缓存
     *
     * @param key
     * @param value
     * @throws IOException
     */
    public void setDiskCache(String key, String value) throws IOException {
        FileOutputStream fos = null;
        try {
            fos = openFileOutput("cache_" + key + ".data", Context.MODE_PRIVATE);
            fos.write(value.getBytes());
            fos.flush();
        } finally {
            try {
                fos.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * 获取磁盘缓存数据
     *
     * @param key
     * @return
     * @throws IOException
     */
    public String getDiskCache(String key) throws IOException {
        FileInputStream fis = null;
        try {
            fis = openFileInput("cache_" + key + ".data");
            byte[] datas = new byte[fis.available()];
            fis.read(datas);
            return new String(datas);
        } finally {
            try {
                fis.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * 获取当前登陆用户名
     *
     * @return
     */
    public String getUserName() {
        if (userName == null) {
            SharedPreferences preferences = PreferenceManager
                    .getDefaultSharedPreferences(this);
            userName = preferences.getString(PREF_USERNAME, null);
        }
        return userName;
    }

    /**
     * 获取密码
     *
     * @return
     */
    public String getPassword() {
        if (password == null) {
            SharedPreferences preferences = PreferenceManager
                    .getDefaultSharedPreferences(this);
            password = preferences.getString(PREF_PWD, null);
        }
        return password;
    }

    /**
     * 设置用户名
     *
     * @param user
     */
    public void setUserName(String username) {
        if (username != null) {
            SharedPreferences preferences = PreferenceManager
                    .getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = preferences.edit();
            if (editor.putString(PREF_USERNAME, username).commit()) {
                userName = username;
            }
        }
    }

    /**
     * 设置密码 下面的实例代码 只是demo，实际的应用中需要加password 加密后存入 preference 环信sdk
     * 内部的自动登录需要的密码，已经加密存储了
     *
     * @param pwd
     */
    public void setPassword(String pwd) {
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        if (editor.putString(PREF_PWD, pwd).commit()) {
            password = pwd;
        }
    }

    /**
     * 退出登录,清空数据
     */
    public void logout() {
        // 先调用sdk logout，在清理app中自己的数据
        EMChatManager.getInstance().logout();
        DbOpenHelper.getInstance(this).closeDB();
        // reset password to null
        setPassword(null);
        setContactList(null);
        setUserName(null);
    }

    private String getAppName(int pID) {
        String processName = null;
        ActivityManager am = (ActivityManager) this
                .getSystemService(ACTIVITY_SERVICE);
        List l = am.getRunningAppProcesses();
        Iterator i = l.iterator();
        PackageManager pm = this.getPackageManager();
        while (i.hasNext()) {
            ActivityManager.RunningAppProcessInfo info = (ActivityManager.RunningAppProcessInfo) (i
                    .next());
            try {
                if (info.pid == pID) {
                    CharSequence c = pm.getApplicationLabel(pm
                            .getApplicationInfo(info.processName,
                                    PackageManager.GET_META_DATA));
                    // Log.d("Process", "Id: "+ info.pid +" ProcessName: "+
                    // info.processName +"  Label: "+c.toString());
                    // processName = c.toString();
                    processName = info.processName;
                    return processName;
                }
            } catch (Exception e) {
                // Log.d("Process", "Error>> :"+ e.toString());
            }
        }
        return processName;
    }

    /**
     * 获取内存中好友user list
     *
     * @return
     */
    public Map<String, User> getContactList() {
        Log.d("HX", "get_________");
        if (contactList != null) {
            for (Map.Entry<String, User> entry : contactList.entrySet()) {
                Log.d("HX", "key= " + entry.getKey() + " and value= "
                        + entry.getValue().toString());
            }
        } else {
            Log.d("HX", String.valueOf(contactList));
        }
        return this.contactList;
    }

    /**
     * 设置好友user list到内存中
     *
     * @param contactList
     */
    public void setContactList(Map<String, User> contactList) {
        this.contactList = contactList;
        Log.d("HX", "set_________");
        if (contactList != null) {
            for (Map.Entry<String, User> entry : contactList.entrySet()) {
                Log.d("HX", "key= " + entry.getKey() + " and value= "
                        + entry.getValue().toString());
            }
        } else {
            Log.d("HX", String.valueOf(contactList));
        }
    }

    class MyConnectionListener implements ConnectionListener {
        @Override
        public void onReConnecting() {

        }

        @Override
        public void onReConnected() {

        }

        @Override
        public void onDisConnected(String errorString) {
            if (errorString != null && errorString.contains("conflict")) {
                if (AppContext.currentMember != null) {
//					Intent intent = new Intent(applicationContext,
//							MainActivity.class);
//					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//					intent.putExtra("conflict", true);
//					startActivity(intent);
                }
            } else {

            }
        }

        @Override
        public void onConnecting(String progress) {

        }

        @Override
        public void onConnected() {
        }
    }


}
