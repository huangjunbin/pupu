package com.financialnet.app.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.StockAdapter;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.StockService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;
import com.umeng.analytics.MobclickAgent;

public class PassAttentionActivity extends BaseActivity implements
		IXListViewListener {
	String TAG = "PassAttentionActivity";
	private HeaderBar headerBar;
	// for XListView
	private XListView xListView;
	private StockAdapter stockAdapter;
	private List<Stock> mstockList;
	private int curPageNum = 0, curPageSize = AppContext.PAGE_SIZE;
	private String tradeID = "";

	@Override
	public void lastLoad() {
		super.lastLoad();
		onRefresh();
	}

	public PassAttentionActivity() {
		super(R.layout.activity_hotinfo);
	}

	@Override
	public void initViews() {
		// for title
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle("过往关注");

		// for Xlist
		xListView = (XListView) findViewById(R.id.hotinfo_list);
		xListView.setHeaderDividersEnabled(false);
		xListView.setFooterDividersEnabled(false);
		xListView.setPullLoadEnable(false);
		xListView.setPullRefreshEnable(true);
	}

	@Override
	public void initData() {
		tradeID = getIntent().getStringExtra("tradeID");
		mstockList = new ArrayList<Stock>();

		stockAdapter = new StockAdapter(this, mstockList,
				StockAdapter.ATTENTION_PASS, R.drawable.pro_ph_mask_default);
		xListView.setAdapter(stockAdapter);
	}

	@Override
	public void bindViews() {
		xListView.setXListViewListener(this);
	}

	public void getSockList() {
		StockService.getInstance(context).getPassAttetion(tradeID,
				curPageNum + "", curPageSize + "",
				new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							@SuppressWarnings("unchecked")
							List<Stock> list = (List<Stock>) baseModel
									.getResult();
							if (list != null && list.size() > 0) {
								mstockList.clear();
								mstockList.addAll(list);
								stockAdapter.notifyDataSetChanged();
								if (!StringUtils.isStringNone(baseModel
										.getTotalCount())) {
									if (mstockList.size() >= Integer
											.parseInt(baseModel.getTotalCount())) {
										xListView.setPullLoadEnable(false);
									} else {
										xListView.setPullLoadEnable(true);
									}
								}
							}
						}
					}

					@Override
					public void onFinish() {
						super.onFinish();
						xListView.stopRefresh();
						xListView.stopLoadMore();
					}
				});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	public void onRefresh() {
		xListView.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		curPageSize = AppContext.PAGE_SIZE;
		getSockList();
	}

	@Override
	public void onLoadMore() {
		curPageSize += AppContext.PAGE_STEP_SIZE;
		getSockList();
	}
}
