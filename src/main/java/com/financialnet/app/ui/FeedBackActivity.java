package com.financialnet.app.ui;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.financialnet.app.R;
import com.umeng.analytics.MobclickAgent;

public class FeedBackActivity extends BaseActivity {
	private TextView btn_top_title, btn_top_right;
	private Button btnBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	public FeedBackActivity() {
		super(R.layout.activity_help_and_feedback);
	}

	@Override
	public void initViews() {
		btn_top_title = (TextView) findViewById(R.id.btn_top_title);
		btnBack = (Button) findViewById(R.id.btn_top_back);
		btn_top_right = (TextView) findViewById(R.id.btn_top_right);

	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		btn_top_right.setVisibility(View.VISIBLE);
		btn_top_right.setText("发送");
		btn_top_title.setText("意见反馈");
		btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				FeedBackActivity.this.finish();
			}
		});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
