package com.financialnet.app.ui;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.StockService;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

import java.util.List;

/**
 * @className：@StockAttentionActivity.java
 * @author: li mingtao
 * @Function:今日关注的股票详情
 * @createDate: @d2014-9-2@下午11:47:48
 * @update:
 */
public class StockAttentionActivity extends BaseActivity {
    private HeaderBar headerBar;
    private Stock CurrentStock;
    private Stock tempStock;
    private TextView txtStockName, txtStockCode;
    private TextView txtStockNowPrice, txtStockTargetPrice, txtStockOutPrice;
    private TextView txtStockCommount;
    private LinearLayout llStockTitle;
    private String id = "";

    @Override
    public void lastLoad() {
        super.lastLoad();
        myStockAttentionToday();
    }

    public StockAttentionActivity() {
        super(R.layout.activity_stockattention);
    }

    @Override
    public void initViews() {
        tempStock = (Stock) getIntent().getSerializableExtra("stock");
        id = getIntent().getStringExtra("id");
        headerBar = (HeaderBar) findViewById(R.id.title);
        if (tempStock != null) {
            headerBar.setTitle(TradeDetailActivity.currentTrader
                    .getTraderName());
        } else {
            headerBar.setTitle("今日关注");
        }
        llStockTitle = (LinearLayout) findViewById(R.id.today_stock_panel);
        txtStockName = (TextView) findViewById(R.id.attention_stockname);
        txtStockCode = (TextView) findViewById(R.id.attention_stockcode);
        txtStockNowPrice = (TextView) findViewById(R.id.attention_nowprice);
        txtStockTargetPrice = (TextView) findViewById(R.id.attention_targetprice);
        txtStockOutPrice = (TextView) findViewById(R.id.attention_targetoutprice);
        txtStockCommount = (TextView) findViewById(R.id.attention_commount);
    }

    @Override
    public void initData() {

    }

    @Override
    public void bindViews() {
        llStockTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CurrentStock != null) {
                    Intent intent = new Intent(context,
                            StockAboutActivity.class);
                    intent.putExtra("stock", CurrentStock);
                    context.startActivity(intent);
                } else {
                    showToast("获取数据失败！");
                }
            }
        });
    }

    /**
     * 跟新股票详情页面
     */
    private void updateStockDetailView() {
        if (CurrentStock != null) {
            txtStockName.setTag(CurrentStock.getStockName());
            txtStockCode.setText(CurrentStock.getStockName() + "("
                    + CurrentStock.getStockCode()
                    + CurrentStock.getMarketCode() + ")");
            txtStockNowPrice.setText(CurrentStock.getStockPrice());
            txtStockTargetPrice.setText(CurrentStock.getTagetPrice());
            txtStockOutPrice.setText(CurrentStock.getTagetSale());
            txtStockCommount.setText(CurrentStock.getComments());
        }
    }

    /**
     * 获取股票今日关注详情
     */
    public void myStockAttentionToday() {
        if (tempStock != null) {
            StockService.getInstance(context).getStockAttentionTodayDetail(
                    tempStock.getStockCode(), tempStock.getTraderId(),
                    tempStock.getMarketID(), "0", "1",
                    new CustomAsyncResponehandler() {
                        @SuppressWarnings("unchecked")
                        @Override
                        public void onSuccess(ResponeModel baseModel) {
                            super.onSuccess(baseModel);
                            if (baseModel.isStatus()) {
                                List<Stock> temps = (List<Stock>) baseModel
                                        .getResult();
                                if (temps != null && temps.size() > 0) {
                                    for (Stock s : temps) {
                                        if (tempStock.getCreateTime().equals(s.getCreateTime())) {
                                            CurrentStock = s;
                                        }
                                    }
                                    updateStockDetailView();
                                }
                            }
                        }
                    });
        } else {
            StockService.getInstance(context).getSysStockAttentionTodayDetail(
                    AppContext.getCurrentMember().getMemberID() + "", id,
                    new CustomAsyncResponehandler() {
                        @SuppressWarnings("unchecked")
                        @Override
                        public void onSuccess(ResponeModel baseModel) {
                            super.onSuccess(baseModel);
                            if (baseModel.isStatus()) {
                                List<Stock> temps = (List<Stock>) baseModel
                                        .getResult();
                                if (temps != null && temps.size() > 0) {
                                    CurrentStock = temps.get(0);
                                    updateStockDetailView();
                                }
                            }
                        }
                    });
        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
