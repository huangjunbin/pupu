package com.financialnet.app.ui;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.MemberService;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;


public class FundActivity extends BaseActivity {
	private HeaderBar headerBar;
	private TextView tvName,tvBDate,tvEDate;
	private TextView tvBenjin,tvBenjinDetail,tvBenjinGet;
	private TextView tvShouyi,tvShouyiDetail,tvShouyiGet;
	private TextView tvXianjin,tvXinajinDetail,tvXianjinGet;
	private RelativeLayout rlOne,rlTwo,rlThree;

	public FundActivity() {
		super(R.layout.activity_fund);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		tvName= (TextView) findViewById(R.id.tv_name);
		tvBDate= (TextView) findViewById(R.id.tv_bdate);
		tvEDate= (TextView) findViewById(R.id.tv_edate);
		tvBenjin= (TextView) findViewById(R.id.tv_benjin);
		tvBenjinDetail= (TextView) findViewById(R.id.tv_benjin_detail);
		tvBenjinGet= (TextView) findViewById(R.id.tv_benjin_get);
		tvShouyi= (TextView) findViewById(R.id.tv_shouyi);
		tvShouyiDetail= (TextView) findViewById(R.id.tv_shouyi_detail);
		tvShouyiGet= (TextView) findViewById(R.id.tv_shouyi_get);
		tvXianjin= (TextView) findViewById(R.id.tv_xianjin);
		tvXinajinDetail= (TextView) findViewById(R.id.tv_xianjin_detail);
		tvXianjinGet= (TextView) findViewById(R.id.tv_xianjin_get);

		rlOne= (RelativeLayout) findViewById(R.id.rl_one);
		rlTwo= (RelativeLayout) findViewById(R.id.rl_two);
		rlThree= (RelativeLayout) findViewById(R.id.rl_three);
		headerBar.setTitle("红天鹅基金");

	}

	@Override
	public void initData() {
	}

	@Override
	public void bindViews() {
		tvName.setText(AppContext.getCurrentMember().getKMemberPlanName());
		tvBDate.setText(AppContext.getCurrentMember().getKMemberPlanActivateTime());
		tvEDate.setText(AppContext.getCurrentMember().getKMemberPlanEndTime());
		tvBenjin.setText(AppContext.getCurrentMember().getKCapitalWord());
		tvShouyi.setText(AppContext.getCurrentMember().getKBonusLeftWord());
		tvXianjin.setText(AppContext.getCurrentMember().getKCashOnHandWord());

		tvBenjinDetail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(FundActivity.this, FundDetailActivity.class);
				intent.putExtra("type", 1);
				intent.putExtra("title", "本金详情");
				startActivity(intent);
			}
		});

		tvShouyiDetail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(FundActivity.this, FundDetailActivity.class);
				intent.putExtra("type", 2);
				intent.putExtra("title", "收益详情");
				startActivity(intent);
			}
		});

		tvXinajinDetail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(FundActivity.this, FundDetailActivity.class);
				intent.putExtra("type", 3);
				intent.putExtra("title", "现金详情");
				startActivity(intent);
			}
		});

		tvXianjinGet.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(FundActivity.this, MoneyGetActivity.class);
				intent.putExtra("type", 1);
				startActivity(intent);
			}
		});

		tvBenjinGet.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(FundActivity.this, MoneyGetActivity.class);
				intent.putExtra("type", 2);
				startActivity(intent);
			}
		});
		rlOne.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(FundActivity.this, WebViewActivity.class);
				intent.putExtra("title", "常见问题");
				intent.putExtra("url", "http://42.120.60.105/pupu/chart/redswanfaq.html");
				startActivity(intent);
			}
		});

		rlTwo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(FundActivity.this, FileActivity.class);
				intent.putExtra("url", AppContext.getCurrentMember().getKMemberPlanContractPDF());
				startActivity(intent);
			}
		});

		rlThree.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				JumpToActivityNoAnim(PactGetActivity.class);
			}
		});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		MemberService.getInstance(context).update(AppContext.getCurrentMember().getMobileNumber(),
				AppContext.getCurrentMember().getPassword(),
				new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {

							bindViews();
						}
					}
				});
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
