package com.financialnet.app.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.model.Pay;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.Urls;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.PayecoService;
import com.financialnet.util.UIHelper;
import com.financialnet.widget.HeaderBar;
import com.payeco.android.plugin.PayecoPluginLoadingActivity;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @MemerPlanActivity.java
 * @author li mingtao
 * @function :会员计划
 * @2013-9-4@下午5:50:54
 * @update:
 */
public class MemerPlanActivity extends BaseActivity {
	private HeaderBar headerBar;
	private RadioGroup radiobtnMemberPlanBar, radiobtnMemberTimeBar;
	private TextView textPlanIntrduce;
	private Button btnPlanNext;
	// 声明广播接收器
	private BroadcastReceiver payecoPayBroadcastReceiver;
	private static String typeId = "1";
	private static String month = "3";

	public MemerPlanActivity() {
		super(R.layout.activity_memerplan);
	}

	@Override
	public void initViews() {
		Intent intent=new Intent(MemerPlanActivity.this,WebViewActivity.class);
		intent.putExtra("title","鲸示");
		intent.putExtra("url", "http://121.40.155.118:8080/pupu/login/toLogin");
		startActivity(intent);
		MemerPlanActivity.this.finish();
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.member_title));
		radiobtnMemberPlanBar = (RadioGroup) findViewById(R.id.member_plan_bar);
		radiobtnMemberTimeBar = (RadioGroup) findViewById(R.id.member_time_bar);
		textPlanIntrduce = (TextView) findViewById(R.id.plan_intrduce);
		btnPlanNext = (Button) findViewById(R.id.plan_next);
		initPayecoPayBroadcastReceiver();

		// 注册支付结果广播接收器
		IntentFilter filter = new IntentFilter();
		filter.addAction(Urls.BROADCAST_PAY_END);
		filter.addCategory(Intent.CATEGORY_DEFAULT);
		registerReceiver(payecoPayBroadcastReceiver, filter);
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		// 会员计划选择
		radiobtnMemberPlanBar
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup arg0, int checkId) {
						switch (checkId) {
						case R.id.member_diamond:
							textPlanIntrduce
									.setText(getResString(R.string.member_diamondclassintrduce));
							typeId = "1";
							break;
						case R.id.member_gold:
							textPlanIntrduce
									.setText(getResString(R.string.member_goldclassintrduce));
							typeId = "3";
							break;
						}
					}
				});

		// 会员计划时间选择
		radiobtnMemberTimeBar
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup arg0, int checkId) {
						switch (checkId) {
						case R.id.threemounth:
							month = "3";
							break;
						case R.id.thelvemounth:
							month = "12";
							break;
						}
					}
				});

		// 下一步
		btnPlanNext.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				PayecoService.getInstance(MemerPlanActivity.this).pay(
						new CustomAsyncResponehandler() {
							@Override
							public void onSuccess(ResponeModel baseModel) {
								Pay pay = (Pay) baseModel.getResult();
								if(pay == null){
									UIHelper.showSysDialog(context, getResString(R.string.server_pay_error));
									return;
								}
								// 跳转至易联支付插件
								JSONObject Json = new JSONObject();
								try {
									Json.put("Version", pay.getVersion());
									Json.put("MerchOrderId",
											pay.getMerchOrderId());
									Json.put("MerchantId", pay.getMerchantId());
									Json.put("Amount", pay.getAmount());
									Json.put("TradeTime", pay.getTradeTime());
									Json.put("OrderId", pay.getOrderId());
									// Json.put("Broadcast",
									// Urls.BROADCAST_PAY_END);
									Json.put("Sign", pay.getSign());
								} catch (JSONException e) {
									e.printStackTrace();
								}

								Intent intent = new Intent(
										MemerPlanActivity.this,
										PayecoPluginLoadingActivity.class);
								intent.putExtra("upPay.Req", Json.toString());
								intent.putExtra("Broadcast", Urls.BROADCAST_PAY_END); //广播接收地址
								intent.putExtra("Environment", "01"); // 00: 测试环境, 01: 生产环境
								startActivity(intent);
								super.onSuccess(baseModel);
							}

						}, AppContext.getCurrentMember().getMemberID() + "",
						AppContext.getCurrentMember().getMobileNumber(), typeId,
						month);

			}
		});
	}

	// 初始化支付结果广播接收器
	private void initPayecoPayBroadcastReceiver() {
		payecoPayBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				// 接收易联支付插件的广播回调
				String action = intent.getAction();
				if (!Urls.BROADCAST_PAY_END.equals(action)) {
					return;
				}

				// 商户的业务处理
				String result = intent.getExtras().getString("upPay.Rsp");
				Log.i("test", "接收到广播内容：" + result);
				// 成为付费会员----------------------------------------------
				// AppContext.currentMember.setFlagIsConfirmed(1);
				// AppContext.currentMember.setMemberClass(typeId);
				// if (MemberService.memberDao != null) {
				// // 保持一个User在DB
				// MemberService.memberDao.delete();
				// // 记录数据库
				// MemberService.memberDao.insert(AppContext.currentMember);
				// }
				// AppManager.getAppManager().finishAllActivity();
				// JumpToActivity(MainActivity.class);
				// ----------------------------------------------
				// final String notifyParams = result;
				//
				// // 使用异步通讯
				// new AsyncTask<Void, Void, String>() {
				// @Override
				// protected String doInBackground(Void... params) {
				// // 用于接收通讯响应的内容
				// String respString = null;
				// // 通知商户服务器
				// try {
				// JSONObject reqJsonParams = new JSONObject(
				// notifyParams);
				//
				// ArrayList<NameValuePair> reqParams = new
				// ArrayList<NameValuePair>();
				// @SuppressWarnings("unchecked")
				// Iterator<String> keys = reqJsonParams.keys();
				// while (keys.hasNext()) {
				// String key = keys.next();
				// String value = reqJsonParams.getString(key);
				// reqParams
				// .add(new BasicNameValuePair(key, value));
				// }
				//
				// Log.i("test", "正在请求：" + Urls.URL_PAY_NOTIFY);
				// respString = httpComm(Urls.URL_PAY_NOTIFY,
				// reqParams);
				// } catch (JSONException e) {
				// Log.e("test", "解析处理失败！", e);
				// e.printStackTrace();
				// } catch (Exception e) {
				// Log.e("test", "通知失败，通讯发生异常", e);
				// e.printStackTrace();
				// }
				// return respString;
				// }
				//
				// @Override
				// protected void onPostExecute(String result) {
				// super.onPostExecute(result);
				//
				// if (result == null) {
				// Log.e("test", "通知失败！");
				// return;
				// }
				//
				// Log.i("test", "响应数据：" + result);
				//
				// try {
				// // 解析响应数据
				// JSONObject json = new JSONObject(result);
				//
				// // 校验返回结果
				// if (!json.has("RetMsg")) {
				// Toast.makeText(MemerPlanActivity.this,
				// "返回数据有误:" + result, Toast.LENGTH_LONG)
				// .show();
				// Log.e("test", "返回数据有误:" + result);
				// return;
				// }
				// Toast.makeText(MemerPlanActivity.this,
				// json.getString("RetMsg"), Toast.LENGTH_LONG)
				// .show();
				// } catch (JSONException e) {
				// Log.e("test", "解析处理失败！", e);
				// e.printStackTrace();
				// }
				//
				// }
				// }.execute();

				// //跳转至支付结果页面
				// Intent resultIntent = new Intent(MemerPlanActivity.this,
				// ResultActivity.class);
				// resultIntent.putExtra("result", result);
				// startActivity(resultIntent);
			}
		};
	}

	// // http通讯
	// private String httpComm(String reqUrl, ArrayList<NameValuePair>
	// reqParams)
	// throws UnsupportedEncodingException, IOException,
	// ClientProtocolException {
	// String respString = null;
	// HttpPost httpPost = new HttpPost(reqUrl);
	// HttpEntity entity = new UrlEncodedFormEntity(reqParams, HTTP.UTF_8);
	// httpPost.setEntity(entity);
	// DefaultHttpClient httpClient = new DefaultHttpClient();
	// HttpResponse httpResp = httpClient.execute(httpPost);
	// int statecode = httpResp.getStatusLine().getStatusCode();
	// if (statecode == 200) {
	// respString = EntityUtils.toString(httpResp.getEntity());
	// } else {
	// Log.e("test", "通讯发生异常，响应码[" + statecode + "]");
	// }
	// return respString;
	// }

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// 注销广播接收器
		if (payecoPayBroadcastReceiver != null) {
			unregisterReceiver(payecoPayBroadcastReceiver);
			payecoPayBroadcastReceiver = null;
		}
	}
}
