package com.financialnet.app.ui;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.TraderService;
import com.financialnet.util.StringUtils;
import com.financialnet.util.UIHelper;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

/**
 * @className：@ComAppActivity.java
 * @author: li mingtao
 * @Function:一起评论
 * @createDate: @d2014-8-31@下午6:25:23
 * @update:
 */
public class ComAppActivity extends BaseActivity {
	private HeaderBar headerBar;
	private EditText editSetUserMotto;
	private Button btnSureDiscuss;
	private RatingBar rbStarts;
	private Intent backIntent;

	public ComAppActivity() {
		super(R.layout.layout_comapp);
	}

	@Override
	public void initViews() {
		backIntent = getIntent();
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.minecenter_commonappraise));
		editSetUserMotto = (EditText) findViewById(R.id.trader_common);
		btnSureDiscuss = (Button) findViewById(R.id.sure_discuss);
		rbStarts = (RatingBar) findViewById(R.id.trader_ratebar);
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		btnSureDiscuss.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (StringUtils.isStringNone(editSetUserMotto.getText()
						.toString())) {
					UIHelper.ShowMessage(context,
							getResString(R.string.discus_cantemty));
					return;
				}

				if (rbStarts.getProgress() < AppContext.RAINNG_STAR) {
					UIHelper.ShowMessage(context,
							getResString(R.string.trader_start));
					return;
				}
				TraderService.getInstance(context).publishTradeComment(
						TradeDetailActivity.currentTrader.getTraderId(),
						AppContext.getCurrentMember().getMemberID() + "",
						editSetUserMotto.getText().toString(),
						rbStarts.getProgress() + "",
						new CustomAsyncResponehandler() {
							public void onSuccess(ResponeModel baseModel) {
								super.onSuccess(baseModel);
								if (baseModel.isStatus()) {
									setResult(
											TraderDiscussActivity.REFREESH_COMMON,
											backIntent);
									finish();
								}
							}
						});
			}
		});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
