package com.financialnet.app.ui;

import com.financialnet.app.R;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

/**
 * @className：PPCommunityActivity.java
 * @author: li mingtao
 * @Function: 普普社区
 * @createDate: 2014-8-18下午3:11:08
 * @update:
 */
public class PPCommunityActivity extends BaseActivity {
	HeaderBar headerBar;

	public PPCommunityActivity() {
		super(R.layout.activity_ppcommunity);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle("普普社区");
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {

	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
