package com.financialnet.app.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.financialnet.app.AppConfig;
import com.financialnet.app.AppContext;
import com.financialnet.app.AppStart;
import com.financialnet.app.R;
import com.financialnet.app.adapter.MyDiamondTraderAdapter;
import com.financialnet.app.adapter.TradeListAdapter;
import com.financialnet.model.Group;
import com.financialnet.model.Member;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Trade;
import com.financialnet.net.Urls;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.MemberService;
import com.financialnet.service.TraderService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.MyAnimation;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 操盘手
 * 
 * @author allen
 * @version 1.0.0
 * @created 2014-8-18
 * 
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
public class TradeActivity extends BaseActivity implements IXListViewListener {
	public static final int MYFAVORITE_SHOW_MAX = 3;// 我最爱的操盘手显示的最多的条数
	// ------
	public static final int MY_TRADER = 1;// 我的操盘手
	public static final int PUPU_TRADER = 2;// 普普操盘手
	public static final int SEVEVN_PROFIT_RANK = 3;// 7天收益排行
	public static final int THIETY_PROFIT_RANK = 4;// 30天收益排行
	public static final int STEADY_PROFIT_RANK = 5;// 收益稳定排行
	public static final int NO_PAY_TRADER = 6;// 未付费首页操盘手
	public static final int NO_PAY_BLOG = 7;// 未付费首页博客
	public static final int NO_PAY_TREND = 8;// 未付费首页操盘手30天前动向
	public static final int NO_PAY_RANK = 9;// 未付费首页操盘手30天前动向
	public static final int PAY_ZEWTREND = 10;// 付费首页面最新动向跳转
	public static final int PAY_RANK = 11;// 付费排行
	public static final int PAY_TODAYFOURSE = 12;// 付费今日关注
	private HeaderBar headerBar;
	// ----
	// private TextView btn_top_title;
	// private Button btnBack;
	private XListView lvTrade;
	private TradeListAdapter tradeListAdapter;
	private MyDiamondTraderAdapter myDiamondTraderAdapter;
	private Intent intent;
	private int traderType;// 操盘手类型
	private String[] titles = new String[] { "我的操盘手", "操盘手一览", "7天收益排行",
			"30天收益排行", "收益稳定排行" };
	private TextView textTraderRank;
	// 普普操盘手数据
	private List<Trade> listPupuTrader = new ArrayList<Trade>();// 操盘手列表
	private int PupuTraderPageNo = 0;
	private int PupuTraderPageCurrentNo = AppContext.PAGE_SIZE;
	private String statTypeStr;// 排行榜类型：1长期，7 七天，30 三十天
	private String typeStr = "0";// 排序类型：0收益，1收益百分比,默认收益排序
	private List<Group> groupKey;
	private List<Object> listDiamondTrade = new ArrayList<Object>();

	@Override
	public void lastLoad() {
		super.lastLoad();
		onRefresh();
	}

	public TradeActivity() {
		super(R.layout.activity_trade);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);

		lvTrade = (XListView) findViewById(R.id.trade_list);
		lvTrade.setPullRefreshEnable(true);
		lvTrade.setPullLoadEnable(false);
		lvTrade.setFooterDividersEnabled(false);
		textTraderRank = (TextView) findViewById(R.id.trader_rank);

		// 获取操盘手类型
		traderType = getIntent().getIntExtra("trade_type", 0);
		// 设置标题
		headerBar.setTitle(titles[traderType - 1]);
		// 排行设置
		if (traderType == TradeActivity.MY_TRADER
				|| traderType == TradeActivity.PUPU_TRADER) {
			textTraderRank.setVisibility(View.GONE);
		}
		if (AppContext.getCurrentMember() == null) {
			Member member = MemberService.getInstance(context)
					.getCacheMember();

			if (member != null) {
				AppContext.setCurrentMember(member);
			}
		}
		if (AppContext.getCurrentMember() == null) {
			startActivity(new Intent(this, AppStart.class));
			finish();
			return;
		}
		if (AppContext.getCurrentMember().getFlagIsConfirmed() == AppConfig.PAY
				&& traderType == TradeActivity.MY_TRADER) {
			headerBar.top_right_btn.setText(R.string.public_edit);
		}
	}
	
	@Override
	public void initData() {
		// 我的操盘手钻石
		if (traderType == MY_TRADER
				&& AppContext.getCurrentMember().getMemberClass() == 3) {
			groupKey = new ArrayList<Group>();
			Group group1 = new Group(MyDiamondTraderAdapter.MYFAVORITE_TRADE,
					"我的收藏");
			Group group2 = new Group(MyDiamondTraderAdapter.MYOLDERED_TRADE,
					"其他操盘手");
			groupKey.add(group1);
			//groupKey.add(group2);
			listDiamondTrade.addAll(groupKey);
			myDiamondTraderAdapter = new MyDiamondTraderAdapter(
					TradeActivity.this, listDiamondTrade, groupKey,
					R.drawable.pro_ph_mask_default);
			lvTrade.setAdapter(myDiamondTraderAdapter);
			lvTrade.setHeaderDividersEnabled(false);
		} else {
			tradeListAdapter = new TradeListAdapter(this, listPupuTrader,
					traderType, R.drawable.pro_ph_mask_default);
			lvTrade.setAdapter(tradeListAdapter);
			switch (traderType) {
			case MY_TRADER:
				break;
			case PUPU_TRADER:
				break;
			case SEVEVN_PROFIT_RANK:
				statTypeStr = "7";
				defaultOlder();
				break;
			case THIETY_PROFIT_RANK:
				statTypeStr = "30";
				defaultOlder();
				break;
			case STEADY_PROFIT_RANK:
				statTypeStr = "1";
				defaultOlder();
				break;
			default:
				statTypeStr = null;
			}
		}
	}

	private void defaultOlder() {
		headerBar.topSpBar.setVisibility(View.VISIBLE);
		headerBar.top_sp_txt.setText(getResString(R.string.main_order));
		headerBar.top_sp_img.setBackgroundResource(R.drawable.sort_arrow);
	}

	@Override
	public void bindViews() {
		headerBar.topSpBar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				headerBar.top_sp_img.clearAnimation();
				if ("0".equals(typeStr)) {
					typeStr = "1";
					MyAnimation.roateAnimation(headerBar.top_sp_img, 300, 0,
							180);
				} else {
					typeStr = "0";
					MyAnimation.roateAnimation(headerBar.top_sp_img, 300, 180,
							360);
				}
				onRefresh();
			}
		});
		
		headerBar.back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				TradeActivity.this.finish();
			}
		});

		if (AppContext.getCurrentMember().getFlagIsConfirmed() == AppConfig.PAY
				&& traderType == TradeActivity.MY_TRADER) {
			headerBar.top_right_btn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(TradeActivity.this, WebViewActivity.class);
					intent.putExtra("title", getResString(R.string.app_name));
					intent.putExtra("url", Urls.ONLINE_HEAD_URL+"login/toLogin");
					startActivity(intent);
				}
			});
		}

		lvTrade.setXListViewListener(this);

		lvTrade.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				if (traderType == MY_TRADER
						&& AppContext.getCurrentMember().getMemberClass() == 3)
					return;
				switch (traderType) {
				case SEVEVN_PROFIT_RANK:
				case THIETY_PROFIT_RANK:
				case STEADY_PROFIT_RANK:
					// if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1)
					// {// 付费
					intent = new Intent();
					intent.putExtra("trade", listPupuTrader.get(arg2 - 1));
					intent.putExtra("trade_type", traderType);

					intent.setClass(TradeActivity.this,
							TradeDetailActivity.class);
					startActivity(intent);
					// } else {
					// JumpToActivity(MemerPlanActivity.class);
					// }
					break;
				case MY_TRADER:
				case PUPU_TRADER:
					intent = new Intent();
					intent.putExtra("trade", listPupuTrader.get(arg2 - 1));
					intent.putExtra("trade_type", traderType);
					intent.setClass(TradeActivity.this,
							TradeDetailActivity.class);
					startActivity(intent);
					break;
				}
			}
		});
	}

	@Override
	public void onRefresh() {
		lvTrade.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));

		PupuTraderPageNo = 0;
		PupuTraderPageCurrentNo = AppContext.PAGE_SIZE;
		getTraderList();
	}

	@Override
	public void onLoadMore() {
		if (listPupuTrader.size() >= PupuTraderPageCurrentNo) {
			PupuTraderPageCurrentNo += AppContext.PAGE_STEP_SIZE;
		}
		getTraderList();
	}

	public void getTraderList() {
		TraderService.getInstance(context).getTraderList(traderType,
				statTypeStr, typeStr, String.valueOf(PupuTraderPageNo),
				String.valueOf(PupuTraderPageCurrentNo),
				AppContext.getCurrentMember().getMemberID() + "",
				AppContext.getCurrentMember().getMemberClass() + "",
				new CustomAsyncResponehandler() {
					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							List<Trade> tempTrade = (List<Trade>) baseModel
									.getResult();
							if (traderType == MY_TRADER
									&& AppContext.getCurrentMember()
											.getMemberClass() == 3) {
								if (tempTrade != null && tempTrade.size() > 0) {
							/*		List<Trade> favorite = new ArrayList<Trade>(), oldered = new ArrayList<Trade>();
								 	for (Trade trade : tempTrade) {
										if (trade
												.getMemberType()
												.equals(MyDiamondTraderAdapter.MYOLDERED_TRADE
														+ "")) {
											favorite.add(trade);
										} else {
											oldered.add(trade);
										}
									}

									if (favorite.size() > MYFAVORITE_SHOW_MAX) {
										favorite = favorite.subList(0,
												MYFAVORITE_SHOW_MAX);
									} */

									listDiamondTrade.clear();
									listDiamondTrade.add(groupKey.get(0));
									listDiamondTrade.addAll(tempTrade);
									//listDiamondTrade.add(groupKey.get(1));
									//listDiamondTrade.addAll(oldered);
									myDiamondTraderAdapter
											.notifyDataSetChanged();
									/*if (listDiamondTrade.size() < Integer
											.parseInt(baseModel.getTotalCount())) {
										lvTrade.setPullLoadEnable(true);
									} else {
										lvTrade.setPullLoadEnable(false);
									}*/
								} else {
									lvTrade.setPullLoadEnable(false);
								}
							} else {
								if (tempTrade != null && tempTrade.size() > 0) {
									listPupuTrader.clear();
									listPupuTrader.addAll(tempTrade);
								/*	if (listPupuTrader.size() < Integer
											.parseInt(baseModel.getTotalCount())) {
										lvTrade.setPullLoadEnable(true);
									} else {
										lvTrade.setPullLoadEnable(false);
									}*/
								} else {
									lvTrade.setPullLoadEnable(false);
								}
								System.out.println(listPupuTrader);
								tradeListAdapter.notifyDataSetChanged();
							}
						}
					}

					@Override
					public void onFinish() {
						super.onFinish();
						lvTrade.stopRefresh();// 停止刷新
						lvTrade.stopLoadMore();// 停止加载更多
					}
				});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
