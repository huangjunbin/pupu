package com.financialnet.app.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.financialnet.app.R;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

/**
 * @EasyEditActivity.java
 * @author li mingtao
 * @function :简单的编辑页面（用户名和金句设置）
 * @2013-9-3@上午11:58:09
 * @update:
 */
public class EasyEditActivity extends BaseActivity {
	public static final int EDIT_USERNAME = 11;// 编辑用户名
	public static final int EDIT_USERMOTTO = 22;// 编辑金句
	public int editType;
	// -----------------------
	private HeaderBar headerBar;
	private EditText editSetUserName, editSetUserMotto;
	private Button btnSubmit;

	public EasyEditActivity() {
		super(R.layout.activity_easyedit);
	}

	@Override
	public void initViews() {
		editType = getIntent().getIntExtra("data", 0);
		headerBar = (HeaderBar) findViewById(R.id.title);
		editSetUserName = (EditText) findViewById(R.id.setuser_name);
		editSetUserMotto = (EditText) findViewById(R.id.setuser_motto);
		btnSubmit = (Button) findViewById(R.id.set_now);
		switch (editType) {
		case EDIT_USERNAME:
			headerBar.setTitle(getResString(R.string.minecenter_setusername));
			editSetUserName.setVisibility(View.VISIBLE);
			break;
		case EDIT_USERMOTTO:
			headerBar.setTitle(getResString(R.string.minecenter_setusermotto));
			editSetUserMotto.setVisibility(View.VISIBLE);
			break;
		}

	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				switch (editType) {
				case EDIT_USERNAME:
					if (cantEmpty(editSetUserName, R.string.username_cantemty))
						return;
					MineCenterActivity.editMember.setMemberName(editSetUserName
							.getText().toString());
					finish();
					break;
				case EDIT_USERMOTTO:
					if (cantEmpty(editSetUserMotto, R.string.usermotto_cantemty))
						return;
					MineCenterActivity.editMember
							.setSelfIntroduction(editSetUserMotto.getText()
									.toString());
					finish();
					break;
				}
			}
		});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
