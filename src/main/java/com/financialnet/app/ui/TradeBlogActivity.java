package com.financialnet.app.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.TradeHomeBlogAdapter;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.TradeBlog;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.TraderService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.JsonUtil;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@SuppressLint("InflateParams")
public class TradeBlogActivity extends BaseActivity implements
		IXListViewListener {

	public TradeBlogActivity(){
		super(R.layout.activity_trade_blog);
	}

	private TextView tvNoData;
	private XListView xListView;
	private TradeHomeBlogAdapter adapter;
	private List<TradeBlog> blogList;
	private int pageNumber = 0;
	private int pageCurrentSize = 10;
	private HeaderBar bar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public void initViews() {
		xListView = (XListView) findViewById(R.id.trade_blog_list);
		tvNoData = (TextView) findViewById(R.id.trade_empty_view);
		bar= (HeaderBar) findViewById(R.id.title);
	}



	@Override
	public void initData() {
		blogList = new ArrayList<TradeBlog>();
		adapter = new TradeHomeBlogAdapter(this, blogList,
				R.drawable.news_ph_tn_default);
		xListView.setAdapter(adapter);
	}

	@Override
	public void bindViews() {
		xListView.setXListViewListener(this);
		bar.setTitle("操盘手专栏");
		onRefresh();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onRefresh() {
		xListView.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		pageCurrentSize =10;
		getBlogList(AppContext.REFRESH);

	}

	@Override
	public void onLoadMore() {

		if (blogList.size() >= pageCurrentSize) {
			pageCurrentSize += 10;
		}
		getBlogList(AppContext.LOADMORE);

	}

	/**
	 * 获取股票列表
	 */
	public void getBlogList(final int state) {
		TraderService.getInstance(context).getTradeBlogByHome(
				AppContext.getCurrentMember().getMemberID()+"", pageNumber + "",
				pageCurrentSize + "", new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							try {
								String result = baseModel.getDataResult()
										.getString("result");
								int totalCount = baseModel.getDataResult()
										.getInt("totalCount");
								if (totalCount == 0) {
									tvNoData.setVisibility(View.VISIBLE);
									xListView.setVisibility(View.GONE);
									return;
								}
								if (result != null) {
									List<TradeBlog> list = JsonUtil
											.convertJsonToList(result,
													TradeBlog.class);
									if (list != null && list.size() > 0) {
										blogList.clear();
										blogList.addAll(list);
										adapter.notifyDataSetChanged();

										if (blogList.size() >= totalCount) {
											xListView.setPullLoadEnable(false);
										} else {
											xListView.setPullLoadEnable(true);
										}

									}
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}

						}
					}

					@Override
					public void onFinish() {
						super.onFinish();
						xListView.stopRefresh();
						xListView.stopLoadMore();
					}
				});
	}

}