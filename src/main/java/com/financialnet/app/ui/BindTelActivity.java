package com.financialnet.app.ui;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.MemberService;
import com.financialnet.util.StringUtils;
import com.financialnet.util.UIHelper;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

/**
 * @BindTelActivity.java
 * @author li mingtao
 * @function :绑定手机号码
 * @2013-8-26下午5:25:35
 * @update:
 */
public class BindTelActivity extends BaseActivity {
	private HeaderBar headerBar;
	private EditText editMobileNumber, editInputAutoCode;
	private Button btnFillData, btnGetAutoCode;
	private Timer timer;
	private int len = AppContext.EFFECT_TIME;

	@SuppressLint("HandlerLeak")
	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				len--;
				btnGetAutoCode.setText(len + "秒");
				if (len <= 0) {
					btnGetAutoCode.setEnabled(true);
					btnGetAutoCode
							.setText(getResString(R.string.register_getcode));
					timer.cancel();
					timer = null;
					len = AppContext.EFFECT_TIME;
				}
				break;
			}
			super.handleMessage(msg);
		}
	};

	public BindTelActivity() {
		super(R.layout.activity_mobilenum);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.register_bindtel));

		editMobileNumber = (EditText) findViewById(R.id.user_tel);
		editInputAutoCode = (EditText) findViewById(R.id.input_code);
		btnFillData = (Button) findViewById(R.id.fill_data);
		btnGetAutoCode = (Button) findViewById(R.id.getAutoCode);
	}

	@Override
	public void initData() {
	}

	@Override
	public void bindViews() {
		// 获取验证码
		btnGetAutoCode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (cantEmpty(editMobileNumber,
						R.string.register_moblenumbernoempty))
					return;

				if (StringUtils.isPhoneNumberValid2(editMobileNumber.getText()
						.toString())) {
					MemberService.getInstance(context).getAutoCode(
							editMobileNumber.getText().toString(),
							new CustomAsyncResponehandler() {
								@Override
								public void onSuccess(ResponeModel baseModel) {
									super.onSuccess(baseModel);
									if (baseModel.isStatus()) {
										// 不可点击
										btnGetAutoCode.setEnabled(false);
										btnGetAutoCode.setText(len + "秒");
										timer = new Timer();
										timer.schedule(new TimerTask() {
											@Override
											public void run() {
												Message msg = new Message();
												msg.what = 1;
												handler.sendMessage(msg);
											}
										}, 1000, 1000);
									}
								}
							});
				} else {
					UIHelper.ShowMessage(context,
							getResString(R.string.register_moblenumbererroe));
					return;
				}
			}
		});

		// 检验验证码
		btnFillData.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (cantEmpty(editMobileNumber,
						R.string.register_moblenumbernoempty))
					return;
				if (cantEmpty(editInputAutoCode,
						R.string.register_autocodenoempty))
					return;

				if (StringUtils.isPhoneNumberValid2(editMobileNumber.getText()
						.toString())) {
					MemberService.getInstance(context).CheckoutAutoCode(
							editMobileNumber.getText().toString(),
							editInputAutoCode.getText().toString(),
							new CustomAsyncResponehandler() {
								@Override
								public void onSuccess(ResponeModel baseModel) {
									super.onSuccess(baseModel);
									if (baseModel.isStatus()) {
										Intent intent = new Intent(
												BindTelActivity.this,
												RegisterActivity.class);
										intent.putExtra("mobileNumber",
												editMobileNumber.getText()
														.toString());
										startActivity(intent);
									}
								}
							});
				} else {
					UIHelper.ShowMessage(context,
							getResString(R.string.register_moblenumbererroe));
					return;
				}
			}
		});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}
}
