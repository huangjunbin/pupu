package com.financialnet.app.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.financialnet.app.R;
import com.umeng.analytics.MobclickAgent;

/**
 * 
 */
public class GuideActivity extends BaseActivity {

	public GuideActivity() {
		super(R.layout.activity_guide);

	}

	private ViewPager banner_viewpager;
	private LinearLayout banner_img_layout;
	private List<View> banner_list;
	private PageAdapter pageAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public void initViews() {

		banner_viewpager = (ViewPager) findViewById(R.id.guide_banner_viewpager);
		banner_img_layout = (LinearLayout) findViewById(R.id.guide_banner_img_layout);
		banner_list = new ArrayList<View>();
	}

	@Override
	public void initData() {
		@SuppressWarnings("unused")
		int ids[] = { R.drawable.logo_pupu, R.drawable.logo_pupu,
				R.drawable.logo_pupu };
		for (int i = 0; i < 3; i++) {
			/*
			 * ImageView imageView = new ImageView(context);
			 * imageView.setScaleType(ScaleType.CENTER_CROP);
			 * imageView.setImageDrawable(getResources().getDrawable(ids[i]));
			 * banner_list.add(imageView);
			 */
			TextView t = new TextView(context);
			t.setText("guide:" + i);
			t.setGravity(Gravity.CENTER);
			banner_list.add(t);
		}
		pageAdapter = new PageAdapter(banner_list);
	}

	@Override
	public void bindViews() {
		banner_viewpager.setAdapter(pageAdapter);
		showAllPage(0);
		banner_viewpager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				showPage(arg0);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});
	}

	/**
	 * 创建通知栏下面的页码小图标
	 * 
	 * @param position
	 */
	private void showAllPage(int position) {
		for (int i = 0; i < 3; i++) {
			ImageView img = new ImageView(this);
			img.setId(R.id.home_banner_img + i);
			img.setPadding(5, 0, 0, 0);
			banner_img_layout.addView(img);
			if (position == i) {
				img.setImageResource(R.drawable.on);
			} else {
				img.setImageResource(R.drawable.noon);
			}
		}

	}

	/**
	 * 显示对应页码
	 * 
	 * @param position
	 *            页码
	 */
	private void showPage(int position) {
		for (int i = 0; i < 3; i++) {
			ImageView img = (ImageView) this.findViewById(R.id.home_banner_img
					+ i);
			if (position == i) {
				img.setImageResource(R.drawable.on);
			} else {
				img.setImageResource(R.drawable.noon);
			}
		}
	}

	void goToMainActivity() {

		this.finish();
	}

	public class PageAdapter extends PagerAdapter {

		private List<View> list;

		public PageAdapter(List<View> list) {
			this.list = list;

		}

		@Override
		public void destroyItem(View view, int position, Object arg2) {
			ViewPager pViewPager = ((ViewPager) view);
			pViewPager.removeView(list.get(position));
		}

		@Override
		public void finishUpdate(View arg0) {
		}

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Object instantiateItem(View view, int position) {
			ViewPager pViewPager = ((ViewPager) view);
			if (position == (list.size() - 1)) {
				list.get(position).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {

						Intent mainIntent = new Intent(GuideActivity.this,
								MainActivity.class);
						mainIntent.putExtra("tag", 0);
						startActivity(mainIntent);

						GuideActivity.this.finish();
					}
				});
			}
			if (list.get(position).getParent() == null) {
				pViewPager.addView(list.get(position));
			} else {
				FrameLayout viewParent = (FrameLayout) list.get(position)
						.getParent();
				viewParent.removeView(list.get(position));
				pViewPager.addView(list.get(position));
			}
			return list.get(position);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) { // back键
			Intent mainIntent = new Intent(GuideActivity.this,
					MainActivity.class);
			mainIntent.putExtra("tag", 0);
			startActivity(mainIntent);
			GuideActivity.this.finish();
		}
		return false;
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
