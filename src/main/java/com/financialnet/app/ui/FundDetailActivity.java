package com.financialnet.app.ui;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.FundDetailAdapter;
import com.financialnet.model.FundDetail;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.MemberService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class FundDetailActivity extends BaseActivity implements IXListViewListener {
	private HeaderBar headerBar;
	private XListView xlistHotInfo;
	private List<FundDetail> listHotInfoData;
	private FundDetailAdapter hotInfoAdp;
	private int type=1;
	private String title="";

	@Override
	public void lastLoad() {
		super.lastLoad();
		onRefresh();
	}

	public FundDetailActivity() {
		super(R.layout.activity_funddetail);
	}

	@Override
	public void initViews() {
		type=getIntent().getIntExtra("type",1);
		title=getIntent().getStringExtra("title");
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(title);
		xlistHotInfo = (XListView) findViewById(R.id.hotinfo_list);
		xlistHotInfo.setHeaderDividersEnabled(false);
		xlistHotInfo.setFooterDividersEnabled(false);
		xlistHotInfo.setPullLoadEnable(false);
		xlistHotInfo.setPullRefreshEnable(true);
	}

	@Override
	public void initData() {
		listHotInfoData = new ArrayList<FundDetail>();
		hotInfoAdp = new FundDetailAdapter(this, listHotInfoData,
				type);
		xlistHotInfo.setAdapter(hotInfoAdp);
	}

	@Override
	public void bindViews() {
		xlistHotInfo.setXListViewListener(this);
	}

	private void getHotinfoList() {
		MemberService.getInstance(context).getFundDetail(type,
				AppContext.getCurrentMember().getMemberID() + "", new CustomAsyncResponehandler() {

					@Override
					public void onFinish() {
						xlistHotInfo.stopRefresh();
						super.onFinish();
					}

					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							List<FundDetail> tempHotinfo = (List<FundDetail>) baseModel
									.getResult();
							if (tempHotinfo != null && tempHotinfo.size() > 0) {
								listHotInfoData.clear();
								listHotInfoData.addAll(tempHotinfo);
								hotInfoAdp.notifyDataSetChanged();
							}

							if (!StringUtils.isStringNone(baseModel
									.getTotalCount())) {
								if (listHotInfoData.size() >= Integer
										.valueOf(baseModel.getTotalCount())) {
									xlistHotInfo.setPullLoadEnable(false);
								} else {
									xlistHotInfo.setPullLoadEnable(true);
								}
							}
						}
					}
				});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	public void onRefresh() {
		xlistHotInfo.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		getHotinfoList();
	}

	@Override
	public void onLoadMore() {
		getHotinfoList();
	}
}
