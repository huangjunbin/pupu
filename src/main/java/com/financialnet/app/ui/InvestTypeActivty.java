package com.financialnet.app.ui;

import com.umeng.analytics.MobclickAgent;

import android.os.Bundle;

/**
 * @className：InvestTypeActivty.java
 * @author: li mingtao
 * @Function: 投资类型
 * @createDate: 2014-8-20上午11:49:46
 * @update:
 */
public class InvestTypeActivty extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	public InvestTypeActivty(int resId) {
		super(resId);
	}

	@Override
	public void initViews() {

	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {

	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
