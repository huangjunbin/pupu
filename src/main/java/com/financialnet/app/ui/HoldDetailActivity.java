package com.financialnet.app.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.HoldDetailAdapter;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.StockService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.JsonUtil;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @className：@HoldDetailActivity.java
 * @author: li mingtao
 * @Function:现时持仓详情
 * @createDate: @d2014-9-3@上午12:22:41
 * @update:
 */
@SuppressLint("InflateParams")
public class HoldDetailActivity extends BaseActivity implements
		IXListViewListener {
	public static final int UPDTTE_VIEW = 1;
	// -------------
	private HeaderBar headerBar;
	private List<Stock> stockList;
	private HoldDetailAdapter HoldDetailAdp;
	private XListView xListHold;
	public Stock tempStock, currentStock, detailStock;
	private View topView;
	private TextView txtStockName, txtStockCode;
	private TextView txtTargetInPrice, txtStopPrice;
	private TextView txtPriceQuality, txtDate;
	private ImageView imgStockImg;
	private TextView txtAppraise;
	private DisplayImageOptions stockImgOption;
	private int curPageNumber = 0, curPageSize = AppContext.PAGE_SIZE;
	private boolean isFirstUpdateDetaill = true;
	private LinearLayout llStockTitle;
	private TextView tvPrice, tvQuality;
	private TextView tvNum;

	public HoldDetailActivity() {
		super(R.layout.activity_holddetail);
	}

	@Override
	public void firstLoad() {
		super.firstLoad();
		tempStock = (Stock) getIntent().getSerializableExtra("stock");
	}

	@Override
	public void lastLoad() {
		super.lastLoad();
		onRefresh();
	}

	@Override
	public void initViews() {

		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(TradeDetailActivity.currentTrader.getTraderName());
		// ----
		xListHold = (XListView) findViewById(R.id.xlist_holdstock);
		xListHold.setPullLoadEnable(false);
		xListHold.setPullRefreshEnable(true);
		xListHold.setFooterDividersEnabled(false);
		xListHold.setHeaderDividersEnabled(false);
		// ---
		topView = getLayoutInflater().inflate(R.layout.top_holddetail_view,
				null);
		tvPrice = (TextView) topView.findViewById(R.id.hold_price);
		tvNum= (TextView) topView.findViewById(R.id.tv_number);
		tvQuality = (TextView) topView.findViewById(R.id.hold_quantity);
		llStockTitle = (LinearLayout) topView
				.findViewById(R.id.doyen_stock_panel);
		txtStockName = (TextView) topView.findViewById(R.id.hold_stock_name);
		txtStockCode = (TextView) topView.findViewById(R.id.hold_stock_code);
		txtTargetInPrice = (TextView) topView
				.findViewById(R.id.hold_targetInPrice);
		txtStopPrice = (TextView) topView.findViewById(R.id.hold_stopPrice);

		txtPriceQuality = (TextView) topView.findViewById(R.id.stock_state);
		txtDate = (TextView) topView.findViewById(R.id.stock_time);
		imgStockImg = (ImageView) topView.findViewById(R.id.hold_stock_img);
		txtAppraise = (TextView) topView
				.findViewById(R.id.hold_appraiseContent);

		topView.findViewById(R.id.tv_buy).setVisibility(View.GONE);
		topView.findViewById(R.id.ly_buy).setVisibility(View.GONE);
	}

	private void updateHead() {
		if (detailStock == null) {
			return;
		}

		txtStockName.setText(detailStock.getStockName());
		txtStockCode.setText("(" + detailStock.getStockCode()
				+ detailStock.getMarketCode() + ")");
		txtTargetInPrice.setText(detailStock.getTargetPrice());
		txtStopPrice.setText(detailStock.getStopPrice());
	}

	private void updateView() {
		if (currentStock == null)
			return;
		Drawable able = null;
		if ("1".equals(currentStock.getFlagAuthentication())) {
			able = getResources().getDrawable(R.drawable.cret_v_icon_a);
			txtPriceQuality.setCompoundDrawablesWithIntrinsicBounds(able, null,
					null, null);
		} else {
			able = getResources().getDrawable(R.drawable.cret_v_icon_b);
			txtPriceQuality.setCompoundDrawablesWithIntrinsicBounds(able, null,
					null, null);
		}

		String inoutstr;
		StringBuffer stateSb = new StringBuffer();
		if ("1".equals(currentStock.getFlagTrade())) {
			inoutstr = " 卖出";
		} else {
			inoutstr = " 买入";
		}

		stateSb.append(currentStock.getPrice() + inoutstr
				+ currentStock.getQuantity() + "股");
		txtPriceQuality.setText(tempStock.getTargetPrice());
		txtDate.setText(currentStock.getTradeTime());
		tvPrice.setText(currentStock.getPrice());
		tvQuality.setText(currentStock.getQuantity());
		AppContext.setImageNo(currentStock.getCertificateAddr(), imgStockImg,
				stockImgOption);
		tvNum.setText(currentStock.getSerialNumber());
		if(currentStock.getSerialNumber()==null||"".equals(currentStock.getSerialNumber())){
			tvNum.setVisibility(View.GONE);
			imgStockImg.setVisibility(View.GONE);
		}
		txtAppraise.setText(currentStock.getComments());

	}

	@SuppressWarnings("deprecation")
	@Override
	public void initData() {
		stockImgOption = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.cret_bg)
				.showImageForEmptyUri(R.drawable.cret_bg)
				.showImageOnFail(R.drawable.cret_bg).cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();

		stockList = new ArrayList<Stock>();
		HoldDetailAdp = new HoldDetailAdapter(HoldDetailActivity.this,
				stockList);
		xListHold.addHeaderView(topView);
		xListHold.addFooterView(getLayoutInflater().inflate(
				R.layout.footview_gray, null));
		xListHold.setAdapter(HoldDetailAdp);
	}

	@Override
	public void bindViews() {
		xListHold.setXListViewListener(this);
		llStockTitle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (detailStock != null) {
					Intent intent = new Intent(context,
							StockAboutActivity.class);
					intent.putExtra("stock", detailStock);
					context.startActivity(intent);
				} else {
					showToast("获取数据失败！");
				}
			}
		});
	}

	public Handler hand = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (UPDTTE_VIEW == msg.what) {
				updateView();
				xListHold.setSelection(0);
			}
		}
	};

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	public void onRefresh() {
		xListHold.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		curPageSize = AppContext.PAGE_SIZE;
		getTraderDetail();
	}

	@Override
	public void onLoadMore() {
		if (stockList != null) {
			if (stockList.size() >= curPageSize)
				curPageSize += AppContext.PAGE_STEP_SIZE;
		}
		getTraderDetail();
	}

	/**
	 * 查询操盘手现时持仓详情
	 */
	public void getTraderDetail() {
		StockService.getInstance(context).getTraderHoldNowDetail(
				TradeDetailActivity.currentTrader.getTraderId(),
				tempStock.getMarketID(), tempStock.getStockCode(),
				String.valueOf(curPageNumber), String.valueOf(curPageSize),
				new CustomAsyncResponehandler() {
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							detailStock = JsonUtil.convertJsonToObject(
									baseModel.getData(), Stock.class);
							updateHead();
							@SuppressWarnings("unchecked")
							List<Stock> soc = (List<Stock>) baseModel
									.getResult();
							if (soc != null && soc.size() > 0) {
								if (isFirstUpdateDetaill) {
									currentStock = soc.get(0);
									updateView();
									isFirstUpdateDetaill = false;
								}
								stockList.clear();
								stockList.addAll(soc);
								HoldDetailAdp.notifyDataSetChanged();
								if (!StringUtils.isStringNone(baseModel
										.getTotalCount())) {
									if (stockList.size() >= Integer
											.parseInt(baseModel.getTotalCount())) {
										xListHold.setPullLoadEnable(false);
									} else {
										xListHold.setPullLoadEnable(true);
									}
								}
							} else {
								xListHold.setPullLoadEnable(false);
							}
						}
					}

					@Override
					public void onFinish() {
						super.onFinish();
						xListHold.stopLoadMore();
						xListHold.stopRefresh();
					}
				});
	}
}
