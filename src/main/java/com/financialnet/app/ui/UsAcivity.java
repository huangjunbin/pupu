package com.financialnet.app.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.financialnet.app.AppConfig;
import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

/**
 * @className：UsAcivity.java
 * @author: li mingtao
 * @Function:关于我们
 * @createDate: 2014-8-18上午11:38:10
 * @update:
 */
public class UsAcivity extends BaseActivity {
    HeaderBar headBar;
    // --
    @SuppressWarnings("unused")
    private TextView textVersionTitle, textVerionContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public UsAcivity() {
        super(R.layout.activity_us);
    }

    public String title = "";

    @Override
    public void initViews() {
        headBar = (HeaderBar) findViewById(R.id.title);
        title = getIntent().getStringExtra("title");
        textVersionTitle = (TextView) findViewById(R.id.version_title);
        textVerionContent = (TextView) findViewById(R.id.version_content);
        if (title == null || "".equals(title)) {
            headBar.setTitle(getResString(R.string.us_title));
            textVersionTitle.setText(getResString(R.string.us_version)
                    + AppConfig.localVersionName);
        } else {
            headBar.setTitle(title);
            textVersionTitle.setVisibility(View.GONE);
            textVerionContent.setText(getResString(R.string.condition));
        }
    }

    @Override
    public void initData() {

    }

    @Override
    public void bindViews() {

    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
