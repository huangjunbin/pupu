package com.financialnet.app.ui;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.financialnet.app.AppConfig;
import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.TraderAppraiseAdapter;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.TraderAppraise;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.TraderService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.CircleImageView;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @TraderDiscussActivity.java
 * @author li mingtao
 * @function :评论操盘手
 * @2013-9-3@下午4:25:59
 * @update:
 */
public class TraderDiscussActivity extends BaseActivity implements
		IXListViewListener {
	public static final int GO_COMMON = 0x111;// 去评价
	public static final int REFREESH_COMMON = 0x112;// 评价成功返回刷新
	// --------------------
	private HeaderBar headerBar;
	private XListView xlistTraderAppraise;
	private TraderAppraiseAdapter traderAppraiseAdp;
	private Button btnAddAppraise;
	private CircleImageView imgMemberheader;
	private TextView txtTradeName, txtTraderMotto;
	private RatingBar rbTraderStarts;
	private int traderPageNo = 0;
	private int traderPageCurrentSizeNo = AppContext.PAGE_SIZE;
	private String tradeId = null;
	public List<TraderAppraise> listTraderAppraise = new ArrayList<TraderAppraise>();// 当前操盘手的评论

	public TraderDiscussActivity() {
		super(R.layout.activity_traderdiscuss);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		xlistTraderAppraise = (XListView) findViewById(R.id.appraise_list);
		btnAddAppraise = (Button) findViewById(R.id.add_appraise);
		imgMemberheader = (CircleImageView) findViewById(R.id.mine_memberheader);
		txtTradeName = (TextView) findViewById(R.id.trade_name_);
		txtTraderMotto = (TextView) findViewById(R.id.trader_motto_);
		rbTraderStarts = (RatingBar) findViewById(R.id.trader_ratebar);
		rbTraderStarts.setMax(10);
		xlistTraderAppraise.setPullLoadEnable(true);
		xlistTraderAppraise.setPullRefreshEnable(true);
	}

	@Override
	public void initData() {
		traderAppraiseAdp = new TraderAppraiseAdapter(
				TraderDiscussActivity.this, listTraderAppraise);
		xlistTraderAppraise.setAdapter(traderAppraiseAdp);
		tradeId = getIntent().getStringExtra("tradeId");
		onRefresh();
	}

	@Override
	public void bindViews() {
		btnAddAppraise.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (AppContext.getCurrentMember().getFlagIsConfirmed() == AppConfig.PAY_NO) {
					JumpToActivity(MemerPlanActivity.class);
				} else if (AppContext.getCurrentMember().getFlagIsConfirmed() == AppConfig.PAY) {
					if (TradeDetailActivity.tradeType == TradeActivity.MY_TRADER
							|| AppContext.getCurrentMember().getMemberClass() == AppConfig.CLASS_DIAMOND) {
						JumpToActivityForResult(ComAppActivity.class, GO_COMMON);
					} else {
						JumpToActivity(MemerPlanActivity.class);
					}
				}
			}
		});
		xlistTraderAppraise.setXListViewListener(this);
	}

	private void updateView() {
		if (TradeDetailActivity.currentTrader != null) {
			AppContext
					.setImageNo(
							TradeDetailActivity.currentTrader.getLogoUrl() == null
									|| "".equals(TradeDetailActivity.currentTrader
											.getLogoUrl()) ? TradeDetailActivity.currentTrader
									.getAvatarsAddress()
									: TradeDetailActivity.currentTrader
											.getLogoUrl(), imgMemberheader,
							AppContext.memberPhotoOption);

			if (!StringUtils.isStringNone(TradeDetailActivity.currentTrader
					.getTraderName())) {
				headerBar.setTitle(TradeDetailActivity.currentTrader
						.getTraderName());
			}

			if (!StringUtils.isStringNone(TradeDetailActivity.currentTrader
					.getTraderName())) {
				txtTradeName.setText(TradeDetailActivity.currentTrader
						.getTraderName());
			}

			if (!StringUtils.isStringNone(TradeDetailActivity.currentTrader
					.getSelfIntroduction())) {
				txtTraderMotto.setText(TradeDetailActivity.currentTrader
						.getSelfIntroduction());
			}

			if (!StringUtils.isStringNone(TradeDetailActivity.currentTrader
					.getScore())) {
				float startNum = Float
						.parseFloat(TradeDetailActivity.currentTrader
								.getScore());
				rbTraderStarts.setProgress((int) startNum);
			}
		}
	}

	@Override
	public void onRefresh() {
		xlistTraderAppraise.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		traderPageCurrentSizeNo = AppContext.PAGE_SIZE;
		getTraderAppraise();
	}

	@Override
	public void onLoadMore() {
		if (listTraderAppraise != null) {
			if (listTraderAppraise.size() >= traderPageCurrentSizeNo) {
				traderPageCurrentSizeNo += AppContext.PAGE_STEP_SIZE;
			}
			getTraderAppraise();
		}
	}

	public void getTraderAppraise() {
		if (tradeId == null) {
			tradeId = TradeDetailActivity.currentTrader.getTraderId();
		}
		TraderService.getInstance(context).getTraderComment(tradeId,
				String.valueOf(traderPageNo),
				String.valueOf(traderPageCurrentSizeNo),
				new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							xlistTraderAppraise.stopRefresh();// 停止刷新
							xlistTraderAppraise.stopLoadMore();// 停止加载更多
							@SuppressWarnings("unchecked")
							List<TraderAppraise> data = (List<TraderAppraise>) baseModel
									.getResult();
							if (data != null && data.size() > 0) {
								listTraderAppraise.clear();
								listTraderAppraise.addAll(data);
								traderAppraiseAdp.notifyDataSetChanged();
							}
						}
					}

					@Override
					public void onFinish() {
						super.onFinish();
						xlistTraderAppraise.stopRefresh();// 停止刷新
						xlistTraderAppraise.stopLoadMore();// 停止加载更多
					}

					@Override
					public void onFailure(Throwable error) {
						super.onFailure(error);
						xlistTraderAppraise.stopRefresh();// 停止刷新
						xlistTraderAppraise.stopLoadMore();// 停止加载更多
					}
				});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == GO_COMMON && resultCode == REFREESH_COMMON) {
			onRefresh();
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		updateView();
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
