package com.financialnet.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.db.dao.DataBookDao;
import com.financialnet.model.Member;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.ComonService;
import com.financialnet.service.MemberService;
import com.financialnet.util.FileUtils;
import com.financialnet.util.MediaUtil;
import com.financialnet.util.PreferenceUtils;
import com.financialnet.util.UIHelper;
import com.financialnet.widget.CircleImageView;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.SwitchButton;
import com.umeng.analytics.MobclickAgent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @className：MineCenterActivity.java
 * @author: li mingtao
 * @Function: 个人中心
 * @createDate: 2014-8-18下午12:08:33
 * @update:
 */
public class MineCenterActivity extends BaseActivity implements OnClickListener {
    private String TAG = "MineCenterActivity";

    private boolean isNoEdit = true;// 不是编辑状态
    private HeaderBar headerBar;
    private View viewUserInfo, viewUserShow, viewWhiteLine;
    private View viewInvestModeBar, viewInvestTypeBar, viewInvestMarjorBar;
    private View viewUserNameBar, viewUserMottoBar, viewUserPhotoBar;
    private View viewMemberTraderBar, viewMeberStockBar, viewModifyPwdBar;
    private View viewJJ;
    private TextView textMeberName, textMeberMotto, textMode, textType,
            textMajor, textTraderName, textTraderMotto, txtPhoneShow, txtJJShow;
    List<ImageView> listArrows;
    private SwitchButton sbFlagStockPush, sbFlagConsultPush, sbFlagTraderPush;
    private SwitchButton sbFlagVoice, sbFlagShake, sbFlagSpeake;
    public static String appPath = "/FinancialNet/";
    private String userPhotoName = "img.jpg";
    public static Member editMember;
    public CircleImageView mineMemberheader, userPhoto;

    @Override
    public void lastLoad() {
        super.lastLoad();
        updateView();
    }

    public MineCenterActivity() {
        super(R.layout.activity_minecenter);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);
        headerBar.setTitle(getResString(R.string.minecenter_title));
        headerBar.setRightText(getResString(R.string.public_edit));
        headerBar.top_right_btn.setTextColor(getResColor(R.color.white));
        // for list
        mineMemberheader = (CircleImageView) findViewById(R.id.mine_memberheader);
        userPhoto = (CircleImageView) findViewById(R.id.user_photo);
        viewUserInfo = findViewById(R.id.user_info);
        viewUserShow = findViewById(R.id.user_show);
        viewWhiteLine = findViewById(R.id.white_line);
        findViewById(R.id.start_bar).setVisibility(View.GONE);

        viewInvestModeBar = findViewById(R.id.invsetmode);
        viewInvestTypeBar = findViewById(R.id.investType);
        viewInvestMarjorBar = findViewById(R.id.investMajor);
        viewUserNameBar = findViewById(R.id.user_name_bar);
        viewUserMottoBar = findViewById(R.id.user_motto_bar);
        viewUserPhotoBar = findViewById(R.id.user_photo_bar);
        viewMemberTraderBar = findViewById(R.id.member_trader_bar);
        viewMeberStockBar = findViewById(R.id.menber_stock_bar);
        viewModifyPwdBar = findViewById(R.id.menber_modifypwd);
        viewJJ = findViewById(R.id.member_jj);

        textMeberMotto = (TextView) findViewById(R.id.member_motto);
        textMeberName = (TextView) findViewById(R.id.member_name);
        textMode = (TextView) findViewById(R.id.mode);
        textType = (TextView) findViewById(R.id.type);
        textMajor = (TextView) findViewById(R.id.major);
        textTraderName = (TextView) findViewById(R.id.trade_name_);
        textTraderMotto = (TextView) findViewById(R.id.trader_motto_);
        txtPhoneShow = (TextView) findViewById(R.id.phone_show);
        txtJJShow = (TextView) findViewById(R.id.jj_show);

        sbFlagStockPush = (SwitchButton) findViewById(R.id.flagStockPush);
        sbFlagConsultPush = (SwitchButton) findViewById(R.id.flagConsultPush);
        sbFlagTraderPush = (SwitchButton) findViewById(R.id.flagTraderPush);
        sbFlagVoice = (SwitchButton) findViewById(R.id.flagchatVoice);
        sbFlagShake = (SwitchButton) findViewById(R.id.flagchatshake);
        sbFlagSpeake = (SwitchButton) findViewById(R.id.flagchatspeaker);

        listArrows = new ArrayList<ImageView>();
        listArrows.add((ImageView) findViewById(R.id.arrow_mx));
        listArrows.add((ImageView) findViewById(R.id.arrow_du));
        listArrows.add((ImageView) findViewById(R.id.arrow_pf));
        listArrows.add((ImageView) findViewById(R.id.arrow_phone));
        // listArrows.add((ImageView) findViewById(R.id.arrow_cps));
        // listArrows.add((ImageView) findViewById(R.id.arrow_kj));
        setArrow();
    }

    @Override
    public void bindViews() {
        if (AppContext.getCurrentMember().getKMemberPlan() != 1) {
            viewJJ.setVisibility(View.GONE);
        }
        viewJJ.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                JumpToActivityNoAnim(FundActivity.class);
            }
        });
        headerBar.top_right_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNoEdit) {
                    isNoEdit = false;
                    editMember = (Member) AppContext.getCurrentMember().deepClone();

                    sbFlagConsultPush.setEnabled(true);
                    sbFlagStockPush.setEnabled(true);
                    sbFlagTraderPush.setEnabled(true);
                    sbFlagVoice.setEnabled(true);
                    sbFlagSpeake.setEnabled(true);
                    sbFlagShake.setEnabled(true);

                    viewUserInfo.setVisibility(View.VISIBLE);
                    viewUserShow.setVisibility(View.GONE);
                    viewWhiteLine.setVisibility(View.GONE);
                    headerBar
                            .setRightText(getResString(R.string.public_finish));
                    setArrow();
                } else {
                    isNoEdit = true;

                    MemberService.getInstance(context).updateMemberInfo(
                            editMember, new CustomAsyncResponehandler() {
                                @Override
                                public void onSuccess(ResponeModel baseModel) {
                                    super.onSuccess(baseModel);
                                    AppContext.setCurrentMember( editMember);
                                    updateView();

                                    viewUserInfo.setVisibility(View.GONE);
                                    viewUserShow.setVisibility(View.VISIBLE);
                                    viewWhiteLine.setVisibility(View.VISIBLE);

                                    headerBar
                                            .setRightText(getResString(R.string.public_edit));
                                    setArrow();
                                }
                            });

                }
            }
        });

        headerBar.back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isNoEdit) {
                    UIHelper.showSysDialog(context,
                            getResString(R.string.minecenter_back),
                            new OnSurePress() {
                                @Override
                                public void onClick(View view) {
                                    isNoEdit = true;
                                    // --------
                                    updateView();
                                    viewUserInfo.setVisibility(View.GONE);
                                    viewUserShow.setVisibility(View.VISIBLE);
                                    viewWhiteLine.setVisibility(View.VISIBLE);

                                    headerBar
                                            .setRightText(getResString(R.string.public_edit));
                                    setArrow();
                                }
                            }, true);
                } else {
                    finish();
                }
            }
        });

        viewInvestModeBar.setOnClickListener(this);
        viewInvestTypeBar.setOnClickListener(this);
        viewInvestMarjorBar.setOnClickListener(this);
        viewUserNameBar.setOnClickListener(this);
        viewUserMottoBar.setOnClickListener(this);
        viewUserPhotoBar.setOnClickListener(this);

        viewMemberTraderBar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1) {
                    Intent intent = new Intent();
                    intent.putExtra("trade_type", TradeActivity.MY_TRADER);
                    intent.setClass(context, TradeActivity.class);
                    context.startActivity(intent);
                } else {
                    JumpToActivity(MemerPlanActivity.class);
                }
            }
        });

        viewMeberStockBar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1) {
                    JumpToActivityNoAnim(MineStockActivity.class);
                } else {
                    JumpToActivity(MemerPlanActivity.class);
                }
            }
        });

        viewModifyPwdBar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MineCenterActivity.this,
                        ConfirmTelActivity.class);
                intent.putExtra("type", ConfirmTelActivity.GO_SETNEWPWD);
                startActivity(intent);
            }
        });

        sbFlagStockPush
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton arg0,
                                                 boolean arg1) {
                        if (!isNoEdit)
                            if (arg1) {
                                editMember.setFlagStockPush(1);
                            } else {
                                editMember.setFlagStockPush(0);
                            }
                    }
                });

        sbFlagConsultPush
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton arg0,
                                                 boolean arg1) {
                        if (!isNoEdit)
                            if (arg1) {
                                editMember.setFlagConsultPush(1);
                            } else {
                                editMember.setFlagConsultPush(0);
                            }
                    }
                });

        sbFlagTraderPush
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton arg0,
                                                 boolean arg1) {
                        if (!isNoEdit)
                            if (arg1) {
                                editMember.setFlagTraderPush(1);
                            } else {
                                editMember.setFlagTraderPush(0);
                            }
                    }
                });

        sbFlagVoice.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                PreferenceUtils.getInstance(context).setSettingMsgSound(
                        isChecked);
            }
        });

        sbFlagShake.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                PreferenceUtils.getInstance(context).setSettingMsgVibrate(
                        isChecked);
            }
        });

        sbFlagSpeake.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                PreferenceUtils.getInstance(context).setSettingMsgSpeaker(
                        isChecked);
            }
        });
    }

    /**
     * 更新未修改的界面数据
     */
    protected void updateView() {
        textMeberName.setText(AppContext.getCurrentMember().getMemberName());
        textMeberMotto.setText(AppContext.getCurrentMember().getSelfIntroduction());
        textTraderMotto.setText(AppContext.getCurrentMember().getSelfIntroduction());
        textTraderName.setText(AppContext.getCurrentMember().getMemberName());
        txtPhoneShow.setText(AppContext.getCurrentMember().getMobileNumber());
        txtJJShow.setText(AppContext.getCurrentMember().getKRedSwanTotalWord());
        AppContext.setImage(AppContext.getCurrentMember().getAvatarsAddress(),
                mineMemberheader, AppContext.memberPhotoOption);
        AppContext.setImage(AppContext.getCurrentMember().getAvatarsAddress(),
                userPhoto, AppContext.memberPhotoOption);

        textMode.setText(AppContext.getCurrentMember().getTypeDescription());
        textType.setText(AppContext.getCurrentMember().getTimeDescription());
        textMajor.setText(AppContext.getCurrentMember().getSpeDescription());

        if ("1".equals(AppContext.getCurrentMember().getFlagStockPush().toString())) {
            sbFlagStockPush.setChecked(true);
        } else {
            sbFlagStockPush.setChecked(false);
        }

        if ("1".equals(AppContext.getCurrentMember().getFlagConsultPush().toString())) {
            sbFlagConsultPush.setChecked(true);
        } else {
            sbFlagConsultPush.setChecked(false);
        }

        if ("1".equals(AppContext.getCurrentMember().getFlagTraderPush().toString())) {
            sbFlagTraderPush.setChecked(true);
        } else {
            sbFlagTraderPush.setChecked(false);
        }

        sbFlagConsultPush.setEnabled(false);
        sbFlagStockPush.setEnabled(false);
        sbFlagTraderPush.setEnabled(false);
        sbFlagShake.setEnabled(false);
        sbFlagVoice.setEnabled(false);
        sbFlagSpeake.setEnabled(false);

        sbFlagVoice.setChecked(PreferenceUtils.getInstance(context)
                .getSettingMsgSound());
        sbFlagShake.setChecked(PreferenceUtils.getInstance(context)
                .getSettingMsgVibrate());
        sbFlagSpeake.setChecked(PreferenceUtils.getInstance(context)
                .getSettingMsgSpeaker());
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View arg0) {
        if (!isNoEdit) {// 编辑页面
            switch (arg0.getId()) {
                case R.id.invsetmode:
                    JumpToActivityForResult(EasySelectActivity.class,
                            DataBookDao.INVESTTYPE_TAG, DataBookDao.INVESTTYPE_TAG);
                    break;
                case R.id.investMajor:
                    JumpToActivityForResult(EasySelectActivity.class,
                            DataBookDao.INVESTSPE_TAG, DataBookDao.INVESTSPE_TAG);
                    break;
                case R.id.investType:
                    JumpToActivityForResult(EasySelectActivity.class,
                            DataBookDao.INVESTTIME_TAG, DataBookDao.INVESTTIME_TAG);
                    break;
                case R.id.user_name_bar:
                    JumpToActivityForResult(EasyEditActivity.class,
                            EasyEditActivity.EDIT_USERNAME,
                            EasyEditActivity.EDIT_USERNAME);
                    break;
                case R.id.user_motto_bar:
                    JumpToActivityForResult(EasyEditActivity.class,
                            EasyEditActivity.EDIT_USERMOTTO,
                            EasyEditActivity.EDIT_USERMOTTO);
                    break;
                case R.id.user_photo_bar:
                    showPhotoDialog(new OnSurePressNoView() {
                        @Override
                        public void onClick() {
                            if (FileUtils.isSDCardExisd()) {
                                MediaUtil.searhcAlbum(MineCenterActivity.this,
                                        MediaUtil.ALBUM);
                            } else {
                                showToast("SDCARD不存在！");
                            }
                        }
                    }, new OnSurePressNoView() {
                        @Override
                        public void onClick() {
                            if (FileUtils.isSDCardExisd()) {
                                MediaUtil.takePhoto(MineCenterActivity.this,
                                        MediaUtil.PHOTO, appPath, userPhotoName);
                            } else {
                                showToast("SDCARD不存在！");
                            }
                        }
                    });
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case DataBookDao.INVESTTYPE_TAG:
                textMode.setText(editMember.getTypeDescription());
                break;
            case DataBookDao.INVESTTIME_TAG:
                textType.setText(editMember.getTimeDescription());
                break;
            case DataBookDao.INVESTSPE_TAG:
                textMajor.setText(editMember.getSpeDescription());
                break;
            case EasyEditActivity.EDIT_USERNAME:
                textMeberName.setText(editMember.getMemberName());
                break;
            case EasyEditActivity.EDIT_USERMOTTO:
                textMeberMotto.setText(editMember.getSelfIntroduction());
                break;
            case MediaUtil.ALBUM:
                if (data != null || resultCode == Activity.RESULT_OK) {
                    startPhotoZoom(data.getData());
                }
                break;
            case MediaUtil.PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    File temp = new File(MediaUtil.ROOTPATH + appPath
                            + userPhotoName);
                    startPhotoZoom(Uri.fromFile(temp));
                }
                break;
            case MediaUtil.ZOOMPHOTO:
                if (data != null || resultCode == Activity.RESULT_OK) {
                    setPicToView(data);
                }
                break;
        }
    }

    private void setPicToView(Intent data) {
        Bundle extras = data.getExtras();
        if (extras != null) {
            final Bitmap img = extras.getParcelable("data");
            userPhoto.setImageBitmap(img);
            ComonService.getInstance(context).uploadImg(
                    AppContext.getCurrentMember().getMemberID() + "",
                    new File(MediaUtil.ROOTPATH + appPath + userPhotoName),
                    new CustomAsyncResponehandler() {
                        @Override
                        public void onSuccess(ResponeModel baseModel) {
                            super.onSuccess(baseModel);
                            if (baseModel.isStatus()) {
                                editMember.setAvatarsAddress(baseModel
                                        .getData());
                                Log.d(TAG, baseModel.getData());
                                userPhoto.setImageBitmap(img);
                            }
                        }
                    });
        }
    }

    /**
     * 裁剪图片方法实现
     *
     * @param uri
     */
    public void startPhotoZoom(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        // 下面这个crop=true是设置在开启的Intent中设置显示的VIEW可裁剪
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        File dir = new File((MediaUtil.ROOTPATH + appPath));
        if (!dir.exists())
            dir.mkdirs();

        File saveFile = new File(MediaUtil.ROOTPATH + appPath + userPhotoName);

        intent.putExtra("output", Uri.fromFile(saveFile)); // 传入目标文件
        intent.putExtra("outputFormat", "JPEG"); // 输入文件格式
        Intent it = Intent.createChooser(intent,
                getResString(R.string.edit_photo));
        startActivityForResult(it, MediaUtil.ZOOMPHOTO);
    }

    /**
     * 箭头的设置
     */
    public void setArrow() {
        for (ImageView view : listArrows) {
            if (isNoEdit) {
                view.setVisibility(View.GONE);
            } else {
                view.setVisibility(View.VISIBLE);
            }
        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        MemberService.getInstance(context).update(AppContext.getCurrentMember().getMobileNumber(),
                AppContext.getCurrentMember().getPassword(),
                new CustomAsyncResponehandler() {
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel.isStatus()) {

                            updateView();
                        }
                    }
                });


    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
