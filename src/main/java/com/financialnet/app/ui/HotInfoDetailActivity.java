package com.financialnet.app.ui;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageView;

import com.financialnet.app.R;
import com.financialnet.model.HotInfo;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.StockService;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

/**
 * @className：InfoDetailActivity.java
 * @author: li mingtao
 * @Function: 热门资讯详情
 * @createDate: 2014-8-18下午5:21:28
 * @update:
 */
public class HotInfoDetailActivity extends BaseActivity {
	public static final int COMEFROM_NULL = 0;// 热门资讯
	public static final int COMEFROM_HOTINFO = 1;// 热门资讯
	public static final int COMEFROM_STOCKNEW = 2;// 股票新闻
	// ---------------------
	private int comeFrom;
	private HeaderBar headerBar;
	// for conten
	private HotInfo detailData;

	private ImageView imgHotNew;
	private WebView webHotDetail;
	private String title = null;

	public HotInfoDetailActivity() {
		super(R.layout.activity_infodetail);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		comeFrom = getIntent().getIntExtra("comefrom", 0);
		detailData = (HotInfo) getIntent().getExtras().get("hotInfo");
		title = getIntent().getStringExtra("title");
		super.onCreate(savedInstanceState);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		// for Content
		webHotDetail = (WebView) findViewById(R.id.hotnewsDetail);
		switch (comeFrom) {
		case COMEFROM_NULL:
			webHotDetail.loadDataWithBaseURL(null, detailData.getContent(),
					"text/html", "utf-8", null);
			break;
		case COMEFROM_HOTINFO:
			headerBar.setTitle(getResString(R.string.hot_title));
			webHotDetail.loadDataWithBaseURL(null, detailData.getContent(),
					"text/html", "utf-8", null);
			break;
		case COMEFROM_STOCKNEW:
			headerBar.setTitle(getResString(R.string.stock_aboutnews));
			getMyStockDeail();
			break;
		}
		// if (detailData != null) {
		// imgviewHotimg.setBackgroundResource(detailData.getHotImg());
		// textDetailTitle.setText(detailData.getHotTitle());
		// textDetailContent.setText(detailData.getHotDetail());
		// textDetailTime.setText(detailData.getDateTime());
		// }
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		if (title != null) {
			headerBar.setTitle(title);
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	/**
	 * 获取我的股票新闻详情
	 */
	private void getMyStockDeail() {
		StockService.getInstance(context).getMyStockNewDetail(
				detailData.getId(), new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							detailData = (HotInfo) baseModel.getResult();
							updateStockNewView();
						}
					}
				});
	}

	/**
	 * 更新股票详情视图
	 */
	private void updateStockNewView() {
		if (detailData == null)
			return;
		webHotDetail.loadDataWithBaseURL(null, detailData.getContent(),
				"text/html", "utf-8", null);

	}
}
