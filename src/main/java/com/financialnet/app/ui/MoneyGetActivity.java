package com.financialnet.app.ui;


import android.content.DialogInterface;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.MemberService;

import com.financialnet.widget.HeaderBar;


public class MoneyGetActivity extends BaseActivity {
    private HeaderBar headerBar;
    private int type=1;
    private TextView tvTitle;
    private TextView tvContent;
    private Button btnGet;
    public MoneyGetActivity() {
        super(R.layout.activity_getmoney);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.header);
        headerBar.setTitle("红天鹅基金");
        tvTitle= (TextView) findViewById(R.id.tv_title);
        btnGet= (Button) findViewById(R.id.btn_get);
        tvContent= (TextView) findViewById(R.id.tv_content);

    }

    @Override
    public void initData() {
        type=getIntent().getIntExtra("type", 1);
    }

    @Override
    public void bindViews() {
        if (type == 1) {//现金
            tvTitle.setText("提取现金");
            tvContent.setText("阁下愿意提取全部现金，本公司将会把全部现金直接转账到阁下之户口。");
            if (AppContext.getCurrentMember().getKLastWDCashStatus() == 0) {
                btnGet.setBackgroundColor(getResColor(R.color.gray_normal));
                btnGet.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showToast("提取正在审核中,请耐心等待。");
                    }
                });
            } else if (AppContext.getCurrentMember().getKCashOnHand() == 0) {
                btnGet.setBackgroundColor(getResColor(R.color.gray_normal));
                btnGet.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showToast("没有现金可提取");
                    }
                });
            } else {
                btnGet.setBackgroundColor(getResColor(R.color.red));
                btnGet.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final EditText inputServer = new EditText(MoneyGetActivity.this);
                        inputServer.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MoneyGetActivity.this);
                        builder.setTitle("输入密码").setMessage("请输入密码，已做核实。").setView(inputServer)
                                .setNegativeButton("取消", null);
                        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                String pwd = inputServer.getText().toString();
                                MemberService.getInstance(MoneyGetActivity.this).redSwanCashWithdrawal(AppContext.getCurrentMember().getMobileNumber() + "", pwd, "kCashOnHand", new CustomAsyncResponehandler() {
                                    public void onFinish() {
                                    }

                                    ;

                                    @SuppressWarnings("unchecked")
                                    @Override
                                    public void onSuccess(ResponeModel baseModel) {
                                        super.onSuccess(baseModel);
                                        if (baseModel != null && baseModel.isStatus()) {
                                            showToast(baseModel.getMsg());
                                            MoneyGetActivity.this.finish();
                                        }
                                    }
                                });
                            }
                        });
                        builder.show();
                    }
                });
            }
         }else {//本金
            tvTitle.setText("提取本金");
            tvContent.setText("阁下愿意提取全部本金，本公司将会把全部本金直接转账到阁下之户口。");
            if (AppContext.getCurrentMember().getKLastWDCapiatlStatus() == 0) {
                btnGet.setBackgroundColor(getResColor(R.color.gray_normal));
                btnGet.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showToast("提取正在审核中,请耐心等待。");
                    }
                });
            } else if (AppContext.getCurrentMember().getKIsAfterPenaltyTime() == 0) {
                btnGet.setBackgroundColor(getResColor(R.color.red));
                btnGet.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showSureDialog(MoneyGetActivity.this, "各下之理财计划尚未\n" +
                                "到期，如要提取，本公司将收取相当于本⾦的15％作为⼿续费，请确认。", new OnSurePress() {
                            @Override
                            public void onClick(View view) {
                                final EditText inputServer = new EditText(MoneyGetActivity.this);
                                inputServer.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MoneyGetActivity.this);
                                builder.setTitle("输入密码").setMessage("请输入密码，已做核实。").setView(inputServer)
                                        .setNegativeButton("取消", null);
                                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        String pwd = inputServer.getText().toString();
                                        MemberService.getInstance(MoneyGetActivity.this).redSwanCapitalWithdrawal(AppContext.getCurrentMember().getMobileNumber() + "", pwd, new CustomAsyncResponehandler() {
                                            public void onFinish() {
                                            }

                                            ;

                                            @SuppressWarnings("unchecked")
                                            @Override
                                            public void onSuccess(ResponeModel baseModel) {
                                                super.onSuccess(baseModel);
                                                if (baseModel != null && baseModel.isStatus()) {
                                                    showToast(baseModel.getMsg());
                                                    MoneyGetActivity.this.finish();
                                                }
                                            }
                                        });
                                    }
                                });
                                builder.show();
                            }
                        }, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {

                            }
                        });

                    }
                });
            } else {
                btnGet.setBackgroundColor(getResColor(R.color.red));
                btnGet.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final EditText inputServer = new EditText(MoneyGetActivity.this);
                        inputServer.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MoneyGetActivity.this);
                        builder.setTitle("输入密码").setMessage("请输入密码，已做核实。").setView(inputServer)
                                .setNegativeButton("取消", null);
                        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                String pwd = inputServer.getText().toString();
                        MemberService.getInstance(MoneyGetActivity.this).redSwanCapitalWithdrawal(AppContext.getCurrentMember().getMobileNumber() + "", pwd, new CustomAsyncResponehandler() {
                                    public void onFinish() {
                                    }

                                    ;

                                    @SuppressWarnings("unchecked")
                                    @Override
                                    public void onSuccess(ResponeModel baseModel) {
                                        super.onSuccess(baseModel);
                                        if (baseModel != null && baseModel.isStatus()) {
                                            showToast(baseModel.getMsg());
                                            MoneyGetActivity.this.finish();
                                        }
                                    }
                                });
                            }
                        });
                        builder.show();
                    }
                });
            }
        }
    }}


