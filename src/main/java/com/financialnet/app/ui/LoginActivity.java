package com.financialnet.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.easemob.EMCallBack;
import com.easemob.chat.EMGroup;
import com.easemob.chat.EMGroupManager;
import com.easemob.exceptions.EaseMobException;
import com.financialnet.app.AppConfig;
import com.financialnet.app.AppContext;
import com.financialnet.app.AppManager;
import com.financialnet.app.R;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.IMService;
import com.financialnet.service.MemberService;
import com.financialnet.util.PreferenceUtils;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

import java.util.List;

public class LoginActivity extends BaseActivity {
	private HeaderBar headerBar;
	private Button btnLogin, btnRegister;
	private EditText etAccount, etPassWord;
	private TextView textForgetPwd;
	private String mobileNumber, password;
	private CheckBox checkRember;
	private RadioGroup pay_;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public LoginActivity() {
		super(R.layout.activity_login);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.login_title));
		headerBar.back.setVisibility(View.GONE);

		btnLogin = (Button) findViewById(R.id.login);
		btnRegister = (Button) findViewById(R.id.go_register);
		etAccount = (EditText) findViewById(R.id.user_account);
		etAccount.setText(PreferenceUtils.getInstance(context)
				.getLastUserAccount());
		etPassWord = (EditText) findViewById(R.id.user_pwd);
		textForgetPwd = (TextView) findViewById(R.id.forget_pwd);
		checkRember = (CheckBox) findViewById(R.id.rember_pwd);
		pay_ = (RadioGroup) findViewById(R.id.paybar);
		// for test
		if (AppConfig.isDevelopMode) {
			etAccount.setText("13535370508");
			etPassWord.setText("123456");
			findViewById(R.id.pay_panel).setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		checkRember.setVisibility(View.GONE);
		if (AppConfig.isDevelopMode) {
			pay_.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(RadioGroup arg0, int chekedid) {
					switch (chekedid) {
					case R.id.payed:
						etAccount.setText("13147567830");
						etPassWord.setText("123");
						break;
					case R.id.nopayed:
						etAccount.setText("13535370508");
						etPassWord.setText("123456");
						break;
					}
				}
			});
		}
		
		btnRegister.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				JumpToActivityNoAnim(BindTelActivity.class);
			}
		});

		btnLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mobileNumber = etAccount.getText().toString();
				password = etPassWord.getText().toString();
				if ("".equals(mobileNumber) && "".equals(password)) {
					showToast(getResString(R.string.login_no_nameandpwd));
					return;
				}
				if (cantEmpty(etAccount, R.string.login_inputname))
					return;
				if (cantEmpty(etPassWord, R.string.login_inputpwd))
					return;
				MemberService.getInstance(context).login(mobileNumber,
						password, true,	new EMCallBack() {
							@Override
							public void onSuccess() {
								new Thread() {
									public void run() {
										// 存储所有群组
										try {
											List<EMGroup> grouplist = EMGroupManager.getInstance().getGroupsFromServer();
										} catch (EaseMobException e) {
											e.printStackTrace();
										}
									};
								}.start();
							}

							@Override
							public void onProgress(int progress, String status) {
								Log.d("HX", "onProgress");
							}

							@Override
							public void onError(int code, final String message) {
								Log.d("HX", "onError");

							}
						},
						new CustomAsyncResponehandler() {
							@Override
							public void onSuccess(ResponeModel baseModel) {
								super.onSuccess(baseModel);
								if (baseModel.isStatus()) {
									PreferenceUtils.getInstance(context)
											.setLastUserAccount(mobileNumber);
									// 获取用户好友列表
									IMService
											.getInstance(context)
											.getFriendsListS(
													mobileNumber,
													new CustomAsyncResponehandler() {
														@Override
														public void onSuccess(
																ResponeModel baseModel) {
															super.onSuccess(baseModel);
															if (baseModel
																	.isStatus()) {
																AppContext
																		.getApplication()
																		.setUserName(
																				mobileNumber);
																JumpToActivityNoAnim(MainActivity.class);
																AppContext
																		.clearData();
																finish();
															}
														}
													}, true);

								}
							}
						});
			}
		});

		textForgetPwd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(LoginActivity.this,
						ConfirmTelActivity.class);
				intent.putExtra("type", ConfirmTelActivity.GO_FORGETPWD);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			AppManager.getAppManager().AppExit(LoginActivity.this);
		}
		return false;
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
