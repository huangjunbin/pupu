package com.financialnet.app.ui;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.NotificationAdapter;
import com.financialnet.db.dao.NotificationDao;
import com.financialnet.model.Notification;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.ComonService;
import com.financialnet.util.DateUtil;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @className：NotificationActivity.java
 * @author: li mingtao
 * @Function: 热门资讯
 * @createDate: 2014-8-18下午3:52:08
 * @update:
 */
public class NotificationActivity extends BaseActivity implements
        IXListViewListener {
    String TAG = "NotificationActivity";
    private HeaderBar headerBar;

    private XListView xlistNotification;
    private List<Notification> listNotificationData;
    private NotificationAdapter NotificationAdp;
    private NotificationDao dao;

    @Override
    public void lastLoad() {
        super.lastLoad();
        onRefresh();
    }

    public NotificationActivity() {
        super(R.layout.activity_notification);
    }

    @Override
    public void initViews() {
        // for title
        headerBar = (HeaderBar) findViewById(R.id.title);
        headerBar.setTitle("操盘手提示");

        // for Xlist
        xlistNotification = (XListView) findViewById(R.id.notification_list);
        xlistNotification.setHeaderDividersEnabled(false);
        xlistNotification.setFooterDividersEnabled(false);
        xlistNotification.setPullLoadEnable(false);
        xlistNotification.setPullRefreshEnable(true);
    }

    @Override
    public void initData() {
        listNotificationData = new ArrayList<Notification>();
        dao = new NotificationDao(this);
        NotificationAdp = new NotificationAdapter(this, listNotificationData);
        xlistNotification.setAdapter(NotificationAdp);
    }

    @Override
    public void bindViews() {
        xlistNotification.setXListViewListener(this);
    }

    private void getNotificationList() {

        ComonService.getInstance(context).getSysNotification(
                AppContext.getCurrentMember().getMemberID() + "",
                new CustomAsyncResponehandler() {

                    @Override
                    public void onFinish() {
                        xlistNotification.stopRefresh();
                        super.onFinish();
                    }

                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel.isStatus()) {

                            @SuppressWarnings("unchecked")
                            List<Notification> tempNotification = (List<Notification>) baseModel
                                    .getResult();
                            if (tempNotification != null
                                    && tempNotification.size() > 0) {
                                listNotificationData.clear();
                                listNotificationData.addAll(tempNotification);
                                NotificationAdp.notifyDataSetChanged();
                                new Thread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dao.setNotification(listNotificationData);

                                    }
                                }).start();

                            }

                        }
                    }
                });

    }

    public void onResume() {
        super.onResume();
        listNotificationData.clear();
        if (dao.findAll() != null) {
            listNotificationData.addAll(0, dao.findAll());
        }
        NotificationAdp.notifyDataSetChanged();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    public void onRefresh() {
        xlistNotification.setRefreshTime(DateUtil.getDateTime(new Date(System
                .currentTimeMillis())));

        getNotificationList();
    }

    @Override
    public void onLoadMore() {

        getNotificationList();
    }
}
