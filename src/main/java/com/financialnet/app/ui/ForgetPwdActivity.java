package com.financialnet.app.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.financialnet.app.R;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

public class ForgetPwdActivity extends BaseActivity {
	private HeaderBar headerBar;
	private Button btnSubmit;

	public ForgetPwdActivity() {
		super(R.layout.activity_forgetpwd);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.login_forget_pwd));

		btnSubmit = (Button) findViewById(R.id.submit);
	}
	
	@Override
	public void initData() {
		
	}
	
	@Override
	public void bindViews() {
		btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				
			}
		});
	}
	
	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
