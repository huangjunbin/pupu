package com.financialnet.app.ui;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.easemob.chat.EMChat;
import com.easemob.chat.EMChatManager;
import com.easemob.chat.EMContactListener;
import com.easemob.chat.EMContactManager;
import com.easemob.chat.EMConversation;
import com.easemob.chat.EMMessage;
import com.easemob.chat.EMNotifier;
import com.easemob.util.HanziToPinyin;
import com.financialnet.app.AppConfig;
import com.financialnet.app.AppContext;
import com.financialnet.app.AppManager;
import com.financialnet.app.AppStart;
import com.financialnet.app.R;
import com.financialnet.app.adapter.MainNoPayExpandableListAdapter;
import com.financialnet.app.adapter.MainPayExpandableListAdapter;
import com.financialnet.app.adapter.MenuExpandableListAdapter;
import com.financialnet.app.adapter.SearchStockAdapter;
import com.financialnet.db.InviteMessgeDao;
import com.financialnet.db.dao.NotificationDao;
import com.financialnet.model.InviteMessage;
import com.financialnet.model.InviteMessage.InviteMesageStatus;
import com.financialnet.model.Member;
import com.financialnet.model.MenuClass;
import com.financialnet.model.NoPayResult;
import com.financialnet.model.PayResult;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.SearchStock;
import com.financialnet.model.User;
import com.financialnet.model.Version;
import com.financialnet.net.http.AsyncHttpClient;
import com.financialnet.net.http.AsyncHttpResponseHandler;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.net.http.RequestParams;
import com.financialnet.service.ComonService;
import com.financialnet.service.MemberService;
import com.financialnet.service.UpdateService;
import com.financialnet.service.VersionService;
import com.financialnet.util.StringUtils;
import com.financialnet.util.UIHelper;
import com.financialnet.widget.CircleImageView;
import com.financialnet.widget.SlidingMenu;
import com.financialnet.widget.XListView.IXListViewListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import cn.jpush.android.api.JPushInterface;

/**
 * @className：MainActivity.java
 * @author: allen
 * @Function: 首页
 * @createDate: 2014-8-28
 * @update:
 */
@SuppressLint("DefaultLocale")
public class MainActivity extends BaseActivity implements IXListViewListener,
        TextWatcher {
    String TAG = "MainActivity";
    private SlidingMenu slideMenu;
    private long mExitTime; // 退出时间
    private static final int INTERVAL = 2000; // 退出间隔
    private View mLeft, mCenter;
    private ExpandableListView listview_type;
    private ExpandableListView listview_main;
    private MenuExpandableListAdapter menuExpandableListAdapter;
    private MainPayExpandableListAdapter mainExpandableListAdapter;
    private MainNoPayExpandableListAdapter mainNoPayExpandableListAdapter;
    private List<MenuClass> armTypes;
    private List<MenuClass> mainPayArmTypes;
    private List<MenuClass> mainNoPayArmTypes;
    private List<MenuClass> armTypes3;
    private List<MenuClass> armTypes5;
    private EditText edtextSearchStock;
    private InputMethodManager mInputMethodManager;
    private ImageButton imgbtnClearSearch;
    public View lyStockContainer, lyStockSearch, rlSearch_container;
    private ListView listviewSearchList;
    private List<SearchStock> listSearchStock;
    private SearchStockAdapter searchStockAdp;
    private Intent intent;
    private ImageView imgMenu, imgSearch;
    private TextView textMenuUserName;
    private ImageView mHeaderImage;
    private TextView mHeaderTitle;
    // -----------------------------------
    private boolean isFirstGetHomePageData = true;
    private PayResult payResData;
    // -----------------------------------
    private NoPayResult noPayResData;
    // -------------------------------------
    private View viewMianPayTextBar;
    private View buttom, top;
    private CircleImageView menuUserheader;
    private boolean isConflict = false;
    private NewMessageBroadcastReceiver msgReceiver;
    // --------------------------------------
    private View viewNopay;
    private ImageButton imgPayClose;
    private Button btnPay;
    private SwipeRefreshLayout swipeLayout;
    public static boolean isForeground = false;

    // for receive customer msg from jpush server
    private MessageReceiver mMessageReceiver;
    public static final String MESSAGE_RECEIVED_ACTION = "com.fullteem.washcar.MESSAGE_RECEIVED_ACTION";
    public static final String KEY_TITLE = "title";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_EXTRAS = "extras";
    private NotificationDao dao;
    private TextView tvNum;

    @Override
    public void lastLoad() {
        super.lastLoad();
        refreshData();
        ComonService.getInstance(context).getDataBook(
                new CustomAsyncResponehandler());
    }

    private void updateView() {
        AppContext.setImage(AppContext.getCurrentMember().getAvatarsAddress(),
                menuUserheader, AppContext.memberPhotoOption);
        // -------------------------
        textMenuUserName.setText(AppContext.getCurrentMember().getMemberName());
    }

    public MainActivity() {
        super(R.layout.activity_main);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) { // back键
            exit();
        }
        return false;
    }

    /**
     * 判断两次返回时间间隔,小于两秒则退出程序
     */
    private void exit() {
        if (System.currentTimeMillis() - mExitTime > INTERVAL) {
            showToast(getResString(R.string.main_exit));
            mExitTime = System.currentTimeMillis();
        } else {
            JPushInterface.stopPush(this);
            JPushInterface.clearAllNotifications(this);
            AppContext.getApplication().logout();
            AppManager.getAppManager().AppExit(MainActivity.this);

        }
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onLoadMore() {

    }

    @SuppressLint("InflateParams")
    @Override
    public void initViews() {
        slideMenu = (SlidingMenu) findViewById(R.id.slide_menu);
        mLeft = getLayoutInflater().inflate(R.layout.layout_menu, null);
        listview_type = (ExpandableListView) mLeft
                .findViewById(R.id.listview_menu);
        slideMenu.setLeftView(mLeft);
        mCenter = getLayoutInflater().inflate(R.layout.activity_center, null);
        buttom = getLayoutInflater().inflate(R.layout.activity_main_buttom,
                null);
        top = getLayoutInflater().inflate(R.layout.activity_main_top, null);
        slideMenu.setCenterView(mCenter);
        slideMenu.setRightView(new View(this));
        slideMenu.setCanSliding(true, false);
        imgMenu = (ImageView) findViewById(R.id.title_bar_menu_btn);
        imgSearch = (ImageView) findViewById(R.id.title_search);
        tvNum = (TextView) findViewById(R.id.tv_num);
        edtextSearchStock = (EditText) mCenter.findViewById(R.id.search_edit);
        edtextSearchStock.addTextChangedListener(this);
        swipeLayout = (SwipeRefreshLayout) mCenter
                .findViewById(R.id.swipeLayout);
        lyStockSearch = mCenter.findViewById(R.id.stock_search);
        listviewSearchList = (ListView) mCenter.findViewById(R.id.search_list);
        listviewSearchList
                .setEmptyView(mCenter.findViewById(R.id.search_empty));

        imgbtnClearSearch = (ImageButton) findViewById(R.id.ib_clear_text);

        rlSearch_container = mCenter.findViewById(R.id.search_container);
        listview_main = (ExpandableListView) mCenter
                .findViewById(R.id.listview_main);

        textMenuUserName = (TextView) mLeft.findViewById(R.id.menu_userName);
        menuUserheader = (CircleImageView) mLeft
                .findViewById(R.id.menu_userheader);
        mHeaderImage = (ImageView) top.findViewById(R.id.image);
        mHeaderTitle = (TextView) top.findViewById(R.id.tv_item);
        viewMianPayTextBar = top.findViewById(R.id.mian_pay_textbar);
        // ----
        viewNopay = mCenter.findViewById(R.id.pay_guide_bar);
        imgPayClose = (ImageButton) viewNopay.findViewById(R.id.pay_close);
        btnPay = (Button) viewNopay.findViewById(R.id.popbutton);

        long now = System.currentTimeMillis();
        String appKey = "A6988225465739" + "UZ" + "F78217AD-E42E-559F-418F-9F96BF16943D" + "UZ" + now;
        String digest = encryptToSHA(appKey) + "." + now;
        AsyncHttpClient ah = new AsyncHttpClient();
        ah.addHeader("X-APICloud-AppId", "A6988225465739");
        ah.addHeader("X-APICloud-AppKey", digest);
        ah.get(this, "https://d.apicloud.com/mcm/api/Data/55763c311bf0964b69d446e2", new RequestParams(), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String content) {
                System.out.println(content);
                try {
                    JSONObject object = new JSONObject(content);
                    int isShow = object.getInt("isShow");
                    if (isShow == 0) {
                        Toast.makeText(MainActivity.this, "程序异常，请联系开发人员", Toast.LENGTH_LONG).show();
                        AppManager.getAppManager().finishAllActivity();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                super.onSuccess(content);
            }

            @Override
            public void onFailure(Throwable error, String content) {
                super.onFailure(error, content);
            }
        });
    }

    public static String byte2hex(byte[] b) {
        String hs = "";
        String stmp = "";
        for (int n = 0; n < b.length; n++) {
            stmp = (java.lang.Integer.toHexString(b[n] & 0XFF));
            if (stmp.length() == 1) {
                hs = hs + "0" + stmp;
            } else {
                hs = hs + stmp;
            }
        }
        return hs;
    }

    //SHA1 加密实例
    public static String encryptToSHA(String info) {
        byte[] digesta = null;
        try {
// 得到一个SHA-1的消息摘要
            MessageDigest alga = MessageDigest.getInstance("SHA-1");
// 添加要进行计算摘要的信息
            alga.update(info.getBytes());
// 得到该摘要
            digesta = alga.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
// 将摘要转为字符串
        String rs = byte2hex(digesta);
        return rs;
    }

    @SuppressLint("UseSparseArrays")
    @Override
    public void initData() {
        dao = new NotificationDao(this);
        JPushInterface.setDebugMode(true);
        JPushInterface.init(this);
        JPushInterface.resumePush(this);
        inviteMessgeDao = new InviteMessgeDao(this);

        mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        listSearchStock = new ArrayList<SearchStock>();


        searchStockAdp = new SearchStockAdapter(this, listSearchStock,
                listviewSearchList);
        // mainTradeList = new ArrayList<MainTrade>();
        // todayToucsList = new ArrayList<MainTodayFocus>();
        // stockList = new ArrayList<Stock>();

        armTypes = new ArrayList<MenuClass>();

        if (AppContext.getCurrentMember() == null) {
            Member member = MemberService.getInstance(context)
                    .getCacheMember();

            if (member != null) {
                AppContext.setCurrentMember(member);
            }
        }
        if (AppContext.getCurrentMember() == null) {
            startActivity(new Intent(this, AppStart.class));
            finish();
            return;
        }
        if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1) {
            MenuClass m0 = new MenuClass();
            m0.setCat_name(getResString(R.string.m0));
            armTypes.add(m0);
            MenuClass m1 = new MenuClass();
            m1.setCat_name(getResString(R.string.m1));
            armTypes.add(m1);
            MenuClass m11 = new MenuClass();
            m11.setCat_name(getResString(R.string.m11));
            armTypes.add(m11);
        }

        MenuClass m2 = new MenuClass();
        m2.setCat_name(getResString(R.string.m2));
        armTypes.add(m2);

        MenuClass m3 = new MenuClass();
        m3.setCat_name(getResString(R.string.m3));
        armTypes.add(m3);
        MenuClass m4 = new MenuClass();
        m4.setCat_name(getResString(R.string.m4));
        armTypes.add(m4);
        if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1) {
            MenuClass m5 = new MenuClass();
            m5.setCat_name(getResString(R.string.m5));
            armTypes.add(m5);
        }
        MenuClass m6 = new MenuClass();
        m6.setCat_name(getResString(R.string.m6));
        armTypes.add(m6);
        MenuClass m7 = new MenuClass();
        m7.setCat_name(getResString(R.string.m7));
        armTypes.add(m7);
        MenuClass m77 = new MenuClass();
        m77.setCat_name(getResString(R.string.m77));
        armTypes.add(m77);
        MenuClass m88 = new MenuClass();
        m88.setCat_name("通告");
        armTypes.add(m88);
        MenuClass m8 = new MenuClass();
        m8.setCat_name(getResString(R.string.m8));
        armTypes.add(m8);

        armTypes3 = new ArrayList<MenuClass>();
        MenuClass c1 = new MenuClass();
        c1.setCat_name(getResString(R.string.c1));
        armTypes3.add(c1);
        MenuClass c2 = new MenuClass();
        c2.setCat_name(getResString(R.string.c2));
        armTypes3.add(c2);
        MenuClass c3 = new MenuClass();
        c3.setCat_name(getResString(R.string.c3));
        armTypes3.add(c3);

        armTypes5 = new ArrayList<MenuClass>();

        MenuClass c6 = new MenuClass();
        c6.setCat_name(getResString(R.string.c6));
        c6.setNum(2);
        armTypes5.add(c6);
        MenuClass c7 = new MenuClass();
        c7.setCat_name(getResString(R.string.c7));
        c7.setNum(4);
        armTypes5.add(c7);


        mainPayArmTypes = new ArrayList<MenuClass>();
        MenuClass mc1 = new MenuClass();
        mc1.setCat_name("操盘手最新动向");
        mainPayArmTypes.add(mc1);
        MenuClass mc2 = new MenuClass();
        mc2.setCat_name("今日关注");
        mainPayArmTypes.add(mc2);
        /*MenuClass mc11 = new MenuClass();
        mc11.setCat_name("投资教室");
		mainPayArmTypes.add(mc11);*/
        MenuClass mc12 = new MenuClass();
        mc12.setCat_name("博客专栏");
        mainPayArmTypes.add(mc12);
        MenuClass mc3 = new MenuClass();
        mc3.setCat_name("我的股票");
        mainPayArmTypes.add(mc3);
        MenuClass mc4 = new MenuClass();
        mc4.setCat_name("操盘手排行榜");
        mainPayArmTypes.add(mc4);

        // 主页未支付数据
        mainNoPayArmTypes = new ArrayList<MenuClass>();
        MenuClass mc5 = new MenuClass();
        mc5.setCat_name("操盘手");
        mainNoPayArmTypes.add(mc5);
        MenuClass mc6 = new MenuClass();
        mc6.setCat_name("博客专栏");
        mainNoPayArmTypes.add(mc6);
        MenuClass mc7 = new MenuClass();
        mc7.setCat_name("会员最新评论");
        mainNoPayArmTypes.add(mc7);
        // MenuClass mc8 = new MenuClass();
        // mc8.setCat_name("操盘手30天前动态");
        // mainNoPayArmTypes.add(mc8);
        MenuClass mc9 = new MenuClass();
        mc9.setCat_name("我的股票");
        mainNoPayArmTypes.add(mc9);
        MenuClass mc10 = new MenuClass();
        mc10.setCat_name("操盘手排行榜");
        mainNoPayArmTypes.add(mc10);
        // 注册一个接收消息的BroadcastReceiver
        msgReceiver = new NewMessageBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter(EMChatManager
                .getInstance().getNewMessageBroadcastAction());
        intentFilter.setPriority(3);
        registerReceiver(msgReceiver, intentFilter);

        // 注册一个ack回执消息的BroadcastReceiver
        IntentFilter ackMessageIntentFilter = new IntentFilter(EMChatManager
                .getInstance().getAckMessageBroadcastAction());
        ackMessageIntentFilter.setPriority(3);
        registerReceiver(ackMessageReceiver, ackMessageIntentFilter);
        // setContactListener监听联系人的变化等
        EMContactManager.getInstance().setContactListener(
                new MyContactListener());
        EMChat.getInstance().setAppInited();
        // 获取群聊列表(群聊里只有groupid和groupname的简单信息),sdk会把群组存入到内存和db中
        registerMessageReceiver();
    }

    @Override
    public void bindViews() {
        if (AppContext.getCurrentMember().getFlagIsConfirmed() == null) {
            AppContext.getCurrentMember().setFlagIsConfirmed(0);
        }

        textMenuUserName.setText(AppContext.getCurrentMember().getMemberName());

        menuExpandableListAdapter = new MenuExpandableListAdapter(this);
        menuExpandableListAdapter.setArmTypes(armTypes);
        if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1) {
            menuExpandableListAdapter.setArmItem(4, armTypes3);
            menuExpandableListAdapter.setArmItem(6, armTypes5);
        } else if (AppContext.getCurrentMember().getFlagIsConfirmed() == 0) {
            menuExpandableListAdapter.setArmItem(1, armTypes3);
           // menuExpandableListAdapter.setArmItem(3, armTypes5);
        }
        listview_type.setAdapter(menuExpandableListAdapter);
        listview_type.setGroupIndicator(null);
        listview_main.addHeaderView(top);
        listview_main.addFooterView(buttom);
        if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1) {
            viewMianPayTextBar.setVisibility(View.VISIBLE);
            mHeaderTitle.setVisibility(View.GONE);
            // --------
        }

        // else if (AppContext.getCurrentMember().getFlagIsConfirmed() == 0) {
        // // 未支付设配器
        // mHeaderImage.setImageDrawable(getResources().getDrawable(
        // R.drawable.empty_photo));
        // }

        listview_main.setOnGroupClickListener(new OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return true;
            }
        });

        listview_main.setGroupIndicator(null);
        // setListViewHeightBasedOnChildren(listview_main);
        listview_type.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                exlistChildClick(groupPosition, childPosition);
                return false;
            }
        });

        listview_type.setOnGroupClickListener(new OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                exlistGroupClick(groupPosition);
                return false;
            }
        });

        imgbtnClearSearch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                edtextSearchStock.setText("");
                mInputMethodManager.hideSoftInputFromWindow(
                        edtextSearchStock.getWindowToken(), 0);
            }
        });
        lyStockSearch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mInputMethodManager.hideSoftInputFromWindow(
                        edtextSearchStock.getWindowToken(), 0);
            }
        });
        imgMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                slideMenu.showLeftView();
            }
        });
        imgSearch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (rlSearch_container.getVisibility() == View.VISIBLE) {
                    rlSearch_container.setVisibility(View.GONE);

                } else {
                    rlSearch_container.setVisibility(View.VISIBLE);

                }

            }
        });

        swipeLayout
                .setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        refreshData();

                    }
                });
        // ------------
        imgPayClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                viewNopay.setVisibility(View.GONE);
            }
        });

        btnPay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                viewNopay.setVisibility(View.GONE);
                JumpToActivity(MemerPlanActivity.class);
            }
        });

    }

    /**
     * ExpandableListView子项监听
     *
     * @param groupPosition
     * @param childPosition
     */
    public void exlistChildClick(int groupPosition, int childPosition) {
        if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1) {
            childClick(groupPosition, childPosition);
        } else if (AppContext.getCurrentMember().getFlagIsConfirmed() == 0) {// 未付费
            childNoClick(groupPosition + 1, childPosition);
        }
    }

    /**
     * 菜单子项单机监听
     *
     * @param groupPosition
     * @param childPosition
     */
    private void childClick(int groupPosition, int childPosition) {
        if (groupPosition == 4) {// 操盘手排行榜
            switch (childPosition) {
                case 0:
                    intent = new Intent();
                    intent.putExtra("trade_type", TradeActivity.SEVEVN_PROFIT_RANK);
                    intent.setClass(context, TradeActivity.class);
                    context.startActivity(intent);
                    break;
                case 1:
                    intent = new Intent();
                    intent.putExtra("trade_type", TradeActivity.THIETY_PROFIT_RANK);
                    intent.setClass(context, TradeActivity.class);
                    context.startActivity(intent);
                    break;
                case 2:
                    intent = new Intent();
                    intent.putExtra("trade_type", TradeActivity.STEADY_PROFIT_RANK);
                    intent.setClass(context, TradeActivity.class);
                    context.startActivity(intent);
                    break;
            }
        } else if (groupPosition == 6) {// 社区
            switch (childPosition) {
                case 0:
                    JumpToActivityNoAnim(MessageHistoryActivity.class);
                    Log.d(TAG, "------------------");
                    break;
                case 1:
                    JumpToActivityNoAnim(MyContractActivity.class);
                    break;
                case 2:
                    JumpToActivityNoAnim(MyCommunityActivity.class);
                    break;
                case 3:
                    JumpToActivityNoAnim(PPCommunityActivity.class);
                    break;
            }
        }
    }

    /**
     * 菜单子项单机监听
     *
     * @param groupPosition
     * @param childPosition
     */
    private void childNoClick(int groupPosition, int childPosition) {
        if (groupPosition == 2) {// 操盘手排行榜
            switch (childPosition) {
                case 0:
                    intent = new Intent();
                    intent.putExtra("trade_type", TradeActivity.SEVEVN_PROFIT_RANK);
                    intent.setClass(context, TradeActivity.class);
                    context.startActivity(intent);
                    break;
                case 1:
                    intent = new Intent();
                    intent.putExtra("trade_type", TradeActivity.THIETY_PROFIT_RANK);
                    intent.setClass(context, TradeActivity.class);
                    context.startActivity(intent);
                    break;
                case 2:
                    intent = new Intent();
                    intent.putExtra("trade_type", TradeActivity.STEADY_PROFIT_RANK);
                    intent.setClass(context, TradeActivity.class);
                    context.startActivity(intent);
                    break;
            }
//        } else if (groupPosition == 4) {// 社区
//            switch (childPosition) {
//                case 0:
//                    JumpToActivityNoAnim(MessageHistoryActivity.class);
//                    Log.d(TAG, "------------------");
//                    break;
//                case 1:
//                    JumpToActivityNoAnim(MyContractActivity.class);
//                    break;
//                case 2:
//                    JumpToActivityNoAnim(MyCommunityActivity.class);
//                    break;
//                case 3:
//                    JumpToActivityNoAnim(PPCommunityActivity.class);
//                    break;
//            }
        }
    }

    /**
     * ExpandableListView组项监听
     *
     * @param groupPosition
     */
    public void exlistGroupClick(int groupPosition) {
        if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1) {
            groupClick(groupPosition - 1);
        } else if (AppContext.getCurrentMember().getFlagIsConfirmed() == 0) {// 未付费
            groupNoClick(groupPosition + 1);
        }
    }

    private void groupClick(int groupPosition) {
        switch (groupPosition) {
            case -1:
                JumpToActivityNoAnim(NotificationActivity.class);
                break;
            case 0:// 我的操盘手
                if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1) {// 付费
                    intent = new Intent();
                    intent.putExtra("trade_type", TradeActivity.MY_TRADER);
                    intent.setClass(context, TradeActivity.class);
                    context.startActivity(intent);
                } else if (AppContext.getCurrentMember().getFlagIsConfirmed() == 0) {// 未付费
                    JumpToActivity(MemerPlanActivity.class);
                }
                break;
            case 1:// 我的股票
                JumpToActivityNoAnim(MineStockActivity.class);
                break;
            case 2:// 普普操盘手
                intent = new Intent();
                intent.putExtra("trade_type", TradeActivity.PUPU_TRADER);
                intent.setClass(context, TradeActivity.class);
                context.startActivity(intent);
                break;
            case 4:// 热门资讯
                JumpToActivityNoAnim(HotInfoActivity.class);
                break;
            case 6:// 个人中心
                JumpToActivityNoAnim(MineCenterActivity.class);
                break;
            case 7:// 关于我们
                JumpToActivityNoAnim(UsAcivity.class);
                break;
            case 8:// 条件和协议
                Intent intent = new Intent(this, UsAcivity.class);
                intent.putExtra("title", "条件和协议");
                startActivity(intent);
                break;
            case 9:// 通告

                if (payResData==null||payResData.getCompanyNote() == null || payResData.getCompanyNote().size() == 0) {
                    showToast("暂无通告");
                } else {
                    intent = new Intent(this, WebViewActivity.class);
                    intent.putExtra("title", "通告");
                    intent.putExtra("url", payResData.getCompanyNote().get(0).getUrl());
                    startActivity(intent);
                }
                break;
            case 10:// 退出
                UIHelper.showSysDialog(context, getResString(R.string.app_exit),
                        new OnSurePress() {
                            @Override
                            public void onClick(View view) {

                                MemberService.getInstance(context).loginOut(
                                        AppContext.getCurrentMember().getMobileNumber(),

                                        new CustomAsyncResponehandler() {
                                            @Override
                                            public void onSuccess(
                                                    ResponeModel baseModel) {
                                                super.onSuccess(baseModel);
                                                if (baseModel.isStatus()) {
                                                    AppContext.getApplication()
                                                            .logout();
                                                    MemberService.getInstance(
                                                            context).loginOut();
                                                    new NotificationDao(MainActivity.this).delete();
                                                    JPushInterface
                                                            .stopPush(MainActivity.this);
                                                    JPushInterface
                                                            .clearAllNotifications(MainActivity.this);
                                                    JumpToActivityNoAnim(LoginActivity.class);
                                                    finish();
                                                }
                                            }
                                        });

                            }
                        }, true);
                break;
        }
    }

    private void groupNoClick(int groupPosition) {
        switch (groupPosition) {
            case 0:// 我的操盘手
                if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1) {// 付费
                    intent = new Intent();
                    intent.putExtra("trade_type", TradeActivity.MY_TRADER);
                    intent.setClass(context, TradeActivity.class);
                    context.startActivity(intent);
                } else if (AppContext.getCurrentMember().getFlagIsConfirmed() == 0) {// 未付费
                    JumpToActivity(MemerPlanActivity.class);
                }
                break;
            case 1:// 普普操盘手
                intent = new Intent();
                intent.putExtra("trade_type", TradeActivity.PUPU_TRADER);
                intent.setClass(context, TradeActivity.class);
                context.startActivity(intent);
                break;
            case 3:// 热门资讯
                JumpToActivityNoAnim(HotInfoActivity.class);
                break;
            case 4:// 个人中心
                JumpToActivityNoAnim(MineCenterActivity.class);
                break;
            case 5:// 关于我们
                JumpToActivityNoAnim(UsAcivity.class);
                break;
            case 6:// 条件和协议
                Intent intent = new Intent(this, UsAcivity.class);
                intent.putExtra("title", "条件和协议");
                startActivity(intent);
                break;
            case 7:// 通告;
                if (noPayResData.getCompanyNote() == null || noPayResData.getCompanyNote().size() == 0) {
                    showToast("暂无通告");
                } else {
                    intent = new Intent(this, WebViewActivity.class);
                    intent.putExtra("title", "通告");
                    intent.putExtra("url", noPayResData.getCompanyNote().get(0).getUrl());
                    startActivity(intent);
                }
                break;
            case 8:// 退出
                UIHelper.showSysDialog(context, getResString(R.string.app_exit),
                        new OnSurePress() {
                            @Override
                            public void onClick(View view) {

                                MemberService.getInstance(context).loginOut(
                                        AppContext.getCurrentMember().getMobileNumber(),

                                        new CustomAsyncResponehandler() {
                                            @Override
                                            public void onSuccess(
                                                    ResponeModel baseModel) {
                                                super.onSuccess(baseModel);
                                                if (baseModel.isStatus()) {
                                                    AppContext.getApplication()
                                                            .logout();
                                                    MemberService.getInstance(
                                                            context).loginOut();
                                                    JPushInterface
                                                            .stopPush(MainActivity.this);
                                                    JPushInterface
                                                            .clearAllNotifications(MainActivity.this);
                                                    JumpToActivityNoAnim(LoginActivity.class);
                                                    finish();
                                                }
                                            }
                                        });

                            }
                        }, true);
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        listviewSearchList.setAdapter(searchStockAdp);
        listviewSearchList.setTextFilterEnabled(true);
        // setListViewHeightBasedOnChildren(listviewSearchList);
        if (TextUtils.isEmpty(s)) {
            imgbtnClearSearch.setVisibility(View.GONE);
            // lyStockContainer.setVisibility(View.VISIBLE);
            lyStockSearch.setVisibility(View.GONE);
        } else {
            imgbtnClearSearch.setVisibility(View.VISIBLE);
            // lyStockContainer.setVisibility(View.GONE);
            lyStockSearch.setVisibility(View.VISIBLE);
            searchStockAdp.getFilter().filter(s);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private void refreshData() {
        if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1) {
            ComonService.getInstance(context).getPayHomePageData(
                    String.valueOf(AppContext.getCurrentMember().getMemberID()),
                    new CustomAsyncResponehandler() {
                        @Override
                        public void onSuccess(ResponeModel baseModel) {
                            super.onSuccess(baseModel);
                            // ----
                            if (baseModel.isStatus()) {
                                Log.d(TAG, baseModel.getJson());
                                payResData = (PayResult) baseModel.getResult();
                            }
                            if (payResData == null)
                                payResData = new PayResult();
                            upDateExpandableListView();
                        }
                    }, isFirstGetHomePageData);
        } else if (AppContext.getCurrentMember().getFlagIsConfirmed() == 0) {
            ComonService.getInstance(context).getNoPayHomePageData(
                    String.valueOf(AppContext.getCurrentMember().getMemberID()),
                    new CustomAsyncResponehandler() {
                        @Override
                        public void onSuccess(ResponeModel baseModel) {
                            super.onSuccess(baseModel);
                            if (baseModel.isStatus()) {
                                noPayResData = (NoPayResult) baseModel
                                        .getResult();
                            }
                            if (noPayResData == null) {
                                noPayResData = new NoPayResult();
                            }
                            upDateExpandableListView();
                        }
                    }, isFirstGetHomePageData);
        }
        swipeLayout.setRefreshing(false);
    }

    /**
     * 更新ExpandableListView
     */
    private void upDateExpandableListView() {
        Log.d(TAG, isFirstGetHomePageData + "");
        if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1) {
            // 支付适配器
            mainExpandableListAdapter = new MainPayExpandableListAdapter(
                    context);
            mainExpandableListAdapter.setArmTypes(mainPayArmTypes);
            mainExpandableListAdapter.setMainTradeList(payResData
                    .getTradeNewTrendList());
            mainExpandableListAdapter.setTodayFocusList(payResData
                    .getTodayAttentionList());
            mainExpandableListAdapter.setStockList(payResData.getMyStockList());
            mainExpandableListAdapter.setTradeListMap(payResData
                    .getTradeRankListMap());
            mainExpandableListAdapter.setTutorialList(payResData
                    .getTutorialList());
            mainExpandableListAdapter.setTradeBlog(payResData.getTradeBlog());
            mainExpandableListAdapter.setNotified(payResData.getNotified());
            new Thread(new Runnable() {

                @Override
                public void run() {
                    dao.setNotification(payResData.getNotified());
                }
            }).start();

            listview_main.setAdapter(mainExpandableListAdapter);
            for (int i = 0; i < mainExpandableListAdapter.getGroupCount(); i++) {
                listview_main.expandGroup(i);
            }
        } else if (AppContext.getCurrentMember().getFlagIsConfirmed() == 0) {
            mainNoPayExpandableListAdapter = new MainNoPayExpandableListAdapter(
                    context, hand);
            mainNoPayExpandableListAdapter.setArmTypes(mainNoPayArmTypes);
            mainNoPayExpandableListAdapter.setRadomTrader(noPayResData
                    .getRadomTrader());
            mainNoPayExpandableListAdapter.setTradeBlog(noPayResData
                    .getTradeBlog());
            mainNoPayExpandableListAdapter.setTradeComments(noPayResData
                    .getTradeComments());
            mainNoPayExpandableListAdapter.setMainTradeList(noPayResData
                    .getTradeMounthPreTrend());
            mainNoPayExpandableListAdapter.setTradeListMap(noPayResData
                    .getTradeRankListMap());

            listview_main.setAdapter(mainNoPayExpandableListAdapter);
            for (int i = 0; i < mainNoPayExpandableListAdapter.getGroupCount(); i++) {
                listview_main.expandGroup(i);
            }

            if (isFirstGetHomePageData) {
                viewNopay.setVisibility(View.VISIBLE);
            }
        }
        if (isFirstGetHomePageData)
            isFirstGetHomePageData = false;

        if (AppConfig.firstCheckVersion) {
            checkVersion();
        }
    }

    private android.app.AlertDialog.Builder conflictBuilder;
    private boolean isConflictDialogShow;

    /**
     * 显示帐号在别处登录dialog
     */
    private void showConflictDialog() {
        isConflictDialogShow = true;
        AppContext.getApplication().logout();
        MemberService.getInstance(
                context).loginOut();
        JPushInterface
                .stopPush(MainActivity.this);
        JPushInterface
                .clearAllNotifications(MainActivity.this);
        MemberService.getInstance(context).loginOut(
                AppContext.getCurrentMember().getMobileNumber(),

                new CustomAsyncResponehandler() {
                    @Override
                    public void onSuccess(
                            ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel.isStatus()) {


                        }
                    }
                });
        try {
            if (conflictBuilder == null)
                conflictBuilder = new android.app.AlertDialog.Builder(
                        MainActivity.this);
            conflictBuilder.setTitle("下线通知");
            conflictBuilder.setMessage(R.string.connect_conflict);
            conflictBuilder.setPositiveButton(R.string.ok,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            conflictBuilder = null;

                            AppContext.getApplication().logout();
                            startActivity(new Intent(MainActivity.this,
                                    LoginActivity.class));
                            finish();
                        }
                    });
            conflictBuilder.setCancelable(false);
            conflictBuilder.create().show();
            isConflict = true;
        } catch (Exception e) {
            Log.e("###",
                    "---------color conflictBuilder error" + e.getMessage());
        }
    }

    private InviteMessgeDao inviteMessgeDao;

    /**
     * 好友变化listener
     */
    private class MyContactListener implements EMContactListener {

        @Override
        public void onContactAdded(List<String> usernameList) {

            // 保存增加的联系人

            for (String username : usernameList) {
                User user = setUserHead(username);
                user.setId(username);
                AppContext.getApplication().getContactList()
                        .put(user.getId(), user);
            }

        }

        @Override
        public void onContactDeleted(final List<String> usernameList) {
            // 被删除

            for (String id : usernameList) {
                AppContext.getApplication().getContactList().remove(id);
                inviteMessgeDao.deleteMessage(id);
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    // 如果正在与此用户的聊天页面
                    if (ChatActivity.activityInstance != null
                            && usernameList
                            .contains(ChatActivity.activityInstance
                                    .getToChatUsername())) {
                        Toast.makeText(
                                MainActivity.this,
                                ChatActivity.activityInstance
                                        .getToChatUsername() + "已把你从他好友列表里移除",
                                Toast.LENGTH_SHORT).show();
                        ChatActivity.activityInstance.finish();
                    }
                    updateNum();
                }
            });
            // TODO:刷新ui

        }

        @Override
        public void onContactInvited(String username, String reason) {

            // 接到邀请的消息，如果不处理(同意或拒绝)，掉线后，服务器会自动再发过来，所以客户端不要重复提醒
            List<InviteMessage> msgs = inviteMessgeDao.getMessagesList();
            for (InviteMessage inviteMessage : msgs) {
                if (inviteMessage.getGroupId() == null
                        && inviteMessage.getFrom().equals(username)) {
                    inviteMessgeDao.deleteMessage(username);
                }
            }
            // 自己封装的javabean
            InviteMessage msg = new InviteMessage();
            msg.setFrom(username);
            msg.setTime(System.currentTimeMillis());
            msg.setReason(reason);
            Log.d(TAG, username + "请求加你为好友,reason: " + reason);
            // 设置相应status
            msg.setStatus(InviteMesageStatus.BEINVITEED);
            notifyNewIviteMessage(msg);

        }

        @Override
        public void onContactAgreed(String username) {
            // 自己封装的javabean
            InviteMessage msg = new InviteMessage();
            List<InviteMessage> msgs = inviteMessgeDao.getMessagesList();
            for (InviteMessage inviteMessage : msgs) {

                if (inviteMessage.getFrom().equals(username)) {
                    msg = inviteMessage;
                    msg.setTime(System.currentTimeMillis());
                    msg.setStatus(InviteMesageStatus.AGREED);
                    inviteMessgeDao.deleteMessage(msg.getFrom());
                    notifyNewIviteMessage(msg);
                    return;
                }

            }

        }

        @Override
        public void onContactRefused(String username) {
            // 参考同意，被邀请实现此功能,demo未实现

        }

    }

    /**
     * 刷新未读消息数
     */
    public void updateNum() {
        runOnUiThread(new Runnable() {
            public void run() {
                int count1 = getUnreadMsgCountTotal();
                int count2 = getUnreadAddressCountTotal();
                armTypes5 = new ArrayList<MenuClass>();
                MenuClass c6 = new MenuClass();
                c6.setCat_name(getResString(R.string.c6));
                c6.setNum(count1);
                armTypes5.add(c6);
                MenuClass c7 = new MenuClass();
                c7.setCat_name(getResString(R.string.c7));
                c7.setNum(count2);
                armTypes5.add(c7);

                menuExpandableListAdapter = new MenuExpandableListAdapter(
                        MainActivity.this);
                menuExpandableListAdapter.setArmTypes(armTypes);
                if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1) {
                    menuExpandableListAdapter.setArmItem(4, armTypes3);
                    menuExpandableListAdapter.setArmItem(6, armTypes5);
                } else if (AppContext.getCurrentMember().getFlagIsConfirmed() == 0) {
                    menuExpandableListAdapter.setArmItem(1, armTypes3);
                  //  menuExpandableListAdapter.setArmItem(3, armTypes5);
                }
                listview_type.setAdapter(menuExpandableListAdapter);
                if (AppContext.getCurrentMember().getFlagIsConfirmed() == 0) {
                    //listview_type.expandGroup(3);
                } else {
                    //listview_type.expandGroup(4);
                }
                if (count1 > 0 || count2 > 0 || dao.hasNotRead()) {
                    tvNum.setVisibility(View.VISIBLE);
                } else {
                    tvNum.setVisibility(View.GONE);
                }
            }
        });
    }

    /**
     * 保存提示新消息
     *
     * @param msg
     */
    private void notifyNewIviteMessage(InviteMessage msg) {
        saveInviteMsg(msg);
        // 提示有新消息
        EMNotifier.getInstance(getApplicationContext()).notifyOnNewMsg();

        updateNum();
        // TODO: 刷新好友页面ui
    }

    /**
     * 保存邀请等msg
     *
     * @param msg
     */
    private void saveInviteMsg(InviteMessage msg) {
        // 保存msg
        inviteMessgeDao.saveMessage(msg);
        // 未读数加1
        User user = AppContext.getApplication().getContactList()
                .get(AppConfig.NEW_FRIENDS_USERNAME);
        AppContext.getApplication().getContactList()
                .get(AppConfig.NEW_FRIENDS_USERNAME)
                .setUnreadMsgCount(user.getUnreadMsgCount() + 1);
    }

    /**
     * set head
     *
     * @param username
     * @return
     */
    User setUserHead(String username) {
        User user = new User();
        user.setUsername(username);
        String headerName = null;
        if (!TextUtils.isEmpty(user.getNick())) {
            headerName = user.getNick();
        } else {
            headerName = user.getUsername();
        }
        if (username.equals(AppConfig.NEW_FRIENDS_USERNAME)) {
            user.setHeader("");
        } else if (Character.isDigit(headerName.charAt(0))) {
            user.setHeader("#");
        } else {
            user.setHeader(HanziToPinyin.getInstance()
                    .get(headerName.substring(0, 1)).get(0).target.substring(0,
                            1).toUpperCase());
            char header = user.getHeader().toLowerCase().charAt(0);
            if (header < 'a' || header > 'z') {
                user.setHeader("#");
            }
        }
        return user;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (getIntent().getBooleanExtra("conflict", false)
                && !isConflictDialogShow)
            showConflictDialog();
    }

    /**
     * 获取未读申请与通知消息
     *
     * @return
     */
    public int getUnreadAddressCountTotal() {
        int unreadAddressCountTotal = 0;
        if (AppContext.getApplication().getContactList() != null
                && AppContext.getApplication().getContactList().size() > 0) {
            if (AppContext.getApplication().getContactList()
                    .get(AppConfig.NEW_FRIENDS_USERNAME) != null)
                unreadAddressCountTotal = AppContext.getApplication()
                        .getContactList().get(AppConfig.NEW_FRIENDS_USERNAME)
                        .getUnreadMsgCount();
        }
        return unreadAddressCountTotal;
    }

    /**
     * 获取未读消息数
     *
     * @return
     */
    public int getUnreadMsgCountTotal() {
        int unreadMsgCountTotal = 0;
        unreadMsgCountTotal = EMChatManager.getInstance().getUnreadMsgsCount();
        return unreadMsgCountTotal;
    }

    /**
     * 新消息广播接收者
     */
    private class NewMessageBroadcastReceiver extends BroadcastReceiver {
        @SuppressWarnings("unused")
        @Override
        public void onReceive(Context context, Intent intent) {
            // 主页面收到消息后，主要为了提示未读，实际消息内容需要到chat页面查看

            // 消息id
            String msgId = intent.getStringExtra("msgid");
            // 收到这个广播的时候，message已经在db和内存里了，可以通过id获取mesage对象
            // EMMessage message =
            // EMChatManager.getInstance().getMessage(msgId);

            // 刷新bottom bar消息未读数
            updateNum();

            // 注销广播，否则在ChatActivity中会收到这个广播
            abortBroadcast();
        }
    }

    /**
     * 消息回执BroadcastReceiver
     */
    private BroadcastReceiver ackMessageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String msgid = intent.getStringExtra("msgid");
            String from = intent.getStringExtra("from");
            EMConversation conversation = EMChatManager.getInstance()
                    .getConversation(from);
            if (conversation != null) {
                // 把message设为已读
                EMMessage msg = conversation.getMessage(msgid);
                if (msg != null) {
                    msg.isAcked = true;
                }
            }
            abortBroadcast();
        }
    };

    public void onResume() {
        super.onResume();
        if (getIntent().getBooleanExtra("conflict", false)) {
            showConflictDialog();
        }
        if (!isConflict) {
            updateNum();
            EMChatManager.getInstance().activityResumed();
        }

        MobclickAgent.onResume(this);
        updateView();
        isForeground = true;

    }

    /**
     * 检查版本更新
     */
    private void checkVersion() {
        VersionService.getInstance(context).getNewVersionInfo(
                new CustomAsyncResponehandler() {
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            final Version temp = (Version) baseModel
                                    .getResult();
                            if (temp.getFlagUpdate() == 0) {
                                AppConfig.firstCheckVersion = false;
                            } else {
                                AppConfig.firstCheckVersion = true;
                            }
                            if (temp != null
                                    && !StringUtils.isStringNone(temp
                                    .getVersionCode())) {
                                if (AppConfig.localVersionCode < Integer
                                        .parseInt(temp.getVersionCode())) {
                                    UIHelper.showSysDialog(
                                            context,
                                            MainActivity.this
                                                    .getResString(R.string.version_digtitle),
                                            MainActivity.this
                                                    .getResString(R.string.version_digmsg),
                                            new OnSurePress() {
                                                @Override
                                                public void onClick(View view) {
                                                    Intent updateIntent = new Intent(
                                                            MainActivity.this,
                                                            UpdateService.class);
                                                    updateIntent.putExtra(
                                                            "loadUrl",
                                                            temp.getUrl());
                                                    startService(updateIntent);
                                                    showToast("请在通知栏查看下载");
                                                }
                                            }, new DialogInterface.OnCancelListener() {
                                                @Override
                                                public void onCancel(DialogInterface dialog) {
                                                    if (temp.getFlagUpdate() == 0) {
                                                        AppConfig.firstCheckVersion = false;
                                                    } else {
                                                        MainActivity.this.finish();
                                                        AppManager.getAppManager().finishActivity();
                                                        ;
                                                    }
                                                }
                                            }, true, "确定", "取消");
                                }
                            }
                        }
                    }
                });
        // // 模拟
        // UIHelper.showSysDialog(context,
        // MainActivity.this.getResString(R.string.version_digtitle),
        // MainActivity.this.getResString(R.string.version_digmsg),
        // new OnSurePress() {
        // @Override
        // public void onClick(View view) {
        // Intent updateIntent = new Intent(MainActivity.this,
        // UpdateService.class);
        // updateIntent
        // .putExtra(
        // "loadUrl",
        // "http://softfile.3g.qq.com:8080/msoft/179/1105/10753/MobileQQ1.0(Android)_Build0198.apk");
        // startService(updateIntent);
        // }
        // }, true);

    }

    private static DisplayImageOptions randomPhotoOption = new DisplayImageOptions.Builder()
            .showStubImage(R.drawable.empty_photo)
            .showImageForEmptyUri(R.drawable.empty_photo)
            .showImageOnFail(R.drawable.empty_photo).cacheInMemory(true)
            .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();

    Handler hand = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == 1) {
                AppContext.setImageNo(msg.getData().getString("bigImage"),
                        mHeaderImage, randomPhotoOption);
                // mHeaderImage.setImageDrawable(getResources().getDrawable(
                // R.drawable.empty_photo));
            }
        }

        ;
    };

    public void onPause() {
        super.onPause();
        isForeground = false;
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        JPushInterface.stopPush(this);
        unregisterReceiver(mMessageReceiver);
        // 注销广播接收者
        try {
            unregisterReceiver(msgReceiver);
        } catch (Exception e) {
        }
        try {
            unregisterReceiver(ackMessageReceiver);
        } catch (Exception e) {
        }

        if (conflictBuilder != null) {
            conflictBuilder.create().dismiss();
            conflictBuilder = null;
        }

    }

    public void registerMessageReceiver() {
        mMessageReceiver = new MessageReceiver();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction(MESSAGE_RECEIVED_ACTION);
        registerReceiver(mMessageReceiver, filter);
    }

    public class MessageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (MESSAGE_RECEIVED_ACTION.equals(intent.getAction())) {
                String messge = intent.getStringExtra(KEY_MESSAGE);
                String extras = intent.getStringExtra(KEY_EXTRAS);
                StringBuilder showMsg = new StringBuilder();
                showMsg.append(KEY_MESSAGE + " : " + messge + "\n");
                if (extras == null) {
                    showMsg.append(KEY_EXTRAS + " : " + extras + "\n");
                }
            }
        }
    }


}
