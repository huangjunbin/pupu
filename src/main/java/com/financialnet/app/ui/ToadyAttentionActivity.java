package com.financialnet.app.ui;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.HTodayAttentionAdapter;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.StockService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * s
 * 
 * @className：@ToadyAttentionActivity.java
 * @author: li mingtao
 * @Function:今日关注
 * @createDate: @d2014-9-14@下午3:30:54
 * @update:
 */
public class ToadyAttentionActivity extends BaseActivity implements
		IXListViewListener {
	private HeaderBar headerBar;
	private List<Stock> stockList;
	private HTodayAttentionAdapter stockAdp;
	private XListView xlistTodayAttention;
	private int curPageNum = 0, curPageSize = AppContext.PAGE_SIZE;

	public ToadyAttentionActivity() {
		super(R.layout.activiy_todayattention);
	}

	@Override
	public void lastLoad() {
		super.lastLoad();
		onRefresh();
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.trader_attentiontoday));
		xlistTodayAttention = (XListView) findViewById(R.id.todayttentionlist);
		xlistTodayAttention.setFooterDividersEnabled(false);
		xlistTodayAttention.setPullLoadEnable(false);
		xlistTodayAttention.setPullRefreshEnable(true);
	}

	@Override
	public void initData() {
		stockList = new ArrayList<Stock>();
		stockAdp = new HTodayAttentionAdapter(ToadyAttentionActivity.this,
				stockList, R.drawable.pro_ph_mask_default);
		xlistTodayAttention.setAdapter(stockAdp);
	}

	@Override
	public void bindViews() {
		xlistTodayAttention.setXListViewListener(this);
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	public void onRefresh() {
		xlistTodayAttention.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		curPageSize = AppContext.PAGE_SIZE;
		getTodayAttention();
	}

	@Override
	public void onLoadMore() {
		curPageSize += AppContext.PAGE_STEP_SIZE;
		getTodayAttention();
	}

	private void getTodayAttention() {
		StockService.getInstance(context).getHomeStockAttentionToday(
				AppContext.getCurrentMember().getMemberID() + "", curPageNum + "",
				curPageSize + "", new CustomAsyncResponehandler() {
					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							List<Stock> temp = (List<Stock>) baseModel
									.getResult();
							if (temp != null && temp.size() > 0) {
								stockList.clear();
								stockList.addAll(temp);
								stockAdp.notifyDataSetChanged();
							}
							if (!StringUtils.isStringNone(baseModel
									.getTotalCount())) {
								if (stockList.size() >= Integer
										.valueOf(baseModel.getTotalCount())) {
									xlistTodayAttention
											.setPullLoadEnable(false);
								} else {
									xlistTodayAttention.setPullLoadEnable(true);
								}
							}
						}
					}

					@Override
					public void onFinish() {
						super.onFinish();
						xlistTodayAttention.stopLoadMore();
						xlistTodayAttention.stopRefresh();
					}
				});
	}
}
