package com.financialnet.app.ui;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.MemberService;
import com.financialnet.util.StringUtils;
import com.financialnet.util.UIHelper;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

/**
 * @ConfirmTelActivity.java
 * @author li mingtao
 * @function :验证手机号码
 * @2013-8-27@下午2:40:57
 * @update:
 */
public class ConfirmTelActivity extends BaseActivity {
	public static final int GO_FORGETPWD = 0x01;// 忘记密码
	public static final int GO_SETNEWPWD = 0x02;// 设置新密码
	private int type;// 类型
	// -----------------
	private HeaderBar headerBar;
	private EditText etMobileNumber, etAutocode;
	private Button btnModifyPwd, btnGetAutocode;
	private Timer timer;
	int len = AppContext.EFFECT_TIME;

	public ConfirmTelActivity() {
		super(R.layout.activity_mobilenum);
	}

	@SuppressLint("HandlerLeak")
	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				len--;
				btnGetAutocode.setText(len + "秒");
				if (len <= 0) {
					btnGetAutocode.setEnabled(true);
					btnGetAutocode
							.setText(getResString(R.string.register_getcode));
					timer.cancel();
					timer = null;
					len = AppContext.EFFECT_TIME;
				}
				break;
			}
			super.handleMessage(msg);
		}
	};

	@Override
	public void initViews() {
		type = getIntent().getIntExtra("type", 0);
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.register_confrim_title));
		etMobileNumber = (EditText) findViewById(R.id.user_tel);
		etAutocode = (EditText) findViewById(R.id.input_code);
		btnModifyPwd = (Button) findViewById(R.id.fill_data);
		btnGetAutocode = (Button) findViewById(R.id.getAutoCode);
		btnModifyPwd.setText(getResString(R.string.register_modify_pwd));
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		btnGetAutocode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (cantEmpty(etMobileNumber,
						R.string.register_moblenumbernoempty))
					return;

				if (StringUtils.isPhoneNumberValid2(etMobileNumber.getText()
						.toString())) {
					MemberService.getInstance(context).ConfirmMobileNumber(
							etMobileNumber.getText().toString(),
							new CustomAsyncResponehandler() {
								@Override
								public void onSuccess(ResponeModel baseModel) {
									super.onSuccess(baseModel);
									if (baseModel.isStatus()) {
										btnGetAutocode.setEnabled(false);
										btnGetAutocode.setText(len + "秒");

										timer = new Timer();
										timer.schedule(new TimerTask() {
											@Override
											public void run() {
												Message msg = new Message();
												msg.what = 1;
												handler.sendMessage(msg);
											}
										}, 1000, 1000);
									}
								}
							});
				} else {
					UIHelper.ShowMessage(context,
							getResString(R.string.register_moblenumbererroe));
					return;
				}
			}
		});

		btnModifyPwd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (cantEmpty(etMobileNumber,
						R.string.register_moblenumbernoempty))
					return;
				if (cantEmpty(etAutocode, R.string.register_autocodenoempty))
					return;

				if (StringUtils.isPhoneNumberValid2(etMobileNumber.getText()
						.toString())) {
					MemberService.getInstance(context).CheckoutAutoCode(
							etMobileNumber.getText().toString(),
							etAutocode.getText().toString(),
							new CustomAsyncResponehandler() {
								@Override
								public void onSuccess(ResponeModel baseModel) {
									super.onSuccess(baseModel);
									if (baseModel.isStatus()) {
										if (timer != null) {
											timer.cancel();
											timer = null;
										}
										// ----
										Intent intent = new Intent(
												ConfirmTelActivity.this,
												ModifyPwdActivity.class);
										intent.putExtra("type", type);
										intent.putExtra("mobileNumber",
												etMobileNumber.getText()
														.toString());
										startActivity(intent);
									}
								}
							});
				} else {
					UIHelper.ShowMessage(context,
							getResString(R.string.register_moblenumbererroe));
					return;
				}
			}
		});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}
}
