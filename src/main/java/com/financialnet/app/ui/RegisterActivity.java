package com.financialnet.app.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.easemob.EMCallBack;
import com.easemob.chat.EMGroup;
import com.easemob.chat.EMGroupManager;
import com.easemob.exceptions.EaseMobException;
import com.financialnet.app.R;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.MemberService;
import com.financialnet.util.StringUtils;
import com.financialnet.util.UIHelper;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

import java.util.List;

/**
 * 注册
 * 
 * @author Administrator
 * 
 */
public class RegisterActivity extends BaseActivity {
	private HeaderBar headerBar;
	private Button btnRegisterNow;
	private EditText editEmail, editNickName, editPassword, editSureNickName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public RegisterActivity() {
		super(R.layout.activity_register);

	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.register_str));

		editEmail = (EditText) findViewById(R.id.user_email);
		editNickName = (EditText) findViewById(R.id.user_name);
		editPassword = (EditText) findViewById(R.id.user_pwd);
		editSureNickName = (EditText) findViewById(R.id.user_surepwd);

		btnRegisterNow = (Button) findViewById(R.id.register_now);
	}

	@Override
	public void initData() {
	}

	@Override
	public void bindViews() {
		btnRegisterNow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (cantEmpty(editEmail, R.string.register_emailnoempty))
					return;
				if (cantEmpty(editNickName, R.string.register_nikenamenoempty))
					return;
				if (cantEmpty(editPassword, R.string.register_pwdnoempty))
					return;
				if (cantEmpty(editSureNickName,
						R.string.register_surepwdnoempty))
					return;

				if (!StringUtils.isEmail(editEmail.getText().toString().trim())) {
					UIHelper.ShowMessage(context,
							getResString(R.string.register_emailerroe));
					return;
				}

				if (!editSureNickName.getText().toString().trim()
						.equals(editPassword.getText().toString().trim())) {
					UIHelper.ShowMessage(context,
							getResString(R.string.register_pwdnotequall));
					return;
				}

				MemberService.getInstance(context).Register(
						getIntent().getStringExtra("mobileNumber"),
						editEmail.getText().toString().trim(),
						editNickName.getText().toString().trim(),
						editPassword.getText().toString().trim(),new EMCallBack() {
							@Override
							public void onSuccess() {
								new Thread() {
									public void run() {
										// 存储所有群组
										try {
											List<EMGroup> grouplist = EMGroupManager.getInstance().getGroupsFromServer();
										} catch (EaseMobException e) {
											e.printStackTrace();
										}
									};
								}.start();
							}

							@Override
							public void onProgress(int progress, String status) {
								Log.d("HX", "onProgress");
							}

							@Override
							public void onError(int code, final String message) {
								Log.d("HX", "onError");

							}
						},
						new CustomAsyncResponehandler() {
							@Override
							public void onSuccess(ResponeModel baseModel) {
								super.onSuccess(baseModel);
								if (baseModel.isStatus()) {
									JumpToActivity(MainActivity.class);
									finish();
								}
							}
						});
			}
		});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

}
