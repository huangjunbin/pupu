package com.financialnet.app.ui;

import android.os.Bundle;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.TradeNewTrendAdapter;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Trade;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.TraderService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

/**
 * @className：@TraderNewTrendActivity.java
 * @author: li mingtao
 * @Function:操盘手最新动向
 * @createDate: @d2014-9-13@下午9:49:41
 * @update:
 */
public class TraderNewTrendActivity extends BaseActivity implements
		IXListViewListener {
	private int TraderNewTrendPageNo = 0;
	private int TraderNewTrendCurrentSize = AppContext.PAGE_SIZE;
	// ------------------
	private HeaderBar headerBar;
	private XListView xlistTraderNewTrend;
	private TradeNewTrendAdapter tradeNewTrendAdapter;
	private List<Trade> listTraderNewTrend;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		onRefresh();
	}

	public TraderNewTrendActivity() {
		super(R.layout.activity_tradernewtrend);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.trader_newtrend));
		xlistTraderNewTrend = (XListView) findViewById(R.id.trade_trend_list);
		xlistTraderNewTrend.setPullLoadEnable(false);
		xlistTraderNewTrend.setPullRefreshEnable(true);
	}

	@Override
	public void initData() {
		listTraderNewTrend = new ArrayList<Trade>();
		tradeNewTrendAdapter = new TradeNewTrendAdapter(
				TraderNewTrendActivity.this, listTraderNewTrend,
				R.drawable.pro_ph_mask_default);
		xlistTraderNewTrend.setAdapter(tradeNewTrendAdapter);
	}

	@Override
	public void bindViews() {
		xlistTraderNewTrend.setXListViewListener(this);
	}

	@Override
	public void onRefresh() {
		xlistTraderNewTrend.setRefreshTime(DateUtil.getCurrentTime());
		getTraderNewTrendList();
	}

	@Override
	public void onLoadMore() {
		if (listTraderNewTrend.size() >= TraderNewTrendCurrentSize) {
			TraderNewTrendCurrentSize += AppContext.PAGE_STEP_SIZE;
			getTraderNewTrendList();
		}
	}

	private void getTraderNewTrendList() {
		TraderService.getInstance(context).getTradeNewTrendList(
				String.valueOf(AppContext.getCurrentMember().getMemberID()),
				String.valueOf(TraderNewTrendPageNo),
				String.valueOf(TraderNewTrendCurrentSize),
				new CustomAsyncResponehandler() {
					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							List<Trade> temp = (List<Trade>) baseModel
									.getResult();
							if (temp != null && temp.size() > 0) {
								listTraderNewTrend.clear();
								listTraderNewTrend.addAll(temp);
								tradeNewTrendAdapter.notifyDataSetChanged();
								if (!StringUtils.isStringNone(baseModel
										.getTotalCount())) {
									if (listTraderNewTrend.size() >= Integer
											.parseInt(baseModel.getTotalCount())) {
										xlistTraderNewTrend
												.setPullLoadEnable(false);
									} else {
										xlistTraderNewTrend
												.setPullLoadEnable(true);
									}
								}
							}
						}
						xlistTraderNewTrend.stopLoadMore();
						xlistTraderNewTrend.stopRefresh();
					}

					@Override
					public void onFinish() {
						super.onFinish();
						xlistTraderNewTrend.stopLoadMore();
						xlistTraderNewTrend.stopRefresh();
					}
				});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
