package com.financialnet.app.ui;

import java.util.ArrayList;
import java.util.List;

import com.financialnet.app.R;
import com.financialnet.app.adapter.Trader30TrendAdapter;
import com.financialnet.model.Stock;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.umeng.analytics.MobclickAgent;

/**
 * @className：@Trader30TrendActivity.java
 * @author: li mingtao
 * @Function:操盘手30天前动态
 * @createDate: @d2014-9-14@上午9:24:38
 * @update:
 */
public class Trader30TrendActivity extends BaseActivity {
	private HeaderBar headerBar;
	private XListView xlistTrader30Trend;
	private List<Stock> listStock;
	private Trader30TrendAdapter trader30TrendAdp;

	public Trader30TrendActivity() {
		super(R.layout.activity_traderoldtrend);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.trader_30trend));
		xlistTrader30Trend = (XListView) findViewById(R.id.trade_30trend_list);
		xlistTrader30Trend.setPullLoadEnable(false);
		xlistTrader30Trend.setPullRefreshEnable(false);
	}

	@Override
	public void initData() {
		listStock = new ArrayList<Stock>();
		trader30TrendAdp = new Trader30TrendAdapter(Trader30TrendActivity.this,
				listStock);
		xlistTrader30Trend.setAdapter(trader30TrendAdp);
	}

	@Override
	public void bindViews() {

	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
