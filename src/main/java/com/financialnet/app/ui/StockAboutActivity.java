package com.financialnet.app.ui;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.fragment.stoctaboutfragment.AboutNewsFragement;
import com.financialnet.app.fragment.stoctaboutfragment.HoldDoyenFragement;
import com.financialnet.app.fragment.stoctaboutfragment.StockMarketFragement;
import com.financialnet.model.AddStockRequest;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.StockService;
import com.financialnet.util.UIHelper;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

/**
 * @className：StockAboutActivity.java
 * @author: li mingtao
 * @Function: 股票相关界面
 * @createDate: 2014-8-19下午2:12:14
 * @update:
 */
public class StockAboutActivity extends BaseActivity implements
		OnClickListener, OnPageChangeListener {
	public HeaderBar headerBar;
	private List<Fragment> listFrags;
	private List<RadioButton> radioAreas;
	private ViewPager viewPage;
	public Stock tempStock;
	private int tag = 0;

	@Override
	public void lastLoad() {
		super.lastLoad();
		setSelectedTab(0, true);
	}

	public StockAboutActivity() {
		super(R.layout.stock_about_activity);
	}

	@Override
	public void initViews() {
		tempStock = (Stock) getIntent().getSerializableExtra("stock");
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(tempStock.getStockName());
		viewPage = (ViewPager) findViewById(R.id.viewPages);
	}

	@Override
	public void initData() {
		radioAreas = new ArrayList<RadioButton>();
		radioAreas.add((RadioButton) findViewById(R.id.tab_area_1));
		radioAreas.add((RadioButton) findViewById(R.id.tab_area_2));
		radioAreas.add((RadioButton) findViewById(R.id.tab_area_3));

		listFrags = new ArrayList<Fragment>();
		listFrags.add(new StockMarketFragement());
		listFrags.add(new AboutNewsFragement());
		listFrags.add(new HoldDoyenFragement());

		viewPage.setAdapter(new StockAboutAdapter(getSupportFragmentManager()));
		tag = getIntent().getIntExtra("tag", 0);
	}

	@Override
	public void bindViews() {
		for (RadioButton radio : radioAreas)
			radio.setOnClickListener(this);
		viewPage.setOnPageChangeListener(this);
		headerBar.setRightText("加入");
		if (tag == 1) {
			headerBar.setRightText("");
		}
		headerBar.top_right_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final String stockDes = tempStock.getStockName() + "("
						+ tempStock.getStockCode() + tempStock.getMarketCode()
						+ ")";
				UIHelper.showSysDialog(StockAboutActivity.this, stockDes
						+ getResString(R.string.stock_isadd),
						new OnSurePress() {
							@Override
							public void onClick(View view) {

								AddStockRequest resa = new AddStockRequest();
								resa.setMarketID(tempStock.getMarketID());
								resa.setMemberId(String
										.valueOf(AppContext.getCurrentMember()
												.getMemberID()));
								resa.setStockCode(tempStock.getStockCode());
								resa.setStockName(tempStock.getStockName());

								StockService.getInstance(
										StockAboutActivity.this).addMyStock(
										resa, new CustomAsyncResponehandler() {
											@Override
											public void onSuccess(
													ResponeModel baseModel) {
												super.onSuccess(baseModel);
												if (baseModel.isStatus()) {
													UIHelper.showSysDialog(
															StockAboutActivity.this,
															"已经成功加入到我的股票");
												} else {
													switch (baseModel.getCode()) {
													case 1001:// 股票超过10条

														UIHelper.showSysDialog2(
																StockAboutActivity.this,
																stockDes
																		+ getResString(R.string.stock_ownmax),
																new OnSurePress() {
																	@Override
																	public void onClick(
																			View view) {
																		startActivity(new Intent(
																				StockAboutActivity.this,
																				EditStockActivity.class));
																	}
																}, true);

														break;
													case 1002:// 重复的股票
														UIHelper.showSysDialog(
																StockAboutActivity.this,
																getResString(R.string.stock_nore));
														break;
													default:
														UIHelper.ShowMessage(
																StockAboutActivity.this,
																baseModel
																		.getMsg());
													}
												}
											}
										});

							}
						}, true);
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tab_area_1:
			setSelectedTab(0, true);
			break;
		case R.id.tab_area_2:
			setSelectedTab(1, true);
			break;
		case R.id.tab_area_3:
			setSelectedTab(2, true);
			break;
		}
	}

	private class StockAboutAdapter extends FragmentPagerAdapter {

		public StockAboutAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int arg0) {
			return listFrags.get(arg0);
		}

		@Override
		public int getCount() {
			return listFrags.size();
		}
	}

	private void setSelectedTab(int tabId, boolean isPage) {
		for (RadioButton radio : radioAreas)
			radio.setBackgroundResource(R.drawable.tab_bg_a);
		radioAreas.get(tabId).setBackgroundResource(R.drawable.tab_bg_b);
		if (isPage)
			viewPage.setCurrentItem(tabId);
		if (tabId != 2) {
			if (headerBar.topSpBar.getVisibility() == View.VISIBLE) {
				headerBar.topSpBar.setVisibility(View.GONE);
			}
		} else {
			headerBar.topSpBar.setVisibility(View.GONE);
		}
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	@Override
	public void onPageSelected(int arg0) {
		setSelectedTab(arg0, false);
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
