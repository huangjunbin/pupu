/**
 * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.financialnet.app.ui;

import java.util.List;

import android.view.View;
import android.widget.ListView;

import com.financialnet.app.AppConfig;
import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.NewFriendsMsgAdapter;
import com.financialnet.db.InviteMessgeDao;
import com.financialnet.model.InviteMessage;
import com.umeng.analytics.MobclickAgent;

/**
 * 申请与通知
 * 
 */
public class NewFriendsMsgActivity extends BaseActivity {

	private ListView listView;

	public NewFriendsMsgActivity() {
		super(R.layout.activity_new_friends_msg);
	}

	public void back(View view) {
		finish();
	}

	@Override
	public void initViews() {
		listView = (ListView) findViewById(R.id.list);
	}

	@Override
	public void initData() {
		InviteMessgeDao dao = new InviteMessgeDao(this);
		List<InviteMessage> msgs = dao.getMessagesList();
		// 设置adapter
		NewFriendsMsgAdapter adapter = new NewFriendsMsgAdapter(this, 1, msgs);
		listView.setAdapter(adapter);
		AppContext.getApplication().getContactList()
				.get(AppConfig.NEW_FRIENDS_USERNAME).setUnreadMsgCount(0);
	}

	@Override
	public void bindViews() {

	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
