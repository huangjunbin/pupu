package com.financialnet.app.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.financialnet.app.R;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.MemberService;
import com.financialnet.util.UIHelper;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

/**
 * @ModifyPwdActivity.java
 * @author li mingtao
 * @function : 修改密码
 * @2013-8-27@下午2:49:47
 * @update:
 */
public class ModifyPwdActivity extends BaseActivity {
	private HeaderBar headerBar;
	private Button btnRegister;
	private EditText etOldPwd, etNewPwd, etSureNewPwd;
	private int type;

	public ModifyPwdActivity() {
		super(R.layout.activity_modifypwd);
	}

	@Override
	public void initViews() {
		type = getIntent().getIntExtra("type", 0);
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.register_modify_pwd));

		etNewPwd = (EditText) findViewById(R.id.user_pwd);
		etSureNewPwd = (EditText) findViewById(R.id.user_surepwd);
		btnRegister = (Button) findViewById(R.id.register_now);
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		if (type == ConfirmTelActivity.GO_SETNEWPWD) {
			etOldPwd = (EditText) findViewById(R.id.user_oldpwd);
			etOldPwd.setVisibility(View.VISIBLE);
		}

		btnRegister.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (type == ConfirmTelActivity.GO_SETNEWPWD) {
					if (cantEmpty(etNewPwd, R.string.register_oldpwdnoempty))
						return;
				}

				if (cantEmpty(etNewPwd, R.string.register_pwdnoempty))
					return;
				if (cantEmpty(etSureNewPwd, R.string.register_surepwdnoempty))
					return;

				if (!etSureNewPwd.getText().toString()
						.equals(etNewPwd.getText().toString())) {
					UIHelper.ShowMessage(context,
							getResString(R.string.register_pwdnotequall));
					return;
				}
				switch (type) {
				case ConfirmTelActivity.GO_FORGETPWD:
					MemberService.getInstance(context).setNewPassword(
							getIntent().getStringExtra("mobileNumber"),
							etNewPwd.getText().toString(),
							new CustomAsyncResponehandler() {
								@Override
								public void onSuccess(ResponeModel baseModel) {
									super.onSuccess(baseModel);
									if (baseModel.isStatus()) {
										JumpToActivityNoAnim(LoginActivity.class);
										finish();
									}
								}
							});
					break;
				case ConfirmTelActivity.GO_SETNEWPWD:
					MemberService.getInstance(context).updatePassword(
							getIntent().getStringExtra("mobileNumber"),
							etOldPwd.getText().toString(),
							etNewPwd.getText().toString(),
							new CustomAsyncResponehandler() {
								@Override
								public void onSuccess(ResponeModel baseModel) {
									super.onSuccess(baseModel);
									if (baseModel.isStatus()) {
										JumpToActivityNoAnim(LoginActivity.class);
										finish();
									}
								}
							});
					break;
				default:
					break;
				}
			}
		});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

}
