package com.financialnet.app.ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.financialnet.app.AppContext;
import com.financialnet.app.AppManager;
import com.financialnet.app.R;
import com.financialnet.util.StringUtils;
import com.financialnet.util.UIHelper;
import com.financialnet.widget.MyProgressDialog;
import com.umeng.analytics.MobclickAgent;

import java.io.Serializable;

/**
 * 基类界面
 * 
 * @author allen
 * @version 1.0.0
 * @created 2014-4-16
 */
public abstract class BaseActivity extends FragmentActivity {
	protected int resId = -1;
	public AppContext appContext;
	protected Context context;
	protected MyProgressDialog dialog;
	public Toast toast;

	public BaseActivity(int resId) {
		this.resId = resId;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		firstLoad();
		// 添加Activity到堆栈
		AppManager.getAppManager().addActivity(this);
		appContext = (AppContext) BaseActivity.this.getApplication();
		context = this;
		if (resId != -1) {
			setContentView(resId);
		}
		initViews();
		initData();
		bindViews();
		lastLoad();
	}
	
	public void showToast(String message) {
		toast = Toast.makeText(getApplicationContext(), message,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

	/**
	 * 
	 * @Title firstLoad
	 * @Description 首次进入程序需要操作的东西
	 */
	public void firstLoad() {
	};

	/**
	 * 
	 * @Title lastLoad
	 * @Description 最后需要操作的东西
	 */
	public void lastLoad() {
	};

	/**
	 * 
	 * @Title initViews
	 * @Description 初始化界面，对界面进行赋值等操作
	 */
	public abstract void initViews();

	/**
	 * 
	 * @Title initViews
	 * @Description 初始化界面，对界面进行赋值等操作
	 */
	public abstract void initData();

	/**
	 * 
	 * @Title initViews
	 * @Description 初始化界面，对界面进行赋值等操作
	 */
	public abstract void bindViews();

	/**
	 * 普通的界面跳转，不带动画
	 * 
	 * @param cls
	 */
	public void JumpToActivityNoAnim(Class<?> cls) {
		Intent intent = new Intent(this, cls);
		startActivity(intent);
	}

	/**
	 * 
	 * @Title JumpToActivity
	 * @Description 不带参数的Activity界面跳转
	 * @param cls
	 *            跳转的目标界面
	 */
	public void JumpToActivity(Class<?> cls) {
		JumpToActivity(cls, null);
	}

	/**
	 * 
	 * @Title JumpToActivity
	 * @Description 带参数的界面跳转
	 * @param cls
	 *            跳转的目标界面
	 * @param obj
	 *            带过去的界面参数
	 */
	public void JumpToActivity(Class<?> cls, Object obj) {
		Intent intent = new Intent(this, cls);
		// 设置跳转标志为如此Activity存在则把其从任务堆栈中取出放到最上方
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		if (obj != null)
			intent.putExtra("data", (Serializable) obj);
		startActivity(intent);
		overridePendingTransition(android.R.anim.fade_in,
				android.R.anim.fade_in);
	}

	/**
	 * 
	 * @Title JumpToActivityForResult
	 * @Description 带返回结果的界面跳转
	 * @param cls
	 *            界面目标跳转目标界面
	 * @param requestCode
	 *            请求参数
	 */
	public void JumpToActivityForResult(Class<?> cls, int requestCode) {
		JumpToActivityForResult(cls, null, requestCode);
	}

	/**
	 * 
	 * @Title JumpToActivityForResult
	 * @Description 带返回结果的界面跳转
	 * @param cls
	 *            目标界面
	 * @param obj
	 *            带过去的参数
	 * @param requestCode
	 *            请求参数
	 */
	public void JumpToActivityForResult(Class<?> cls, Object obj,
			int requestCode) {
		Intent intent = new Intent(this, cls);
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);// 设置跳转标志为如此Activity存在则把其从任务堆栈中取出放到最上方
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		if (obj != null)
			intent.putExtra("data", (Serializable) obj);
		startActivityForResult(intent, requestCode);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// 结束Activity&从堆栈中移除
		AppManager.getAppManager().finishActivity(this);
	}

	public void showLoadingDialog() {
		dialog = new MyProgressDialog(this, true);
		dialog.show();
	}

	public void showLoadingDialog(String title) {
		dialog = new MyProgressDialog(this, title, true);
		dialog.show();
	}

	public void dismissLoadingDialog() {
		dialog.dismiss();
	}

	/** 确认按钮 */
	public interface OnSurePress {
		void onClick(View view);
	}

	/** 确认按钮 */
	public interface OnSurePressNoView {
		void onClick();
	}

	
	/** 确认对话框 */
	@SuppressLint("InflateParams")
	public void showSureDialog(Context context, String hintContent,
			final OnSurePress sureDo, OnCancelListener cancel) {
		final Dialog dialog = new Dialog(context, R.style.MySureDialog);
		View view = getLayoutInflater().inflate(R.layout.dialog_normal, null);
		dialog.setContentView(view);
		TextView WTitle = (TextView) view.findViewById(R.id.dialog_hintContent);
		WTitle.setText(hintContent);
		Button sureButton = (Button) view.findViewById(R.id.dialog_sure);
		Button cancelButton = (Button) view.findViewById(R.id.dialog_cancel);
		sureButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				sureDo.onClick(v);
				dialog.dismiss();
			}
		});
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.setOnCancelListener(cancel);
		dialog.show();
	}

	/** 显示编辑对话框 */
	public void showEditDialog(String title, final OnSurePress sureDo) {
		final View view = new EditText(this);
		new AlertDialog.Builder(this)
				.setTitle(title)
				.setIcon(android.R.drawable.ic_menu_edit)
				.setView(view)
				.setPositiveButton(getResString(R.string.sure),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								sureDo.onClick(view);
								arg0.dismiss();
							}
						})
				.setNegativeButton(getResString(R.string.cancle), null).show();
	}

	/**
	 * 设置图像
	 * 
	 * @param searchAlbumDo
	 * @param takePhotoDo
	 */
	public void showPhotoDialog(final OnSurePressNoView searchAlbumDo,
			final OnSurePressNoView takePhotoDo) {
		new AlertDialog.Builder(BaseActivity.this)
				.setTitle(getResString(R.string.minecenter_setphoto))
				.setIcon(R.drawable.icon_user)
				.setNeutralButton(getResString(R.string.public_album),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								searchAlbumDo.onClick();
								dialog.dismiss();
								// if (MediaUtil.isSDCardExisd()) {
								// MediaUtil.searhcAlbum(MyNoticeActivity.this,
								// MediaUtil.ALBUM);
								// } else {
								// showToast("SDCARD不存在！");
								// }
							}
						})
				.setPositiveButton(getResString(R.string.public_takephoto),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								takePhotoDo.onClick();
								dialog.dismiss();
								// if (MediaUtil.isSDCardExisd()) {
								// MediaUtil.takePhoto(MyNoticeActivity.this,
								// MediaUtil.PHOTO, appPath, userPhotoName);
								// } else {
								// showToast("SDCARD不存在！");
								// }
							}
						})
				.setNegativeButton(getResString(R.string.cancle), null).show()
				.setCanceledOnTouchOutside(true);
	}

	public String getResString(int strId) {
		return getResources().getString(strId);
	}

	public int getResColor(int colId) {
		return getResources().getColor(colId);
	}

	/**
	 * EditText内容为空提示
	 * 
	 * @param edit
	 * @param showToast
	 */
	public boolean cantEmpty(EditText edit, int showToast) {
		if (StringUtils.isStringNone(edit.getText().toString())) {
			UIHelper.ShowMessage(context, getResString(showToast));
			return true;
		}
		return false;
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	public void activityExchangeAnim() {
		rightinleftout();
	}

	public void rightinleftout() {
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	public void zoominzooout() {
		overridePendingTransition(R.anim.zoomin, R.anim.zoomout);
	}
}
