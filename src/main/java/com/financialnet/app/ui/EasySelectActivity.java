package com.financialnet.app.ui;

import java.util.List;

import android.widget.ListView;

import com.financialnet.app.R;
import com.financialnet.app.adapter.EasySelectAdapter;
import com.financialnet.db.dao.DataBookDao;
import com.financialnet.model.DataBook;
import com.financialnet.service.ComonService;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

/**
 * @className：InvestModeActivity.java
 * @author: li mingtao
 * @Function: 投资模式
 * @createDate: 2014-8-19上午11:36:10
 * @update:
 */
public class EasySelectActivity extends BaseActivity {
	private HeaderBar headerBar;

	private String[] setTitle = { "专供投资项目", "投资时间类型", "投资模式" };

	private int setType;// 设置类型
	private ListView listSelect;
	private List<DataBook> selectSet;
	private EasySelectAdapter easySelectAdp;

	public EasySelectActivity() {
		super(R.layout.activity_easyselect);
	}

	@Override
	public void initViews() {
		setType = getIntent().getIntExtra("data", 0);
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(setTitle[setType]);

		listSelect = (ListView) findViewById(R.id.easyselect_list);
		selectSet = ComonService.getInstance(context)
				.getDataBookByType(setType);

		if (selectSet != null) {
			easySelectAdp = new EasySelectAdapter(this, selectSet, setType);
			listSelect.setAdapter(easySelectAdp);
		}
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {

	}

	// private void setSelectedItem(int item) {
	// for (int i = 0; i < listRightS.size(); i++) {
	// listRightS.get(i).setVisibility(View.INVISIBLE);
	// }
	// listRightS.get(item).setVisibility(View.VISIBLE);
	// }

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
