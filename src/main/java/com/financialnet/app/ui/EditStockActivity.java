package com.financialnet.app.ui;

import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.MineOwnStockAdapter;
import com.financialnet.app.adapter.MineSearchStockAdapter;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.model.StockSearchRequest;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.StockService;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @className：EditStockActivity.java
 * @author: lmt
 * @Function: 股票编辑
 * @createDate: 2014-11-26 上午11:19:42
 * @update:
 */
public class EditStockActivity extends BaseActivity implements TextWatcher,
		IXListViewListener {
	private String TAG = "EditStockActivity";
	private HeaderBar headerBar;

	public static final int HAND_ADD_STOCK = 0x001;// 增加股票
	public static final int HAND_DELETE_STOCK = 0x002;// 删除股票
	// ---
	private static final int DEFAULT_SEARCH_SIZE = 10;// 默认获取的最多股票数量
	private static final int START_SEARCH_SIZE = 3;// 开始搜索

	private XListView listOwnStock, listSearchStock;
	private TextView textSearchAdd;
	public EditText editSearchStock;
	private ImageButton imgbtnClearSearch;
	public InputMethodManager mInputMethodManager;
	private MineOwnStockAdapter mineOwnStockAdp;
	private MineSearchStockAdapter mineSearchStockAdp;
	private List<Stock> searchStock, ownStock;
	private int pageNumber = 0;
	private int pageCurrentSize = AppContext.PAGE_SIZE;
	private StockSearchRequest currentStockSearch;

	public EditStockActivity() {
		super(R.layout.activity_edit_stock);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.stock_mine));
		headerBar.setRightText(getResString(R.string.public_finish));

		editSearchStock = (EditText) findViewById(R.id.search_edit);
		imgbtnClearSearch = (ImageButton) findViewById(R.id.ib_clear_text);
		listOwnStock = (XListView) findViewById(R.id.list_ownstock);
		textSearchAdd = (TextView) findViewById(R.id.search_newadd);
		listOwnStock.setPullLoadEnable(false);
		listOwnStock.setPullRefreshEnable(true);
		// ---
		listSearchStock = (XListView) findViewById(R.id.list_searchstock);
		listSearchStock.setPullLoadEnable(false);
		listSearchStock.setPullRefreshEnable(false);
	}

	@Override
	public void initData() {
		mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		ownStock = new ArrayList<Stock>();
		mineOwnStockAdp = new MineOwnStockAdapter(EditStockActivity.this,
				ownStock);
		listOwnStock.setAdapter(mineOwnStockAdp);
		// ---
		searchStock = new ArrayList<Stock>();
		mineSearchStockAdp = new MineSearchStockAdapter(EditStockActivity.this,
				searchStock);
		listSearchStock.setAdapter(mineSearchStockAdp);
	}

	@Override
	public void bindViews() {
		editSearchStock.addTextChangedListener(this);
		listOwnStock.setXListViewListener(this);
		headerBar.top_right_btn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				finish();
			}});
		// 清除按钮关闭编辑框
		imgbtnClearSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (!TextUtils.isEmpty(editSearchStock.getText().toString())) {
					editSearchStock.setText("");
					ownStock.clear();
					mineOwnStockAdp.notifyDataSetChanged();
					mInputMethodManager.hideSoftInputFromWindow(
							editSearchStock.getWindowToken(), 0);
				}
			}
		});

		// 触摸listview关闭编辑框
		listOwnStock.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				mInputMethodManager.hideSoftInputFromWindow(
						editSearchStock.getWindowToken(), 0);
				return false;
			}
		});
	}

	@Override
	public void lastLoad() {
		super.lastLoad();
		myGetMineStockList();
	}
	
	public Handler hand = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case HAND_ADD_STOCK:
				addOwnRefresh((Stock) msg.getData().getSerializable("data"));
				editSearchStock.setText("");
				break;
			case HAND_DELETE_STOCK:
				deleteOwnRefresh((Stock) msg.getData().getSerializable("data"));
				break;
			}
		}
	};

	/**
	 * 增加股票刷新
	 * 
	 * @param stock
	 */
	private void addOwnRefresh(Stock stock) {
		ownStock.add(stock);
		Log.d(TAG, stock.toString());
		mineOwnStockAdp.notifyDataSetChanged();
	}

	/**
	 * 删除股票刷新
	 * 
	 * @param stock
	 */
	private void deleteOwnRefresh(Stock stock) {
		for (Stock pass : ownStock) {
			if (stock.getStockCode().equals(pass.getStockCode())
					&& stock.getMarketCode().equals(pass.getMarketCode())) {
				ownStock.remove(pass);
			}
		}
		mineOwnStockAdp.notifyDataSetChanged();
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	@Override
	public void afterTextChanged(Editable s) {
		if (TextUtils.isEmpty(s)) {
			imgbtnClearSearch.setVisibility(View.GONE);
			listOwnStock.setVisibility(View.VISIBLE);
			listSearchStock.setVisibility(View.GONE);
		} else {
			if (s.toString().length() >= START_SEARCH_SIZE) {
				if (currentStockSearch == null)
					currentStockSearch = new StockSearchRequest();
				currentStockSearch.setStockCode(s.toString());
				currentStockSearch.setPageNumber(0);
				currentStockSearch.setPageSize(DEFAULT_SEARCH_SIZE);
				searchStock();
			} else {
				searchStock.clear();
				mineSearchStockAdp.notifyDataSetChanged();
			}
			imgbtnClearSearch.setVisibility(View.VISIBLE);
			listSearchStock.setVisibility(View.VISIBLE);
			listOwnStock.setVisibility(View.GONE);
		}
	}

	private void myGetMineStockList() {
		StockService.getInstance(context).getMineStock(
				AppContext.getCurrentMember().getMemberID() + "", "0", "100",
				new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							@SuppressWarnings("unchecked")
							List<Stock> tempStok = (List<Stock>) baseModel
									.getResult();
							if (tempStok != null && tempStok.size() > 0) {
								ownStock.clear();
								ownStock.addAll(tempStok);
								mineOwnStockAdp.notifyDataSetChanged();
							}
						}
					}

					@Override
					public void onFinish() {
						super.onFinish();
						listOwnStock.stopRefresh();
						listOwnStock
								.setEmptyView(findViewById(R.id.stock_empty));
					}
				});
	}
	
	private void searchStock() {
		StockService.getInstance(context).searchStock(currentStockSearch,
				new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							@SuppressWarnings("unchecked")
							List<Stock> temp = (List<Stock>) baseModel
									.getResult();
							if (temp != null) {
								searchStock.clear();
								searchStock.addAll(temp);
								mineSearchStockAdp.notifyDataSetChanged();
							}
						}
					}

					@Override
					public void onFinish() {
						super.onFinish();
						listSearchStock
								.setEmptyView(findViewById(R.id.stock_empty));
					}
				});

	}

	@Override
	public void onRefresh() {
		myGetMineStockList();
	}

	@Override
	public void onLoadMore() {

	}
}
