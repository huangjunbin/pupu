package com.financialnet.app.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.PassTransactionAdapter;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.StockService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.JsonUtil;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.umeng.analytics.MobclickAgent;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * PassTransactionDetailActivity.java
 *
 * @author: allen
 * @Function: 达人过往交易详情
 * @createDate: 2014-8-19
 */
@SuppressLint({"HandlerLeak", "InflateParams"})
public class PassTransactionDetailActivity extends BaseActivity implements
        IXListViewListener {
    public static final int UPDTTE_VIEW = 1;
    // -------------
    private HeaderBar headBar;
    private XListView listPassTranstation;
    private PassTransactionAdapter passTransactionAdp;
    private TextView txtStockName, txtStockCode;
    private TextView txtStockAction, txtStockTradeTime;
    private TextView txtStockProfitAndRate;
    private LinearLayout lyStockProfitAndRate;
    private ImageView imgStockImg;
    private DisplayImageOptions stockImgOption;
    private TextView txtStockAppraiseContent;
    private View viewTradeDetail;
    private LinearLayout llStockTitle;
    public Stock currentStock, tempStock, backStock;
    private int pageNum = 0, pageCurrentSize = AppContext.PAGE_SIZE;
    private String id;
    private List<Stock> stockList;
    private boolean isFirstUpdateDetaill = true;
    private int type = 0;
    private TextView tvHistory;
    private TextView tvNumber;
    private ImageView ivImg, ivPhoto;
    private WebView webview;
    private TextView tvTargetPrice,tvStockPrice;

    public PassTransactionDetailActivity() {
        super(R.layout.activity_trade_passtransaction_detail);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onRefresh();
    }

    @Override
    public void initViews() {
        tempStock = (Stock) getIntent().getSerializableExtra("stock");
        type = getIntent().getIntExtra("type", 0);
        id = getIntent().getStringExtra("id");
        headBar = (HeaderBar) findViewById(R.id.title);

        viewTradeDetail = getLayoutInflater().inflate(
                R.layout.headview_passtrade, null);
        llStockTitle = (LinearLayout) viewTradeDetail
                .findViewById(R.id.stock_title);
        txtStockName = (TextView) viewTradeDetail.findViewById(R.id.stock_name);
        txtStockCode = (TextView) viewTradeDetail.findViewById(R.id.stock_code);
        tvTargetPrice= (TextView) viewTradeDetail.findViewById(R.id.stock_target);
        tvStockPrice= (TextView) viewTradeDetail.findViewById(R.id.stock_price);
        tvNumber = (TextView) viewTradeDetail.findViewById(R.id.tv_Number);
        ivImg = (ImageView) viewTradeDetail.findViewById(R.id.stock_img);
        ivPhoto = (ImageView) viewTradeDetail.findViewById(R.id.stock_photo);
        webview = (WebView) viewTradeDetail.findViewById(R.id.webView);
        txtStockAction = (TextView) viewTradeDetail
                .findViewById(R.id.stock_action);
        txtStockTradeTime = (TextView) viewTradeDetail
                .findViewById(R.id.stock_tradetime);
        txtStockProfitAndRate = (TextView) viewTradeDetail
                .findViewById(R.id.stock_profitAndRate);
        lyStockProfitAndRate = (LinearLayout) viewTradeDetail
                .findViewById(R.id.ll_profitAndRate);
        imgStockImg = (ImageView) findViewById(R.id.stock_img);
        txtStockAppraiseContent = (TextView) viewTradeDetail
                .findViewById(R.id.stock_appraiseContent);
        tvHistory = (TextView) viewTradeDetail.findViewById(R.id.tv_history);
        listPassTranstation = (XListView) findViewById(R.id.xlist_stock);
        listPassTranstation.setFooterDividersEnabled(false);
        listPassTranstation.setHeaderDividersEnabled(false);
        listPassTranstation.setPullLoadEnable(false);
        listPassTranstation.setPullRefreshEnable(true);

        if (tempStock == null || TradeDetailActivity.currentTrader == null) {
            headBar.setTitle("过往交易");
        } else {
            headBar.setTitle(TradeDetailActivity.currentTrader.getTraderName());
        }
        initWebView();
    }

    private void updateTitle() {

        if (backStock == null) {
            return;
        }
        txtStockName.setText(backStock.getStockName());
        txtStockCode.setText("(" + backStock.getStockCode()
                + backStock.getMarketCode() + ")");
    }

    @SuppressWarnings("deprecation")
    @Override
    public void initData() {
        stockImgOption = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.cret_bg)
                .showImageForEmptyUri(R.drawable.cret_bg)
                .showImageOnFail(R.drawable.cret_bg).cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();

        stockList = new ArrayList<Stock>();

        passTransactionAdp = new PassTransactionAdapter(
                PassTransactionDetailActivity.this, stockList);
        listPassTranstation.addHeaderView(viewTradeDetail);
        listPassTranstation.addFooterView(getLayoutInflater().inflate(
                R.layout.footview_gray, null));
        listPassTranstation.setAdapter(passTransactionAdp);
    }

    /**
     * 更新股票详情页面
     */
    private void updateStockDetailView() {
        String comments = currentStock.getComments();
        String certificateAddr = currentStock.getCertificateAddr();
        String stockPhotoAddr = currentStock.getStockPhotoAddr();
        String chat = currentStock.getChartResultUrl();
        String targetPrice=currentStock.getTargetPrice();
        String price=currentStock.getPrice();
        if (tempStock != null) {
            currentStock = tempStock;
        }
        if (currentStock.getComments() == null
                || "".equals(currentStock.getComments())) {
            currentStock.setComments(comments);
        }
        currentStock.setCertificateAddr(certificateAddr);
        currentStock.setStockPhotoAddr(stockPhotoAddr);
        currentStock.setChartResultUrl(chat);
        currentStock.setTagetPrice(targetPrice);
        currentStock.setPrice(price);
        Drawable able = null;
        tvTargetPrice.setText(currentStock.getTagetPrice());
        tvStockPrice.setText(currentStock.getPrice());
        if ("1".equals(currentStock.getFlagAuthentication())) {
            able = getResources().getDrawable(R.drawable.cret_v_icon_a);
            txtStockAction.setCompoundDrawablesWithIntrinsicBounds(able, null,
                    null, null);
            ivImg.setVisibility(View.VISIBLE);
            ivPhoto.setVisibility(View.VISIBLE);
            tvNumber.setVisibility(View.VISIBLE);
            AppContext.setImageNo(currentStock.getCertificateAddr(), ivImg,
                    AppContext.imageOption);
            AppContext.setImageNo(currentStock.getStockPhotoAddr(), ivPhoto,
                    AppContext.imageOption);

            tvNumber.setText(currentStock.getSerialNumber());
            ivPhoto.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(
                            PassTransactionDetailActivity.this,
                            BigImageActivity.class);
                    intent.putExtra("url", currentStock.getStockPhotoAddr());
                    startActivity(intent);
                }
            });
        } else {
            if (currentStock.getStockPhotoAddr() != null&&!"".equals(currentStock.getStockPhotoAddr())) {
                ivPhoto.setVisibility(View.VISIBLE);
                AppContext.setImageNo(currentStock.getStockPhotoAddr(), ivPhoto,
                        AppContext.imageOption);
                ivPhoto.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(
                                PassTransactionDetailActivity.this,
                                BigImageActivity.class);
                        intent.putExtra("url", currentStock.getStockPhotoAddr());
                        startActivity(intent);
                    }
                });
            }
            able = getResources().getDrawable(R.drawable.cret_v_icon_b);
            txtStockAction.setCompoundDrawablesWithIntrinsicBounds(able, null,
                    null, null);

        }

        String inoutstr;
        StringBuffer stateSb = new StringBuffer();
        if ("1".equals(currentStock.getFlagTrade() + "")) {
            inoutstr = " 卖出";
            lyStockProfitAndRate.setVisibility(View.VISIBLE);
            findViewById(R.id.ll_price).setVisibility(View.GONE);
        } else {
            inoutstr = " 买入";
            lyStockProfitAndRate.setVisibility(View.GONE);
            findViewById(R.id.ll_price).setVisibility(View.VISIBLE);
        }

        stateSb.append(currentStock.getStockPrice() + inoutstr
                + currentStock.getQuantity() + "股");
        txtStockAction.setText(stateSb.toString());

        txtStockTradeTime.setText(currentStock.getTradeTime());

        StringBuffer prate = new StringBuffer();
        prate.append(currentStock.getEarningsMoney());

        prate.append("(" + currentStock.getEarningPercentage() + ")");
        txtStockProfitAndRate.setText(prate.toString());
        if (currentStock.getEarningPercentage() != null
                && "-".equals(currentStock.getEarningPercentage().substring(0,
                1))) {
            txtStockProfitAndRate.setTextColor(getResources().getColor(
                    R.color.green));
        } else {
            txtStockProfitAndRate.setTextColor(getResources().getColor(
                    R.color.main_color_red));
        }
        AppContext.setImage(currentStock.getCertificateAddr(), imgStockImg,
                stockImgOption);

        txtStockAppraiseContent.setText(currentStock.getComments());
        // 加载 asset 文件

        if (currentStock.getChartResultUrl() != null) {

            webview.loadUrl(currentStock.getChartResultUrl());
        } else {
            webview.setVisibility(View.GONE);
            findViewById(R.id.tv_chart).setVisibility(View.GONE);
        }
    }

    private void initWebView() {
        WebSettings settings = webview.getSettings();
        // 设置WebView属性，能够执行Javascript脚本
        settings.setJavaScriptEnabled(true);

        // 设置WebView不支持缩放 settings.setBuiltInZoomControls(false);
        // 设置WebView允许缓存 settings.setAppCacheEnabled(true);
        String cache_dir = getApplicationContext()
                .getDir("cache", Context.MODE_PRIVATE).getPath();
        settings.setAppCachePath(cache_dir);// 设置应用缓存的路径 // 设置缓存的模式
        // 如果内容已经存在cache
        // 则使用cache，即使是过去的历史记录。如果cache中不存在，从网络中获取
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setAppCacheMaxSize(1024 * 1024 * 8);// 设置应用缓存的最大尺寸
        settings.setAllowFileAccess(true);// 可以读取文件缓存(manifest生效)
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setBlockNetworkImage(false);

        // webview.requestFocus();
    }

    @Override
    public void bindViews() {
        listPassTranstation.setXListViewListener(this);

        llStockTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (backStock != null) {
                    Intent intent = new Intent(context,
                            StockAboutActivity.class);
                    intent.putExtra("stock", backStock);
                    context.startActivity(intent);
                } else {
                    showToast("获取数据失败！");
                }
            }
        });
    }

    /**
     * 查询操盘手过往交易详情
     */
    public void getTraderDetail() {

        if (tempStock != null&&TradeDetailActivity.currentTrader!=null) {
            StockService.getInstance(context).getStockPassTradeDetail(
                    TradeDetailActivity.currentTrader.getTraderId(),
                    tempStock.getMarketID(), tempStock.getStockCode(),
                    pageNum + "", pageCurrentSize + "",
                    new CustomAsyncResponehandler() {
                        @SuppressWarnings("unchecked")
                        public void onSuccess(ResponeModel baseModel) {
                            super.onSuccess(baseModel);
                            if (baseModel.isStatus()) {
                                backStock = JsonUtil.convertJsonToObject(
                                        baseModel.getData(), Stock.class);
                                updateTitle();
                                List<Stock> soc = (List<Stock>) baseModel
                                        .getResult();
                                if (soc != null && soc.size() > 0) {
                                    for (Stock s : soc) {
                                        if (s.getTradeTimeSample() != null && s.getTradeTimeSample().equals(tempStock.getTradeTime())) {
                                            currentStock = s;
                                        }
                                    }
                                    if (currentStock != null) {
                                        currentStock.setChartResultUrl(backStock.getChartResultUrl());
                                        soc.remove(currentStock);
                                    } else {
                                        currentStock = soc.get(0);
                                    }
                                    if (isFirstUpdateDetaill) {


                                        updateStockDetailView();
                                        isFirstUpdateDetaill = false;
                                    }

                                    if (type == 0) {

                                        stockList.clear();
                                        stockList.addAll(soc);

                                        passTransactionAdp
                                                .notifyDataSetChanged();
                                    } else {
                                        tvHistory.setVisibility(View.GONE);
                                    }
                                    if (!StringUtils.isStringNone(baseModel
                                            .getTotalCount())) {
                                        if (stockList.size() >= Integer
                                                .parseInt(baseModel
                                                        .getTotalCount())) {
                                            listPassTranstation
                                                    .setPullLoadEnable(false);
                                        } else {
                                            listPassTranstation
                                                    .setPullLoadEnable(true);
                                        }
                                    }
                                } else {
                                    listPassTranstation
                                            .setPullLoadEnable(false);
                                }
                            }
                        }

                        @Override
                        public void onFinish() {
                            super.onFinish();
                            listPassTranstation.stopLoadMore();
                            listPassTranstation.stopRefresh();
                        }
                    });
        } else {
            StockService.getInstance(context).getSysStockPassTradeDetail(
                    AppContext.getCurrentMember().getMemberID() + "", id,
                    new CustomAsyncResponehandler() {
                        @SuppressWarnings("unchecked")
                        public void onSuccess(ResponeModel baseModel) {
                            super.onSuccess(baseModel);
                            if (baseModel.isStatus()) {
                                backStock = JsonUtil.convertJsonToObject(
                                        baseModel.getData(), Stock.class);
                                List<Stock> soc = (List<Stock>) baseModel
                                        .getResult();
                                if (soc != null && soc.size() > 0) {
                                    currentStock = soc.get(0);
                                    if (backStock != null) {
                                        currentStock.setChartResultUrl(backStock.getChartResultUrl());

                                    }
                                    backStock = soc.get(0);
                                   // soc.clear();


                                    updateTitle();
                                    if (isFirstUpdateDetaill) {

                                        updateStockDetailView();
                                        isFirstUpdateDetaill = false;
                                    }

                                    if (type == 0) {
                                        tvHistory.setVisibility(View.GONE);
                                        stockList.clear();
                                        stockList.addAll(soc);
                                        passTransactionAdp
                                                .notifyDataSetChanged();
                                    } else {
                                        tvHistory.setVisibility(View.GONE);
                                    }
                                    if (!StringUtils.isStringNone(baseModel
                                            .getTotalCount())) {
                                        if (stockList.size() >= Integer
                                                .parseInt(baseModel
                                                        .getTotalCount())) {
                                            listPassTranstation
                                                    .setPullLoadEnable(false);
                                        } else {
                                            listPassTranstation
                                                    .setPullLoadEnable(true);
                                        }
                                    }
                                } else {
                                    listPassTranstation
                                            .setPullLoadEnable(false);
                                }

                            }
                        }

                        @Override
                        public void onFinish() {
                            super.onFinish();
                            listPassTranstation.stopLoadMore();
                            listPassTranstation.stopRefresh();
                        }
                    });
        }
    }

    @Override
    public void onRefresh() {
        listPassTranstation.setRefreshTime(DateUtil.getDateTime(new Date(System
                .currentTimeMillis())));
        pageCurrentSize = AppContext.PAGE_SIZE;
        getTraderDetail();

    }

    @Override
    public void onLoadMore() {
        if (stockList != null) {
            if (stockList.size() >= pageCurrentSize)
                pageCurrentSize += AppContext.PAGE_STEP_SIZE;
        }
        getTraderDetail();
    }

    public Handler hand = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (UPDTTE_VIEW == msg.what) {
                updateStockDetailView();
                showToast("已刷新！");
                listPassTranstation.requestFocusFromTouch();
                listPassTranstation.setSelection(0);
            }
        }
    };

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /*
* 获取html文件
*/
    public String getFromAssets(String fileName) {
        try {
            InputStreamReader inputReader = new InputStreamReader(
                    getResources().getAssets().open(fileName));
            BufferedReader bufReader = new BufferedReader(inputReader);
            String line = "";
            String Result = "";
            while ((line = bufReader.readLine()) != null)
                Result += line;
            return Result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
