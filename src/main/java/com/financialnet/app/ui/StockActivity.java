package com.financialnet.app.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.financialnet.app.R;
import com.financialnet.app.adapter.StockListAdapter;
import com.financialnet.model.Stock;
import com.financialnet.widget.XListView;
import com.umeng.analytics.MobclickAgent;

/**
 * 股票搜索
 * 
 * @author allen
 * @version 1.0.0
 * @created 2014-8-18
 */
public class StockActivity extends BaseActivity {
	private TextView btn_top_title;
	private Button btnBack;
	private XListView lvstock;
	private StockListAdapter stockListAdapter;
	private List<Stock> stockList;

	public Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	public StockActivity() {
		super(R.layout.activity_stock);
	}

	@Override
	public void initViews() {
		btn_top_title = (TextView) findViewById(R.id.btn_top_title);
		btnBack = (Button) findViewById(R.id.btn_top_back);
		lvstock = (XListView) findViewById(R.id.stock_list);
	}

	@Override
	public void initData() {

		stockList = new ArrayList<Stock>();
		stockListAdapter = new StockListAdapter(this, stockList);
	}

	@Override
	public void bindViews() {
		btn_top_title.setText("我的股票");
		btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				StockActivity.this.finish();
			}
		});
		lvstock.setPullRefreshEnable(false);
		lvstock.setPullLoadEnable(false);
		lvstock.setFooterDividersEnabled(false);
		lvstock.setAdapter(stockListAdapter);
		lvstock.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				intent = new Intent();

			}
		});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

}
