package com.financialnet.app.ui;

import java.io.File;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.financialnet.app.AppConfig;
import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.net.Urls;
import com.financialnet.net.http.AsyncHttpClient;
import com.financialnet.net.http.FileHttpResponseHandler;
import com.financialnet.util.FileUtils;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.MyProgressDialog;


public class FileActivity extends BaseActivity {

    private HeaderBar headerBar;
    private TextView tvPath, tvName;
    private Button btnEdit;
    String c;
    private String path = "";
    private MyProgressDialog dialog;
    private final String[][] MIME_MapTable = {
            // {后缀名， MIME类型}
            {".3gp", "video/3gpp"},
            {".apk", "application/vnd.android.package-archive"},
            {".asf", "video/x-ms-asf"},
            {".avi", "video/x-msvideo"},
            {".bin", "application/octet-stream"},
            {".bmp", "image/bmp"},
            {".c", "text/plain"},
            {".class", "application/octet-stream"},
            {".conf", "text/plain"},
            {".cpp", "text/plain"},
            {".doc", "application/msword"},
            {".docx",
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
            {".xls", "application/vnd.ms-excel"},
            {".xlsx",
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
            {".exe", "application/octet-stream"},
            {".gif", "image/gif"},
            {".gtar", "application/x-gtar"},
            {".gz", "application/x-gzip"},
            {".h", "text/plain"},
            {".htm", "text/html"},
            {".html", "text/html"},
            {".jar", "application/java-archive"},
            {".java", "text/plain"},
            {".jpeg", "image/jpeg"},
            {".jpg", "image/jpeg"},
            {".js", "application/x-javascript"},
            {".log", "text/plain"},
            {".m3u", "audio/x-mpegurl"},
            {".m4a", "audio/mp4a-latm"},
            {".m4b", "audio/mp4a-latm"},
            {".m4p", "audio/mp4a-latm"},
            {".m4u", "video/vnd.mpegurl"},
            {".m4v", "video/x-m4v"},
            {".mov", "video/quicktime"},
            {".mp2", "audio/x-mpeg"},
            {".mp3", "audio/x-mpeg"},
            {".mp4", "video/mp4"},
            {".mpc", "application/vnd.mpohun.certificate"},
            {".mpe", "video/mpeg"},
            {".mpeg", "video/mpeg"},
            {".mpg", "video/mpeg"},
            {".mpg4", "video/mp4"},
            {".mpga", "audio/mpeg"},
            {".msg", "application/vnd.ms-outlook"},
            {".ogg", "audio/ogg"},
            {".pdf", "application/pdf"},
            {".png", "image/png"},
            {".pps", "application/vnd.ms-powerpoint"},
            {".ppt", "application/vnd.ms-powerpoint"},
            {".pptx",
                    "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
            {".prop", "text/plain"}, {".rc", "text/plain"},
            {".rmvb", "audio/x-pn-realaudio"}, {".rtf", "application/rtf"},
            {".sh", "text/plain"}, {".tar", "application/x-tar"},
            {".tgz", "application/x-compressed"}, {".txt", "text/plain"},
            {".wav", "audio/x-wav"}, {".wma", "audio/x-ms-wma"},
            {".wmv", "audio/x-ms-wmv"},
            {".wps", "application/vnd.ms-works"}, {".xml", "text/plain"},
            {".z", "application/x-compress"},
            {".zip", "application/x-zip-compressed"}, {"", "*/*"}};

    public FileActivity() {
        super(R.layout.activity_file);

    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.header);
        tvPath = (TextView) findViewById(R.id.tv_path);
        tvName = (TextView) findViewById(R.id.tv_name);
        btnEdit = (Button) findViewById(R.id.btn_edit);
    }

    @Override
    public void initData() {
        c = getIntent().getStringExtra("url");
        dialog = new MyProgressDialog(this, "正在下载", true);

    }

    Handler normalHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            showToast("文件异常,下载失败");
        }
    };

    /**
     * 打开文件
     *
     * @param file
     */
    private void openFile(File file) {

        try {
            Intent intent = new Intent();
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // 设置intent的Action属性
            intent.setAction(Intent.ACTION_VIEW);
            // 获取文件file的MIME类型
            String type = getMIMEType(file);
            // 设置intent的data和Type属性。
            intent.setDataAndType(/* uri */Uri.fromFile(file), type);
            // 跳转
            startActivity(intent);
        } catch (Exception e) {
            showToast("未找到打开该文件的软件");
        }

    }

    /**
     * 根据文件后缀名获得对应的MIME类型。
     *
     * @param file
     */
    private String getMIMEType(File file) {

        String type = "*/*";
        String fName = file.getName();
        // 获取后缀名前的分隔符"."在fName中的位置。
        int dotIndex = fName.lastIndexOf(".");
        if (dotIndex < 0) {
            return type;
        }
        /* 获取文件的后缀名 */
        String end = fName.substring(dotIndex, fName.length()).toLowerCase();
        if (end == "")
            return type;
        // 在MIME和文件类型的匹配表中找到对应的MIME类型。
        for (int i = 0; i < MIME_MapTable.length; i++) { // MIME_MapTable??在这里你一定有疑问，这个MIME_MapTable是什么？
            if (end.equals(MIME_MapTable[i][0]))
                type = MIME_MapTable[i][1];
        }
        return type;
    }

    @Override
    public void bindViews() {
        headerBar.setTitle("文件");
        String[] str = c.split("[.]");
        path = AppConfig.FILEDIR + "/" + "电子合约" + "."
                + str[1];
        tvName.setText("电子合约");
        if (FileUtils.checkFileExists(path)) {
            btnEdit.setText("打开文件");
            tvPath.setText("路径:" + path);
        } else {
            btnEdit.setText("下载文件");
        }
        btnEdit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                String[] str = c.split("[.]");
                path = AppConfig.FILEDIR + "/" + "电子合约" + "."
                        + str[1];
                if (FileUtils.checkFileExists(path)) {
                    openFile(new File(path));
                    return;
                }

                dialog.show();
                AsyncHttpClient httpClient = new AsyncHttpClient();
                System.out.println(Urls.ONLINE_IMAGE_URL
                        + c);
                try {
                    httpClient.download(Urls.ONLINE_IMAGE_URL
                                    + c,
                            new FileHttpResponseHandler(path) {

                                @Override
                                protected void sendFailureMessage(Throwable e,
                                                                  byte[] responseBody) {
                                    normalHandler.sendEmptyMessage(1);
                                    dialog.hide();

                                    super.sendFailureMessage(e, responseBody);
                                }

                                @Override
                                public void onFailure(Throwable error,
                                                      byte[] binaryData) {

                                    dialog.hide();
                                    showToast("文件异常,下载失败");
                                    super.onFailure(error, binaryData);
                                }

                                @Override
                                public void onSuccess(byte[] binaryData) {
                                    dialog.hide();
                                    btnEdit.setText("打开文件");
                                    tvPath.setText("路径:" + path);
                                    super.onSuccess(binaryData);
                                }

                            });
                } catch (Exception e) {

                    dialog.hide();
                    showToast("文件异常,下载失败");
                }
            }
        });
    }

}
