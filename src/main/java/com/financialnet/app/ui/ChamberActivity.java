package com.financialnet.app.ui;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.ChatAdapter;
import com.financialnet.model.DBMessage;
import com.financialnet.util.DateUtil;
import com.financialnet.util.UIHelper;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ChamberActivity.java
 * @author li mingtao
 * @function :会议厅
 * @2013-9-11@下午4:20:56
 * @update:
 */
@SuppressLint("HandlerLeak")
public class ChamberActivity extends BaseActivity implements IXListViewListener {
	private HeaderBar headBar;

	private XListView xlistChatDisplay;
	private ChatAdapter chatAdp;
	private List<DBMessage> messageListAll = new ArrayList<DBMessage>();
	private Button btnSendText;
	private EditText editChatContent;
	private InputMethodManager mInputMethodManager;
	private String testReciver = "陌上花开";

	public ChamberActivity() {
		super(R.layout.activity_chamber);
	}

	@Override
	public void initViews() {
		headBar = (HeaderBar) findViewById(R.id.title);
		headBar.setTitle(getResString(R.string.chat_title));
		headBar.top_right_btn.setTextColor(getResColor(R.color.white));
		headBar.setRightText("119人在线");

		xlistChatDisplay = (XListView) findViewById(R.id.chat_display);
		btnSendText = (Button) findViewById(R.id.chat_send);
		editChatContent = (EditText) findViewById(R.id.chat_content);
	}

	@Override
	public void initData() {
		DBMessage in1 = new DBMessage();
		in1.setSender(testReciver);
		in1.setReceiver(AppContext.getCurrentMember().getMemberName());
		in1.setTimestamp(new Date().getTime() - 1000);
		in1.setContent("这个化身足够对付监狱暴乱和其它叛乱");

		DBMessage in2 = new DBMessage();
		in2.setSender(AppContext.getCurrentMember().getMemberName());
		in2.setReceiver("boby");
		in2.setTimestamp(new Date().getTime() - 800);
		in2.setContent("班那哈罗是猛兽狂怒的极致化身.");

		DBMessage in3 = new DBMessage();
		in3.setSender(testReciver);
		in3.setReceiver(AppContext.getCurrentMember().getMemberName());
		in3.setTimestamp(new Date().getTime() - 1000);
		in3.setContent("最后, 本文介绍包含虚拟人技术的应用系统:团体操演练系统.");

		messageListAll.add(in1);
		messageListAll.add(in2);
		messageListAll.add(in3);

		chatAdp = new ChatAdapter(ChamberActivity.this, messageListAll);
		xlistChatDisplay.setAdapter(chatAdp);
		xlistChatDisplay.setSelection(messageListAll.size());
		xlistChatDisplay.setXListViewListener(this);
		xlistChatDisplay.setPullLoadEnable(false);
		mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
	}

	@Override
	public void bindViews() {
		btnSendText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// 获取当前输入的聊天内容
				String content = editChatContent.getText().toString().trim();
				// 聊天内容不为空时执行
				if (!TextUtils.isEmpty(content)) {
					// 添加聊天信息
					// TODO：发送文本
					DBMessage db = new DBMessage();
					db.setSender(AppContext.getCurrentMember().getMemberName());
					db.setReceiver(testReciver);
					db.setTimestamp(new Date().getTime());
					db.setContent(content);
					showLoadingDialog("正在发送");
					android.os.Message mg = new android.os.Message();
					mg.what = ChatAdapter.SENDSUCCESS;
					mg.obj = db;
					handler.sendMessage(mg);
				}
			}
		});
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case ChatAdapter.SENDSUCCESS:
				dismissLoadingDialog();
				// 更新界面并滚动到最后一条信息,并清空输入框
				messageListAll.add(messageListAll.size(), (DBMessage) msg.obj);
				chatAdp.notifyDataSetChanged();
				xlistChatDisplay.setSelection(messageListAll.size());
				editChatContent.setText("");
				break;
			case ChatAdapter.SENDFAILED:
				dismissLoadingDialog();
				// 更新界面并滚动到最后一条信息,并清空输入框
				messageListAll.add(messageListAll.size(), (DBMessage) msg.obj);
				chatAdp.notifyDataSetChanged();
				xlistChatDisplay.setSelection(messageListAll.size());
				editChatContent.setText("");
				UIHelper.ShowMessage(ChamberActivity.this, "发送失败");
				break;
			}
			super.handleMessage(msg);
			mInputMethodManager.hideSoftInputFromWindow(
					editChatContent.getWindowToken(), 0);
		}
	};

	@Override
	public void onRefresh() {
		xlistChatDisplay.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		xlistChatDisplay.stopRefresh();
	}

	@Override
	public void onLoadMore() {

	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
