package com.financialnet.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.easemob.EMCallBack;
import com.easemob.chat.EMGroup;
import com.easemob.chat.EMGroupManager;
import com.easemob.exceptions.EaseMobException;
import com.financialnet.app.AppContext;
import com.financialnet.app.AppStart;
import com.financialnet.app.R;
import com.financialnet.app.adapter.StockAdapter;
import com.financialnet.app.fragment.ItemFragment;
import com.financialnet.app.fragment.TradeBaseInfoFragment;
import com.financialnet.app.fragment.TradeBlogFragment;
import com.financialnet.app.fragment.TradeCountFragment;
import com.financialnet.app.fragment.TradeInvestFragment;
import com.financialnet.app.fragment.TradePassTransactionFragment;
import com.financialnet.model.Group;
import com.financialnet.model.Member;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.model.Trade;
import com.financialnet.net.Urls;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.CountService;
import com.financialnet.service.IMService;
import com.financialnet.service.MemberService;
import com.financialnet.service.TraderService;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.TabPageIndicator;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

public class TradeDetailActivity extends BaseActivity {
	private static final String[] TITLE = new String[] { "基本资料", "过往交易",
			"今日关注", "现时持仓", "博客专栏", "投资教室", "表现统计" };
	private HeaderBar headBar;
	private ViewPager pager;
	private TabPageIndicator indicator;
	public static int tradeType;
	public static int tradeExtraType;
	private List<Stock> stockList;
	public Trade tempTrade;
	public static Trade currentTrader;// 当前操盘手
	public String actionFlag = null;// {0=过往交易, 1=今日关注, 2=敎学, 3=博客}

	@Override
	public void lastLoad() {
		super.lastLoad();

	}

	public TradeDetailActivity() {
		super(R.layout.activity_trade_detail);
	}

	public void initViews() {
		headBar = (HeaderBar) findViewById(R.id.title);
		pager = (ViewPager) findViewById(R.id.pager);
		indicator = (TabPageIndicator) findViewById(R.id.indicator);
	}

	public void initData() {
		if (AppContext.getCurrentMember() == null) {
			Member member = MemberService.getInstance(context)
					.getCacheMember();

			if (member != null) {
				AppContext.setCurrentMember(member);
			}
		}
		if (AppContext.getCurrentMember() == null) {
			startActivity(new Intent(this, AppStart.class));
			finish();
			return;
		}
		tempTrade = (Trade) getIntent().getSerializableExtra("trade");
		tradeType = getIntent().getIntExtra("trade_type", 0);
		tradeExtraType = getIntent().getIntExtra("trade_extra_type", 0);
		actionFlag = getIntent().getStringExtra("actionFlag");
		// 统计进入改页面数量
		CountService.getInstance(TradeDetailActivity.this)
				.setTraderFirstPageInsert(
						AppContext.getCurrentMember().getMemberID() + "",
						tempTrade.getTraderId() + "", "0",
						new CustomAsyncResponehandler() {
						});
	}

	public void bindViews() {
		headBar.setTitle(tempTrade.getTraderName());
		headBar.top_right_btn.setTextColor(getResources().getColor(
				R.color.white));
		

		TraderService.getInstance(context).getTraderInfo(
				tempTrade.getTraderId(),
				AppContext.getCurrentMember().getMemberID() + "",
				new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							if ((Trade) baseModel.getResult() != null) {
								TradeDetailActivity.currentTrader = (Trade) baseModel
										.getResult();
								tempTrade
										.setMemberType(TradeDetailActivity.currentTrader
												.getMemberType());
								// ViewPager的adapter
								FragmentPagerAdapter adapter = new TabPageIndicatorAdapter(
										getSupportFragmentManager());
								pager.setAdapter(adapter);
								// 实例化TabPageIndicator然后设置ViewPager与之关联
								indicator.setViewPager(pager);
								// 如果我们要对ViewPager设置监听，用indicator设置就行了
								indicator
										.setOnPageChangeListener(new OnPageChangeListener() {
											@Override
											public void onPageSelected(int tabId) {
												// if (tradeType ==
												// TradeActivity.PUPU_TRADER) {
												// if (tabId == 1) {
												// if
												// (AppContext.getCurrentMember().getFlagIsConfirmed()
												// == 1) {
												// Intent intent = new Intent(
												// TradeDetailActivity.this,
												// MemerPlanActivity.class);
												// //
												// 设置跳转标志为如此Activity存在则把其从任务堆栈中取出放到最上方
												// intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
												// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
												// startActivity(intent);
												// overridePendingTransition(android.R.anim.fade_in,
												// android.R.anim.fade_in);
												// } else if
												// (AppContext.getCurrentMember()
												// .getFlagIsConfirmed() == 0) {
												//
												// }
												// } else if (tabId == 2 ||
												// tabId == 3 || tabId == 5) {
												// // 今日关注，现时持仓，达人教学付费
												// Intent intent = new
												// Intent(TradeDetailActivity.this,
												// MemerPlanActivity.class);
												// //
												// 设置跳转标志为如此Activity存在则把其从任务堆栈中取出放到最上方
												// intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
												// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
												// startActivity(intent);
												// overridePendingTransition(android.R.anim.fade_in,
												// android.R.anim.fade_in);
												// }
												// }
											}

											@Override
											public void onPageScrolled(
													int arg0, float arg1,
													int arg2) {

											}

											@Override
											public void onPageScrollStateChanged(
													int arg0) {

											}
										});
								if (tradeExtraType == TradeActivity.NO_PAY_TRADER) {
									indicator.setCurrentItem(1);//
								} else if (tradeExtraType == TradeActivity.NO_PAY_BLOG) {
									indicator.setCurrentItem(4);// 专栏
								} else if (tradeExtraType == TradeActivity.NO_PAY_TREND) {
									indicator.setCurrentItem(1);// 专栏
								} else if (tradeExtraType == TradeActivity.NO_PAY_RANK) {
									indicator.setCurrentItem(1);// 过往交易
								} else if (tradeExtraType == TradeActivity.PAY_ZEWTREND) {
									indicator.setCurrentItem(1);// 过往交易
								} else if (tradeExtraType == TradeActivity.PAY_RANK) {
									indicator.setCurrentItem(1);// 过往交易
								} else if (tradeExtraType == TradeActivity.PAY_TODAYFOURSE) {
									indicator.setCurrentItem(2);// 今日关注
								} else {
									if (AppContext.getCurrentMember()
											.getFlagIsConfirmed() == 1) {
										indicator.setCurrentItem(1);
									} else {
										indicator.setCurrentItem(0);
									}
								}
								if (actionFlag != null
										&& !"".equals(actionFlag)) {
									int flag = Integer.parseInt(actionFlag);
									indicator.setCurrentItem(flag + 1);
								}
								if ("1".equals(tempTrade.getMemberType())) {
									headBar.setRightText(getResources().getString(
											R.string.stock_chamber));
								} else {
									headBar.setRightText(getResources().getString(
											R.string.trader_follow));
								}
							}
						}
					}
				});

		// 会议厅
		headBar.top_right_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (AppContext.getCurrentMember().getFlagIsConfirmed() == 1
						&& "1".equals(tempTrade.getMemberType())) {

					IMService.getInstance(TradeDetailActivity.this)
							.getGroupByTradeId(tempTrade.getTraderId() + "",
									new CustomAsyncResponehandler() {

										@Override
										public void onSuccess(
												ResponeModel baseModel) {

											Group groupList = (Group) baseModel
													.getResult();
											if (groupList == null
													|| groupList.getGroupID() == null) {
												showToast(tempTrade
														.getTraderName()
														+ "尚未开通会议厅");
											} else {
												EMGroup group = EMGroupManager
														.getInstance()
														.getGroup(
																groupList
																		.getGroupID());
												if (group != null) {
													// 进入群聊
													Intent intent = new Intent(
															TradeDetailActivity.this,
															ChatActivity.class);
													// it is group chat
													intent.putExtra(
															"chatType",
															ChatActivity.CHATTYPE_GROUP);
													intent.putExtra(
															"groupId",
															groupList
																	.getGroupID());
													startActivityForResult(
															intent, 0);
												} else {
													MemberService.getInstance(TradeDetailActivity.this).loginHX(
															AppContext.getCurrentMember().getMobileNumber(), AppContext.getCurrentMember().getPassword(), new EMCallBack() {
																@Override
																public void onSuccess() {
																	new Thread() {
																		public void run() {
																			// 存储所有群组
																			try {
																				List<EMGroup> grouplist = EMGroupManager.getInstance().getGroupsFromServer();
																			} catch (EaseMobException e) {
																				e.printStackTrace();
																			}
																		}

																		;
																	}.start();
																}

																@Override
																public void onProgress(int progress, String status) {
																	Log.d("HX", "onProgress");
																}

																@Override
																public void onError(int code, final String message) {
																	Log.d("HX", "onError");

																}
															});
													showToast("获取群组信息失败,请重试.");
												}
											}
											super.onSuccess(baseModel);
										}

									}, true);

				} else {
					JumpToActivity(MemerPlanActivity.class);
				}
			}
		});
	}

	/**
	 * ViewPager适配器
	 * 
	 * @author len
	 * 
	 */
	class TabPageIndicatorAdapter extends FragmentPagerAdapter {
		public TabPageIndicatorAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// 新建一个Fragment来展示ViewPager item的内容，并传递参数
			if (position == 0) {
				return TradeBaseInfoFragment.newInstance(position);
			} else if (position == 1) {
				stockList = new ArrayList<Stock>();
				return TradePassTransactionFragment.newInstance(position,
						StockAdapter.PASS_TRADE, stockList);
				// }
			} else if (position == 2) {
				stockList = new ArrayList<Stock>();
				return TradePassTransactionFragment.newInstance(position,
						StockAdapter.ATTENTION_TODAY, stockList);
			} else if (position == 3) {
				stockList = new ArrayList<Stock>();
				return TradePassTransactionFragment.newInstance(position,
						StockAdapter.HOLD_NOW, stockList);
			} else if (position == 4) {

				return TradeBlogFragment.newInstance(position);
			} else if (position == 5) {
				return TradeInvestFragment.newInstance(position);
			} else if (position == 6) {
				Fragment fragment = new TradeCountFragment();
				Bundle args = new Bundle();
				args.putString("url", Urls.tradecount + "?traderId="
						+ tempTrade.getTraderId());
				fragment.setArguments(args);
				return fragment;
			} else {
				Fragment fragment = new ItemFragment();
				Bundle args = new Bundle();
				args.putString("arg", TITLE[position]);
				fragment.setArguments(args);
				return fragment;
			}
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {

		}

		@Override
		public CharSequence getPageTitle(int position) {
			return TITLE[position % TITLE.length];
		}

		@Override
		public int getCount() {
			return TITLE.length;
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
