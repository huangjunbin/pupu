package com.financialnet.app.ui;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

import com.financialnet.app.R;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

/**
 * @className: TraderRankActivity.java
 * @author: limingtao
 * @function: 操盘手排行榜
 * @date: 2013年9月12日下午5:31:17
 * @update:
 */
public class TraderRankActivity extends BaseActivity implements OnClickListener {
	private HeaderBar headerBar;
	private View viewRank7, viewRank30, viewRankLong;
	private Intent intent;

	public TraderRankActivity() {
		super(R.layout.activity_traderrank);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.trader_rank));
		viewRank7 = findViewById(R.id.trader_rank_7);
		viewRank30 = findViewById(R.id.trader_rank_30);
		viewRankLong = findViewById(R.id.trader_rank_long);
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		viewRank7.setOnClickListener(this);
		viewRank30.setOnClickListener(this);
		viewRankLong.setOnClickListener(this);
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.trader_rank_7:
			intent = new Intent();
			intent.putExtra("trade_type", TradeActivity.SEVEVN_PROFIT_RANK);
			intent.setClass(context, TradeActivity.class);
			context.startActivity(intent);
			break;
		case R.id.trader_rank_30:
			intent = new Intent();
			intent.putExtra("trade_type", TradeActivity.THIETY_PROFIT_RANK);
			intent.setClass(context, TradeActivity.class);
			context.startActivity(intent);
			break;
		case R.id.trader_rank_long:
			intent = new Intent();
			intent.putExtra("trade_type", TradeActivity.STEADY_PROFIT_RANK);
			intent.setClass(context, TradeActivity.class);
			context.startActivity(intent);
			break;
		}
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
