package com.financialnet.app.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.HotInfoAdapter;
import com.financialnet.model.HotInfo;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.HotInfoService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;
import com.umeng.analytics.MobclickAgent;

/**
 * @className：HotInfoActivity.java
 * @author: li mingtao
 * @Function: 热门资讯
 * @createDate: 2014-8-18下午3:52:08
 * @update:
 */
public class HotInfoActivity extends BaseActivity implements IXListViewListener {
	String TAG = "HotInfoActivity";
	private HeaderBar headerBar;
	// for XListView
	private XListView xlistHotInfo;
	private List<HotInfo> listHotInfoData;
	private HotInfoAdapter hotInfoAdp;
	private int curPageNum = 0, curPageSize = AppContext.PAGE_SIZE;

	@Override
	public void lastLoad() {
		super.lastLoad();
		onRefresh();
	}

	public HotInfoActivity() {
		super(R.layout.activity_hotinfo);
	}

	@Override
	public void initViews() {
		// for title
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.hot_title));

		// for Xlist
		xlistHotInfo = (XListView) findViewById(R.id.hotinfo_list);
		xlistHotInfo.setHeaderDividersEnabled(false);
		xlistHotInfo.setFooterDividersEnabled(false);
		xlistHotInfo.setPullLoadEnable(false);
		xlistHotInfo.setPullRefreshEnable(true);
	}

	@Override
	public void initData() {
		listHotInfoData = new ArrayList<HotInfo>();

		hotInfoAdp = new HotInfoAdapter(this, listHotInfoData,
				R.drawable.news_ph_tn_default,
				HotInfoDetailActivity.COMEFROM_HOTINFO);
		xlistHotInfo.setAdapter(hotInfoAdp);
	}

	@Override
	public void bindViews() {
		xlistHotInfo.setXListViewListener(this);
	}

	private void getHotinfoList() {
		HotInfoService.getInstance(context).getHotinfoList(curPageNum + "",
				curPageSize + "", new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							@SuppressWarnings("unchecked")
							List<HotInfo> tempHotinfo = (List<HotInfo>) baseModel
									.getResult();
							if (tempHotinfo != null && tempHotinfo.size() > 0) {
								listHotInfoData.clear();
								listHotInfoData.addAll(tempHotinfo);
								hotInfoAdp.notifyDataSetChanged();
							}

							if (!StringUtils.isStringNone(baseModel
									.getTotalCount())) {
								if (listHotInfoData.size() >= Integer
										.valueOf(baseModel.getTotalCount())) {
									xlistHotInfo.setPullLoadEnable(false);
								} else {
									xlistHotInfo.setPullLoadEnable(true);
								}
							}
						}
					}
				});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	public void onRefresh() {
		xlistHotInfo.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		curPageSize = AppContext.PAGE_SIZE;
		getHotinfoList();
	}

	@Override
	public void onLoadMore() {
		curPageSize += AppContext.PAGE_STEP_SIZE;
		getHotinfoList();
	}
}
