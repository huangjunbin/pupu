package com.financialnet.app.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.adapter.MineStockAdapter;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.StockService;
import com.financialnet.util.DateUtil;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.HeaderBar;
import com.financialnet.widget.XListView;
import com.financialnet.widget.XListView.IXListViewListener;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @className：MineStockActivity.java
 * @author: lmt
 * @Function:我的股票
 * @createDate: 2014-10-17 下午3:09:56
 * @update:
 */
@SuppressLint("ClickableViewAccessibility")
public class MineStockActivity extends BaseActivity implements
		IXListViewListener {
 
	private static int EDIT_STOCK = 1;// 股票编辑
	public static final int MINMAXSTOKNUM = 10;// 默认可以拥有的最大股票数量
	private HeaderBar headerBar;
	private XListView listMineStock;
	private List<Stock> mineStock;
	private MineStockAdapter mineStockAdapter;

	// private List<Stock> searchStock;
	// private MineSearchStockAdapter MineSearchStockAdp;
	// private View viewSeachStock;
	// private XListView listSearchStock;
	// private TextView textSearchAdd;
	// public EditText editSearchStock;
	// private ImageButton imgbtnClearSearch;
	// public InputMethodManager mInputMethodManager;

	private int pageNumber = 0;
	private int pageCurrentSize = AppContext.PAGE_SIZE;

	// private StockSearchRequest currentStockSearch;

	public MineStockActivity() {
		super(R.layout.layout_minestock);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.stock_mine));
		headerBar.setRightText(getResString(R.string.public_edit));
		headerBar.top_right_btn.setTextColor(getResources().getColor(
				R.color.white));
		// viewSeachStock = findViewById(R.id.minestock_search);
		// editSearchStock = (EditText) findViewById(R.id.search_edit);
		// imgbtnClearSearch = (ImageButton) findViewById(R.id.ib_clear_text);
		// listSearchStock = (XListView) findViewById(R.id.list_allstock);
		// textSearchAdd = (TextView) findViewById(R.id.search_newadd);
		// listSearchStock.setPullLoadEnable(false);
		// listSearchStock.setPullRefreshEnable(false);
		// listSearchStock.setEmptyView(findViewById(R.id.stock_empty));
		listMineStock = (XListView) findViewById(R.id.list_minestock);
		listMineStock.setPullLoadEnable(false);
		listMineStock.setPullRefreshEnable(false);
	}
	
	@Override
	public void initData() {
		// mInputMethodManager = (InputMethodManager)
		// getSystemService(INPUT_METHOD_SERVICE);
		// ---
		mineStock = new ArrayList<Stock>();
		mineStockAdapter = new MineStockAdapter(MineStockActivity.this,
				mineStock);
		listMineStock.setAdapter(mineStockAdapter);
	}

	@Override
	public void bindViews() {
		listMineStock.setXListViewListener(this);
		// // 新增
		// textSearchAdd.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View arg0) {
		//
		// }
		// });

		// editSearchStock.addTextChangedListener(this);
		// // 清除按钮关闭编辑框
		// imgbtnClearSearch.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View arg0) {
		// if (!TextUtils.isEmpty(editSearchStock.getText().toString())) {
		// editSearchStock.setText("");
		// searchStock.clear();
		// MineSearchStockAdp.notifyDataSetChanged();
		// mInputMethodManager.hideSoftInputFromWindow(
		// editSearchStock.getWindowToken(), 0);
		// }
		// }
		// });
		//
		// // 触摸listview关闭编辑框
		// listSearchStock.setOnTouchListener(new OnTouchListener() {
		// @Override
		// public boolean onTouch(View arg0, MotionEvent arg1) {
		// mInputMethodManager.hideSoftInputFromWindow(
		// editSearchStock.getWindowToken(), 0);
		// return false;
		// }
		// });

		headerBar.top_right_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startActivityForResult(new Intent(MineStockActivity.this,
						EditStockActivity.class), EDIT_STOCK);
				// if (isEdit) {
				// headerBar.setRightText(getResString(R.string.public_edit));
				// viewSeachStock.setVisibility(View.GONE);
				// listMineStock.setVisibility(View.VISIBLE);
				// if (searchStock != null && searchStock.size() > 0) {
				// searchStock.clear();
				// MineSearchStockAdp.notifyDataSetChanged();
				// editSearchStock.setText("");
				// }
				//
				// onRefresh();
				// mInputMethodManager.hideSoftInputFromWindow(
				// editSearchStock.getWindowToken(), 0);
				// isEdit = false;
				// } else {
				// headerBar
				// .setRightText(getResString(R.string.public_finish));
				// viewSeachStock.setVisibility(View.VISIBLE);
				// listMineStock.setVisibility(View.GONE);
				// if (searchStock == null) {
				// searchStock = new ArrayList<Stock>();
				// MineSearchStockAdp = new MineSearchStockAdapter(
				// MineStockActivity.this, searchStock, mineStock);
				// listSearchStock.setAdapter(MineSearchStockAdp);
				// } else {
				// MineSearchStockAdp.notifyDataSetChanged();
				// }
				//
				// isEdit = true;
				// }
			}
		});
	}

	// private void searchStock() {
	// StockService.getInstance(context).searchStock(currentStockSearch,
	// new CustomAsyncResponehandler() {
	// @Override
	// public void onSuccess(ResponeModel baseModel) {
	// super.onSuccess(baseModel);
	// if (baseModel.isStatus()) {
	// @SuppressWarnings("unchecked")
	// List<Stock> temp = (List<Stock>) baseModel
	// .getResult();
	// if (temp != null) {
	// searchStock.clear();
	// searchStock.addAll(temp);
	// MineSearchStockAdp.notifyDataSetChanged();
	// }
	// }
	// }
	// });
	//
	// }

	// @Override
	// public void afterTextChanged(Editable arg0) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// @Override
	// public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
	// int arg3) {
	// // TODO Auto-generated method stub
	// }
	//
	// @Override
	// public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
	// if (TextUtils.isEmpty(s)) {
	// imgbtnClearSearch.setVisibility(View.GONE);
	// } else {
	// if (s.toString().length() >= START_SEARCH_SIZE) {
	// if (currentStockSearch == null)
	// currentStockSearch = new StockSearchRequest();
	// currentStockSearch.setStockCode(s.toString());
	// currentStockSearch.setPageNumber(0);
	// currentStockSearch.setPageSize(DEFAULT_SEARCH_SIZE);
	// searchStock();
	// } else {
	// searchStock.clear();
	// MineSearchStockAdp.notifyDataSetChanged();
	// }
	// imgbtnClearSearch.setVisibility(View.VISIBLE);
	// }
	// }

	@Override
	public void onRefresh() {
		listMineStock.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		pageCurrentSize = AppContext.PAGE_SIZE;
		myGetMineStockList();
	}

	@Override
	public void onLoadMore() {
		if (mineStock != null) {
			if (mineStock.size() >= pageCurrentSize)
				pageCurrentSize += AppContext.PAGE_STEP_SIZE;
		}
		myGetMineStockList();
	}

	private void myGetMineStockList() {
		StockService.getInstance(context).getMineStock(
				AppContext.getCurrentMember().getMemberID() + "", pageNumber + "",
				pageCurrentSize + "", new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel.isStatus()) {
							@SuppressWarnings("unchecked")
							List<Stock> tempStok = (List<Stock>) baseModel
									.getResult();
							if (tempStok != null && tempStok.size() > 0) {
								mineStock.clear();
								mineStock.addAll(tempStok);
								mineStockAdapter.notifyDataSetChanged();
								if (!StringUtils.isStringNone(baseModel
										.getTotalCount())) {
									if (mineStock.size() >= Integer
											.parseInt(baseModel.getTotalCount())) {
										listMineStock.setPullLoadEnable(false);
									} else {
										listMineStock.setPullLoadEnable(false);
									}
								}
							} else {
								listMineStock.setPullLoadEnable(false);
							}
						}
					}
					
					@Override
					public void onFinish() {
						super.onFinish();
						listMineStock.stopLoadMore();
						listMineStock.stopRefresh();
					}
				});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		onRefresh();
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
