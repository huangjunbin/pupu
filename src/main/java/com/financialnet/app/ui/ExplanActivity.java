package com.financialnet.app.ui;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.financialnet.app.R;
import com.umeng.analytics.MobclickAgent;

/**
 * 项目说明
 * 
 * @author allen
 * @version 1.0.0
 * @created 2014-7-4
 */
public class ExplanActivity extends BaseActivity {
	private TextView btn_top_title;
	private Button btnBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	public ExplanActivity() {
		super(R.layout.activity_explan);
	}

	@Override
	public void initViews() {
		btn_top_title = (TextView) findViewById(R.id.btn_top_title);
		btnBack = (Button) findViewById(R.id.btn_top_back);
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		btn_top_title.setText("项目说明");
		btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ExplanActivity.this.finish();
			}
		});
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

}
