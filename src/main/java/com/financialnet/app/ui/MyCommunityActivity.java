package com.financialnet.app.ui;

import com.financialnet.app.R;
import com.financialnet.widget.HeaderBar;
import com.umeng.analytics.MobclickAgent;

/**
 * @className：MyCommunityActivity.java
 * @author: li mingtao
 * @Function: 我的社区
 * @createDate: 2014-8-18下午3:22:24
 * @update:
 */
public class MyCommunityActivity extends BaseActivity {
	private HeaderBar headerBar;

	public MyCommunityActivity() {
		super(R.layout.activity_mycommunity);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle("我的社区");
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

}
