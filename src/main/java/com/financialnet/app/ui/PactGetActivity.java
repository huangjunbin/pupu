package com.financialnet.app.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.MemberService;
import com.financialnet.util.StringUtils;
import com.financialnet.util.UIHelper;
import com.financialnet.widget.HeaderBar;


public class PactGetActivity extends BaseActivity {
    private HeaderBar headerBar;
    private EditText editTextContent, editTextTel;

    public PactGetActivity() {
        super(R.layout.activity_feedback);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.header);
        headerBar.setTitle("索取合约正本");
        headerBar.top_right_btn.setText("发出");

        editTextContent = (EditText) findViewById(R.id.feedback_content);
        editTextTel = (EditText) findViewById(R.id.feedback_tel);
    }

    @Override
    public void initData() {

    }

    @Override
    public void bindViews() {
        headerBar.top_right_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkInput()) {

                }
            }
        });
    }

    private boolean checkInput() {
        if (StringUtils.isEmpty(editTextContent.getText().toString())) {
            UIHelper.ShowMessage(context, "请先输入地址");
            return false;
        }
        if (StringUtils.isEmpty(editTextTel.getText().toString())) {
            UIHelper.ShowMessage(context, "请输入联系电话");
            return false;
        }

        MemberService.getInstance(PactGetActivity.this).redSwanRequestContractPDF(AppContext.getCurrentMember().getMemberID() + "", editTextTel.getText().toString(), editTextContent.getText().toString(), new CustomAsyncResponehandler() {
            public void onFinish() {

            }

            ;

            @SuppressWarnings("unchecked")
            @Override
            public void onSuccess(ResponeModel baseModel) {
                super.onSuccess(baseModel);
                if (baseModel != null && baseModel.isStatus()) {
                    showToast(baseModel.getMsg());
                    PactGetActivity.this.finish();
                }
            }
        });
        return true;
    }
}
