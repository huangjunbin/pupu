package com.financialnet.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ProgressBar;

import com.easemob.EMCallBack;
import com.easemob.chat.EMGroup;
import com.easemob.chat.EMGroupManager;
import com.easemob.exceptions.EaseMobException;
import com.financialnet.app.ui.BaseActivity.OnSurePress;
import com.financialnet.app.ui.LoginActivity;
import com.financialnet.app.ui.MainActivity;
import com.financialnet.model.Member;
import com.financialnet.service.MemberService;
import com.financialnet.util.UIHelper;
import com.umeng.analytics.MobclickAgent;

import java.util.List;

/**
 * 应用程序启动类：显示欢迎界面并跳转到主界面
 *
 * @author allen
 * @version 1.0.0
 * @created 2014-4-16
 */
public class AppStart extends Activity {
    private String TAG = "AppStart";
    private Member member;
    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final View view = View.inflate(this, R.layout.activity_start, null);
        setContentView(view);
        // 渐变展示启动屏
        progressBar = (ProgressBar) findViewById(R.id.progress);
        AlphaAnimation aa = new AlphaAnimation(0.3f, 1.0f);
        aa.setDuration(2000);
        view.startAnimation(aa);
        aa.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationStart(Animation animation) {

                    preLogin();

            }
        });
    }

    private void preLogin() {
        member = MemberService.getInstance(AppStart.this).getCacheMember();

        if (member != null && member.getIsRemember() == 1) {
            AppContext.setCurrentMember(member);
            // 登陆环信
            MemberService.getInstance(AppStart.this).loginHX(
                    member.getMobileNumber(), member.getPassword(),new EMCallBack() {
                @Override
                public void onSuccess() {
                    new Thread() {
                        public void run() {
                            // 存储所有群组
                            try {
                                List<EMGroup> grouplist = EMGroupManager.getInstance().getGroupsFromServer();
                            } catch (EaseMobException e) {
                                e.printStackTrace();
                            }
                        };
                    }.start();
                }

                @Override
                public void onProgress(int progress, String status) {
                    Log.d("HX", "onProgress");
                }

                @Override
                public void onError(int code, final String message) {
                    Log.d("HX", "onError");

                }
            });
            Intent goMain = new Intent(AppStart.this,
                    MainActivity.class);
            startActivity(goMain);
            finish();
//            // 获取用户好友列表
//            IMService.getInstance(AppStart.this).getFriendsListS(
//                    member.getMobileNumber(), new CustomAsyncResponehandler() {
//                        @Override
//                        public void onSuccess(ResponeModel baseModel) {
//                            super.onSuccess(baseModel);
//                            if (baseModel.isStatus()) {
//
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Throwable error, String content) {
//                            super.onFailure(error, content);
//                            Message msg = new Message();
//                            msg.what = 1;
//                            hand.sendMessageDelayed(msg, 2000);
//                        }
//                    }, false);
        } else {
            Intent goLogin = new Intent(AppStart.this, LoginActivity.class);
            startActivity(goLogin);
            finish();
        }
    }

    Handler hand = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    UIHelper.showSysDialog(AppStart.this, AppStart.this
                                    .getResources().getString(R.string.init_error),
                            new OnSurePress() {
                                @Override
                                public void onClick(View view) {
                                    AppStart.this.finish();
                                }
                            }, false);
                    break;
            }
            super.handleMessage(msg);
        }
    };

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
