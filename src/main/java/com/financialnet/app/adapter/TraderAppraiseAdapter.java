package com.financialnet.app.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.financialnet.app.R;
import com.financialnet.model.TraderAppraise;
import com.financialnet.util.DateUtil;
import com.financialnet.util.StringUtils;

/**
 * @className：@TraderAppraiseAdapter.java
 * @author: li mingtao
 * @Function:操盘手评论适配器
 * @createDate: @d2014-8-30@下午11:16:22
 * @update:
 */
public class TraderAppraiseAdapter extends BaseListAdapter<TraderAppraise> {

	public TraderAppraiseAdapter(Activity context, List<TraderAppraise> list) {
		super(context, list);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TraderAppraise traderAppraise = mList.get(position);
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_appraise, null);
			holder.appraiseRate = (RatingBar) convertView
					.findViewById(R.id.trader_appraiseBar);
			holder.appraiseContent = (TextView) convertView
					.findViewById(R.id.appraise_content);
			holder.appraiseTime = (TextView) convertView
					.findViewById(R.id.appraise_time);
			holder.appraiser = (TextView) convertView
					.findViewById(R.id.appraiser);
			holder.appraiseRate.setMax(10);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if (!StringUtils.isStringNone(traderAppraise.getScore())) {
			float score = Float.parseFloat(traderAppraise.getScore());
			holder.appraiseRate.setProgress((int) score);
		}

		if (!StringUtils.isStringNone(traderAppraise.getComment())) {
			holder.appraiseContent.setText(traderAppraise.getComment());
		}

		if (!StringUtils.isStringNone(traderAppraise.getCreateDate())) {
			holder.appraiseTime.setText(traderAppraise.getCreateDate());
		}
		
		if (!StringUtils.isStringNone(traderAppraise.getNickName())) {
			holder.appraiser.setText(traderAppraise.getNickName() + "评论");
		}

		return convertView;
	}

	class ViewHolder {
		RatingBar appraiseRate;
		TextView appraiseContent;
		TextView appraiseTime;
		TextView appraiser;
	}
}
