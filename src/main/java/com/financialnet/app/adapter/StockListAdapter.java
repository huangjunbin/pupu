package com.financialnet.app.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.model.Stock;

/**
 * 操盘手
 * 
 * @author allen
 * @time 2014-8-18
 */
public class StockListAdapter extends BaseListAdapter<Stock> {

	/**
	 * @param context
	 * @param list
	 */
	@SuppressWarnings("unused")
	private Context mContext;

	public StockListAdapter(BaseActivity context, List<Stock> list) {
		super(context, list);
		mContext = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Stock stock = mList.get(position);
		View rowView = convertView;
		ViewHolder holder = null;
		if (rowView == null) {
			holder = new ViewHolder();
			rowView = mInflater.inflate(R.layout.item_stock_list, null);
			holder.name_tv = (TextView) rowView
					.findViewById(R.id.item_stock_List_name);
			holder.profit_tv = (TextView) rowView
					.findViewById(R.id.item_stock_List_profit);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		holder.name_tv.setText(stock.getName());
		holder.profit_tv.setText(stock.getIndex());
		return rowView;
	}

	class ViewHolder {
		public TextView name_tv;
		public TextView profit_tv;

	}

}
