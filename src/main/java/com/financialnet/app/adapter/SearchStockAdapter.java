package com.financialnet.app.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.app.ui.StockAboutActivity;
import com.financialnet.model.SearchStock;

/**
 * @className：SearchStockAdapter.java
 * @author: li mingtao
 * @Function:股票搜索适配器
 * @createDate: d2014-8-19下午11:38:54
 * @update:
 */
public class SearchStockAdapter extends BaseListAdapter<SearchStock> implements
		Filterable {

	private List<SearchStock> listAllStocks;
	private ListView listView;

	public SearchStockAdapter(BaseActivity context, List<SearchStock> allList,
			ListView listview) {
		super(context, null);
		listAllStocks = allList;
		listView = listview;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.search_list_item, null);
			holder.textSearchOlder = (TextView) convertView
					.findViewById(R.id.search_older);
			holder.textSearchName = (TextView) convertView
					.findViewById(R.id.search_name);
			convertView.setTag(R.id.tag_view, holder);
		} else {
			holder = (ViewHolder) convertView.getTag(R.id.tag_view);
		}
		convertView.setTag(R.id.tag_id, position);
		convertView.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// int id = (Integer) v.getTag(R.id.tag_id);
				Intent intentToStock = new Intent(baseActivity,
						StockAboutActivity.class);
				baseActivity.startActivity(intentToStock);
			}
		});
		SearchStock data = mList.get(position);
		holder.textSearchOlder.setText(data.getSearchOlder());
		holder.textSearchName.setText(data.getSearchName());
		return convertView;
	}

	class ViewHolder {
		TextView textSearchOlder, textSearchName;
	}

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {
			@SuppressWarnings("unchecked")
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				mList = (ArrayList<SearchStock>) results.values;
				setListViewHeightBasedOnChildren(listView);
				if (results.count > 0) {
					notifyDataSetChanged();
				} else {
					notifyDataSetInvalidated();
				}
			}

			protected FilterResults performFiltering(CharSequence s) {
				// String str = s.toString().toUpperCase();
				// mFilterStr = str;
				FilterResults results = new FilterResults();
				List<SearchStock> ctockList = new ArrayList<SearchStock>();
				if (listAllStocks != null) {
					ctockList = listAllStocks;
				}
				results.values = ctockList;
				results.count = ctockList.size();
				return results;
			}
		};
		return filter;
	}

	public void setListViewHeightBasedOnChildren(ListView listView) {
		SearchStockAdapter listAdapter = (SearchStockAdapter) listView
				.getAdapter();
		if (listAdapter == null) {
			return;
		}

		int totalHeight = 0;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		((MarginLayoutParams) params).setMargins(10, 10, 10, 10);
		listView.setLayoutParams(params);
	}
}
