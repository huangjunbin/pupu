package com.financialnet.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.HoldDetailActivity;
import com.financialnet.app.ui.TradeActivity;
import com.financialnet.app.ui.TradeDetailActivity;
import com.financialnet.model.Stock;
import com.financialnet.model.Trade;

@SuppressLint("InflateParams")
public class HoldDoyenAdapter extends BaseListAdapter<Trade> {
	public HoldDoyenAdapter(Activity context, List<Trade> list, int defaultId) {
		super(context, list, defaultId);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.holdhoyen_item, null);
			holder.imgviewDoyenPhoto = (ImageView) convertView
					.findViewById(R.id.doyen_photo);
			holder.textDoyenNickName = (TextView) convertView
					.findViewById(R.id.doyen_name);
			holder.textDoyenProfit = (TextView) convertView
					.findViewById(R.id.doyen_profit);
			holder.textDoyenPrice = (TextView) convertView
					.findViewById(R.id.doyen_price);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final Trade data = mList.get(position);
		AppContext.setImageNo(data.getAvatarsAddress(),
				holder.imgviewDoyenPhoto, options);

		holder.textDoyenNickName.setText(data.getTraderName());
		holder.textDoyenProfit.setText(data.getEarningsMoney());
		holder.textDoyenPrice.setText(data.getCostPrice());
		if (data.getEarningPercentage() != null
				&& data.getEarningPercentage().length() > 0

				&& "-".equals(data.getEarningPercentage().substring(0, 1))) {
			holder.textDoyenProfit.setTextColor(activity.getResources()
					.getColor(R.color.green));
		} else {
			holder.textDoyenProfit.setTextColor(activity.getResources()
					.getColor(R.color.main_color_red));
		}
		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent();
			 
				
				intent = new Intent();
				intent.putExtra("trade_type",
						TradeActivity.MY_TRADER);
				intent.putExtra("trade_extra_type",
						TradeActivity.PAY_ZEWTREND);
				Trade trade = new Trade();
				trade.setTraderId(data.getTraderId());
				trade.setTraderName(data.getTraderName());
				intent.putExtra("trade", trade);
		 
				intent.setClass(activity, TradeDetailActivity.class);
 
				activity.startActivity(intent);
			}
		});

		return convertView;
	}

	class ViewHolder {
		ImageView imgviewDoyenPhoto;
		TextView textDoyenNickName, textDoyenProfit, textDoyenPrice;
	}

	private Stock tradeToStock(Trade trade) {
		Stock stock = new Stock();
		stock.setStockCode(trade.getStockCode());
		stock.setMarketID(trade.getMarketID());
		stock.setTraderId(trade.getTraderId());

		return stock;
	}
}
