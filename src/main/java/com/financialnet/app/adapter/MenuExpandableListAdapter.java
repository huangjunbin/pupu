package com.financialnet.app.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.db.dao.NotificationDao;
import com.financialnet.model.MenuClass;

public class MenuExpandableListAdapter implements ExpandableListAdapter {
	private Context context;
	@SuppressWarnings("unused")
	private Intent intent;
	private MenuClass menuClass;
	private NotificationDao dao;

	public MenuExpandableListAdapter(Context context) {
		this.context = context;
		dao = new NotificationDao(context);
		armTypes = new ArrayList<MenuClass>();
		arms = new ArrayList<List<MenuClass>>();

	}

	public List<MenuClass> getArmTypes() {
		return armTypes;
	}

	public boolean isRead(int groupPosition) {
		return arms.get(groupPosition) == null
				|| arms.get(groupPosition).isEmpty() ? true : false;
	}

	public String armsId(int groupPosition) {
		return armTypes.size() > groupPosition ? armTypes.get(groupPosition)
				.getAcskey() : "0";
	}

	public String armId(int groupPosition, int childPosition) {
		return arms.get(groupPosition).size() > groupPosition ? arms
				.get(groupPosition).get(childPosition).getAcskey() : "0";
	}

	public String armName(int groupPosition, int childPosition) {
		return arms.get(groupPosition).size() > groupPosition ? arms
				.get(groupPosition).get(childPosition).getCat_name() : "";
	}

	public void setArmItem(int groupPosition, List<MenuClass> armTypes) {
		if (arms.size() > groupPosition) {
			arms.set(groupPosition, armTypes);
		}
	}

	public void setArmTypes(List<MenuClass> armTypes) {
		this.armTypes = armTypes;
		if (armTypes != null) {
			for (int i = 0; i < armTypes.size(); i++) {
				arms.add(new ArrayList<MenuClass>());
			}
		}
	}

	private List<MenuClass> armTypes;
	private List<List<MenuClass>> arms;

	// 获取指定组位置、指定子列表项处的子列表项数据
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return arms.get(groupPosition).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return arms.get(groupPosition).size();
	}

	// 该方法决定每个子选项的外观
	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		ViewHolderChild viewHolder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.item_menu_options_child, null);
			viewHolder = new ViewHolderChild();
			viewHolder.item_tv = (TextView) convertView
					.findViewById(R.id.item_tv);
			viewHolder.item_num = (TextView) convertView
					.findViewById(R.id.item_num);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolderChild) convertView.getTag();
		}
		viewHolder.item_tv.setText(((MenuClass) getChild(groupPosition,
				childPosition)).getCat_name());
		if (((MenuClass) getChild(groupPosition, childPosition)).getNum() > 0) {
			viewHolder.item_num.setVisibility(View.VISIBLE);
			viewHolder.item_num.setText(((MenuClass) getChild(groupPosition,
					childPosition)).getNum() + "");
		} else {
			viewHolder.item_num.setVisibility(View.GONE);
		}
		intent = new Intent();
		return convertView;
	}

	final static class ViewHolderChild {
		TextView item_tv;
		TextView item_num;
	}

	// 获取指定组位置处的组数据
	@Override
	public Object getGroup(int groupPosition) {
		return armTypes.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return armTypes.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	// 该方法决定每个组选项的外观
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		menuClass = armTypes.get(groupPosition);
		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.item_menu_options, null);
			viewHolder = new ViewHolder();
			viewHolder.item_tv = (TextView) convertView
					.findViewById(R.id.item_tv);
			viewHolder.icon_iv = (ImageView) convertView
					.findViewById(R.id.icon_iv);
			viewHolder.expansion_iv = (ImageView) convertView
					.findViewById(R.id.expansion_iv);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (isExpanded) {
			viewHolder.expansion_iv.setImageResource(R.drawable.expansion);
		} else {
			viewHolder.expansion_iv.setImageResource(R.drawable.collapse);
		}

		if (getChildrenCount(groupPosition) <= 0) {
			viewHolder.expansion_iv.setVisibility(View.GONE);
		} else {
			viewHolder.expansion_iv.setVisibility(View.VISIBLE);
		}
		if (groupPosition == 0) {
			if (dao.hasNotRead()) {
				viewHolder.expansion_iv.setImageDrawable(context.getResources()
						.getDrawable(R.drawable.red_circle));
				viewHolder.expansion_iv.setVisibility(View.VISIBLE);
			} else {
				viewHolder.expansion_iv.setVisibility(View.GONE);
			}
		}
		viewHolder.item_tv.setText(menuClass.getCat_name());
		intent = new Intent();
		return convertView;
	}

	final static class ViewHolder {
		TextView item_tv;
		ImageView icon_iv;
		ImageView expansion_iv;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {

	}

	@Override
	public long getCombinedChildId(long groupId, long childId) {
		return 0;
	}

	@Override
	public long getCombinedGroupId(long groupId) {
		return 0;
	}

}
