package com.financialnet.app.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.MineStockActivity;
import com.financialnet.app.ui.StockAboutActivity;
import com.financialnet.app.ui.ToadyAttentionActivity;
import com.financialnet.app.ui.TradeActivity;
import com.financialnet.app.ui.TradeBlogActivity;
import com.financialnet.app.ui.TradeDetailActivity;
import com.financialnet.app.ui.TraderNewTrendActivity;
import com.financialnet.app.ui.TraderRankActivity;
import com.financialnet.model.MainTradeProfit;
import com.financialnet.model.MenuClass;
import com.financialnet.model.Notification;
import com.financialnet.model.Stock;
import com.financialnet.model.Trade;
import com.financialnet.model.Tutorial;
import com.financialnet.widget.CircleImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainPayExpandableListAdapter implements ExpandableListAdapter {
	private Context context;
	private Intent intent;
	private MenuClass menuClass;
	private List<MenuClass> armTypes;
	private List<Trade> mainTradeList;
	private List<Trade> todayFocusList;
	private List<Stock> stockList;
	private List<Tutorial> tutorialList;
	private List<Notification> notified;
	private Map<Integer, MainTradeProfit> tradeListMap;
	public int childHeight;
	ViewBlogHolder viewBlogHolder = null;
	ViewTutorialHolder viewTutorialHolder = null;
	private List<Trade> tradeBlog;
	private static DisplayImageOptions traderPhotoOption;

	public MainPayExpandableListAdapter(Context context) {
		this.context = context;
		armTypes = new ArrayList<MenuClass>();
		mainTradeList = new ArrayList<Trade>();
		todayFocusList = new ArrayList<Trade>();
		stockList = new ArrayList<Stock>();
		tutorialList = new ArrayList<Tutorial>();
		traderPhotoOption = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.pro_ph_mask_default)
				.showImageForEmptyUri(R.drawable.pro_ph_mask_default)
				.showImageOnFail(R.drawable.pro_ph_mask_default)
				.cacheInMemory(true).cacheOnDisc(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
	}
	
	public List<MenuClass> getArmTypes() {

		return armTypes;
	}

	public List<Notification> getNotified() {
		return notified;
	}

	public void setNotified(List<Notification> notified) {
		this.notified = notified;
	}

	public List<Tutorial> getTutorialList() {
		return tutorialList;
	}

	public void setTutorialList(List<Tutorial> tutorialList) {
		this.tutorialList = tutorialList;
	}

	public void setArmTypes(List<MenuClass> armTypes) {
		if (armTypes != null) {
			this.armTypes = armTypes;
		}
	}

	public List<Trade> getMainTradeList() {
		return mainTradeList;
	}

	public void setMainTradeList(List<Trade> mainTradeList) {
		if (mainTradeList != null) {
			this.mainTradeList = mainTradeList;
		}
	}

	public List<Trade> getTodayFocusList() {
		return todayFocusList;
	}

	public void setTodayFocusList(List<Trade> todayFocusList) {
		if (todayFocusList != null) {
			this.todayFocusList = todayFocusList;
		}
	}

	public List<Stock> getStockList() {
		return stockList;
	}

	public void setStockList(List<Stock> stockList) {
		if (stockList != null) {
			this.stockList = stockList;
		}
	}

	public Map<Integer, MainTradeProfit> getTradeListMap() {
		return tradeListMap;
	}

	public void setTradeListMap(Map<Integer, MainTradeProfit> tradeListMap) {
		if (tradeListMap != null) {
			this.tradeListMap = tradeListMap;
		}
	}

	public List<Trade> getTradeBlog() {
		return tradeBlog;
	}

	public void setTradeBlog(List<Trade> tradeBlog) {
		if (tradeBlog != null) {
			this.tradeBlog = tradeBlog;
		}
	}

	// 获取指定组位置、指定子列表项处的子列表项数据
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		if (groupPosition == 0) {
			return mainTradeList.get(childPosition);
		} else if (groupPosition == 1) {
			return todayFocusList.get(childPosition);
//		} else if (groupPosition == 2) {
//			return tutorialList.get(childPosition);
		} else if (groupPosition == 2) {
			return tradeBlog.get(childPosition);
		} else if (groupPosition == 3) {
			return stockList.get(childPosition);
		} else if (groupPosition == 4) {
			return tradeListMap.get(childPosition);
		} else {
			return 0;
		}

	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (groupPosition == 0) {
			if (mainTradeList == null) {
				return 0;
			} else {
				return mainTradeList.size();
			}
		} else if (groupPosition == 1) {
			if (todayFocusList == null) {
				return 0;
			} else {
				return todayFocusList.size();
			}
//		} else if (groupPosition == 2) {
//			if (tutorialList == null) {
//				return 0;
//			} else {
//				return tutorialList.size();
//			}
 	} else if (groupPosition == 2) {
			if (tradeBlog == null) {
				return 0;
			} else {
				return tradeBlog.size();
			}
	} else if (groupPosition == 3) {
			if (stockList == null) {
				return 0;
			} else {
				return stockList.size();
			}
		} else if (groupPosition == 4) {
			if (tradeListMap == null) {
				return 0;
			} else {
				return tradeListMap.size();
			}
		} else {
			return 0;
		}

	}

	// 该方法决定每个子选项的外观
	@SuppressWarnings({ "unused" })
	@SuppressLint("InflateParams")
	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		convertView = null;
		if (groupPosition == 0) {
			ViewTradeHolder viewTradeHolder = null;
			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.item_main_trade, null);
				viewTradeHolder = new ViewTradeHolder();
				viewTradeHolder.textTradeName = (TextView) convertView
						.findViewById(R.id.text_main_trade_name);
				viewTradeHolder.textTradeStock = (TextView) convertView
						.findViewById(R.id.text_main_trade_stock);
				viewTradeHolder.textTradeStockTime = (TextView) convertView
						.findViewById(R.id.text_main_trade_stocktime);
				viewTradeHolder.textTradeStockValue = (TextView) convertView
						.findViewById(R.id.text_main_trade_stockvalue);
				viewTradeHolder.textTradeTime = (TextView) convertView
						.findViewById(R.id.text_main_trade_time);
				viewTradeHolder.ivPhoto=(ImageView)convertView.findViewById(R.id.iv_photo);
				convertView.setTag(viewTradeHolder);
			} else {
				viewTradeHolder = (ViewTradeHolder) convertView.getTag();
			}
			final Trade tradeNewTrend = ((Trade) getChild(groupPosition,
					childPosition));

			viewTradeHolder.textTradeName
					.setText(tradeNewTrend.getTraderName());
			viewTradeHolder.textTradeStock.setText(tradeNewTrend.getStockName()
					+ "(" + tradeNewTrend.getStockCode()
					+ tradeNewTrend.getMarketCode() + ")");
			if ("0".equals(tradeNewTrend.getFlagTrade())) {
				viewTradeHolder.textTradeStockTime.setText(tradeNewTrend
						.getQuantity() + "买入");
				viewTradeHolder.textTradeStockValue.setVisibility(View.INVISIBLE);

			} else if ("1".equals(tradeNewTrend.getFlagTrade())) {
				viewTradeHolder.textTradeStockTime.setText(tradeNewTrend
						.getQuantity() + "卖出");
				viewTradeHolder.textTradeStockValue.setVisibility(View.VISIBLE);
			}

			viewTradeHolder.textTradeStockValue.setText(tradeNewTrend
					.getEarningsMoney());
			if (tradeNewTrend.getEarningsMoney() != null
					&& "-".equals(tradeNewTrend.getEarningsMoney().substring(0,
							1))) {
				viewTradeHolder.textTradeStockValue.setTextColor(context
						.getResources().getColor(R.color.green));
			} else {
				viewTradeHolder.textTradeStockValue.setTextColor(context
						.getResources().getColor(R.color.main_color_red));
			}
			viewTradeHolder.textTradeTime.setText(tradeNewTrend.getTradeTime());

			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					intent = new Intent();
					intent.putExtra("trade_type", TradeActivity.MY_TRADER);
					intent.putExtra("trade_extra_type",
							TradeActivity.PAY_ZEWTREND);
					intent.putExtra("trade", tradeNewTrend);
					intent.setClass(context, TradeDetailActivity.class);
					context.startActivity(intent);
				}
			});
			AppContext.setImageNo(tradeNewTrend.getAvatarsAddress(),
					viewTradeHolder.ivPhoto, traderPhotoOption);
		} /*else if (groupPosition == 2) {
			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.item_main_tutorial, null);
				viewTutorialHolder = new ViewTutorialHolder();
				viewTutorialHolder.txtTraderName = (TextView) convertView
						.findViewById(R.id.text_main_trade_name);
				viewTutorialHolder.txtTraderValue = (TextView) convertView
						.findViewById(R.id.text_main_trade_value);
				viewTutorialHolder.txtTraderTime = (TextView) convertView
						.findViewById(R.id.text_main_trade_time);
				convertView.setTag(viewTutorialHolder);
			} else {
				viewTutorialHolder = (ViewTutorialHolder) convertView.getTag();
			}
			final Tutorial item = ((Tutorial) getChild(groupPosition,
					childPosition));

			viewTutorialHolder.txtTraderName.setText(item.getTraderName());

			viewTutorialHolder.txtTraderValue.setText(item.getTitle());

			viewTutorialHolder.txtTraderTime.setText(item.getCreateTime());

			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					intent = new Intent();
					intent.putExtra("trade_type", TradeActivity.MY_TRADER);
					intent.putExtra("trade_extra_type",
							TradeActivity.PAY_ZEWTREND);
					Trade trade = new Trade();
					trade.setTraderId(item.getTraderId());
					trade.setTraderName(item.getTraderName());
					intent.putExtra("trade", trade);
					intent.putExtra("actionFlag", "4");
					intent.setClass(context, TradeDetailActivity.class);
					context.startActivity(intent);
				}
			});
		}*/ else if (groupPosition == 1) {
			ViewTodayFocusHolder viewTodayFocusHolder = null;
			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.item_main_today_focus, null);
				viewTodayFocusHolder = new ViewTodayFocusHolder();
				viewTodayFocusHolder.textTodayFocusName = (TextView) convertView
						.findViewById(R.id.text_main_todayfocus_name);
				viewTodayFocusHolder.textTodayFocusContent = (TextView) convertView
						.findViewById(R.id.text_main_todayfocus_content);
				viewTodayFocusHolder.textTodayFocusTime = (TextView) convertView
						.findViewById(R.id.text_main_todayfocus_time);
				viewTodayFocusHolder.ivPhoto=(ImageView) convertView.findViewById(R.id.iv_photo);
				convertView.setTag(viewTodayFocusHolder);
			} else {
				viewTodayFocusHolder = (ViewTodayFocusHolder) convertView
						.getTag();
			}

			final Trade todayFocusTrade = ((Trade) getChild(groupPosition,
					childPosition));

			viewTodayFocusHolder.textTodayFocusName.setText(todayFocusTrade
					.getTraderName());
			viewTodayFocusHolder.textTodayFocusContent.setText(todayFocusTrade
					.getComments());
			viewTodayFocusHolder.textTodayFocusTime.setText(todayFocusTrade
					.getCreateTime());
			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					intent = new Intent();
					intent.putExtra("trade_type", TradeActivity.MY_TRADER);
					intent.putExtra("trade_extra_type",
							TradeActivity.PAY_TODAYFOURSE);
					intent.putExtra("trade", todayFocusTrade);
					TradeDetailActivity.currentTrader = todayFocusTrade;
					intent.setClass(context, TradeDetailActivity.class);
					context.startActivity(intent);
				}
			});
			AppContext.setImageNo(todayFocusTrade.getAvatarsAddress(),
					viewTodayFocusHolder.ivPhoto, traderPhotoOption);
		} else if (groupPosition ==2) {

			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.item_main_comment, null);
				viewBlogHolder = new ViewBlogHolder();
				viewBlogHolder.traderPhoto = (CircleImageView) convertView
						.findViewById(R.id.item_trade_List_img);
				viewBlogHolder.txtTraderName = (TextView) convertView
						.findViewById(R.id.text_main_comment_name);
				viewBlogHolder.txtTraderStalker = (TextView) convertView
						.findViewById(R.id.text_main_comment_usercount);
				viewBlogHolder.txtTraderComment = (TextView) convertView
						.findViewById(R.id.text_main_comment_content);
				viewBlogHolder.txtTraderTime = (TextView) convertView
						.findViewById(R.id.text_main_comment_time);
				convertView.setTag(viewBlogHolder);
			} else {
				viewBlogHolder = (ViewBlogHolder) convertView.getTag();
			}
			final Trade item = ((Trade) getChild(groupPosition, childPosition));
			AppContext.setImageNo(item.getAvatarsAddress(),
					viewBlogHolder.traderPhoto, traderPhotoOption);

			viewBlogHolder.txtTraderName.setText(item.getTraderName());
			viewBlogHolder.txtTraderStalker.setText(item.getStalker());
			viewBlogHolder.txtTraderComment.setText(item.getTitle());
			viewBlogHolder.txtTraderTime.setText(item.getCreateTime());
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					intent = new Intent();
					intent.putExtra("trade_type", TradeActivity.PUPU_TRADER);
					intent.putExtra("trade_extra_type", TradeActivity.PAY_RANK);
					intent.putExtra("trade", item);
					intent.putExtra("actionFlag", "3");
					intent.setClass(context, TradeDetailActivity.class);
					context.startActivity(intent);
				}
			});

		} else if (groupPosition == 3) {
			ViewStockHolder viewStockHolder = null;
			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.item_main_stock, null);
				viewStockHolder = new ViewStockHolder();
				viewStockHolder.textStockName = (TextView) convertView
						.findViewById(R.id.text_main_stock_name);
				viewStockHolder.textStockValue = (TextView) convertView
						.findViewById(R.id.text_main_stock_value);
				convertView.setTag(viewStockHolder);
			} else {
				viewStockHolder = (ViewStockHolder) convertView.getTag();
			}

			final Stock stock = ((Stock) getChild(groupPosition, childPosition));
			viewStockHolder.textStockName.setText(stock.getStockName() + "("
					+ stock.getStockCode() + stock.getMarketCode() + ")");
			viewStockHolder.textStockValue.setText("现价  "
					+ stock.getStockPrice());

			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(context,
							StockAboutActivity.class);
					intent.putExtra("stock", stock);
					context.startActivity(intent);
				}
			});
		} else if (groupPosition == 4) {
			ViewProfitHolder viewProfitHolder = null;
			if (viewProfitHolder == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.item_main_profit, null);
				viewProfitHolder = new ViewProfitHolder();
				viewProfitHolder.layoutProfit = (LinearLayout) convertView
						.findViewById(R.id.layout_main_profit);
				viewProfitHolder.textProfitName = (TextView) convertView
						.findViewById(R.id.text_main_profit_name);
				convertView.setTag(viewProfitHolder);
			} else {
				viewProfitHolder = (ViewProfitHolder) convertView.getTag();
			}

			MainTradeProfit mainTradeProfit = ((MainTradeProfit) getChild(
					groupPosition, childPosition));
			List<Trade> tradeList = mainTradeProfit.getTradeList();
			for (int i = 0; i < tradeList.size(); i++) {
				View v = LayoutInflater.from(context).inflate(
						R.layout.item_main_profit_child, null);
				TextView textOrder = (TextView) v
						.findViewById(R.id.text_main_profit_order);
				TextView textName = (TextView) v
						.findViewById(R.id.text_main_profit_name);
				TextView textProfit = (TextView) v
						.findViewById(R.id.text_main_profit_profit);

				final Trade tempTrade = tradeList.get(i);
				textOrder.setText((i + 1) + "");
				textName.setText(tempTrade.getTraderName());
				textProfit.setText(tempTrade.getEarningsMoney() + "("
						+ tempTrade.getEarningPercentage() + ")");

				v.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						intent = new Intent();
						intent.putExtra("trade_type",
								TradeActivity.STEADY_PROFIT_RANK);
						intent.putExtra("trade_extra_type",
								TradeActivity.PAY_RANK);
						intent.putExtra("trade", tempTrade);
						TradeDetailActivity.currentTrader = tempTrade;
						intent.setClass(context, TradeDetailActivity.class);
						context.startActivity(intent);
					}
				});
				viewProfitHolder.layoutProfit.addView(v);
			}
			viewProfitHolder.textProfitName
					.setText(mainTradeProfit.getProfit());

		}
		return convertView;
	}

	// 获取指定组位置处的组数据
	@Override
	public Object getGroup(int groupPosition) {
		return armTypes.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return armTypes.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	// 该方法决定每个组选项的外观
	@SuppressLint("InflateParams")
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		menuClass = armTypes.get(groupPosition);
		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.item_main_options, null);
			viewHolder = new ViewHolder();
			viewHolder.item_tv = (TextView) convertView
					.findViewById(R.id.item_tv);
			viewHolder.textMainMore = (TextView) convertView
					.findViewById(R.id.mian_more);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.item_tv.setText(menuClass.getCat_name());
		if (groupPosition == 0) {// 操盘手最新动向

			viewHolder.textMainMore.setVisibility(View.VISIBLE);

		} else if (groupPosition == 1) {// 今日关注

			viewHolder.textMainMore.setVisibility(View.VISIBLE);

		} else if (groupPosition == 2) {// 博客专栏

			viewHolder.textMainMore.setVisibility(View.VISIBLE);

		} else {
			viewHolder.textMainMore.setVisibility(View.VISIBLE);
		}
		viewHolder.textMainMore.setTag(groupPosition);
		viewHolder.textMainMore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				int pos = (Integer) arg0.getTag();
				payMoreItemPressed(pos);
			}
		});
		return convertView;
	}

	final static class ViewHolder {
		TextView item_tv, textMainMore;
		ImageView icon_iv;
		ImageView expansion_iv;
	}

	final static class ViewTradeHolder {
		ImageView ivPhoto;
		TextView textTradeName, textTradeStock, textTradeStockTime,
				textTradeStockValue, textTradeTime;

	}

	final static class ViewTodayFocusHolder {
		ImageView ivPhoto;
		TextView textTodayFocusName, textTodayFocusContent,textTodayFocusTime;

	}

	final static class ViewStockHolder {
		TextView textStockName, textStockValue;

	}

	final static class ViewProfitHolder {
		LinearLayout layoutProfit;
		TextView textProfitName;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {

	}

	@Override
	public long getCombinedChildId(long groupId, long childId) {
		return 0;
	}

	@Override
	public long getCombinedGroupId(long groupId) {
		return 0;
	}

	protected void payMoreItemPressed(int pos) {
		switch (pos) {
		case 0:// 操盘手最新动向
			intent = new Intent(context, TraderNewTrendActivity.class);
			context.startActivity(intent);
			break;
		case 1:// 今日关注
			intent = new Intent(context, ToadyAttentionActivity.class);
			context.startActivity(intent);
			break;
		case 2:// 博客专栏
			intent = new Intent(context, TradeBlogActivity.class);
			context.startActivity(intent);
			break;
		case 3:// 我的股票
			intent = new Intent(context, MineStockActivity.class);
			context.startActivity(intent);
			break;
		case 4:// 操盘手排行榜
			intent = new Intent(context, TraderRankActivity.class);
			context.startActivity(intent);
			break;
		}
	}

	final static class ViewBlogHolder {
		CircleImageView traderPhoto;
		TextView txtTraderName, txtTraderStalker;
		TextView txtTraderComment;
		TextView txtTraderTime;
	}

	final static class ViewTutorialHolder {
		TextView txtTraderName;
		TextView txtTraderValue;
		TextView txtTraderTime;
	}

	private Stock traderToStock(Trade trade) {
		Stock stock = new Stock();
		stock.setStockCode(trade.getStockCode());
		stock.setTraderId(trade.getTraderId());
		stock.setMarketID(trade.getMarketID());
		return stock;
	}
}
