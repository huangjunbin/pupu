package com.financialnet.app.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.app.ui.HotInfoDetailActivity;
import com.financialnet.model.HotInfo;

/**
 * @className：HotInfoAdapter.java
 * @author: li mingtao
 * @Function: 热门资讯适配器
 * @createDate: 2014-8-19上午10:51:33
 * @update:
 */
public class HotInfoAdapter extends BaseListAdapter<HotInfo> {
	private int comeFrom;
	private Intent intent;

	public HotInfoAdapter(Activity context, List<HotInfo> list, int defaultId,
			int mcomeFrom) {
		super(context, list, defaultId);
		comeFrom = mcomeFrom;
	}

	public HotInfoAdapter(BaseActivity context, List<HotInfo> list,
			int defaultId, int mcomeFrom) {
		super(context, list, defaultId);
		comeFrom = mcomeFrom;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		final HotInfo data = mList.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.hotlist_item, null);
			holder.imgviewHotImg = (ImageView) convertView
					.findViewById(R.id.item_hot_img);
			holder.textHotContent = (TextView) convertView
					.findViewById(R.id.item_hot_content);
			holder.hotDatetime = (TextView) convertView
					.findViewById(R.id.item_hot_datetime);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		AppContext.setImage(data.getSmallImageFile(), holder.imgviewHotImg,
				options);
		holder.textHotContent.setText(data.getTitle());
		holder.hotDatetime.setText(data.getCreateTime());

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (comeFrom) {
				case HotInfoDetailActivity.COMEFROM_HOTINFO:
					intent = new Intent(baseActivity,
							HotInfoDetailActivity.class);
					intent.putExtra("hotInfo", data);
					intent.putExtra("comefrom",
							HotInfoDetailActivity.COMEFROM_HOTINFO);
					baseActivity.startActivity(intent);
					break;
				case HotInfoDetailActivity.COMEFROM_STOCKNEW:
					intent = new Intent(activity, HotInfoDetailActivity.class);
					intent.putExtra("hotInfo", data);
					intent.putExtra("comefrom",
							HotInfoDetailActivity.COMEFROM_STOCKNEW);
					activity.startActivity(intent);
					break;
				}
			}

		});

		return convertView;
	}

	class ViewHolder {
		ImageView imgviewHotImg;
		TextView textHotContent;
		TextView hotDatetime;
	}
}
