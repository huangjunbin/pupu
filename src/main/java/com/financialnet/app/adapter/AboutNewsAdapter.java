package com.financialnet.app.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.financialnet.app.R;
import com.financialnet.model.AboutNew;

public class AboutNewsAdapter extends BaseListAdapter<AboutNew> {

	// public AboutNewsAdapter(BaseActivity context, List<AboutNew> list) {
	// super(context, list);
	// }

	public AboutNewsAdapter(Activity context, List<AboutNew> list) {
		super(context, list);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.aboutnew_item, null);
			holder.textNewIntrduce = (TextView) convertView
					.findViewById(R.id.news_intrduce);
			holder.textNewsFrom = (TextView) convertView
					.findViewById(R.id.news_from);
			holder.textNewsTime = (TextView) convertView
					.findViewById(R.id.news_time);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		AboutNew data = mList.get(position);
		holder.textNewIntrduce.setText(data.getNewsIntrduce());
		holder.textNewsFrom.setText("来自:" + data.getNewsFrom());
		holder.textNewsTime.setText(data.getNewsTime());

		return convertView;
	}

	class ViewHolder {
		TextView textNewIntrduce, textNewsFrom, textNewsTime;
	}
}
