/**
 * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.financialnet.app.adapter;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.easemob.chat.EMContactManager;
import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.AlertDialog;
import com.financialnet.db.InviteMessgeDao;
import com.financialnet.model.InviteMessage;
import com.financialnet.model.InviteMessage.InviteMesageStatus;
import com.financialnet.model.Member;
import com.financialnet.util.JsonUtil;
import com.financialnet.widget.CircleImageView;

/**
 * 简单的好友Adapter实现
 * 
 */
public class AddFriendsAdapter extends ArrayAdapter<Member> {

	private LayoutInflater layoutInflater;
	private ProgressDialog progressDialog;
	private int res;
	private Context mContext;

	public AddFriendsAdapter(Context context, int resource, List<Member> objects) {
		super(context, resource, objects);
		this.res = resource;
		mContext = context;
		layoutInflater = LayoutInflater.from(context);
		inviteMessgeDao = new InviteMessgeDao(mContext);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = layoutInflater.inflate(res, null);
		}

		TextView nameText = (TextView) convertView.findViewById(R.id.name);
		Button btnAdd = (Button) convertView.findViewById(R.id.indicator);
		CircleImageView avatar = (CircleImageView) convertView
				.findViewById(R.id.avatar);

		Member member = getItem(position);
		String username = member.getMemberName();

		nameText.setText(username);

		AppContext.setImageNo(member.getAvatarsAddress(), avatar,
				AppContext.memberPhotoOption);
		btnAdd.setTag(member);
		btnAdd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				addContact((Member) v.getTag());
			}
		});
		return convertView;
	}

	private InviteMessgeDao inviteMessgeDao;

	/**
	 * 添加contact
	 * 
	 * @param view
	 */
	public void addContact(final Member member) {
		if (AppContext.getCurrentMember().getMemberID().equals(member.getMemberID())) {
			mContext.startActivity(new Intent(mContext, AlertDialog.class)
					.putExtra("msg", "不能添加自己"));
			return;
		}

		if (AppContext.getApplication().getContactList()
				.containsKey(member.getMemberID() + "")) {
			mContext.startActivity(new Intent(mContext, AlertDialog.class)
					.putExtra("msg", "此用户已是你的好友"));
			return;
		}
		progressDialog = new ProgressDialog(mContext);
		progressDialog.setMessage("正在发送请求...");
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.show();

		new Thread(new Runnable() {
			public void run() {

				try {

					// demo写死了个reason，实际应该让用户手动填入
					EMContactManager
							.getInstance()
							.addContact(
									member.getMobileNumber(),
									JsonUtil.convertObjectToJson(AppContext.getCurrentMember()));

					((Activity) mContext).runOnUiThread(new Runnable() {
						public void run() {
							progressDialog.dismiss();
							InviteMessage message = new InviteMessage();
							message.setFrom(member.getMobileNumber());
							message.setStatus(InviteMesageStatus.WAIT);
							message.setReason(JsonUtil
									.convertObjectToJson(member));
							message.setTime(System.currentTimeMillis());
							inviteMessgeDao.saveMessage(message);
							Toast.makeText(mContext, "发送请求成功,等待对方验证",
									Toast.LENGTH_SHORT).show();
						}
					});
				} catch (final Exception e) {
					((Activity) mContext).runOnUiThread(new Runnable() {
						public void run() {
							progressDialog.dismiss();
							Toast.makeText(mContext,
									"请求添加好友失败:" + e.getMessage(),
									Toast.LENGTH_SHORT).show();
						}
					});
				}
			}
		}).start();
	}

	@Override
	public Member getItem(int position) {
		return super.getItem(position);
	}

	@Override
	public int getCount() {

		return super.getCount();
	}

}
