package com.financialnet.app.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.app.ui.TradeActivity;
import com.financialnet.app.ui.TradeDetailActivity;
import com.financialnet.model.Stock;
import com.financialnet.model.Trade;

import java.util.List;

public class HTodayAttentionAdapter extends BaseListAdapter<Stock> {
	/**
	 * @param context
	 * @param list
	 */
	private ViewHolder holder;
	private Context mContext;
	private Intent intent;

	public HTodayAttentionAdapter(Activity context, List<Stock> list,
			int defaultId) {
		super(context, list, defaultId);
		mContext = context;
	}

	public HTodayAttentionAdapter(BaseActivity context, List<Stock> list,
			int defaultId) {
		super(context, list, defaultId);
		mContext = context;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final Stock stock = mList.get(position);
		View rowView = convertView;
		holder = null;
		if (rowView == null) {
			holder = new ViewHolder();
			rowView = LayoutInflater.from(baseActivity).inflate(
					R.layout.item_main_today_focus, null);
			holder.textTodayFocusName = (TextView) rowView
					.findViewById(R.id.text_main_todayfocus_name);
			holder.textTodayFocusContent = (TextView) rowView
					.findViewById(R.id.text_main_todayfocus_content);
			holder.textTodayFocusTime = (TextView) rowView
					.findViewById(R.id.text_main_todayfocus_time);
			holder.ivPhoto=(ImageView) rowView.findViewById(R.id.iv_photo);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}


		holder.textTodayFocusName.setText(stock
				.getTraderName());
		holder.textTodayFocusContent.setText(stock
				.getComments());
		holder.textTodayFocusTime.setText(stock
				.getCreateTime());

		AppContext.setImageNo(stock.getAvatarsAddress(),
				holder.ivPhoto, AppContext.memberPhotoOption);
		rowView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				intent = new Intent();
				intent.putExtra("trade_type", TradeActivity.MY_TRADER);
				intent.putExtra("trade_extra_type",
						TradeActivity.PAY_TODAYFOURSE);
				intent.putExtra("trade", stockToTrade(stock));
				TradeDetailActivity.currentTrader = stockToTrade(stock);
				intent.setClass(mContext, TradeDetailActivity.class);
				mContext.startActivity(intent);
			}
		});
		return rowView;
	}

	class ViewHolder {
		ImageView ivPhoto;
		TextView textTodayFocusName, textTodayFocusContent,textTodayFocusTime;

	}

	private Trade stockToTrade(Stock stock) {
		Trade trade = new Trade();
		trade.setTraderId(stock.getTraderId());
		trade.setTraderName(stock.getTraderName());
		return trade;
	}
}
