package com.financialnet.app.adapter;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.financialnet.app.R;
import com.financialnet.model.Stock;

/**
 * @className：@Trader30TrendAdapter.java
 * @author: li mingtao
 * @Function:操盘手30天前最新动向
 * @createDate: @d2014-9-14@上午9:32:09
 * @update:
 */
public class Trader30TrendAdapter extends BaseListAdapter<Stock> {

	public Trader30TrendAdapter(Activity context, List<Stock> list) {
		super(context, list);
	}

	@Override
	public View getView(int pos, View rowView, ViewGroup vg) {
		ViewHolder holder;
		if (rowView == null) {
			holder = new ViewHolder();
			rowView = mInflater.inflate(R.layout.item_traderoldtrend, null);
			holder.txtStockHoldeName = (TextView) rowView
					.findViewById(R.id.txt_stockHoldeName);
			holder.txtStockcase = (TextView) rowView
					.findViewById(R.id.txt_stockcase);
			holder.txtProfit = (TextView) rowView.findViewById(R.id.txt_profit);
			holder.txtDate = (TextView) rowView.findViewById(R.id.txtDate_);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}
		Stock stock = mList.get(pos);

		holder.txtStockHoldeName.setText(stock.getStockHoldeName());
		holder.txtStockcase.setText(stock.getStockCase());
		holder.txtProfit.setText(stock.getProfit());
		holder.txtDate.setText(stock.getDate());

		return rowView;
	}

	class ViewHolder {
		TextView txtStockHoldeName, txtStockcase, txtProfit, txtDate;
	}
}
