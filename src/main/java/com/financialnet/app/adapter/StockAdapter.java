package com.financialnet.app.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.app.ui.HoldDetailActivity;
import com.financialnet.app.ui.MemerPlanActivity;
import com.financialnet.app.ui.PassTransactionDetailActivity;
import com.financialnet.app.ui.StockAttentionActivity;
import com.financialnet.model.Stock;
import com.financialnet.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 股票
 * 
 * @author allen
 * @time 2014-8-18
 */
public class StockAdapter extends BaseListAdapter<Stock> {
	public static final int PASS_TRADE = 1;// 过往交易
	public static final int ATTENTION_TODAY = 2;// 今日关注
	public static final int HOLD_NOW = 3;// 现时持仓
	public static final int ATTENTION_PASS = 4;// 过往关注

	/**
	 * @param context
	 * @param list
	 */
	private ViewHolder holder;
	private Context mContext;
	private Intent intent;
	private int type;

	public StockAdapter(Activity context, List<Stock> list, int mType,
			int defaultId) {
		super(context, list, defaultId);
		mContext = context;
		type = mType;
	}

	public StockAdapter(BaseActivity context, List<Stock> list, int mType,
			int defaultId) {
		super(context, list, defaultId);
		mContext = context;
		type = mType;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Stock stock = mList.get(position);
		View rowView = convertView;
		holder = null;
		if (rowView == null) {
			holder = new ViewHolder();
			rowView = mInflater.inflate(R.layout.item_trade_passtransaction,
					null);
			holder.txtId = (TextView) rowView.findViewById(R.id.txtStockId);
			holder.txtName = (TextView) rowView.findViewById(R.id.txtStockName);

			holder.txtTraderName = (TextView) rowView
					.findViewById(R.id.trader_name);

			holder.left = rowView.findViewById(R.id.left);
			holder.right = rowView.findViewById(R.id.right);

			holder.viewFlagStock = rowView.findViewById(R.id.flag_icon);
			holder.imgviewIconFlag = (ImageView) rowView
					.findViewById(R.id.icon_flag);
			holder.txtTitle = (TextView) rowView.findViewById(R.id.txtTitle);

			holder.viewTargetPriceBar = rowView
					.findViewById(R.id.target_price_bar);
			holder.txtTargetPrice = (TextView) rowView
					.findViewById(R.id.targetPrice);

			holder.txtDetailContent = (TextView) rowView
					.findViewById(R.id.detailcontent);
			holder.txtCenter = (TextView) rowView.findViewById(R.id.txtCenter);
			holder.viewProfitBar = rowView.findViewById(R.id.profit_bar);
			holder.txtInput = (TextView) rowView.findViewById(R.id.txtCenter);
			holder.txtContent = (TextView) rowView
					.findViewById(R.id.txtContent);

			holder.txtDate = (TextView) rowView.findViewById(R.id.txtDate);
			holder.txtDate2 = (TextView) rowView.findViewById(R.id.txtDate2);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		holder.stock = stock;

		holder.txtId.setText("(" + stock.getStockCode() + stock.getMarketCode()
				+ ")");
		holder.txtName.setText(stock.getStockName());
		holder.txtDate.setText(stock.getTradeTime());

		switch (type) {
		case PASS_TRADE:// 过往交易
			holder.viewFlagStock.setVisibility(View.VISIBLE);
			holder.viewProfitBar.setVisibility(View.VISIBLE);

			// ----
			if ("1".equals(stock.getFlagAuthentication() + "")) {
				holder.imgviewIconFlag
						.setBackgroundResource(R.drawable.cret_v_icon_a);
			} else {
				holder.imgviewIconFlag
						.setBackgroundResource(R.drawable.cret_v_icon_b);
			}

			String where = " 买入";
			StringBuffer num_price_str = new StringBuffer();
			if ("1".equals(stock.getFlagTrade() + "")) {
				where = " 卖出";
				num_price_str.append(stock.getStockPrice() + where
						+ stock.getQuantity() + "股");
				holder.txtContent.setVisibility(View.VISIBLE);
				holder.txtCenter.setVisibility(View.VISIBLE);
			} else {
				holder.txtContent.setVisibility(View.GONE);
				holder.txtCenter.setVisibility(View.GONE);
				num_price_str.append(stock.getStockPrice() + where
						+ stock.getQuantity() + "股");
			}
			holder.txtTitle.setText(num_price_str);

			StringBuffer profot_str = new StringBuffer();
			profot_str.append(stock.getEarningsMoney());

			if (!StringUtils.isStringNone(stock.getEarningPercentage())) {
				profot_str.append("(" + stock.getEarningPercentage() + ")");
			}
			holder.txtContent.setText(profot_str.toString());
			if (stock.getEarningPercentage() != null
					&& "-".equals(stock.getEarningPercentage().substring(0,1))) {
				holder.txtContent.setTextColor(mContext.getResources().getColor(R.color.green));
			} else {
				holder.txtContent.setTextColor(mContext.getResources().getColor(R.color.main_color_red));
			}
			break;
		case ATTENTION_TODAY:// 今日关注
			holder.txtDetailContent.setVisibility(View.VISIBLE);
			holder.viewTargetPriceBar.setVisibility(View.VISIBLE);
			// ---
			holder.txtDetailContent.setText(stock.getComments());
			holder.txtTargetPrice.setText(stock.getTagetPrice());
			holder.txtDate.setText(stock.getCreateTime());
			break;
		case HOLD_NOW:// 现时持仓
//			holder.viewFlagStock.setVisibility(View.VISIBLE);
//
//			if ("1".equals(stock.getFlagAuthentication() + "")) {
//				holder.imgviewIconFlag
//						.setBackgroundResource(R.drawable.cret_v_icon_a);
//			} else {
//				holder.imgviewIconFlag
//						.setBackgroundResource(R.drawable.cret_v_icon_b);
//			}
//
//			holder.txtTitle.setText(" 平均买入价:" + stock.getStockPrice() + "\n"
//					+ " 持有:" + stock.getQuantity() + "股");
			holder.viewFlagStock.setVisibility(View.VISIBLE);
			holder.viewProfitBar.setVisibility(View.VISIBLE);

			// ----
			if ("1".equals(stock.getFlagAuthentication() + "")) {
				holder.imgviewIconFlag
						.setBackgroundResource(R.drawable.cret_v_icon_a);
			} else {
				holder.imgviewIconFlag
						.setBackgroundResource(R.drawable.cret_v_icon_b);
			}

			String where1 = " 买入";
			StringBuffer num_price_str1 = new StringBuffer();
			if ("1".equals(stock.getFlagTrade() + "")) {
				where = " 卖出";
				num_price_str1.append("平均" + where+"价:"
						+ stock.getStockPrice());
				holder.txtContent.setVisibility(View.GONE);
				holder.txtCenter.setVisibility(View.GONE);
			} else {
				holder.txtContent.setVisibility(View.VISIBLE);
				holder.txtCenter.setVisibility(View.VISIBLE);
				num_price_str1.append("平均" + where1+"价:"
						+ stock.getStockPrice());
			}
			holder.txtTitle.setText(num_price_str1);

			StringBuffer profot_str1 = new StringBuffer();
			profot_str1.append(stock.getEarningsMoney());

			if (!StringUtils.isStringNone(stock.getEarningPercentage())) {
				profot_str1.append("(" + stock.getEarningPercentage() + ")");
			}
			holder.txtContent.setText(profot_str1.toString());
			if (stock.getEarningPercentage() != null
					&& "-".equals(stock.getEarningPercentage().substring(0,1))) {
				holder.txtContent.setTextColor(mContext.getResources().getColor(R.color.green));
			} else {
				holder.txtContent.setTextColor(mContext.getResources().getColor(R.color.main_color_red));
			}

			break;
		case ATTENTION_PASS:// 过往关注
			holder.txtDetailContent.setVisibility(View.VISIBLE);
			holder.viewTargetPriceBar.setVisibility(View.VISIBLE);
			// ---
			holder.txtDetailContent.setText(stock.getComments());
			holder.txtTargetPrice.setText(stock.getTagetPrice());

			// 准备第一个模板，从字符串中提取出日期数字
			String pat1 = "yyyy-MM-dd";

			SimpleDateFormat sdf1 = new SimpleDateFormat(pat1); // 实例化模板对象

			Date d = null;
			try {
				d = sdf1.parse(stock.getCreateTime()); // 将给定的字符串中的日期提取出来
			} catch (Exception e) { // 如果提供的字符串格式有错误，则进行异常处理
				e.printStackTrace(); // 打印异常信息
			}
			holder.txtDate2.setText(sdf1.format(d));
			holder.txtDate.setText(stock.getCreateTime());
			holder.txtDate2.setVisibility(View.VISIBLE);
			break;
		}

		rowView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Stock gp = ((ViewHolder) arg0.getTag()).stock;
				switch (type) {
				case PASS_TRADE:// 过往交易
					if (AppContext.getCurrentMember().getFlagIsConfirmed() == 0) {
						intent = new Intent(mContext, MemerPlanActivity.class);
						// 设置跳转标志为如此Activity存在则把其从任务堆栈中取出放到最上方
						intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						mContext.startActivity(intent);
						((Activity) mContext).overridePendingTransition(
								android.R.anim.fade_in, android.R.anim.fade_in);
					} else {
						intent = new Intent(mContext,
								PassTransactionDetailActivity.class);
						intent.putExtra("stock", gp);
						mContext.startActivity(intent);
					}
					break;
				case ATTENTION_TODAY:// 今日关注
					if (AppContext.getCurrentMember().getFlagIsConfirmed() == 0) {
						intent = new Intent(mContext, MemerPlanActivity.class);
						// 设置跳转标志为如此Activity存在则把其从任务堆栈中取出放到最上方
						intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						mContext.startActivity(intent);
						((Activity) mContext).overridePendingTransition(
								android.R.anim.fade_in, android.R.anim.fade_in);
					} else {
						intent = new Intent(mContext,
								StockAttentionActivity.class);
						System.out.println("gp::::"+gp);;
						intent.putExtra("stock", gp);
						mContext.startActivity(intent);
					}
					break;
				case ATTENTION_PASS:// 过往关注
					if (AppContext.getCurrentMember().getFlagIsConfirmed() == 0) {
						intent = new Intent(mContext, MemerPlanActivity.class);
						// 设置跳转标志为如此Activity存在则把其从任务堆栈中取出放到最上方
						intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						mContext.startActivity(intent);
						((Activity) mContext).overridePendingTransition(
								android.R.anim.fade_in, android.R.anim.fade_in);
					} else {
						intent = new Intent(mContext,
								StockAttentionActivity.class);
						intent.putExtra("id", gp.getId());
						mContext.startActivity(intent);
					}
					break;
				case HOLD_NOW:// 现时持仓
					if (AppContext.getCurrentMember().getFlagIsConfirmed() == 0) {
						intent = new Intent(mContext, MemerPlanActivity.class);
						// 设置跳转标志为如此Activity存在则把其从任务堆栈中取出放到最上方
						intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						mContext.startActivity(intent);
						((Activity) mContext).overridePendingTransition(
								android.R.anim.fade_in, android.R.anim.fade_in);
					} else {
						intent = new Intent(mContext, HoldDetailActivity.class);
						intent.putExtra("stock", gp);
						mContext.startActivity(intent);
					}
					break;
				}
			}
		});
		return rowView;
	}

	class ViewHolder {
		public View viewFlagStock, viewTargetPriceBar, viewProfitBar;
		public TextView txtId, txtName, txtTitle, txtTraderName, txtContent,
				txtCenter, txtDate, txtTargetPrice, txtDetailContent, txtInput,
				txtDate2;
		public ImageView imgviewIconFlag;
		public View left, right;
		private Stock stock;
	}
}
