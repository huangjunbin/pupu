package com.financialnet.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.app.ui.TradeActivity;
import com.financialnet.model.Trade;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.CircleImageView;

/**
 * 操盘手
 * 
 * @author allen
 * @time 2014-8-18
 */
@SuppressLint("InflateParams")
public class TradeListAdapter extends BaseListAdapter<Trade> {
	String TAG = "TradeListAdapter";
	/**
	 * @param context
	 * @param list
	 */
	@SuppressWarnings("unused")
	private Context mContext;
	private int traderType;

	public TradeListAdapter(BaseActivity context, List<Trade> list,
			int mTraderType, int defaultId) {
		super(context, list, defaultId);
		mContext = context;
		traderType = mTraderType;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Trade trade = mList.get(position);
		View rowView = convertView;
		ViewHolder holder = null;
		if (rowView == null) {
			holder = new ViewHolder();
			rowView = mInflater.inflate(R.layout.item_trade_list, null);
			holder.textTraderRankId = (TextView) rowView
					.findViewById(R.id.trader_rank_id);
			holder.TraderImg = (CircleImageView) rowView
					.findViewById(R.id.item_trade_List_img);
			holder.textTraderName = (TextView) rowView
					.findViewById(R.id.item_trader_name);
			holder.ratStartProgress = (RatingBar) rowView
					.findViewById(R.id.trader_ratebarprogress);
			holder.ratStartProgress.setMax(10);
			holder.textTraderProfit = (TextView) rowView
					.findViewById(R.id.item_trade_List_profit);
			holder.textTraderProfitRate = (TextView) rowView
					.findViewById(R.id.item_trade_List_profitRate);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		if (trade.getLogoUrl() == null || "".equals(trade.getLogoUrl())) {
			AppContext.setImageNo(trade.getAvatarsAddress(), holder.TraderImg,
					options);
		} else {
			AppContext
					.setImageNo(trade.getLogoUrl(), holder.TraderImg, options);
		}
		Log.d(TAG, trade.getLogoUrl());

		if (traderType == TradeActivity.SEVEVN_PROFIT_RANK
				|| traderType == TradeActivity.THIETY_PROFIT_RANK
				|| traderType == TradeActivity.STEADY_PROFIT_RANK) {
			holder.textTraderRankId.setVisibility(View.VISIBLE);
			holder.textTraderRankId.setText(trade.getRank());
		} else if (traderType == TradeActivity.MY_TRADER
				|| traderType == TradeActivity.PUPU_TRADER) {
			holder.textTraderRankId.setVisibility(View.GONE);
		}

		// AppContext.setImage(trade.getLogoUrl(), holder.TraderImg, options);
		if (!StringUtils.isStringNone(trade.getTraderName())) {
			holder.textTraderName.setText(trade.getTraderName());
		}
		if (!StringUtils.isStringNone(trade.getScore())) {
			float score = Float.parseFloat(trade.getScore());
			holder.ratStartProgress.setProgress((int) score);
		}

		if (traderType == TradeActivity.MY_TRADER
				|| traderType == TradeActivity.PUPU_TRADER) {
			holder.textTraderProfit.setText(trade.getEarningsMoney());
			holder.textTraderProfitRate.setText("("
					+ trade.getEarningPercentage() + ")");
		} else {
			holder.textTraderProfit.setText(trade.getSelft());
			holder.textTraderProfitRate.setText("(" + trade.getSelftInfo()
					+ ")");
		}
		return rowView;
	}

	class ViewHolder {
		public TextView textTraderRankId;
		public CircleImageView TraderImg;
		public TextView textTraderName;
		public RatingBar ratStartProgress;
		public TextView textTraderProfit, textTraderProfitRate;
	}
}
