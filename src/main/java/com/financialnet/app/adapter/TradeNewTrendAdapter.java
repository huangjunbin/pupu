package com.financialnet.app.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.app.ui.TradeActivity;
import com.financialnet.app.ui.TradeDetailActivity;
import com.financialnet.model.Trade;

import java.util.List;

/**
 * @className：TradeNewTrendAdapter.java
 * @author: lmt
 * @Function: 操盘手最新动向适配器
 * @createDate: 2014-10-30 下午4:34:01
 * @update:
 */
public class TradeNewTrendAdapter extends BaseListAdapter<Trade> {
	/**
	 * @param context
	 * @param list
	 */
	private ViewTradeHolder holder;
	private Intent intent;

	public TradeNewTrendAdapter(Activity context, List<Trade> list,
			int defaultId) {
		super(context, list, defaultId);
	}

	public TradeNewTrendAdapter(BaseActivity context, List<Trade> list,
			int defaultId) {
		super(context, list, defaultId);
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final Trade tradeNewTrend = mList.get(position);
		ViewTradeHolder viewTradeHolder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(baseActivity).inflate(
					R.layout.item_main_trade, null);
			viewTradeHolder = new ViewTradeHolder();
			viewTradeHolder.textTradeName = (TextView) convertView
					.findViewById(R.id.text_main_trade_name);
			viewTradeHolder.textTradeStock = (TextView) convertView
					.findViewById(R.id.text_main_trade_stock);
			viewTradeHolder.textTradeStockTime = (TextView) convertView
					.findViewById(R.id.text_main_trade_stocktime);
			viewTradeHolder.textTradeStockValue = (TextView) convertView
					.findViewById(R.id.text_main_trade_stockvalue);
			viewTradeHolder.textTradeTime = (TextView) convertView
					.findViewById(R.id.text_main_trade_time);
			viewTradeHolder.ivPhoto=(ImageView)convertView.findViewById(R.id.iv_photo);
			convertView.setTag(viewTradeHolder);
		} else {
			viewTradeHolder = (ViewTradeHolder) convertView.getTag();
		}


		viewTradeHolder.textTradeName
				.setText(tradeNewTrend.getTraderName());
		viewTradeHolder.textTradeStock.setText(tradeNewTrend.getStockName()
				+ "(" + tradeNewTrend.getStockCode()
				+ tradeNewTrend.getMarketCode() + ")");
		if ("0".equals(tradeNewTrend.getFlagTrade())) {
			viewTradeHolder.textTradeStockTime.setText(tradeNewTrend
					.getQuantity() + "买入");
			viewTradeHolder.textTradeStockValue.setVisibility(View.INVISIBLE);

		} else if ("1".equals(tradeNewTrend.getFlagTrade())) {
			viewTradeHolder.textTradeStockTime.setText(tradeNewTrend
					.getQuantity() + "卖出");
			viewTradeHolder.textTradeStockValue.setVisibility(View.VISIBLE);
		}

		viewTradeHolder.textTradeStockValue.setText(tradeNewTrend
				.getEarningsMoney());
		if (tradeNewTrend.getEarningsMoney() != null
				&& "-".equals(tradeNewTrend.getEarningsMoney().substring(0,
				1))) {
			viewTradeHolder.textTradeStockValue.setTextColor(baseActivity
					.getResources().getColor(R.color.green));
		} else {
			viewTradeHolder.textTradeStockValue.setTextColor(baseActivity
					.getResources().getColor(R.color.main_color_red));
		}
		viewTradeHolder.textTradeTime.setText(tradeNewTrend.getTradeTime());

		AppContext.setImageNo(tradeNewTrend.getAvatarsAddress(),
				viewTradeHolder.ivPhoto, AppContext.memberPhotoOption);

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				intent = new Intent();
				intent.putExtra("trade_type", TradeActivity.MY_TRADER);
				intent.putExtra("trade_extra_type", TradeActivity.PAY_ZEWTREND);

				intent.putExtra("trade", tradeNewTrend);
				intent.setClass(baseActivity, TradeDetailActivity.class);
				baseActivity.startActivity(intent);
			}
		});
		return convertView;
	}

	class ViewTradeHolder {
		ImageView ivPhoto;
		TextView textTradeName, textTradeStock, textTradeStockTime,
				textTradeStockValue, textTradeTime;
	}
}
