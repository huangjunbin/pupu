package com.financialnet.app.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.app.ui.TradeActivity;
import com.financialnet.app.ui.TradeDetailActivity;
import com.financialnet.model.Trade;
import com.financialnet.model.TradeBlog;
import com.financialnet.widget.CircleImageView;

import java.util.List;

;

/**
 * @className：TradeBlogAdapter.java
 * @author: li mingtao
 * @Function: 操盘手博客列表
 * @createDate: 2014-8-19上午10:51:33
 * @update:
 */
public class TradeHomeBlogAdapter extends BaseListAdapter<TradeBlog> {

	public TradeHomeBlogAdapter(Activity context, List<TradeBlog> list,
								int defaultId) {
		super(context, list, defaultId);

	}

	public TradeHomeBlogAdapter(BaseActivity context, List<TradeBlog> list,
								int defaultId) {
		super(context, list, defaultId);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewBlogHolder = null;
		final TradeBlog data = mList.get(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(baseActivity).inflate(
					R.layout.item_main_comment, null);
			viewBlogHolder = new ViewHolder();
			viewBlogHolder.traderPhoto = (CircleImageView) convertView
					.findViewById(R.id.item_trade_List_img);
			viewBlogHolder.txtTraderName = (TextView) convertView
					.findViewById(R.id.text_main_comment_name);
			viewBlogHolder.txtTraderStalker = (TextView) convertView
					.findViewById(R.id.text_main_comment_usercount);
			viewBlogHolder.txtTraderComment = (TextView) convertView
					.findViewById(R.id.text_main_comment_content);
			viewBlogHolder.txtTraderTime = (TextView) convertView
					.findViewById(R.id.text_main_comment_time);
			viewBlogHolder.tvMore= (TextView) convertView
					.findViewById(R.id.tv_more);
			convertView.setTag(viewBlogHolder);
		} else {
			viewBlogHolder = (ViewHolder) convertView.getTag();
		}
		final TradeBlog item=mList.get(position);
		AppContext.setImageNo(item.getAvatarsAddress(),
				viewBlogHolder.traderPhoto, AppContext.memberPhotoOption);

		viewBlogHolder.txtTraderName.setText(item.getTraderName());
		viewBlogHolder.txtTraderStalker.setVisibility(View.GONE);
		viewBlogHolder.tvMore.setVisibility(View.GONE);
		viewBlogHolder.txtTraderComment.setText(item.getTitle());
		viewBlogHolder.txtTraderTime.setText(item.getCreateTime());
		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.putExtra("trade_type", TradeActivity.PUPU_TRADER);
				intent.putExtra("trade_extra_type", TradeActivity.PAY_RANK);
				Trade trade=new Trade();
				trade.setTraderId(item.getTraderId());
				trade.setTraderName(item.getTraderName());
				intent.putExtra("trade", trade);
				intent.putExtra("actionFlag", "3");
				intent.setClass(baseActivity, TradeDetailActivity.class);
				baseActivity.startActivity(intent);
			}
		});

		return convertView;
	}

	class ViewHolder {
		CircleImageView traderPhoto;
		TextView txtTraderName, txtTraderStalker;
		TextView txtTraderComment;
		TextView txtTraderTime,tvMore;
	}
}
