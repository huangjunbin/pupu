package com.financialnet.app.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.financialnet.app.AppConfig;
import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.app.ui.BaseActivity.OnSurePress;
import com.financialnet.app.ui.HotInfoDetailActivity;
import com.financialnet.app.ui.TradeDetailActivity;
import com.financialnet.model.HotInfo;
import com.financialnet.model.Investment;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.TraderService;
import com.financialnet.util.UIHelper;

import org.json.JSONException;

import java.util.List;

;

/**
 * @className：TTradeInvestmentAdapter.java
 * @author: li mingtao
 * @Function: 视频教学
 * @createDate: 2014-8-19上午10:51:33
 * @update:
 */
public class TradeInvestmentAdapter extends BaseListAdapter<Investment> {
	private TradeDetailActivity detailActivity;

	public TradeInvestmentAdapter(Activity context, List<Investment> list,
			int defaultId) {
		super(context, list, defaultId);
		detailActivity = (TradeDetailActivity) context;
	}

	public TradeInvestmentAdapter(BaseActivity context, List<Investment> list,
			int defaultId) {
		super(context, list, defaultId);
		detailActivity = (TradeDetailActivity) context;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		final Investment data = mList.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_trade_investment,
					null);

			holder.textInvestContent = (TextView) convertView
					.findViewById(R.id.item_invest_content);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.textInvestContent.setText(data.getTitle());

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if ("1".equals(detailActivity.tempTrade.getMemberType())
						|| AppContext.getCurrentMember().getMemberClass() == AppConfig.CLASS_DIAMOND) {
					TraderService.getInstance(baseActivity)
							.getTradeInvestDetail(data.getId(),
									new CustomAsyncResponehandler() {
										@Override
										public void onSuccess(
												ResponeModel baseModel) {
											super.onSuccess(baseModel);
											if (baseModel.isStatus()) {
												try {
													String content = baseModel
															.getDataResult()
															.getString(
																	"content");
													Intent intent = new Intent();
													intent.putExtra("title",
															"投资教室");
													HotInfo hotInfo = new HotInfo();
													hotInfo.setContent(content);
													intent.putExtra("hotInfo",
															hotInfo);
													intent.setClass(
															baseActivity,
															HotInfoDetailActivity.class);
													baseActivity
															.startActivity(intent);
												} catch (JSONException e) {
													e.printStackTrace();
												}

											}
										}

										@Override
										public void onFinish() {
											super.onFinish();

										}
									});
				} else {
					UIHelper.showSysDialog(detailActivity,
							"您尚未跟踪该操盘手",
							new OnSurePress() {
								@Override
								public void onClick(View view) {

								}
							}, false);
				}
			}
		});

		return convertView;
	}

	class ViewHolder {

		TextView textInvestContent;

	}
}
