package com.financialnet.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.financialnet.app.R;
import com.financialnet.app.ui.MineCenterActivity;
import com.financialnet.db.dao.DataBookDao;
import com.financialnet.model.DataBook;
import com.financialnet.util.StringUtils;

@SuppressLint("InflateParams")
public class EasySelectAdapter extends BaseListAdapter<DataBook> {
	private int setType;

	public EasySelectAdapter(Activity context, List<DataBook> list, int mSetType) {
		super(context, list);
		setType = mSetType;
	}

	@Override
	public View getView(int pos, View rootView, ViewGroup arg2) {
		ViewHolder holder;
		if (rootView == null) {
			holder = new ViewHolder();
			rootView = mInflater.inflate(R.layout.item_easy_select, null);
			holder.textSelectContent = (TextView) rootView
					.findViewById(R.id.item_select_content);
			holder.imgSelectedFlag = (ImageView) rootView
					.findViewById(R.id.item_select_flag);
			rootView.setTag(holder);
		} else {
			holder = (ViewHolder) rootView.getTag();
		}

		DataBook db = mList.get(pos);
		holder.id = pos;
		holder.imgSelectedFlag.setVisibility(View.GONE);

		switch (setType) {
		case DataBookDao.INVESTTYPE_TAG:
			if (StringUtils.isStringNone(MineCenterActivity.editMember
					.getTypeDBCode())) {// 默认选中第一条
				if (pos == 0)
					holder.imgSelectedFlag.setVisibility(View.VISIBLE);
			} else {
				if (MineCenterActivity.editMember.getTypeDBCode().equals(
						db.getDbCode())) {
					holder.imgSelectedFlag.setVisibility(View.VISIBLE);
				}
			}
			break;
		case DataBookDao.INVESTTIME_TAG:
			if (StringUtils.isStringNone(MineCenterActivity.editMember
					.getGitDBcode())) {
				if (pos == 0) {
					holder.imgSelectedFlag.setVisibility(View.VISIBLE);
				}
			} else {
				if (MineCenterActivity.editMember.getGitDBcode().equals(
						db.getDbCode())) {
					holder.imgSelectedFlag.setVisibility(View.VISIBLE);
				}
			}
			break;
		case DataBookDao.INVESTSPE_TAG:
			if (StringUtils.isStringNone(MineCenterActivity.editMember
					.getSpeDBCode())) {
				if (pos == 0) {
					holder.imgSelectedFlag.setVisibility(View.VISIBLE);
				}
			} else {
				if (MineCenterActivity.editMember.getSpeDBCode().equals(
						db.getDbCode())) {
					holder.imgSelectedFlag.setVisibility(View.VISIBLE);
				}
			}
			break;
		}

		holder.textSelectContent.setText(mList.get(pos).getValue());

		rootView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (setType) {
				case DataBookDao.INVESTTYPE_TAG:
					MineCenterActivity.editMember.setTypeDBCode(mList.get(
							((ViewHolder) v.getTag()).id).getDbCode());
					MineCenterActivity.editMember.setTypeDescription(mList.get(
							((ViewHolder) v.getTag()).id).getValue());
					break;
				case DataBookDao.INVESTTIME_TAG:
					MineCenterActivity.editMember.setGitDBcode(mList.get(
							((ViewHolder) v.getTag()).id).getDbCode());
					MineCenterActivity.editMember.setTimeDescription(mList.get(
							((ViewHolder) v.getTag()).id).getValue());
					break;
				case DataBookDao.INVESTSPE_TAG:
					MineCenterActivity.editMember.setSpeDBCode(mList.get(
							((ViewHolder) v.getTag()).id).getDbCode());
					MineCenterActivity.editMember.setSpeDescription(mList.get(
							((ViewHolder) v.getTag()).id).getValue());
					break;
				}
				notifyDataSetChanged();
			}
		});
		return rootView;
	}

	class ViewHolder {
		int id;
		TextView textSelectContent;
		ImageView imgSelectedFlag;
	}
}
