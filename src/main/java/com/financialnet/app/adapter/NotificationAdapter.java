package com.financialnet.app.adapter;

import java.util.List;

import org.json.JSONException;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.app.ui.HotInfoDetailActivity;
import com.financialnet.app.ui.PassTransactionDetailActivity;
import com.financialnet.app.ui.StockAttentionActivity;
import com.financialnet.db.dao.NotificationDao;
import com.financialnet.model.HotInfo;
import com.financialnet.model.Notification;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.TraderService;

public class NotificationAdapter extends BaseListAdapter<Notification> {

	public NotificationAdapter(BaseActivity context, List<Notification> list) {

		super(context, list);
		dao = new NotificationDao(context);
	}

	private NotificationDao dao;
	private Intent intent;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		final Notification data = mList.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_notification, null);

			holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);

			holder.tvNum = (TextView) convertView.findViewById(R.id.tv_num);

			holder.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tvTitle.setText(data.getAlert());
		if (data.getState() == 0) {
			holder.tvNum.setVisibility(View.VISIBLE);
		} else {
			holder.tvNum.setVisibility(View.INVISIBLE);
		}
		holder.tvDate.setText(data.getCreatedTime());
		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if ("0".equals(data.getType())) {
					data.setState(1);
					dao.update(data);
					intent = new Intent(baseActivity,
							PassTransactionDetailActivity.class);
					intent.putExtra("id", data.getId());
					baseActivity.startActivity(intent);
				}

				if ("1".equals(data.getType())) {
					data.setState(1);
					dao.update(data);
					intent = new Intent(baseActivity,
							StockAttentionActivity.class);
					intent.putExtra("id", data.getId());
                    intent.putExtra("type", 1);
					baseActivity.startActivity(intent);
				}
				if ("2".equals(data.getType())) {

					TraderService.getInstance(baseActivity)
							.getTradeInvestDetail(data.getId(),
									new CustomAsyncResponehandler() {
										@Override
										public void onSuccess(
												ResponeModel baseModel) {
											super.onSuccess(baseModel);
											if (baseModel.isStatus()) {
												try {
													data.setState(1);
													dao.update(data);
													String content = baseModel
															.getDataResult()
															.getString(
																	"content");
													Intent intent = new Intent();
													intent.putExtra("title",
															"投资教室");
													HotInfo hotInfo = new HotInfo();
													hotInfo.setContent(content);
													intent.putExtra("hotInfo",
															hotInfo);
													intent.setClass(
															baseActivity,
															HotInfoDetailActivity.class);
													baseActivity
															.startActivity(intent);
												} catch (JSONException e) {
													e.printStackTrace();
												}

											}
										}

										@Override
										public void onFinish() {
											super.onFinish();

										}
									});
				}
				if ("3".equals(data.getType())) {
					TraderService.getInstance(baseActivity).getTradeBlogDetail(
							data.getId(), new CustomAsyncResponehandler() {
								@Override
								public void onSuccess(ResponeModel baseModel) {
									super.onSuccess(baseModel);
									if (baseModel.isStatus()) {
										try {
											data.setState(1);
											dao.update(data);
											String contents = baseModel
													.getDataResult().getString(
															"contents");
											Intent intent = new Intent();
											intent.putExtra("title", "博客专栏");
											HotInfo hotInfo = new HotInfo();
											hotInfo.setContent(contents);// comeFrom
											intent.putExtra("hotInfo", hotInfo);
											intent.setClass(baseActivity,
													HotInfoDetailActivity.class);
											baseActivity.startActivity(intent);
										} catch (JSONException e) {
											e.printStackTrace();
										}

									}
								}

								@Override
								public void onFinish() {
									super.onFinish();

								}
							});
				}

			}
		});

		return convertView;
	}

	class ViewHolder {

		TextView tvTitle, tvDate, tvNum;

	}
}
