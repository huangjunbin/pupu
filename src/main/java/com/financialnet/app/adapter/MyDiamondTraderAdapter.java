package com.financialnet.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.TradeActivity;
import com.financialnet.app.ui.TradeDetailActivity;
import com.financialnet.model.Group;
import com.financialnet.model.Trade;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.CircleImageView;

/**
 * @className：MyDiamondTraderAdapter.java
 * @author: lmt
 * @Function: 钻石我的操盘手适配器
 * @createDate: 2014-11-12 下午4:18:03
 * @update:
 */
@SuppressLint("InflateParams")
public class MyDiamondTraderAdapter extends BaseListAdapter<Object> {
	public static final int MYFAVORITE_TRADE = 0;// 我最爱的操盘手
	public static final int MYOLDERED_TRADE = 1;// 我已订阅的操盘手
	public List<Group> groupKey;

	public MyDiamondTraderAdapter(Activity context, List<Object> list,
			List<Group> group, int defaultId) {
		super(context, list, defaultId);
		groupKey = group;
	}

	@Override
	public View getView(int position, View rowView, ViewGroup parent) {
		rowView = null;
		if (groupKey.contains(getItem(position))) {
			Group group = (Group) getItem(position);
			rowView = mInflater.inflate(R.layout.item_group_title, null);
			TextView txtGroup = (TextView) rowView
					.findViewById(R.id.group_name);
			txtGroup.setText(group.getGroupDescription());
		} else {
			final Trade trade = (Trade) getItem(position);
			ViewHolder holder = null;
			if (rowView == null) {
				holder = new ViewHolder();
				rowView = mInflater.inflate(R.layout.item_trade_list, null);
				holder.textTraderRankId = (TextView) rowView
						.findViewById(R.id.trader_rank_id);
				holder.TraderImg = (CircleImageView) rowView
						.findViewById(R.id.item_trade_List_img);
				holder.textTraderName = (TextView) rowView
						.findViewById(R.id.item_trader_name);
				holder.ratStartProgress = (RatingBar) rowView
						.findViewById(R.id.trader_ratebarprogress);
				holder.ratStartProgress.setMax(10);
				holder.textTraderProfit = (TextView) rowView
						.findViewById(R.id.item_trade_List_profit);
				holder.textTraderProfitRate = (TextView) rowView
						.findViewById(R.id.item_trade_List_profitRate);
				rowView.setTag(holder);
			} else {
				holder = (ViewHolder) rowView.getTag();
			}

			if (trade.getLogoUrl() == null || "".equals(trade.getLogoUrl())) {
				AppContext.setImageNo(trade.getAvatarsAddress(),
						holder.TraderImg, options);
			} else {
				AppContext.setImageNo(trade.getLogoUrl(), holder.TraderImg,
						options);
			}

			holder.textTraderRankId.setVisibility(View.GONE);

			if (!StringUtils.isStringNone(trade.getTraderName())) {
				holder.textTraderName.setText(trade.getTraderName());
			}
			if (!StringUtils.isStringNone(trade.getScore())) {
				float score = Float.parseFloat(trade.getScore());
				holder.ratStartProgress.setProgress((int) score);
			}

			holder.textTraderProfit.setText(trade.getEarningsMoney());
			holder.textTraderProfitRate.setText("("
					+ trade.getEarningPercentage() + ")");

			rowView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent();
					intent.putExtra("trade", trade);
					intent.putExtra("trade_type", TradeActivity.MY_TRADER);
					intent.setClass(activity, TradeDetailActivity.class);
					activity.startActivity(intent);
				}
			});
		}
		return rowView;
	}

	class ViewHolder {
		public TextView textTraderRankId;
		public CircleImageView TraderImg;
		public TextView textTraderName;
		public RatingBar ratStartProgress;
		public TextView textTraderProfit, textTraderProfitRate;
	}
}
