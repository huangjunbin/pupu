package com.financialnet.app.adapter;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.app.ui.BaseActivity.OnSurePress;
import com.financialnet.app.ui.EditStockActivity;
import com.financialnet.model.AddStockRequest;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.StockService;
import com.financialnet.util.UIHelper;

import java.util.List;

/**
 * @className：MineSearchStockAdapter.java
 * @author: lmt
 * @Function: 我搜索的股票
 * @createDate: 2014-11-26 下午2:14:33
 * @update:
 */
public class MineSearchStockAdapter extends BaseListAdapter<Stock> {

	public MineSearchStockAdapter(BaseActivity context, List<Stock> list) {
		super(context, list);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_stocksearch, null);
			holder.textStockDes = (TextView) convertView
					.findViewById(R.id.search_name);
			holder.textStockAdd = (TextView) convertView
					.findViewById(R.id.search_add);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		final Stock stock = (Stock) getItem(position);
		final String stockDes = stock.getStockName() + "("
				+ stock.getStockCode() + stock.getMarketCode() + ")";
		holder.textStockDes.setText(stockDes);
		holder.textStockAdd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((EditStockActivity) baseActivity).mInputMethodManager
						.hideSoftInputFromWindow(
								((EditStockActivity) baseActivity).editSearchStock
										.getWindowToken(), 0);

				UIHelper.showSysDialog(
						baseActivity,
						stockDes
								+ baseActivity
										.getResString(R.string.stock_isadd),
						new OnSurePress() {
							@Override
							public void onClick(View view) {

								AddStockRequest resa = new AddStockRequest();
								resa.setMarketID(stock.getMarketID());
								resa.setMemberId(String
										.valueOf(AppContext.getCurrentMember()
												.getMemberID()));
								resa.setStockCode(stock.getStockCode());
								resa.setStockName(stock.getStockName());
								
								StockService
										.getInstance(baseActivity)
										.addMyStock(
												resa,
												new CustomAsyncResponehandler() {
													@Override
													public void onSuccess(
															ResponeModel baseModel) {
														super.onSuccess(baseModel);
														if (baseModel
																.isStatus()) {
															// // 从搜索股票中删除该股票
															// mList.remove(stock);
															// notifyDataSetChanged();
															Message msg = new Message();
															Bundle data = new Bundle();
															data.putSerializable("data", stock);
															msg.setData(data);
															msg.what = EditStockActivity.HAND_ADD_STOCK;
															
															((EditStockActivity) baseActivity).hand.handleMessage(msg);
															UIHelper.showSysDialog(
																	baseActivity,
																	stockDes
																			+ baseActivity
																					.getResString(R.string.stock_addsuccess));
														} else {
															switch (baseModel
																	.getCode()) {
															case 1001:// 股票超过10条
																UIHelper.showSysDialog(
																		baseActivity,
																		baseActivity
																				.getResString(R.string.stock_ownmax));
																break;
															case 1002:// 重复的股票
																UIHelper.showSysDialog(
																		baseActivity,
																		baseActivity
																				.getResString(R.string.stock_nore));
																break;
															default:
																UIHelper.ShowMessage(
																		baseActivity,
																		baseModel
																				.getMsg());
															}
														}
													}
												});

							}
						}, true);

			}
		});

		return convertView;
	}

	class ViewHolder {
		TextView textStockDes, textStockAdd;
	}
}
