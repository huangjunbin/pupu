package com.financialnet.app.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.MemerPlanActivity;
import com.financialnet.app.ui.TradeActivity;
import com.financialnet.app.ui.TradeDetailActivity;
import com.financialnet.app.ui.TraderDiscussActivity;
import com.financialnet.app.ui.TraderRankActivity;
import com.financialnet.model.MainTodayFocus;
import com.financialnet.model.MainTradeProfit;
import com.financialnet.model.MenuClass;
import com.financialnet.model.Stock;
import com.financialnet.model.Trade;
import com.financialnet.util.DateUtil;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.CircleImageView;
import com.financialnet.widget.RoundProgressBar;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class MainNoPayExpandableListAdapter implements ExpandableListAdapter {
	private Context context;
	private Intent intent;

	private MenuClass menuClass;
	private List<MenuClass> armTypes;
	private Trade radomTrader;
	private List<Trade> tradeBlog;
	private List<Trade> TradeComments;
	private List<Trade> mainTradeList;
	private List<MainTodayFocus> todayFocusList;
	private List<Stock> stockList;
	private Map<Integer, MainTradeProfit> tradeListMap;
	private int winProgress = 0;
	private int lossProgress = 0;
	private static DisplayImageOptions traderPhotoOption;
	ViewTradeHolder viewTradeHolder = null;
	ViewBlogHolder viewBlogHolder = null;
	ViewCommentHolder viewCommentHolder = null;
	Handler hand;

	@SuppressWarnings("deprecation")
	@SuppressLint("UseSparseArrays")
	public MainNoPayExpandableListAdapter(Context context, Handler hand) {
		this.context = context;
		this.hand = hand;
		armTypes = new ArrayList<MenuClass>();
		radomTrader = new Trade();
		tradeBlog = new ArrayList<Trade>();
		TradeComments = new ArrayList<Trade>();
		mainTradeList = new ArrayList<Trade>();
		todayFocusList = new ArrayList<MainTodayFocus>();
		stockList = new ArrayList<Stock>();
		tradeListMap = new HashMap<Integer, MainTradeProfit>();
		traderPhotoOption = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.pro_ph_mask_default)
				.showImageForEmptyUri(R.drawable.pro_ph_mask_default)
				.showImageOnFail(R.drawable.pro_ph_mask_default)
				.cacheInMemory(true).cacheOnDisc(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
	}

	public List<MenuClass> getArmTypes() {

		return armTypes;
	}

	public void setArmTypes(List<MenuClass> armTypes) {
		if (armTypes != null) {
			this.armTypes = armTypes;
		}
	}

	public Trade getRadomTrader() {
		return radomTrader;
	}

	public void setRadomTrader(Trade radomTrader) {
		if (radomTrader != null) {
			this.radomTrader = radomTrader;
		}
	}

	public List<Trade> getTradeBlog() {
		return tradeBlog;
	}

	public void setTradeBlog(List<Trade> tradeBlog) {
		if (tradeBlog != null) {
			this.tradeBlog = tradeBlog;
		}
	}

	public List<Trade> getTradeComments() {
		return TradeComments;
	}

	public void setTradeComments(List<Trade> tradeComments) {
		if (TradeComments != null) {
			TradeComments = tradeComments;
		}
	}

	public List<Trade> getMainTradeList() {
		return mainTradeList;
	}

	public void setMainTradeList(List<Trade> mainTradeList) {
		if (mainTradeList != null) {
			this.mainTradeList = mainTradeList;
		}
	}

	public List<MainTodayFocus> getTodayFocusList() {
		return todayFocusList;
	}

	public void setTodayFocusList(List<MainTodayFocus> todayFocusList) {
		this.todayFocusList = todayFocusList;
	}

	public List<Stock> getStockList() {
		return stockList;
	}

	public void setStockList(List<Stock> stockList) {
		this.stockList = stockList;
	}

	public Map<Integer, MainTradeProfit> getTradeListMap() {
		return tradeListMap;
	}

	public void setTradeListMap(Map<Integer, MainTradeProfit> tradeListMap) {
		if (tradeListMap != null) {
			this.tradeListMap = tradeListMap;
		}
	}

	// 获取指定组位置、指定子列表项处的子列表项数据
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		if (groupPosition == 0) {
			return 0;
		} else if (groupPosition == 1) {
			return tradeBlog.get(childPosition);
			// } else if (groupPosition == 2) {
			// return TradeComments.get(childPosition);
			// } else if (groupPosition == 3) {
			// return mainTradeList.get(childPosition);
		} else if (groupPosition == 2) {
			return TradeComments.get(childPosition);
		} else if (groupPosition == 3) {
			return stockList.get(childPosition);
		} else if (groupPosition == 4) {
			return tradeListMap.get(childPosition);

		} else {
			return 0;
		}

	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (groupPosition == 0) {
			return 1;
		} else if (groupPosition == 1) {
			if (tradeBlog == null) {
				return 0;
			} else {
				return tradeBlog.size();
			}
			// } else if (groupPosition == 2) {
			// if (TradeComments != null) {
			// return TradeComments.size();
			// } else {
			// return 0;
			// }
			// } else if (groupPosition == 3) {
			// if (mainTradeList.size() > 0) {
			// return mainTradeList.size();
			// } else {
			// return 0;
			// }
		} else if (groupPosition == 2) {
			if (TradeComments == null) {
				return 0;
			} else {
				return TradeComments.size();
			}
		} else if (groupPosition == 3) {
			return 0;
		} else if (groupPosition == 4) {
			if (tradeListMap == null) {
				return 0;
			} else {
				if (tradeListMap.size() > 0) {
					return tradeListMap.size();
				} else {
					return 0;
				}
			}
		} else {
			return 0;
		}
	}

	// 该方法决定每个子选项的外观
	@SuppressWarnings({ "unused" })
	@SuppressLint("InflateParams")
	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		convertView = null;
		if (groupPosition == 0) {
			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.item_main_trade_count, null);
				viewTradeHolder = new ViewTradeHolder();
				viewTradeHolder.txtStalker = (TextView) convertView
						.findViewById(R.id.a_stalker);
				viewTradeHolder.txtOption = (TextView) convertView
						.findViewById(R.id.a_option);
				viewTradeHolder.txtSevenProfit = (TextView) convertView
						.findViewById(R.id.a_sevenProfit);
				viewTradeHolder.txtSevenProfitPer = (TextView) convertView
						.findViewById(R.id.a_sevenProfitPer);
				viewTradeHolder.txtTheleveMoney = (TextView) convertView
						.findViewById(R.id.a_theleveMoney);
				viewTradeHolder.txtTheleveMoneyPer = (TextView) convertView
						.findViewById(R.id.a_theleveMoneyPer);
				viewTradeHolder.mRoundProgressBar1 = (RoundProgressBar) convertView
						.findViewById(R.id.roundProgressBar1);
				viewTradeHolder.mRoundProgressBar2 = (RoundProgressBar) convertView
						.findViewById(R.id.roundProgressBar2);
				viewTradeHolder.txtWinProfit = (TextView) convertView
						.findViewById(R.id.a_winProfit);
				viewTradeHolder.txtLossProfit = (TextView) convertView
						.findViewById(R.id.a_lossProfit);
				convertView.setTag(viewTradeHolder);
			} else {
				viewTradeHolder = (ViewTradeHolder) convertView.getTag();
			}
			Message msg = new Message();
			msg.what = 1;
			Bundle data = new Bundle();
			data.putString("bigImage", radomTrader.getBigImage());
			msg.setData(data);
			this.hand.sendMessage(msg);
			viewTradeHolder.txtStalker.setText(radomTrader.getStalker());
			viewTradeHolder.txtOption.setText(radomTrader.getTrader());
			viewTradeHolder.txtSevenProfit.setText(radomTrader.getDay());
			viewTradeHolder.txtSevenProfitPer.setText("("
					+ radomTrader.getDayPercentage() + ")");
			viewTradeHolder.txtTheleveMoney.setText(radomTrader.getMonth());
			viewTradeHolder.txtTheleveMoneyPer.setText(radomTrader
					.getMonthPercentage());
			viewTradeHolder.txtWinProfit.setText(radomTrader
					.getProfitEarningsMoney() + "\n盈利交易");
			viewTradeHolder.txtLossProfit.setText(radomTrader
					.getFailEarningsMoney() + "\n亏损交易");

			if (!StringUtils.isStringNone(radomTrader.getProfit())) {
				final int profitNum = (int) (Float.parseFloat(radomTrader
						.getProfit()) * 100);
				new Thread(new Runnable() {

					@Override
					public void run() {
						while (winProgress <= profitNum) {
							winProgress += 2;

							if (winProgress >= profitNum) {
								winProgress = profitNum;
								viewTradeHolder.mRoundProgressBar1
										.setProgress(winProgress);
								break;
							}

							viewTradeHolder.mRoundProgressBar1
									.setProgress(winProgress);

							try {
								Thread.sleep(150);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}

					}
				}).start();

			}

			if (!StringUtils.isStringNone(radomTrader.getFail())) {
				final int lossNum = (int) (Float.parseFloat(radomTrader
						.getFail()) * 100);

				new Thread(new Runnable() {

					@Override
					public void run() {
						while (lossProgress <= lossNum) {
							lossProgress += 2;

							if (lossProgress >= lossNum) {
								lossProgress = lossNum;
								viewTradeHolder.mRoundProgressBar2
										.setProgress(lossProgress);
								break;
							}

							viewTradeHolder.mRoundProgressBar2
									.setProgress(lossProgress);
							try {
								Thread.sleep(150);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				}).start();
			}

		} else if (groupPosition == 1) {

			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.item_main_comment, null);
				viewBlogHolder = new ViewBlogHolder();
				viewBlogHolder.traderPhoto = (CircleImageView) convertView
						.findViewById(R.id.item_trade_List_img);
				viewBlogHolder.txtTraderName = (TextView) convertView
						.findViewById(R.id.text_main_comment_name);
				viewBlogHolder.txtTraderStalker = (TextView) convertView
						.findViewById(R.id.text_main_comment_usercount);
				viewBlogHolder.txtTraderComment = (TextView) convertView
						.findViewById(R.id.text_main_comment_content);
				viewBlogHolder.txtTraderTime = (TextView) convertView
						.findViewById(R.id.text_main_comment_time);
				convertView.setTag(viewBlogHolder);
			} else {
				viewBlogHolder = (ViewBlogHolder) convertView.getTag();
			}
			final Trade item = ((Trade) getChild(groupPosition, childPosition));
			AppContext.setImageNo(item.getAvatarsAddress(),
					viewBlogHolder.traderPhoto, traderPhotoOption);

			viewBlogHolder.txtTraderName.setText(item.getTraderName());
			viewBlogHolder.txtTraderStalker.setText(item.getStalker());
			viewBlogHolder.txtTraderComment.setText(item.getTitle());
			viewBlogHolder.txtTraderTime.setText(item.getCreateTime());
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					intent = new Intent();
					intent.putExtra("trade_type", TradeActivity.PUPU_TRADER);
					intent.putExtra("trade_extra_type",
							TradeActivity.NO_PAY_RANK);
					intent.putExtra("trade", item);
					intent.putExtra("actionFlag", "3");
					intent.setClass(context, TradeDetailActivity.class);
					context.startActivity(intent);
				}
			});

		} else if (groupPosition == 2) {
			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.item_appraise, null);
				viewCommentHolder = new ViewCommentHolder();
				viewCommentHolder.traderPhoto = (CircleImageView) convertView
						.findViewById(R.id.item_trade_List_img);
				viewCommentHolder.txtTraderName = (TextView) convertView
						.findViewById(R.id.text_main_comment_name);
				viewCommentHolder.rbAppraiseBar = (RatingBar) convertView
						.findViewById(R.id.trader_appraiseBar);
				viewCommentHolder.rbAppraiseBar
						.setMax(AppContext.RAINNGTOTAL_STAR);
				viewCommentHolder.txtAppraiseContent = (TextView) convertView
						.findViewById(R.id.appraise_content);
				viewCommentHolder.txtAppraiseTime = (TextView) convertView
						.findViewById(R.id.appraise_time);
				viewCommentHolder.txtAppraiser = (TextView) convertView
						.findViewById(R.id.appraiser);
				convertView.setTag(viewCommentHolder);
			} else {
				viewCommentHolder = (ViewCommentHolder) convertView.getTag();
			}

			final Trade commentTrade = (Trade) getChild(groupPosition,
					childPosition);
			if (!StringUtils.isStringNone(commentTrade.getScore())) {
				viewCommentHolder.rbAppraiseBar.setProgress(Integer
						.parseInt(commentTrade.getScore()));
			}

			viewCommentHolder.txtAppraiseContent.setText(commentTrade
					.getComment());

			if (!StringUtils.isStringNone(commentTrade.getCreateDate())) {
				viewCommentHolder.txtAppraiseTime.setText(commentTrade
						.getCreateDate());
			}
			viewCommentHolder.traderPhoto.setVisibility(View.VISIBLE);
			viewCommentHolder.txtTraderName.setVisibility(View.VISIBLE);
			viewCommentHolder.txtAppraiser.setText(commentTrade.getNickName());
			AppContext.setImageNo(commentTrade.getAvatarsAddress(),
					viewCommentHolder.traderPhoto, traderPhotoOption);

			viewCommentHolder.txtTraderName.setText(commentTrade
					.getTraderName());
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(context,
							TraderDiscussActivity.class);
					TradeDetailActivity.currentTrader = commentTrade;
					context.startActivity(intent);
				}
			});
			// }else if (groupPosition == 3) {
			// ViewTradeTrendsHolder viewTradeHolder = null;
			// if (convertView == null) {
			// convertView = LayoutInflater.from(context).inflate(
			// R.layout.item_main_trade, null);
			// viewTradeHolder = new ViewTradeTrendsHolder();
			// viewTradeHolder.textTradeName = (TextView) convertView
			// .findViewById(R.id.text_main_trade_name);
			// viewTradeHolder.textTradeStock = (TextView) convertView
			// .findViewById(R.id.text_main_trade_stock);
			// viewTradeHolder.textTradeStockTime = (TextView) convertView
			// .findViewById(R.id.text_main_trade_stocktime);
			// viewTradeHolder.textTradeStockValue = (TextView) convertView
			// .findViewById(R.id.text_main_trade_stockvalue);
			// viewTradeHolder.textTradeTime = (TextView) convertView
			// .findViewById(R.id.text_main_trade_time);
			// convertView.setTag(viewTradeHolder);
			// } else {
			// viewTradeHolder = (ViewTradeTrendsHolder) convertView.getTag();
			// }
			// final Trade mainTrade = ((Trade) getChild(groupPosition,
			// childPosition));
			//
			// viewTradeHolder.textTradeName.setText(mainTrade.getTraderName());
			// viewTradeHolder.textTradeStock.setText(mainTrade.getStockName()
			// + "(" + mainTrade.getStockCode()
			// + mainTrade.getMarketCode() + ")");
			//
			// viewTradeHolder.textTradeStockTime.setText(mainTrade.getQuantity()
			// + "卖出");
			//
			// if (!StringUtils.isStringNone(mainTrade.getStopPrice())) {
			// if (Float.parseFloat(mainTrade.getStopPrice()) > 0) {
			// viewTradeHolder.textTradeStockValue.setText("+"
			// + mainTrade.getStockPrice() + "万");
			// } else {
			// viewTradeHolder.textTradeStockValue.setText(mainTrade
			// .getStockPrice() + "万");
			// }
			// }
			//
			// viewTradeHolder.textTradeTime.setText(mainTrade.getTradeTime());
			//
			// convertView.setOnClickListener(new OnClickListener() {
			// @Override
			// public void onClick(View v) {
			// intent = new Intent();
			// intent.putExtra("trade_type", TradeActivity.PUPU_TRADER);
			// intent.putExtra("trade_extra_type",
			// TradeActivity.NO_PAY_TREND);
			// intent.putExtra("trade", mainTrade);
			// intent.setClass(context, TradeDetailActivity.class);
			// context.startActivity(intent);
			// }
			// });
		} else if (groupPosition == 3) {
			ViewStockHolder viewStockHolder = null;
			if (convertView == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.item_main_stock, null);
				viewStockHolder = new ViewStockHolder();
				viewStockHolder.textStockName = (TextView) convertView
						.findViewById(R.id.text_main_stock_name);
				viewStockHolder.textStockValue = (TextView) convertView
						.findViewById(R.id.text_main_stock_value);
				convertView.setTag(viewStockHolder);
			} else {
				viewStockHolder = (ViewStockHolder) convertView.getTag();
			}

			Stock stock = ((Stock) getChild(groupPosition, childPosition));
			viewStockHolder.textStockName.setText(stock.getName());
			viewStockHolder.textStockValue.setText(stock.getStockDes());
		} else if (groupPosition == 4) {
			ViewProfitHolder viewProfitHolder = null;
			if (viewProfitHolder == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.item_main_profit, null);
				viewProfitHolder = new ViewProfitHolder();
				viewProfitHolder.layoutProfit = (LinearLayout) convertView
						.findViewById(R.id.layout_main_profit);
				viewProfitHolder.textProfitName = (TextView) convertView
						.findViewById(R.id.text_main_profit_name);
				convertView.setTag(viewProfitHolder);
			} else {
				viewProfitHolder = (ViewProfitHolder) convertView.getTag();
			}

			MainTradeProfit mainTradeProfit = ((MainTradeProfit) getChild(
					groupPosition, childPosition));
			List<Trade> tradeList = mainTradeProfit.getTradeList();

			for (int i = 0; i < tradeList.size(); i++) {
				View v = LayoutInflater.from(context).inflate(
						R.layout.item_main_profit_child, null);
				TextView textOrder = (TextView) v
						.findViewById(R.id.text_main_profit_order);
				TextView textName = (TextView) v
						.findViewById(R.id.text_main_profit_name);
				TextView textProfit = (TextView) v
						.findViewById(R.id.text_main_profit_profit);

				final Trade tempTrade = tradeList.get(i);

				textOrder.setText((i + 1) + "");
				textName.setText(tempTrade.getTraderName());
				textProfit.setText(tempTrade.getEarningsMoney() + "("
						+ tempTrade.getEarningPercentage() + ")");
				// -------------

				v.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						intent = new Intent();
						intent.putExtra("trade_type", TradeActivity.PUPU_TRADER);
						intent.putExtra("trade_extra_type",
								TradeActivity.NO_PAY_RANK);
						intent.putExtra("trade", tempTrade);
						intent.setClass(context, TradeDetailActivity.class);
						context.startActivity(intent);
					}
				});

				viewProfitHolder.layoutProfit.addView(v);
			}
			viewProfitHolder.textProfitName
					.setText(mainTradeProfit.getProfit());
		}
		return convertView;
	}

	// 获取指定组位置处的组数据
	@Override
	public Object getGroup(int groupPosition) {
		return armTypes.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return armTypes.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	// 该方法决定每个组选项的外观
	@SuppressLint("InflateParams")
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		menuClass = armTypes.get(groupPosition);
		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.item_main_options, null);
			viewHolder = new ViewHolder();
			viewHolder.item_tv = (TextView) convertView
					.findViewById(R.id.item_tv);
			viewHolder.textMainMore = (TextView) convertView
					.findViewById(R.id.mian_more);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		TextView headTitle = null;
		headTitle = (TextView) convertView.findViewById(R.id.item_tv_head);
		if (groupPosition == 0) {
			headTitle.setText(getRadomTrader().getTraderName());
			headTitle.setVisibility(View.VISIBLE);
			viewHolder.item_tv.setVisibility(View.GONE);
			viewHolder.textMainMore.setText(R.string.know_more);
			viewHolder.textMainMore.setVisibility(View.VISIBLE);
		} else if (groupPosition == 1) {
			viewHolder.item_tv.setText(menuClass.getCat_name());
			viewHolder.textMainMore.setVisibility(View.GONE);
		} else if (groupPosition == 2) {
			viewHolder.item_tv.setText(menuClass.getCat_name());
			viewHolder.textMainMore.setVisibility(View.GONE);
		} else {
			viewHolder.textMainMore.setVisibility(View.VISIBLE);
			headTitle.setVisibility(View.GONE);
			viewHolder.item_tv.setVisibility(View.VISIBLE);
			viewHolder.item_tv.setText(menuClass.getCat_name());
		}

		viewHolder.textMainMore.setTag(groupPosition);
		viewHolder.textMainMore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				int pos = (Integer) arg0.getTag();
				noPayMoreItemPressed(pos);
			}
		});

		return convertView;
	}

	final static class ViewHolder {
		TextView item_tv;
		ImageView icon_iv;
		ImageView expansion_iv;
		TextView textMainMore;
	}

	final static class ViewTradeHolder {
		TextView txtStalker, txtOption;
		TextView txtSevenProfit, txtSevenProfitPer, txtTheleveMoney,
				txtTheleveMoneyPer;
		TextView txtWinProfit, txtLossProfit;
		RoundProgressBar mRoundProgressBar1, mRoundProgressBar2;
	}

	final static class ViewBlogHolder {
		CircleImageView traderPhoto;
		TextView txtTraderName, txtTraderStalker;
		TextView txtTraderComment;
		TextView txtTraderTime;
	}

	final static class ViewCommentHolder {
		CircleImageView traderPhoto;
		TextView txtTraderName;
		RatingBar rbAppraiseBar;
		TextView txtAppraiseContent;
		TextView txtAppraiseTime, txtAppraiser;
	}

	final static class ViewTradeTrendsHolder {
		TextView textTradeName, textTradeStock, textTradeStockTime,
				textTradeStockValue, textTradeTime;
	}

	final static class ViewTodayFocusHolder {
		TextView textTodayFocusName, textTodayFocusContent;

	}

	final static class ViewStockHolder {
		TextView textStockName, textStockValue;

	}

	final static class ViewProfitHolder {
		LinearLayout layoutProfit;
		TextView textProfitName;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {

	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {

	}

	@Override
	public long getCombinedChildId(long groupId, long childId) {
		return 0;
	}

	@Override
	public long getCombinedGroupId(long groupId) {
		return 0;
	}

	public void noPayMoreItemPressed(int pos) {
		switch (pos) {
		case 0:// 普普操盘手
			intent = new Intent();
			intent.putExtra("trade_type", TradeActivity.PUPU_TRADER);
			intent.putExtra("trade_extra_type", TradeActivity.NO_PAY_TRADER);
			intent.putExtra("trade", getRadomTrader());
			intent.setClass(context, TradeDetailActivity.class);
			context.startActivity(intent);
			break;
		case 1:// 博客专栏
				// intent = new Intent();
				// intent.putExtra("trade_type", TradeActivity.PUPU_TRADER);
				// intent.putExtra("trade_extra_type",
				// TradeActivity.NO_PAY_BLOG);
				// intent.putExtra("trade", getRadomTrader());
				// intent.setClass(context, TradeDetailActivity.class);
				// context.startActivity(intent);
			intent = new Intent(context, MemerPlanActivity.class);
			// 设置跳转标志为如此Activity存在则把其从任务堆栈中取出放到最上方
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			context.startActivity(intent);
			((Activity) context).overridePendingTransition(
					android.R.anim.fade_in, android.R.anim.fade_in);
			break;
		// case 2:// 网民最新评论
		// TradeDetailActivity.currentTrader = getRadomTrader();
		// Intent intent = new Intent(context, TraderDiscussActivity.class);
		// context.startActivity(intent);
		// break;
		// case 3:// 操盘手30天动态
		// // nonononononono
		// break;
		case 3:// 我的股票
			intent = new Intent(context, MemerPlanActivity.class);
			// 设置跳转标志为如此Activity存在则把其从任务堆栈中取出放到最上方
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			context.startActivity(intent);
			((Activity) context).overridePendingTransition(
					android.R.anim.fade_in, android.R.anim.fade_in);
			break;
		case 4:// 操盘手排行榜
			intent = new Intent(context, TraderRankActivity.class);
			context.startActivity(intent);
			break;
		}
	}
}
