package com.financialnet.app.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.app.ui.BaseActivity.OnSurePress;
import com.financialnet.app.ui.StockAboutActivity;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.StockService;
import com.financialnet.util.UIHelper;

import java.util.List;

/**
 * @MineStockAdapter.java
 * @author li mingtao
 * @function :我的股票适配器
 * @2013-9-3@下午5:53:45
 * @update:
 */
public class MineStockAdapter extends BaseListAdapter<Stock> {
	public MineStockAdapter(BaseActivity context, List<Stock> list) {
		super(context, list);
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int pos, View rowView, ViewGroup arg2) {
		ViewHolder holder;
		if (rowView == null) {
			holder = new ViewHolder();
			rowView = mInflater.inflate(R.layout.item_minestock, null);
			holder.textMineStockId = (TextView) rowView
					.findViewById(R.id.txtmineStockId);
			holder.textMineStockName = (TextView) rowView
					.findViewById(R.id.txtmineStockName);
			holder.textLowerest = (TextView) rowView
					.findViewById(R.id.stock_nowprice);
			holder.textHigher = (TextView) rowView
					.findViewById(R.id.texthigher);
			holder.textRiseFall = (TextView) rowView
					.findViewById(R.id.text_risefall);
			holder.textNowPrice = (TextView) rowView
					.findViewById(R.id.stock_nowprice);

			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		final Stock stock = (Stock) getItem(pos);
		holder.stock = stock;

		holder.textMineStockId.setText("(" + stock.getStockCode() + ")");
		holder.textMineStockName.setText(stock.getStockName());
		holder.textNowPrice.setText(stock.getStockPrice());
		holder.textLowerest.setText(stock.getLowPrice());
		holder.textHigher.setText(stock.getUpPrice());

		holder.textRiseFall.setText(stock.getPercentage());

		rowView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(baseActivity,
						StockAboutActivity.class);
				intent.putExtra("stock", ((ViewHolder) arg0.getTag()).stock);
				intent.putExtra("tag", 1);
				baseActivity.startActivity(intent);
			}
		});

		rowView.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {

				UIHelper.showSysDialog(baseActivity,
						baseActivity.getResString(R.string.stock_delete),
						new OnSurePress() {
							@Override
							public void onClick(View view) {
								StockService
										.getInstance(baseActivity)
										.deleteStock(
												stock.getMarketID(),
												stock.getStockCode(),
												AppContext.getCurrentMember()
														.getMemberID() + "",
												new CustomAsyncResponehandler() {
													@Override
													public void onSuccess(
															ResponeModel baseModel) {
														super.onSuccess(baseModel);
														if (baseModel
																.isStatus()) {
															mList.remove(stock);
															notifyDataSetChanged();
														}
													}
												});
							}
						}, true);

				return false;
			}
		});

		return rowView;
	}

	class ViewHolder {
		private TextView textMineStockName, textMineStockId, textNowPrice,
				textLowerest, textHigher, textRiseFall;
		private Stock stock;
	}
}
