package com.financialnet.app.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.model.DBMessage;
import com.financialnet.util.StringUtils;
import com.financialnet.widget.CircleImageView;

import java.sql.Date;
import java.util.List;

/**
 * @className: ChatAdapter.java
 * @author: limingtao
 * @function: 聊天适配器
 * @date: 2013年9月16日下午4:37:06
 * @update:
 */
public class ChatAdapter extends BaseListAdapter<DBMessage> {

	public static final int SENDSUCCESS = 1;
	public static final int SENDFAILED = 2;

	public ChatAdapter(Activity context, List<DBMessage> list) {
		super(context, list);
	}

	@Override
	public View getView(int pos, View rowView, ViewGroup vg) {
		ViewHolder holder;
		if (rowView == null) {
			holder = new ViewHolder();
			rowView = mInflater.inflate(R.layout.item_chat, null);
			holder.chatInPanel = rowView.findViewById(R.id.chat_in_panel);
			holder.chatOutPanel = rowView.findViewById(R.id.chat_out_panel);
			holder.chatInNickName = (TextView) rowView
					.findViewById(R.id.chat_in_nickname);
			holder.chatInContext = (TextView) rowView
					.findViewById(R.id.chat_in_content);
			holder.chatOutContext = (TextView) rowView
					.findViewById(R.id.chat_out_content);
			holder.chatInTime = (TextView) rowView
					.findViewById(R.id.chat_int_time);
			holder.chatOutTime = (TextView) rowView
					.findViewById(R.id.chat_out_time);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		DBMessage message = mList.get(pos);
		if (AppContext.getCurrentMember().getMemberName()
				.equals(message.getSender())) {// 自己发送
			holder.chatInPanel.setVisibility(View.GONE);
			holder.chatOutPanel.setVisibility(View.VISIBLE);

			holder.chatOutTime.setText(StringUtils.friendly_time(new Date(
					message.getTimestamp())));
			holder.chatOutContext.setText(message.getContent());

		} else {// 别人发送
			holder.chatInPanel.setVisibility(View.VISIBLE);
			holder.chatOutPanel.setVisibility(View.GONE);

			holder.chatInNickName.setText(message.getSender());
			holder.chatInTime.setText(StringUtils.friendly_time(new Date(
					message.getTimestamp())));
			holder.chatInContext.setText(message.getContent());
		}
		return rowView;
	}

	class ViewHolder {
		View chatInPanel, chatOutPanel;
		CircleImageView chatInUserhead, chatOutUserhead;
		TextView chatInNickName;
		TextView chatInContext, chatInTime, chatOutContext, chatOutTime;
	}
}
