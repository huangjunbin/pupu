package com.financialnet.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.financialnet.app.R;
import com.financialnet.app.ui.MemerPlanActivity;
import com.financialnet.model.Group;
import com.financialnet.model.Stock;
import com.financialnet.util.StringUtils;

/**
 * @className：PassTradeAdapter.java
 * @author: lmt
 * @Function: 未付费过往交易列表
 * @createDate: 2014-10-23 下午11:23:23
 * @update:
 */
@SuppressLint("InflateParams")
public class PassTradeAdapter extends BaseListAdapter<Object> {
	public static final int lastTradeRecorde = 0x001;// 最新交易
	public static final int passTradeRecorde = 0x002;// 过往交易列表
	private int cureentGroup;
	public List<Group> groupKey;

	public PassTradeAdapter(Activity context, List<Object> list,
			List<Group> mGroupKey) {
		super(context, list);
		groupKey = mGroupKey;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (groupKey.contains(getItem(position))) {
			Group group = (Group) getItem(position);
			cureentGroup = group.getBelongroup();
			convertView = mInflater.inflate(R.layout.item_group_title, null);
			TextView txtGroup = (TextView) convertView
					.findViewById(R.id.group_name);
			txtGroup.setText(group.getGroupDescription());
		} else {
			Stock stock = ((Stock) getItem(position));
			cureentGroup = stock.getBelongroup();
			if (cureentGroup == lastTradeRecorde) {
				convertView = mInflater.inflate(
						R.layout.item_trade_loack_passtransation, null);
				TextView txt = (TextView) convertView
						.findViewById(R.id.lock_content);
				txt.setText("收益 " + stock.getEarningsMoney() + "("
						+ stock.getEarningPercentage() + ")");
			} else if (cureentGroup == passTradeRecorde) {
				ViewHolder holder = new ViewHolder();
				convertView = mInflater.inflate(
						R.layout.item_trade_passtransaction, null);

				holder.txtId = (TextView) convertView
						.findViewById(R.id.txtStockId);
				holder.txtName = (TextView) convertView
						.findViewById(R.id.txtStockName);
				holder.viewFlagStock = convertView.findViewById(R.id.flag_icon);
				holder.imgviewIconFlag = (ImageView) convertView
						.findViewById(R.id.icon_flag);
				holder.txtTitle = (TextView) convertView
						.findViewById(R.id.txtTitle);
				holder.viewProfitBar = convertView
						.findViewById(R.id.profit_bar);
				holder.txtContent = (TextView) convertView
						.findViewById(R.id.txtContent);
				holder.txtDate = (TextView) convertView
						.findViewById(R.id.txtDate);
				// ----
				holder.txtId.setText(stock.getStockCode() + "("
						+ stock.getMarketCode() + ")");
				holder.txtName.setText(stock.getStockName());
				holder.txtDate.setText(stock.getTradeTime());

				holder.viewFlagStock.setVisibility(View.VISIBLE);
				holder.viewProfitBar.setVisibility(View.VISIBLE);

				// ----
				if ("1".equals(stock.getFlagAuthentication())) {
					holder.imgviewIconFlag
							.setBackgroundResource(R.drawable.cret_v_icon_a);
				} else {
					holder.imgviewIconFlag
							.setBackgroundResource(R.drawable.cret_v_icon_b);
				}
				String inoutstr="";
				if (stock.getFlagTrade()==1) {
					inoutstr = " 卖出";
				} else {
					inoutstr = " 买入";
				}
				holder.txtTitle.setText(stock.getStopPrice() + inoutstr
						+ stock.getQuantity() + "股");

				StringBuffer profot_str = new StringBuffer();
				profot_str.append(stock.getEarningsMoney() + "");

				profot_str.append("(" + stock.getEarningPercentage() + ")");
				holder.txtContent.setText(profot_str.toString());
			}

			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(activity,
							MemerPlanActivity.class);
					// 设置跳转标志为如此Activity存在则把其从任务堆栈中取出放到最上方
					intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					activity.startActivity(intent);
					activity.overridePendingTransition(android.R.anim.fade_in,
							android.R.anim.fade_in);
				}
			});
		}
		return convertView;
	}

	class ViewHolder {
		public TextView txtId, txtName, txtDate, txtTitle, txtContent;
		public View viewFlagStock, viewProfitBar;
		public ImageView imgviewIconFlag;
	}
}
