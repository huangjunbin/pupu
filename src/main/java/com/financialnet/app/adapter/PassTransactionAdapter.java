package com.financialnet.app.adapter;

import java.util.List;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.app.ui.PassTransactionDetailActivity;
import com.financialnet.model.Stock;

public class PassTransactionAdapter extends BaseListAdapter<Stock> {

	public PassTransactionAdapter(BaseActivity context, List<Stock> list) {
		super(context, list);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater
					.inflate(R.layout.item_passtransaction, null);
			holder.textStockState = (TextView) convertView
					.findViewById(R.id.stock_state);
			holder.textStockTime = (TextView) convertView
					.findViewById(R.id.stock_time);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final Stock stock = mList.get(position);

		holder.textStockState.setText(stock.getStockCase());

		Drawable able = null;
		if ("1".equals(stock.getFlagAuthentication())) {
			able = baseActivity.getResources().getDrawable(
					R.drawable.cret_v_icon_a);
			holder.textStockState.setCompoundDrawablesWithIntrinsicBounds(able,
					null, null, null);
		} else {
			able = baseActivity.getResources().getDrawable(
					R.drawable.cret_v_icon_b);
			holder.textStockState.setCompoundDrawablesWithIntrinsicBounds(able,
					null, null, null);
		}

		holder.textStockTime.setText(stock.getTradeTime());

		String inoutstr;
		StringBuffer stateSb = new StringBuffer();
		if (stock.getFlagTrade()==1) {
			inoutstr = " 卖出";
		} else {
			inoutstr = " 买入";
		}

		stateSb.append(stock.getStockPrice() + inoutstr + stock.getQuantity() + "股");
		holder.textStockState.setText(stateSb.toString());

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/*AppContext.shake();
				((PassTransactionDetailActivity) baseActivity).currentStock = stock;
				Message msg = new Message();
				msg.what = PassTransactionDetailActivity.UPDTTE_VIEW;
				((PassTransactionDetailActivity) baseActivity).hand
						.sendMessage(msg);*/
				Intent intent = new Intent(baseActivity,
						PassTransactionDetailActivity.class);
				intent.putExtra("stock", stock);
				intent.putExtra("type", 1);
				baseActivity.startActivity(intent);
			}
		});
		return convertView;
	}

	class ViewHolder {
		TextView textStockState, textStockTime;
	}
}
