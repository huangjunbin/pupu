package com.financialnet.app.adapter;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.app.ui.BaseActivity.OnSurePress;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.StockService;
import com.financialnet.util.MyAnimation;
import com.financialnet.util.UIHelper;

import java.util.List;

/**
 * @MineSearchStockAdapter.java
 * @author li mingtao
 * @function :我的股票搜素适配器
 * @2013-9-4@上午11:07:56
 * @update:
 */
public class MineOwnStockAdapter extends BaseListAdapter<Stock> {
	String TAG = "MineSearchStockAdapter";

	public MineOwnStockAdapter(BaseActivity context, List<Stock> list) {
		super(context, list);
	}

	@Override
	public View getView(int pos, View rowView, ViewGroup vg) {
		ViewHolder holder;
		if (rowView == null) {
			holder = new ViewHolder();
			rowView = mInflater.inflate(R.layout.item_stockown, null);
			holder.imgShowdelete = (ImageView) rowView
					.findViewById(R.id.showdelete);
			holder.textSearchStockName = (TextView) rowView
					.findViewById(R.id.searchstock_name);
			holder.imgDeleteSearchStock = (ImageView) rowView
					.findViewById(R.id.delete_searchstock);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}

		final Stock stock = (Stock) getItem(pos);
		final String nameAndId = stock.getStockName() + "("
				+ stock.getStockCode() + stock.getMarketCode() + ")";
		holder.textSearchStockName.setText(nameAndId);

		holder.imgShowdelete.clearAnimation();
		holder.imgShowdelete.setTag(holder.imgDeleteSearchStock);
		holder.imgShowdelete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				View delete = (View) view.getTag();
				if (delete.getVisibility() == View.VISIBLE) {
					delete.setVisibility(View.INVISIBLE);
					MyAnimation.startAnimationOUT(view, 200, 0);
				} else {
					MyAnimation.startAnimationIN(view, 200);
					delete.setVisibility(View.VISIBLE);
				}
			}
		});

		holder.imgDeleteSearchStock.setTag(holder.imgShowdelete);
		holder.imgDeleteSearchStock.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View delete) {
				delete.setVisibility(View.GONE);
				MyAnimation.startAnimationOUT((View) delete.getTag(), 0, 0);
				UIHelper.showSysDialog(
						baseActivity,
						nameAndId
								+ baseActivity
										.getResString(R.string.stock_exit),
						new OnSurePress() {
							@Override
							public void onClick(View view) {
								StockService
										.getInstance(baseActivity)
										.deleteStock(
												stock.getMarketID(),
												stock.getStockCode(),
												AppContext.getCurrentMember()
														.getMemberID() + "",
												new CustomAsyncResponehandler() {
													@Override
													public void onSuccess(
															ResponeModel baseModel) {
														super.onSuccess(baseModel);
														if (baseModel
																.isStatus()) {
															// 移除股票
															mList.remove(stock);
															notifyDataSetChanged();
														}
													}
												});
							}
						}, true);
			}
		});
		return rowView;
	}

	class ViewHolder {
		public TextView textSearchStockName;
		public ImageView imgShowdelete, imgDeleteSearchStock;
	}
}
