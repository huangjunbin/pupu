package com.financialnet.app.adapter;

import java.util.List;

import org.json.JSONException;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.financialnet.app.AppContext;
import com.financialnet.app.R;
import com.financialnet.app.ui.BaseActivity;
import com.financialnet.app.ui.HotInfoActivity;
import com.financialnet.app.ui.HotInfoDetailActivity;
import com.financialnet.model.HotInfo;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.TradeBlog;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.service.TraderService;

;

/**
 * @className：TradeBlogAdapter.java
 * @author: li mingtao
 * @Function: 操盘手博客列表
 * @createDate: 2014-8-19上午10:51:33
 * @update:
 */
public class TradeBlogAdapter extends BaseListAdapter<TradeBlog> {

	public TradeBlogAdapter(Activity context, List<TradeBlog> list,
			int defaultId) {
		super(context, list, defaultId);

	}

	public TradeBlogAdapter(BaseActivity context, List<TradeBlog> list,
			int defaultId) {
		super(context, list, defaultId);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		final TradeBlog data = mList.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_tradeblog, null);
			holder.imgviewBlogImg = (ImageView) convertView
					.findViewById(R.id.item_blog_img);
			holder.textBlogContent = (TextView) convertView
					.findViewById(R.id.item_blog_content);
			holder.BlogDatetime = (TextView) convertView
					.findViewById(R.id.item_blog_datetime);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		AppContext.setImage(data.getAvatarsAddress(), holder.imgviewBlogImg,
				options);
		holder.textBlogContent.setText(data.getTitle());
		holder.BlogDatetime.setText(data.getCreateTime());

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				TraderService.getInstance(baseActivity).getTradeBlogDetail(
						data.getId(), new CustomAsyncResponehandler() {
							@Override
							public void onSuccess(ResponeModel baseModel) {
								super.onSuccess(baseModel);
								if (baseModel.isStatus()) {
									try {
										String contents = baseModel
												.getDataResult().getString(
														"contents");
										Intent intent = new Intent();
										intent.putExtra("title", "博客专栏");
										HotInfo hotInfo = new HotInfo();
										hotInfo.setContent(contents);//comeFrom
										intent.putExtra("hotInfo", hotInfo);
										intent.setClass(baseActivity,
												HotInfoDetailActivity.class);
										baseActivity.startActivity(intent);
									} catch (JSONException e) {
										e.printStackTrace();
									}

								}
							}

							@Override
							public void onFinish() {
								super.onFinish();

							}
						});
			}

		});

		return convertView;
	}

	class ViewHolder {
		ImageView imgviewBlogImg;
		TextView textBlogContent;
		TextView BlogDatetime;
	}
}
