package com.financialnet.app.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.financialnet.app.R;
import com.financialnet.model.FundDetail;

import java.util.List;

public class FundDetailAdapter extends BaseListAdapter<FundDetail> {
    private int comeFrom;
    private Intent intent;

    public FundDetailAdapter(Activity context, List<FundDetail> list,
                             int mcomeFrom) {
        super(context, list);
        comeFrom = mcomeFrom;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        final FundDetail data = mList.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_fund_detail, null);
            holder.tvDate = (TextView) convertView
                    .findViewById(R.id.tv_date);
            holder.tvMoney = (TextView) convertView
                    .findViewById(R.id.tv_money);
            holder.tvStatus = (TextView) convertView
                    .findViewById(R.id.tv_state);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(comeFrom==2){
            holder.tvDate.setText(data.getBonusTime());
            holder.tvStatus.setText(data.getBonusType());
        }else{
            holder.tvDate.setText(data.getTradeTime());
            holder.tvStatus.setText(data.getStatus());
        }
        holder.tvMoney.setText(data.getAmount());




        return convertView;
    }

    class ViewHolder {

        TextView tvMoney,tvDate,tvStatus;
    }
}
