package com.financialnet.app;

import android.os.Environment;

import java.io.File;

/**
 * 应用程序配置类：用于保存用户相关信息及设置
 * 
 * @author bin
 * @version 1.0.0
 * @created 2014-2-24
 */

public class AppConfig {
	public static final boolean isDevelopMode = false;
	public static final String NEW_FRIENDS_USERNAME = "item_new_friends";
	public static final String GROUP_USERNAME = "item_groups";
	public static final String MESSAGE_ATTR_IS_VOICE_CALL = "is_voice_call";
	public final static String CONF_APP_UNIQUEID = "APP_UNIQUEID";
	public final static String SAVE_IMAGE_PATH = "photo";
	public final static String CONF_VOICE = "perf_voice";
	public final static String DEFAULT_SAVE_PATH = Environment
			.getExternalStorageDirectory()
			+ File.separator
			+ "FinancialNet"
			+ File.separator;
	public final static String CHATPHOTO_PATH = DEFAULT_SAVE_PATH
			+ "FinancialNet" + File.separator;
	public final static String APK_PATH = DEFAULT_SAVE_PATH + "Apkload"
			+ File.separator;
	public static final String FILEDIR = DEFAULT_SAVE_PATH + "file";
	public static boolean firstCheckVersion = true;
	public static int localVersionCode = 1;
	public static String localVersionName = "0.0.1";

	// ------------------
	public static final int PAY_NO = 0;
	public static final int PAY = 1;
	
	public static final int CLASS_GOLD = 1;// 1黄金
	public static final int CLASS_WHITEGOD = 2;// 2白金
	public static final int CLASS_DIAMOND = 3;// 3钻石
}
