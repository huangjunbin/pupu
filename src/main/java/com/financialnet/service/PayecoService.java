package com.financialnet.service;

import android.content.Context;

import com.financialnet.model.Pay;
import com.financialnet.model.RequestModel;
import com.financialnet.net.Urls;
import com.financialnet.net.http.CustomAsyncHttpClient;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.net.http.RequestParams;

public class PayecoService {
	@SuppressWarnings("unused")
	private static CustomAsyncHttpClient httpClient;
	private static Context context;

	// 内部全局唯一实例
	static PayecoService mInstance;

	public static PayecoService getInstance(Context mcontext) {
		httpClient = new CustomAsyncHttpClient(context);
		context = mcontext;
		if (mInstance == null) {
			// 只有第一次才彻底执行这里的代码
			synchronized (MemberService.class) {
				// 再检查一次
				if (mInstance == null)
					mInstance = new PayecoService();
			}
		}
		return mInstance;
	}

	/**
	 * 获取热门资讯 memberID String true 会员id typeId String true
	 * 付费类型(1:钻石会员;2:白金会员;3:黄金会员) month String true 充值时间(3:三个月;6:六个月;12:十二个月)
	 */
	public void pay(final CustomAsyncResponehandler handler, String memberID,
			String mobileNumber, String typeId, String month) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("memberID", memberID);
		params.put("typeId", typeId);
		params.put("mobileNumber", mobileNumber);
		params.put("month", month);
		requestModel.setParams(params);
		requestModel.setCls(Pay.class);
		requestModel.setShowDialog(true);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.PayOrder);
		// requestModel.setUrl(add);
		httpClient.postOrign(requestModel, handler);
	}
}
