package com.financialnet.service;

import android.content.Context;

import com.financialnet.app.AppConfig;
import com.financialnet.model.RequestModel;
import com.financialnet.model.Version;
import com.financialnet.net.Urls;
import com.financialnet.net.http.CustomAsyncHttpClient;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.net.http.RequestParams;

public class VersionService {
	private static CustomAsyncHttpClient httpClient;
	static VersionService mInstance;

	public static VersionService getInstance(Context context) {
		httpClient = new CustomAsyncHttpClient(context);
		if (mInstance == null) {
			// 只有第一次才彻底执行这里的代码
			synchronized (VersionService.class) {
				// 再检查一次
				if (mInstance == null)
					mInstance = new VersionService();
			}
		}
		return mInstance;
	}

	/**
	 * 获取最新版本信息
	 */
	public void getNewVersionInfo(final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("type", "1");
		params.put("versionCode", AppConfig.localVersionCode + "");
		requestModel.setParams(params);
		requestModel.setCls(Version.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(false);
		requestModel.setUrl(Urls.version_action);
		httpClient.post(requestModel, handler);
	}
}
