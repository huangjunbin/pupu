package com.financialnet.service;

import android.content.Context;

import com.financialnet.app.AppContext;
import com.financialnet.app.adapter.StockAdapter;
import com.financialnet.model.AddStockRequest;
import com.financialnet.model.HotInfo;
import com.financialnet.model.RequestModel;
import com.financialnet.model.Stock;
import com.financialnet.model.StockSearchRequest;
import com.financialnet.net.Urls;
import com.financialnet.net.http.CustomAsyncHttpClient;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.net.http.RequestParams;
import com.financialnet.util.DateUtil;

/**
 * @className: StockService.java
 * @author: limingtao
 * @function: 操盘手业务操作
 * @date: 2013年9月23日下午3:42:08
 * @update:
 */
public class StockService {
	String TAG = "StockService";
	private static CustomAsyncHttpClient httpClient;
	static StockService mInstance;

	public static StockService getInstance(Context context) {
		httpClient = new CustomAsyncHttpClient(context);
		if (mInstance == null) {
			// 只有第一次才彻底执行这里的代码
			synchronized (StockService.class) {
				// 再检查一次
				if (mInstance == null)
					mInstance = new StockService();
			}
		}
		return mInstance;
	}

	/**
	 * 获取操盘手的过往交易
	 * 
	 * @param traderId
	 *            操盘手id
	 * @param pageNumber
	 *            页码
	 * @param pageSize
	 *            页数
	 */
	public void getStockPassTrade(String traderId, String pageNumber,
			String pageSize, final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("traderId", traderId);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Stock.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetTraderPassTrade);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手过往交易详情
	 * 
	 * @param traderId
	 *            操盘手id
	 * @param marketID
	 *            市场id
	 * @param stockCode
	 *            股票编码
	 * @param pageNumber
	 *            页码
	 * @param pageSize
	 *            页数
	 */
	public void getStockPassTradeDetail(String traderId, String marketID,
			String stockCode, String pageNumber, String pageSize,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("traderId", traderId);
		params.put("marketID", marketID);
		params.put("stockCode", stockCode);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Stock.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetTradePassTradeDetail);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手系统过往交易详情

	 *            页数
	 */
	public void getSysStockPassTradeDetail(String memberId, String id,

	final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("memberId", memberId);
		params.put("id", id);
		;
		requestModel.setParams(params);
		requestModel.setCls(Stock.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.sysPassTrade);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手今日关注股票列表
	 * 
	 * @param traderId
	 * @param pageNumber
	 * @param pageSize
	 */
	public void getStockAttentionToday(String memberId, String traderId,
			String createTime, String pageNumber, String pageSize,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		// params.put("memberId", memberId);
		params.put("traderID", traderId);
		params.put("createTime", createTime);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Stock.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetStockAttentionToday);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手今日关注股票列表
	 * 

	 * @param pageNumber
	 * @param pageSize
	 */
	public void getHomeStockAttentionToday(String memberId, String pageNumber,
			String pageSize, final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("memberId", memberId);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Stock.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.sysToday);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手今日关注详情
	 * 
	 * @param stockCode
	 *            股票编号

	 *            操盘手id
	 * @param marketID
	 *            市场id
	 * @param pageNumber
	 * @param pageSize
	 */
	public void getStockAttentionTodayDetail(String stockCode, String traderId,
			String marketID, String pageNumber, String pageSize,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("stockCode", stockCode);
		params.put("traderID", traderId);
		params.put("marketID", marketID);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Stock.class);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetStockAttentionTodayDetail);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘系统手今日关注详情
	 * 

	 */
	public void getSysStockAttentionTodayDetail(String memberId, String id,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("memberId", memberId);
		params.put("id", id);
		requestModel.setParams(params);
		requestModel.setCls(Stock.class);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.sysToday);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手现时持仓
	 * 
	 * @param traderId
	 * @param pageNumber
	 * @param pageSize
	 */
	public void getTraderHoldNow(String traderId, String pageNumber,
			String pageSize, final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("traderId", traderId);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Stock.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetTraderNowHold);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手现时持仓详情
	 * 
	 * @param traderId
	 *            操盘手id
	 * @param marketID
	 *            市场id
	 * @param stockCode
	 *            股票编号
	 * @param pageNumber
	 * @param pageSize
	 * @param handler
	 */
	public void getTraderHoldNowDetail(String traderId, String marketID,
			String stockCode, String pageNumber, String pageSize,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("traderId", traderId);
		params.put("marketID", marketID);
		params.put("stockCode", stockCode);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Stock.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetTraderNowHoldDetail);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手的股票
	 * 
	 * @param stockType
	 * @param traderId
	 * @param pageNumber
	 * @param pageSize
	 * @param handler
	 */
	public void getStockTradeList(int stockType, String traderId,
			String pageNumber, String pageSize,
			final CustomAsyncResponehandler handler) {
		switch (stockType) {
		case StockAdapter.PASS_TRADE:
			getStockPassTrade(traderId, pageNumber, pageSize, handler);
			break;
		case StockAdapter.ATTENTION_TODAY:
			getStockAttentionToday(
					String.valueOf(AppContext.getCurrentMember().getMemberID()),
					traderId, DateUtil.getCurrentTime("yyyy-MM-dd"),
					pageNumber, pageSize, handler);
			break;
		case StockAdapter.HOLD_NOW:
			getTraderHoldNow(traderId, pageNumber, pageSize, handler);
			break;
		default:
		}
	}

	/**
	 * 获取未付费过往交易（整合最新交易与30天前交易）
	 * 
	 * @param traderId
	 *            操盘手id
	 * @param pageNumber
	 * @param pageSize
	 * @param handler
	 */
	public void getPassTradeStockList(String traderId, String pageNumber,
			String pageSize, final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("traderId", traderId);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetPassTradeStockList);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 我的股票
	 * 
	 * @param memberId
	 * @param pageNumber
	 * @param pageSize
	 */
	public void getMineStock(String memberId, String pageNumber,
			String pageSize, final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("memberId", memberId);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Stock.class);
		requestModel.setShowDialog(true);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetMyStock);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取股票详情
	 * 
	 * @param marketCode
	 * @param stockCode
	 * @param type
	 */
	public void getMineStockDetail(String marketCode, String stockCode,
			String type, final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("marketCode", marketCode);
		params.put("stockCode", stockCode);
		params.put("type", type);
		requestModel.setParams(params);
		requestModel.setCls(Stock.class);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetMyStockDetail);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 股票的搜索
	 * 

	 * @param handler
	 */
	public void searchStock(StockSearchRequest request,
			CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("stockCode", request.getStockCode());
		params.put("pageNumber", String.valueOf(request.getPageNumber()));
		params.put("pageSize", String.valueOf(request.getPageSize()));
		requestModel.setParams(params);
		requestModel.setCls(Stock.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.SearchStock_action);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 添加股票
	 * 
	 * @param request
	 * @param handler
	 */
	public void addMyStock(AddStockRequest request,
			CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("marketID", request.getMarketID());
		params.put("stockCode", request.getStockCode());
		params.put("memberID", request.getMemberId());
		params.put("stockName", request.getStockName());
		requestModel.setParams(params);
		requestModel.setCls(Stock.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setLetsChildShow(true);
		requestModel.setUrl(Urls.AddStock_action);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取我的股票新闻
	 * 
	 * @param marketID
	 *            市场id
	 * @param stockCode
	 *            股票编码
	 * @param pageNumber
	 * @param pageSize
	 * @param handler
	 */
	public void getMyStockNews(String marketID, String stockCode,
			String pageNumber, String pageSize,
			CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("marketID", marketID);
		params.put("stockCode", stockCode);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(HotInfo.class);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetMyStockNews);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 我的股票新闻详情
	 * 
	 * @param id
	 *            新闻id
	 * @param handler
	 */
	public void getMyStockNewDetail(String id, CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("id", id);
		requestModel.setParams(params);
		requestModel.setCls(HotInfo.class);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetMyStockNewsDetail);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 删除股票
	 * 
	 * @param marketID
	 *            市场Id
	 * @param stockCode
	 *            股票编号
	 * @param memberID
	 *            用户Id
	 * @param handler
	 */
	public void deleteStock(String marketID, String stockCode, String memberID,
			CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("marketID", marketID);
		params.put("stockCode", stockCode);
		params.put("memberID", memberID);
		requestModel.setParams(params);
		requestModel.setCls(HotInfo.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.deleteStock_action);
		httpClient.post(requestModel, handler);
	}

	public void getPassAttetion(String traderID, String pageNumber,
			String pageSize, final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("traderID", traderID);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Stock.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.attentionPass);
		httpClient.post(requestModel, handler);
	}

}
