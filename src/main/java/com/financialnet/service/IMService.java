package com.financialnet.service;

import android.content.Context;
import android.text.TextUtils;

import com.easemob.chat.EMGroupManager;
import com.easemob.exceptions.EaseMobException;
import com.easemob.util.HanziToPinyin;
import com.financialnet.app.AppConfig;
import com.financialnet.app.AppContext;
import com.financialnet.model.Group;
import com.financialnet.model.GroupMember;
import com.financialnet.model.Member;
import com.financialnet.model.RequestModel;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.User;
import com.financialnet.net.Urls;
import com.financialnet.net.http.CustomAsyncHttpClient;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.net.http.RequestParams;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IMService {
	private static CustomAsyncHttpClient httpClient;
	static IMService mInstance;

	public static IMService getInstance(Context context) {
		httpClient = new CustomAsyncHttpClient(context);
		if (mInstance == null) {
			// 只有第一次才彻底执行这里的代码
			synchronized (IMService.class) {
				// 再检查一次
				if (mInstance == null)
					mInstance = new IMService();
			}
		}
		return mInstance;
	}

	/**
	 * 获取好友列表
	 */
	public void getFriendsList(String moblieNumber,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("mobileNumber", moblieNumber);
		requestModel.setParams(params);
		requestModel.setCls(Member.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetFriendList);
		httpClient.post(requestModel, handler);
		new Thread() {
			public void run() {
				// 存储所有群组
				try {
					EMGroupManager.getInstance().getGroupsFromServer();
				} catch (EaseMobException e) {
					e.printStackTrace();
				}
			};
		}.start();
	}

	/**
	 * 获取好友列表S
	 */
	public void getFriendsListS(String moblieNumber,
			final CustomAsyncResponehandler handler, boolean show) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("mobileNumber", moblieNumber);
		requestModel.setParams(params);
		requestModel.setCls(Member.class);
		requestModel.setShowDialog(show);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetFriendList);
		httpClient.post(requestModel, new CustomAsyncResponehandler() {

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
				handler.onFailure(error, content);
			}

			@SuppressWarnings("unchecked")
			@Override
			public void onSuccess(ResponeModel baseModel) {
				if (baseModel.isStatus()) {
					List<Member> memberList = (List<Member>) baseModel
							.getResult();
					Map<String, User> userlist = new HashMap<String, User>();
					if (memberList != null) {
						for (Member member : memberList) {
							User user = new User();
							user.setUsername(member.getMemberName());
							setUserHearder(member.getMemberName(), user);
							user.setId(member.getMobileNumber());
							user.setUrl(member.getAvatarsAddress());
							userlist.put(member.getMobileNumber() + "", user);
						}

						// 添加user"申请与通知"
						User newFriends = new User();
						newFriends.setUsername(AppConfig.NEW_FRIENDS_USERNAME);
						newFriends.setNick("申请与通知");
						newFriends.setHeader("");
						userlist.put(AppConfig.NEW_FRIENDS_USERNAME, newFriends);

						// 存入内存
						AppContext.getApplication().setContactList(userlist);
					}
				}
				handler.onSuccess(baseModel);
			}
		});
	}

	/**
	 * 搜索好友
	 */
	public void searchFriends(String member,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("member", member);
		requestModel.setParams(params);
		requestModel.setCls(Member.class);
		requestModel.setShowDialog(true);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.SearchFriends);
		httpClient.post(requestModel, handler);
	}

	public void setUserHearder(String username, User user) {
		String headerName = null;
		if (!TextUtils.isEmpty(user.getNick())) {
			headerName = user.getNick();
		} else {
			headerName = user.getUsername();
		}
		if (username.equals(AppConfig.NEW_FRIENDS_USERNAME)) {
			user.setHeader("");
		} else if (Character.isDigit(headerName.charAt(0))) {
			user.setHeader("#");
		} else {
			user.setHeader(HanziToPinyin.getInstance()
					.get(headerName.substring(0, 1)).get(0).target.substring(0,
					1).toUpperCase());
			char header = user.getHeader().toLowerCase().charAt(0);
			if (header < 'a' || header > 'z') {
				user.setHeader("#");
			}
		}
	}

	/**
	 * 根据组id获取成员
	 */
	public void getGroupMember(String groupId,
			final CustomAsyncResponehandler handler, boolean show) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("groupID", groupId);
		requestModel.setParams(params);
		requestModel.setCls(GroupMember.class);
		requestModel.setShowDialog(show);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.getGroupMember);
		httpClient.post(requestModel, new CustomAsyncResponehandler() {

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
				handler.onFailure(error, content);
			}

			@Override
			public void onSuccess(ResponeModel baseModel) {

				handler.onSuccess(baseModel);
			}
		});

	}

	/**
	 * 根据操盘手ID获取组的ID
	 */
	public void getGroupByTradeId(String treadeID,
			final CustomAsyncResponehandler handler, boolean show) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("traderID", treadeID);
		requestModel.setParams(params);
		requestModel.setCls(Group.class);
		requestModel.setShowDialog(show);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.getGroupByTradeId);
		httpClient.post(requestModel, new CustomAsyncResponehandler() {

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
				handler.onFailure(error, content);
			}

			@Override
			public void onSuccess(ResponeModel baseModel) {

				handler.onSuccess(baseModel);
			}
		});

	}
}
