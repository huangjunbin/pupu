package com.financialnet.service;

import android.content.Context;
import android.util.Log;

import com.easemob.EMCallBack;
import com.easemob.chat.EMChatManager;
import com.financialnet.app.AppConfig;
import com.financialnet.app.AppContext;
import com.financialnet.db.dao.MemberDao;
import com.financialnet.model.FundDetail;
import com.financialnet.model.Member;
import com.financialnet.model.RequestModel;
import com.financialnet.model.ResponeModel;
import com.financialnet.net.Urls;
import com.financialnet.net.http.CustomAsyncHttpClient;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.net.http.RequestParams;
import com.financialnet.net.md5.MD5;

import cn.jpush.android.api.JPushInterface;

/**
 * MemberService.java
 *
 * @author: allen
 * @Function: 会员业务操作
 * @createDate: 2014-8-29
 * @update:
 */
public class MemberService {
    String TAG = "MemberService";
    private static CustomAsyncHttpClient httpClient;
    public static MemberDao memberDao;
    private Member member;
    public static boolean isLoginIngHX;
    // 内部全局唯一实例
    static MemberService mInstance;
    private static Context mContext;

    public static MemberService getInstance(Context context) {
        httpClient = new CustomAsyncHttpClient(context);
        memberDao = new MemberDao(context);
        mContext = context;
        if (mInstance == null) {
            // 只有第一次才彻底执行这里的代码
            synchronized (MemberService.class) {
                // 再检查一次
                if (mInstance == null)
                    mInstance = new MemberService();
            }
        }
        return mInstance;
    }

    /**
     * 登陆环信
     *
     * @param mobileNumber
     * @param password
     */
    public void loginHX(final String mobileNumber, final String password, final EMCallBack callback) {
        MD5 md5 = new MD5();
        md5.Update(password);
        final String mPassword = md5.asHex();
        AppContext.getApplication().setPassword(mPassword);
        Log.d("HX", mobileNumber + "_____" + mPassword);
        // 调用sdk登陆方法登陆聊天服务器
        new Thread(new Runnable() {
            public void run() {
                isLoginIngHX = true;

                EMChatManager.getInstance().login(mobileNumber, mPassword,

                        callback);
            }
        }).start();
    }

    /**
     * 登陆
     *
     * @param mobileNumber 账号
     * @param password     密码
     */
    public void login(final String mobileNumber, final String password,
                      final boolean isRememberPassword, final EMCallBack callBack,
                      final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("mobileNumber", mobileNumber);
        params.put("password", password);
        params.put("deviceClient", "Android");
        params.put("appVersion", AppConfig.localVersionName);
        params.put("deviceToken", JPushInterface.getRegistrationID(mContext));
        requestModel.setParams(params);
        requestModel.setCls(Member.class);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.login_action);

        httpClient.post(requestModel, new CustomAsyncResponehandler() {
            @Override
            public void onSuccess(final ResponeModel baseModel) {
                super.onSuccess(baseModel);
                if (baseModel.isStatus()) {
                    // 设置全局变量
                    AppContext.setCurrentMember ((Member)baseModel.getResult());
                    AppContext.getCurrentMember().setPassword(password);
                    if (isRememberPassword) {
                        AppContext.getCurrentMember().setIsRemember(1);
                    } else {
                        AppContext.getCurrentMember().setIsRemember(0);
                    }


                    if (memberDao != null) {
                        // 保持一个User在DB
                        memberDao.delete();
                        // 记录数据库
                        memberDao.insert(AppContext.getCurrentMember());
                    }
                    loginHX(mobileNumber, password, callBack);
                }
                handler.onSuccess(baseModel);
            }

            @Override
            public void onFailure(Throwable error, String content) {
                handler.onFailure(error, content);
                super.onFailure(error, content);
            }
        });
    }

    public void update(final String mobileNumber, final String password,
                      final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("mobileNumber", mobileNumber);
        params.put("password", password);
        params.put("deviceClient", "Android");
        params.put("appVersion", AppConfig.localVersionName);
        params.put("deviceToken", JPushInterface.getRegistrationID(mContext));
        requestModel.setParams(params);
        requestModel.setCls(Member.class);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.login_action);

        httpClient.post(requestModel, new CustomAsyncResponehandler() {
            @Override
            public void onSuccess(final ResponeModel baseModel) {
                super.onSuccess(baseModel);
                if (baseModel.isStatus()) {
                    // 设置全局变量
                    AppContext.setCurrentMember( (Member) baseModel.getResult());
                    AppContext.getCurrentMember().setPassword(password);

                    if (memberDao != null) {
                        // 保持一个User在DB
                        memberDao.delete();
                        // 记录数据库
                        memberDao.insert(AppContext.getCurrentMember());
                    }
                }
                handler.onSuccess(baseModel);
            }

            @Override
            public void onFailure(Throwable error, String content) {
                handler.onFailure(error, content);
                super.onFailure(error, content);
            }
        });
    }

    /**
     * 登出
     *
     * @param mobileNumber 账号
     *                     密码
     */
    public void loginOut(final String mobileNumber,
                         final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("mobileNumber", mobileNumber);
        params.put("deviceClient", "Android");
        params.put("deviceToken", JPushInterface.getRegistrationID(mContext));
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.login_out_action);
        httpClient.post(requestModel, handler);
    }

    /**
     * 获取验证码
     *
     * @param mobileNumber 电话号码
     */
    public void getAutoCode(String mobileNumber,
                            final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("mobileNumber", mobileNumber);
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.GetAutoCode_action);
        httpClient.post(requestModel, handler);
    }

    /**
     * 校验验证码
     *
     * @param mobileNumber 电话号码
     * @param autoCode     验证码
     */
    public void CheckoutAutoCode(String mobileNumber, String autoCode,
                                 final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("mobileNumber", mobileNumber);
        params.put("autoCode", autoCode);
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.CheckoutAutoCode_action);

        httpClient.post(requestModel, handler);
    }

    /**
     * 校验手机号码
     *
     * @param mobileNumber 手机号码
     *                     验证码
     */
    public void ConfirmMobileNumber(String mobileNumber,
                                    final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("mobileNumber", mobileNumber);
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.ConfirmMobileNumber_action);
        httpClient.post(requestModel, handler);
    }

    /**
     * 注册
     *
     * @param mobileNumber 电话号码
     * @param emailAddress 邮箱
     *                     昵称
     * @param password
     */
    public void Register(final String mobileNumber, String emailAddress,
                         String memberName, final String password, final EMCallBack callBack,
                         final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("mobileNumber", mobileNumber);
        params.put("emailAddress", emailAddress);
        params.put("memberName", memberName);
        params.put("password", password);
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setCls(Member.class);
        requestModel.setUrl(Urls.Register_action);
        httpClient.post(requestModel, new CustomAsyncResponehandler() {
            @Override
            public void onSuccess(ResponeModel baseModel) {
                super.onSuccess(baseModel);
                if (baseModel.isStatus()) {
                    // 设置全局变量
                    AppContext.setCurrentMember ((Member)baseModel.getResult());
                    AppContext.getCurrentMember().setIsRemember(0);// 默认不记住密码
                    AppContext.getCurrentMember().setPassword(password);
                    if (memberDao != null) {
                        // 保持一个User在DB
                        memberDao.delete();
                        // 记录数据库
                        memberDao.insert(AppContext.getCurrentMember());
                    }

                    loginHX(mobileNumber, password, callBack);
                    handler.onSuccess(baseModel);
                }

            }
        });
    }

    /**
     * 忘记密码
     *
     * @param mobileNumber 电话号码
     *                     新密码
     */
    public void setNewPassword(String mobileNumber, String newPassword,
                               final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("mobileNumber", mobileNumber);
        params.put("password", newPassword);
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.setNewPassword_action);
        httpClient.post(requestModel, handler);
    }

    /**
     * 修改密码
     *
     * @param mobileNumber 电话号码
     *                     新密码
     */
    public void updatePassword(String mobileNumber, String oldPassword,
                               String newPassword, final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("mobileNumber", mobileNumber);
        params.put("oldPassword", oldPassword);
        params.put("newPassword", newPassword);
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.updatePassword_action);
        httpClient.post(requestModel, handler);
    }

    /**
     * 修改个人中心
     *
     * @param member
     */
    public void updateMemberInfo(Member member,
                                 final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("memberID", member.getMemberID() + "");
        params.put("mobileNumber", member.getMobileNumber());
        params.put("memberName", member.getMemberName());
        params.put("selfIntroduction", member.getSelfIntroduction());
        params.put("avatarsAddress", member.getAvatarsAddress());
        params.put("typeDBCode", member.getTypeDBCode() + "");
        params.put("speDBCode", member.getSpeDBCode() + "");
        params.put("gitDBcode", member.getGitDBcode() + "");
        params.put("flagStockPush", member.getFlagStockPush() + "");
        params.put("flagConsultPush", member.getFlagConsultPush() + "");
        params.put("flagTraderPush", member.getFlagTraderPush() + "");
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.updateMemberInfo_action);

        httpClient.post(requestModel, handler);
    }

    public void redSwanRequestContractPDF(String memberId, String contact, String address,
                                          final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("memberId", memberId);
        params.put("contact", contact);
        params.put("address", address);
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.redSwanRequestContractPDF);
        httpClient.post(requestModel, handler);
    }

    public void getFundDetail(int type, String memberId,
                              final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("memberId", memberId);
        requestModel.setParams(params);
        requestModel.setCls(FundDetail.class);
        requestModel.setShowDialog(true);
        requestModel.setShowErrorMessage(true);
        if (1 == type) {
            requestModel.setUrl(Urls.redSwanCapitalWithdrawHistory);
        }
        if (2 == type) {
            requestModel.setUrl(Urls.redSwanBonusHistory);
        }
        if (3 == type) {
            requestModel.setUrl(Urls.redSwanCashWithdrawHistory);
        }
        httpClient.post(requestModel, handler);
    }

    public void redSwanCashWithdrawal(String mobileNumber, String password, String withdrawAmount,
                                          final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("mobileNumber", mobileNumber);
        params.put("password", password);
        params.put("withdrawAmount", withdrawAmount);
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.redSwanCashWithdrawal);
        httpClient.post(requestModel, handler);
    }

    public void redSwanCapitalWithdrawal(String mobileNumber, String password,
                                      final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("mobileNumber", mobileNumber);
        params.put("password", password);
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.redSwanCapitalWithdrawal);
        httpClient.post(requestModel, handler);
    }

    public void loginOut() {
        memberDao.delete();
    }

    public Member getCacheMember() {
        member = memberDao.getMemberByDefault();
        return member;
    }
}
