package com.financialnet.service;

import android.content.Context;

import com.financialnet.model.HotInfo;
import com.financialnet.model.RequestModel;
import com.financialnet.net.Urls;
import com.financialnet.net.http.CustomAsyncHttpClient;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.net.http.RequestParams;

public class HotInfoService {
	private static CustomAsyncHttpClient httpClient;
	static HotInfoService mInstance;

	public static HotInfoService getInstance(Context context) {
		httpClient = new CustomAsyncHttpClient(context);
		if (mInstance == null) {
			// 只有第一次才彻底执行这里的代码
			synchronized (HotInfoService.class) {
				// 再检查一次
				if (mInstance == null)
					mInstance = new HotInfoService();
			}
		}
		return mInstance;
	}

	public String add = "http://192.168.0.149:8080/pupu/toConsult/phoneQueryHotNews.action";

	/**
	 * 获取热门资讯
	 */
	public void getHotinfoList(String pageNumber, String pageSize,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(HotInfo.class);
		requestModel.setShowDialog(true);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetHotInfoList);
		// requestModel.setUrl(add);
		httpClient.postOrign(requestModel, handler);
	}
}
