package com.financialnet.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;

import com.financialnet.app.AppConfig;
import com.financialnet.app.R;
import com.financialnet.util.FileUtils;
import com.financialnet.util.MediaUtil;
import com.financialnet.util.UIHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class UpdateService extends Service {
	private String TAG = "UpdateService";
	public static final int NOTIFY_DPWNLOADAPK_ID = 1;
	public static final int DOWNLOAD_COMPLETE = 2;
	public static final int DOWNLOAD_FAIL = 3;
	private String loadUrl;// 下载地址
	// 通知栏
	private NotificationManager updateNotificationManager = null;
	private Notification updateNotification = null;
	// 通知栏跳转Intent
	private Intent updateIntent = null;
	private PendingIntent updatePendingIntent = null;
	// 文件存储
	private File updateDir = null;
	private File updateFile = null;

	private NotificationManager mNotifyManager;
	private NotificationCompat.Builder mBuilder;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		loadUrl = intent.getStringExtra("loadUrl");
		if (!FileUtils.isSDCardExisd()) {
			UIHelper.ShowMessage(UpdateService.this,
					getResources().getString(R.string.version_nosdcard));
			return -1;
		}

		updateDir = new File(MediaUtil.ROOTPATH, AppConfig.APK_PATH);
		updateFile = new File(updateDir.getPath(), getResources().getString(
				R.string.app_name)
				+ ".apk");

//		this.updateNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//		int icon = R.drawable.icon;
//		CharSequence tickerText = "开始下载";
//		long when = System.currentTimeMillis();
//		this.updateNotification = new Notification(icon, tickerText, when);
//		// 放置在"正在运行"栏目中
//		updateNotification.flags = Notification.FLAG_ONGOING_EVENT;
//		// 设置通知栏显示内容
//		RemoteViews contentView = new RemoteViews(getPackageName(),
//				R.layout.notify_updateversionview);
//		contentView.setTextViewText(R.id.notify_name,
//				getResources().getString(R.string.version_notifytitle));
//		// 指定个性化视图
//		updateNotification.contentView = contentView;
//
//		// 设置下载过程中，点击通知栏，回到主界面
//		// updateIntent = new Intent(this, MainActivity.class);
//		updateIntent = new Intent();
//		updatePendingIntent = PendingIntent.getActivity(this, 0, updateIntent,
//				0);
//		// 发出通知
//		updateNotificationManager.notify(NOTIFY_DPWNLOADAPK_ID,
//				updateNotification);

		mNotifyManager =
				(NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		mBuilder = new NotificationCompat.Builder(this);
		mBuilder.setAutoCancel(true);
		mBuilder.setContentTitle(getString(R.string.app_name))
				.setContentText("正在下载")
				.setSmallIcon(R.drawable.icon);

		// 开启一个新的线程下载，如果使用Service同步下载，会导致ANR问题，Service本身也会阻塞
		new Thread(new updateRunnable()).start();

		return super.onStartCommand(intent, flags, startId);
	}

	private Handler updateHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case DOWNLOAD_COMPLETE:
				// 点击安装PendingIntent
				Uri uri = Uri.fromFile(updateFile);
				Intent installIntent = new Intent(Intent.ACTION_VIEW);
				installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				installIntent.setDataAndType(uri,
						"application/vnd.android.package-archive");
//				updatePendingIntent = PendingIntent.getActivity(
//						UpdateService.this, 0, installIntent, 0);
//				updateNotification.contentIntent = updatePendingIntent;
//				updateNotification.flags = Notification.FLAG_AUTO_CANCEL;
//				updateNotification.defaults = Notification.DEFAULT_SOUND;// 铃声提醒
//				RemoteViews contentview = updateNotification.contentView;
//				contentview.setTextViewText(R.id.notify_name, "下载完成,点击安装");
//				contentview.setTextViewText(R.id.tv_progress, "");
//				contentview.setProgressBar(R.id.progressbar, 100, 100, false);
//				updateNotificationManager.notify(NOTIFY_DPWNLOADAPK_ID,
//						updateNotification);
				PendingIntent notifyIntent = PendingIntent.getActivity(UpdateService.this, 0, installIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				mBuilder.setContentIntent(notifyIntent);
				mBuilder.setContentText("下载完成")
						.setProgress(0, 0, false);
				mBuilder.setContentInfo("点击安装");
				mNotifyManager.notify(NOTIFY_DPWNLOADAPK_ID, mBuilder.build());
				// 停止服务
				stopSelf();
				break;
			case DOWNLOAD_FAIL:
				// 下载失败
//				updateNotification.flags = Notification.FLAG_AUTO_CANCEL;
//				updateNotification.setLatestEventInfo(UpdateService.this,
//						getResources().getString(R.string.app_name), "下载失败",
//						updatePendingIntent);
//				updateNotificationManager.notify(NOTIFY_DPWNLOADAPK_ID,
//						updateNotification);
				mBuilder.setContentText("下载失败")
						.setContentInfo("")
						.setProgress(0, 0, false);
				mNotifyManager.notify(NOTIFY_DPWNLOADAPK_ID, mBuilder.build());
				break;
			default:
				stopSelf();
			}
		}
	};

	class updateRunnable implements Runnable {
		Message message = updateHandler.obtainMessage();

		public void run() {
			message.what = DOWNLOAD_COMPLETE;
			try {
				// 增加权限;
				if (!updateDir.exists()) {
					updateDir.mkdirs();
				}

				if (!updateFile.exists()) {
					updateFile.createNewFile();
				}
				mBuilder.setProgress(100, 0, false);
				mBuilder.setContentText("0%").setContentInfo("正在下载");
				mNotifyManager.notify(NOTIFY_DPWNLOADAPK_ID, mBuilder.build());
				// 下载函数，以QQ为例子
				// 增加权限;
				long downloadSize = downloadUpdateFile(loadUrl, updateFile);
				if (downloadSize > 0) {
					// 下载成功
					updateHandler.sendMessage(message);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				message.what = DOWNLOAD_FAIL;
				// 下载失败
				updateHandler.sendMessage(message);
			}
		}
	}

	public long downloadUpdateFile(String downloadUrl, File saveFile)
			throws Exception {
		// 这样的下载代码很多，我就不做过多的说明
		int downloadCount = 0;
		int currentSize = 0;
		long totalSize = 0;
		int updateTotalSize = 0;

		HttpURLConnection httpConnection = null;
		InputStream is = null;
		FileOutputStream fos = null;

		try {
			URL url = new URL(downloadUrl);
			httpConnection = (HttpURLConnection) url.openConnection();
			httpConnection
					.setRequestProperty("User-Agent", "PacificHttpClient");
			if (currentSize > 0) {
				httpConnection.setRequestProperty("RANGE", "bytes="
						+ currentSize + "-");
			}
			httpConnection.setConnectTimeout(10000);
			httpConnection.setReadTimeout(20000);
			updateTotalSize = httpConnection.getContentLength();
			if (httpConnection.getResponseCode() == 404) {
				throw new Exception("fail!");
			}
			is = httpConnection.getInputStream();
			fos = new FileOutputStream(saveFile, false);
			byte buffer[] = new byte[4096];
			int readsize = 0;
			while ((readsize = is.read(buffer)) > 0) {
				fos.write(buffer, 0, readsize);
				totalSize += readsize;
				// 为了防止频繁的通知导致应用吃紧，百分比增加10才通知一次
				if ((downloadCount == 0)
						|| (int) (totalSize * 100 / updateTotalSize) - 10 > downloadCount) {
					downloadCount += 10;
//					RemoteViews contentview = updateNotification.contentView;
//					contentview.setTextViewText(R.id.tv_progress,
//							(int) totalSize * 100 / updateTotalSize + "%");
//					contentview.setProgressBar(R.id.progressbar, 100,
//							(int) totalSize * 100 / updateTotalSize, false);
//					updateNotificationManager.notify(NOTIFY_DPWNLOADAPK_ID,
//							updateNotification);
					mBuilder.setProgress(100,(int) totalSize * 100 / updateTotalSize , false);
					mBuilder.setContentText((int) totalSize * 100 / updateTotalSize + "%").setContentInfo("正在下载");
					mNotifyManager.notify(NOTIFY_DPWNLOADAPK_ID, mBuilder.build());
				}
			}
		} finally {
			if (httpConnection != null) {
				httpConnection.disconnect();
			}
			if (is != null) {
				is.close();
			}
			if (fos != null) {
				fos.close();
			}
		}
		return totalSize;
	}
}
