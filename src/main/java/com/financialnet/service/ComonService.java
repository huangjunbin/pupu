package com.financialnet.service;

import android.content.Context;
import android.util.Log;

import com.financialnet.db.dao.DataBookDao;
import com.financialnet.model.Company;
import com.financialnet.model.DataBook;
import com.financialnet.model.MainTradeProfit;
import com.financialnet.model.NoPayResult;
import com.financialnet.model.Notification;
import com.financialnet.model.PayResult;
import com.financialnet.model.RequestModel;
import com.financialnet.model.ResponeModel;
import com.financialnet.model.Stock;
import com.financialnet.model.Trade;
import com.financialnet.model.Tutorial;
import com.financialnet.net.Urls;
import com.financialnet.net.http.CustomAsyncHttpClient;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.net.http.RequestParams;
import com.financialnet.util.JsonUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * @className: ComonService.java
 * @author: limingtao
 * @function: 一些公共的操作
 * @date: 2013年9月19日下午1:10:37
 * @update:
 */
public class ComonService {
	String TAG = "ComonService";
	private static DataBookDao dataBookDao;
	private static CustomAsyncHttpClient httpClient;
	static ComonService mInstance;

	public static ComonService getInstance(Context context) {
		httpClient = new CustomAsyncHttpClient(context);
		dataBookDao = new DataBookDao(context);
		if (mInstance == null) {
			// 只有第一次才彻底执行这里的代码
			synchronized (TraderService.class) {
				// 再检查一次
				if (mInstance == null)
					mInstance = new ComonService();
			}
		}
		return mInstance;
	}

	public static String add = "http://192.168.0.114:8080/pupu/toUploadFile/uploadFile.action";

	/**
	 * 
	 * 上传文件
	 * 
	 * @param file
	 *            文件
	 */
	public void uploadImg(String memberID, File file,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("memberID", memberID);
		try {
			params.put("imgFile", file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		requestModel.setParams(params);
		requestModel.setShowDialog(true);
		requestModel.setShowErrorMessage(true);
		requestModel.setParams(params);
		requestModel.setUrl(Urls.UpLoadImg_action);
		// requestModel.setUrl(add);
		httpClient.post(requestModel, handler);
		// httpClient.postFile(requestModel, handler);
	}

	// -----------------------------------
	/**
	 * 
	 * 获取数据字典
	 * 
	 * @param handler
	 */
	public void getDataBook(final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		requestModel.setParams(params);
		requestModel.setShowErrorMessage(false);
		requestModel.setShowDialog(false);
		requestModel.setCls(DataBook.class);
		requestModel.setUrl(Urls.GetDataBook_action);
		httpClient.post(requestModel, new CustomAsyncResponehandler() {
			@SuppressWarnings("unchecked")
			@Override
			public void onSuccess(ResponeModel baseModel) {
				super.onSuccess(baseModel);
				if (baseModel.isStatus()) {
					final List<DataBook> temp = (List<DataBook>) baseModel
							.getResult();
					if (dataBookDao != null && temp != null && temp.size() > 0) {
						new Thread(new Runnable() {
							@Override
							public void run() {
								dataBookDao.delete();
								dataBookDao.insert(temp);
							}
						}).start();
					}
				}
				handler.onSuccess(baseModel);
			}

			@Override
			public void onFailure(Throwable error, String content) {
				handler.onFailure(error, content);
			}
		});
	}

	/**
	 * 获取某一类型的数据字典
	 * 
	 * @param type
	 * @return
	 */
	public List<DataBook> getDataBookByType(int type) {
		return dataBookDao.getDataBookByType(type);
	}

	/**
	 * 是否有数据字典缓存
	 * 
	 * @return
	 */
	public boolean isHaveDataBookCache() {
		List<DataBook> temp = dataBookDao.getDataBook();
		if (temp != null && temp.size() > 0) {
			return true;
		}
		return false;
	}

	// ------------------------------------

	/**
	 * 获取付费首页的数据
	 * 
	 * @param memberId
	 * @param handler
	 */
	public void getPayHomePageData(String memberId,
			final CustomAsyncResponehandler handler, boolean isShowLoadDialog) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("memberId", memberId);
		requestModel.setParams(params);
		requestModel.setShowErrorMessage(true);
		if (isShowLoadDialog) {
			requestModel.setShowDialog(true);
		} else {
			requestModel.setShowDialog(false);
		}
		requestModel.setUrl(Urls.GetPayHomePageData);

		httpClient.post(requestModel, new CustomAsyncResponehandler() {
			@Override
			public void onSuccess(ResponeModel baseModel) {
				super.onSuccess(baseModel);
				if (baseModel.isStatus()) {
					try {
						Log.d(TAG, baseModel.getJson());
						JSONObject object = new JSONObject(baseModel.getData());
						PayResult payRes = new PayResult();

						if (!object.isNull("newList")) {// 最新动向
							payRes.setTradeNewTrendList(JsonUtil
									.convertJsonToList(object.get("newList")
											.toString(), Trade.class));
						}
						if (!object.isNull("todayList")) {// 今日关注
							payRes.setTodayAttentionList(JsonUtil
									.convertJsonToList(object.get("todayList")
											.toString(), Trade.class));
						}
						if (!object.isNull("traderBlogList")) {// 博客专栏
							payRes.setTradeBlog(JsonUtil.convertJsonToList(
									object.get("traderBlogList").toString(),
									Trade.class));
						}
						if (!object.isNull("traderTutorialList")) {// 投资教师
							payRes.setTutorialList(JsonUtil
									.convertJsonToList(
											object.get("traderTutorialList")
													.toString(), Tutorial.class));
						}
						if (!object.isNull("myStockList")) {// 我的股票
							payRes.setMyStockList(JsonUtil.convertJsonToList(
									object.get("myStockList").toString(),
									Stock.class));
						}
						if (!object.isNull("notified")) {// 系统通知
							payRes.setNotified(JsonUtil.convertJsonToList(
									object.get("notified").toString(),
									Notification.class));
						}
						if (!object.isNull("companyNote")) {// 系统通知
							payRes.setCompanyNote(JsonUtil.convertJsonToList(
									object.get("companyNote").toString(),
									Company.class));
						}
						if (!object.isNull("sevenList")) {// 7天排行
							List<Trade> seven = JsonUtil.convertJsonToList(
									object.get("sevenList").toString(),
									Trade.class);
							MainTradeProfit mtp1 = new MainTradeProfit();
							mtp1.setProfit("7天收益排行");
							mtp1.setTradeList(seven);
							payRes.getTradeRankListMap().put(0, mtp1);
						} else {
							MainTradeProfit mtp1 = new MainTradeProfit();
							mtp1.setProfit("7天收益排行");
							mtp1.setTradeList(new ArrayList<Trade>());
							payRes.getTradeRankListMap().put(0, mtp1);
						}

						if (!object.isNull("bankList")) {// 30天排行
							List<Trade> Monthseven = JsonUtil
									.convertJsonToList(object.get("bankList")
											.toString(), Trade.class);

							MainTradeProfit mtp2 = new MainTradeProfit();
							mtp2.setProfit("30天收益排行");
							mtp2.setTradeList(Monthseven);
							payRes.getTradeRankListMap().put(1, mtp2);
						} else {
							MainTradeProfit mtp2 = new MainTradeProfit();
							mtp2.setProfit("30天收益排行");
							mtp2.setTradeList(new ArrayList<Trade>());
							payRes.getTradeRankListMap().put(1, mtp2);
						}
						baseModel.setResult(payRes);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					handler.onSuccess(baseModel);
				}
			}
		});
	}

	/**
	 * 获取未付费首页的数据
	 * 
	 * @param memberId
	 * @param handler
	 */
	public void getNoPayHomePageData(String memberId,
			final CustomAsyncResponehandler handler, boolean isShowLoadDialog) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("memberId", memberId);
		requestModel.setParams(params);
		requestModel.setShowErrorMessage(true);
		if (isShowLoadDialog) {
			requestModel.setShowDialog(true);
		} else {
			requestModel.setShowDialog(false);
		}
		requestModel.setUrl(Urls.GetNoPayHomePageData);

		httpClient.post(requestModel, new CustomAsyncResponehandler() {

			@Override
			public void onSuccess(ResponeModel baseModel) {
				super.onSuccess(baseModel);
				if (baseModel.isStatus()) {
					JSONObject object;
					try {
						object = new JSONObject(baseModel.getData());
						NoPayResult noPayResult = new NoPayResult();

						if (!object.isNull("traderMap")) {// 操盘手
							noPayResult.setRadomTrader(JsonUtil
									.convertJsonToObject(object
											.get("traderMap").toString(),
											Trade.class));
						}

						if (!object.isNull("traderBlogList")) {// 博客
							List<Trade> blog = JsonUtil.convertJsonToList(
									object.get("traderBlogList").toString(),
									Trade.class);
							if (blog != null && blog.size() > 0) {
								noPayResult.setTradeBlog(blog);
							}
						}

						if (!object.isNull("commentList")) {// 评论
							noPayResult.setTradeComments(JsonUtil
									.convertJsonToList(object
											.get("commentList").toString(),
											Trade.class));
						}
						if (!object.isNull("companyNote")) {// 系统通知
							noPayResult.setCompanyNote(JsonUtil.convertJsonToList(
									object.get("companyNote").toString(),
									Company.class));
						}
						if (!object.isNull("monthList")) {// 30天前动向
							noPayResult.setTradeMounthPreTrend(JsonUtil
									.convertJsonToList(object.get("monthList")
											.toString(), Trade.class));

						}

						if (!object.isNull("sevenList")) {// 7天排行
							List<Trade> seven = JsonUtil.convertJsonToList(
									object.get("sevenList").toString(),
									Trade.class);
							MainTradeProfit mtp1 = new MainTradeProfit();
							mtp1.setProfit("7天收益排行");
							mtp1.setTradeList(seven);
							noPayResult.getTradeRankListMap().put(0, mtp1);
						} else {
							MainTradeProfit mtp1 = new MainTradeProfit();
							mtp1.setProfit("7天收益排行");
							mtp1.setTradeList(new ArrayList<Trade>());
							noPayResult.getTradeRankListMap().put(0, mtp1);
						}

						if (!object.isNull("rankList")) {// 30天排行
							List<Trade> Monthseven = JsonUtil
									.convertJsonToList(object.get("rankList")
											.toString(), Trade.class);

							MainTradeProfit mtp2 = new MainTradeProfit();
							mtp2.setProfit("30天收益排行");
							mtp2.setTradeList(Monthseven);
							noPayResult.getTradeRankListMap().put(1, mtp2);
						} else {
							MainTradeProfit mtp2 = new MainTradeProfit();
							mtp2.setProfit("30天收益排行");
							mtp2.setTradeList(new ArrayList<Trade>());
							noPayResult.getTradeRankListMap().put(1, mtp2);
						}
						baseModel.setResult(noPayResult);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				handler.onSuccess(baseModel);
			}

		});
	}

	public void getSysNotification(String memberId,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("memberId", memberId);
		requestModel.setParams(params);
		requestModel.setShowErrorMessage(false);
		requestModel.setShowDialog(true);
		requestModel.setCls(Notification.class);
		requestModel.setUrl(Urls.sysNotification);
		httpClient.post(requestModel, handler);
	}
}
