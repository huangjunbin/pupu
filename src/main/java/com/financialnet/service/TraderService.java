package com.financialnet.service;

import android.content.Context;

import com.financialnet.app.ui.TradeActivity;
import com.financialnet.model.RequestModel;
import com.financialnet.model.Trade;
import com.financialnet.model.TraderAppraise;
import com.financialnet.net.Urls;
import com.financialnet.net.http.CustomAsyncHttpClient;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.net.http.RequestParams;
import com.financialnet.util.StringUtils;

/**
 * @className: TraderService.java
 * @author: limingtao
 * @function: 操盘手业务操作
 * @date: 2013年9月18日下午6:03:31
 * @update:
 */
public class TraderService {
	private static CustomAsyncHttpClient httpClient;
	static TraderService mInstance;

	// // 普普操盘手数据
	// public static boolean isFirstLoadPupuTrader = true;
	// public static int PupuTraderPageNo = 0;
	// public static int PupuTraderPageCurrentNo = AppContext.PAGE_SIZE;
	// public static List<Trade> listPupuTrader = new ArrayList<Trade>();

	public static TraderService getInstance(Context context) {
		httpClient = new CustomAsyncHttpClient(context);
		if (mInstance == null) {
			// 只有第一次才彻底执行这里的代码
			synchronized (TraderService.class) {
				// 再检查一次
				if (mInstance == null)
					mInstance = new TraderService();
			}
		}
		return mInstance;
	}

	/**
	 * 获取操盘手列表
	 * 
	 * @param flag
	 *            操盘手类型
	 * @param statType
	 * 
	 * @param type
	 * @param pageNumber
	 * @param pageSize
	 */
	public void getTraderList(int flag, String statType, String type,
			String pageNumber, String pageSize, String memberId,
			String memberClass, final CustomAsyncResponehandler handler) {
		switch (flag) {
		case TradeActivity.MY_TRADER:
			getPupuTrader(memberId, pageNumber, pageSize, memberClass, handler);
			break;
		case TradeActivity.PUPU_TRADER:
			getPupuTrader(null, pageNumber, pageSize, memberClass, handler);
			break;
		case TradeActivity.SEVEVN_PROFIT_RANK:
		case TradeActivity.THIETY_PROFIT_RANK:
		case TradeActivity.STEADY_PROFIT_RANK:
			getPupuTraderRank(statType, type, pageNumber, pageSize, handler);
			break;
		}
	}

	/**
	 * 获取普普/我的操盘手
	 * 
	 * @param pageNumber
	 *            页码
	 * @param pageSize
	 *            页数
	 */
	public void getPupuTrader(String memberId, String pageNumber,
			String pageSize, String memberClass,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();

		if (!StringUtils.isStringNone(memberId)) {
			params.put("memberId", memberId);
		}
		params.put("memberClass", memberClass);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Trade.class);
		requestModel.setShowDialog(true);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetMyPupuTrader);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手排行榜
	 * 
	 * @param statType
	 *            排行榜类型
	 * @param type
	 *            排序类型
	 * @param pageNumber
	 *            页码
	 * @param pageSize
	 *            页数
	 */
	public void getPupuTraderRank(String statType, String type,
			String pageNumber, String pageSize,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("statType", statType);
		params.put("type", type);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Trade.class);
		requestModel.setShowDialog(true);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetPupuTraderRank);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手信息
	 * 
	 * @param traderId
	 *            操盘手id
	 */
	public void getTraderInfo(String traderId, String memberId,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("traderId", traderId);
		params.put("memberId", memberId);
		requestModel.setParams(params);
		requestModel.setCls(Trade.class);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetTraderInfo);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手的评论
	 * 
	 * @param traderId
	 *            操盘手id
	 * @param pageNumber
	 *            页码
	 * @param pageSize
	 *            页数
	 */
	public void getTraderComment(String traderId, String pageNumber,
			String pageSize, final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("traderId", traderId);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(TraderAppraise.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetTraderComment);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 发布评论
	 * 
	 * @param traderId
	 *            操盘手id
	 * @param memberId
	 *            评论人id
	 * @param comment
	 *            评论内容
	 * @param score
	 *            评分
	 */
	public void publishTradeComment(String traderId, String memberId,
			String comment, String score,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("traderId", traderId);
		params.put("comment", comment);
		params.put("score", score);
		params.put("memberId", memberId);
		requestModel.setParams(params);
		requestModel.setCls(TraderAppraise.class);
		requestModel.setShowDialog(true);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.PublistComment);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手最新动向
	 * 
	 * @param memberId
	 *            会员id
	 * @param pageNumber
	 * @param pageSize
	 * @param handler
	 */
	public void getTradeNewTrendList(String memberId, String pageNumber,
			String pageSize, final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("memberId", memberId);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Trade.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetTraderNewTrend_action);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取持仓操盘手
	 * 
	 * @param marketID
	 * @param stockCode
	 * @param type
	 * @param pageNumber
	 * @param pageSize
	 * @param handler
	 */
	public void getTradeHaveStock(String marketID, String stockCode,
			String type, String pageNumber, String pageSize, String nowPrice,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("marketID", marketID);
		params.put("stockCode", stockCode);
		params.put("type", type);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		params.put("nowPrice", nowPrice);
		requestModel.setParams(params);
		requestModel.setCls(Trade.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.GetTraderHaveStock_action);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手博客
	 * 
	 * @param traderID
	 *            会员id
	 * @param pageNumber
	 * @param pageSize
	 * @param handler
	 */
	public void getTradeBlog(String traderID, String pageNumber,
			String pageSize, final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("traderID", traderID);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Trade.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.traderBlog);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取博客详情
	 * 
	 * @param id
	 */
	public void getTradeBlogDetail(String id,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("id", id);
		requestModel.setParams(params);
		requestModel.setShowDialog(true);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.traderBlogDetail);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手博客
	 * 

	 *            会员id
	 * @param pageNumber
	 * @param pageSize
	 * @param handler
	 */
	public void getTradeInvest(String traderID, String pageNumber,
			String pageSize, final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("traderID", traderID);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Trade.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.traderInvest);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取博客详情
	 * 
	 * @param id
	 */
	public void getTradeInvestDetail(String id,
			final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("id", id);
		requestModel.setParams(params);
		requestModel.setShowDialog(true);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.traderInvestDetail);
		httpClient.post(requestModel, handler);
	}

	/**
	 * 获取操盘手博客
	 *
	 * @param memberId
	 *            会员id
	 * @param pageNumber
	 * @param pageSize
	 * @param handler
	 */
	public void getTradeBlogByHome(String memberId, String pageNumber,
							 String pageSize, final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("memberId", memberId);
		params.put("pageNumber", pageNumber);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setCls(Trade.class);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.getTraderBolg);
		httpClient.post(requestModel, handler);
	}
}
