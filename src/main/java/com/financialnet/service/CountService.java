package com.financialnet.service;

import android.content.Context;

import com.financialnet.model.HotInfo;
import com.financialnet.model.RequestModel;
import com.financialnet.net.Urls;
import com.financialnet.net.http.CustomAsyncHttpClient;
import com.financialnet.net.http.CustomAsyncResponehandler;
import com.financialnet.net.http.RequestParams;

public class CountService {
	private static CustomAsyncHttpClient httpClient;
	static CountService mInstance;

	public static CountService getInstance(Context context) {
		httpClient = new CustomAsyncHttpClient(context);
		if (mInstance == null) {
			// 只有第一次才彻底执行这里的代码
			synchronized (CountService.class) {
				// 再检查一次
				if (mInstance == null)
					mInstance = new CountService();
			}
		}
		return mInstance;
	}

	/**
	 * 统计页面数量 便于后台结算
	 */
	public void setTraderFirstPageInsert(String memberId, String traderId,
			String page, final CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("memberId", memberId);
		params.put("traderId", traderId);
		params.put("device", 0 + "");// 0表示Android
		params.put("page", page);
		requestModel.setParams(params);
		requestModel.setShowDialog(false);
		requestModel.setShowErrorMessage(false);
		requestModel.setUrl(Urls.traderFirstPageInsert);
		httpClient.postOrign(requestModel, handler);
	}
}
